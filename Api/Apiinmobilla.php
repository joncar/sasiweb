<?php

class Apiinmobilla{

	protected $agencia = 2;
	protected $password = '82ku9xz2aw3';
	protected $idioma = 1;
	protected $json = 1;
	protected $url = 'http://apiweb.inmovilla.com/apiweb/apiweb.php';


	protected $key;
	protected $peticiones = array();

	function __construct(){
		$this->key = $this->agencia.';'.$this->password.';'.$this->idioma.';';
		$this->key.= "lostipos";		
	}
	/***
		Tablas maestras 
	****/	

	/****** Funcion para obtener tipos de propiedades *******/
	function getTipos(){
		$this->set('tipos',1,100,'','');
		return $this->get();
	}

	/***** Funcion para obtener provincias ******/
	function getProvincias(){
		$this->set('provincias',1,100,'','');
		return $this->get();
	}

	/***** Funcion para obtener provincias con ofertas *****/
	function getProvinciasOfertas(){
		$this->set('provinciasofertas',1,100,'','');
		return $this->get();
	}

	/***** Funcion para obtener ciudades*****/
	function getCiudades($provincia = ''){
		$where = !empty($provincia)?'codprov='.$provincia:'';
		$this->set('ciudades',1,100,$where,'');
		return $this->get();
	}

	/***** Funcion para obtener zonas *****/
	function getZonas($ciudad = ''){
		$where = !empty($ciudad)?'ciudad.cod_ciu='.$ciudad:'';
		$this->set('provinciasofertas',1,100,$where,'');
		return $this->get();
	}

	/***** Funcion para obtener zonas *****/
	function getTiposConservacion(){		
		$this->set('tipos_conservacion',1,100,'','');
		return $this->get();
	}

	/***** 
		Propiedades
	******/

	/****** Funcion para obtener las propiedades registradas ************/
	function getPropiedades($page = 1,$cant = 100,$where = '',$order = ''){
		$this->set('paginacion',$page,$cant,$where,$order);
		return $this->get();
	}

	/***** Funcion para propiedades destacadas *****/
	function getDestacados(){		
		$this->set('destacados',1,100,'','');
		return $this->get();
	}

	/**** 
		Función para obtener disponibilidad de alquiler
		
	****/
	function getDisponibilidadAlquiler($cod,$apartir){		
		$where = 'codigo = '.$cod.' AND fechafin >='.$apartir;		
		$this->set('alquilerdisponibilidad',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener temporadas de alquiler registradas
		
	****/
	function getTemporadas($cod){		
		$where = "keyclave=$cod";				
		$this->set('alquilertemporada',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getPromociones($page = 1){						
		$this->set('paginacion_promociones',$page,100,'','');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getPromocion($cod,$agencia){
		$where = "codobra=$cod.$agencia";
		$this->set('ficha_promo',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getPropiedad($cod){
		$where = "cod_ofer = $cod";
		$this->set('ficha',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getDescripciones($cod){	
		$where = "cod_ofer = $cod";	
		$this->set('descripciones',1,100);
		return $this->get();
	}



	function setIdioma($lang = 'es'){
		switch($lang){
			case 'es': //Español
				$this->idioma = 1;
			break;
			case 'en':	//Ingles
				$this->idioma = 2;
			break;
			case 'al': //Aleman
				$this->idioma = 3;
			break;
			case 'fr': //Frances
				$this->idioma = 4;
			break;
			case 'ho': //Holandes
				$this->idioma = 5;
			break;
			case 'no': //Norruego
				$this->idioma = 6;
			break;
			case 'ru': //Ruso
				$this->idioma = 7;
			break;
			case 'po': //portugues
				$this->idioma = 8;
			break;
			case 'su': //Sueco
				$this->idioma = 9;
			break;
			case 'fi': //Finlandes
				$this->idioma = 10;
			break;
			case 'ch': //Chino 
				$this->idioma = 11;
			break;
			case 'ca': //Catalan
				$this->idioma = 12;
			break;
			case 'it': //Italiano
				$this->idioma = 13;
			break;
			case 'eu': //Euskera
				$this->idioma = 14;
			break;
			case 'po': //Polaco
				$this->idioma = 15;
			break;
			case 'ga': //Gallego
				$this->idioma = 16;
			break;
		}
	}

	



	function set($tipo,$posinicial,$numelementos,$where,$orden){
		$this->peticiones[]=$tipo;
	    $this->peticiones[]=$posinicial;
	    $this->peticiones[]=$numelementos;
	    if($where){
	    	$this->peticiones[]=$where;
		}
		if($orden){
	    	$this->peticiones[]=$orden;
		}
	}

	function get(){
		$texto = $this->key.';';
		foreach($this->peticiones as $p) {
	        $texto.="$p;";
	    }
	    $texto = substr($texto,0,strlen($texto)-1);	    
	    $texto = urlencode($texto);

	    if($this->json==1){
	    	$texto = "param=$texto&json=1";
	    }else{
	    	$texto = "param=$texto";
	    }
	    
	    return $this->connect($texto);
	}

	protected function connect($campospost){			    
	    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
	    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
	    $header[] = "Cache-Control: max-age=0";
	    $header[] = "Connection: keep-alive";
	    $header[] = "Keep-Alive: 300";
	    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
	    $header[] = "Accept-Language: en-us,en;q=0.5";
	    $header[] = "Pragma: ";

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $this->url);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_POSTFIELDS,'');
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    if (strlen($campospost)>0) {
	        //los datos tienen que ser reales, de no ser asi se desactivara el servicio
	        $campospost=$campospost . "&ia=" .$_SERVER["REMOTE_ADDR"] ."&ib=" .$_SERVER['HTTP_X_FORWARDED_FOR'];
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $campospost);
	    }
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
	    $page = curl_exec($ch);    
	    curl_close($ch);

	    if(!$this->json) {	        
	        @eval($page);
	    }else{
	    	$page = json_decode($page);
	    }
	    
	    return $page;
	}
}