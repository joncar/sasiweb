<?php
$lang['contact_success'] = 'Vielen Dank für Ihre Kontaktaufnahme, wir werden uns in Kürze mit Ihnen in Verbindung setzen';
$lang['contact_error'] = 'Bitte füllen Sie die angeforderten Informationen aus {form}';