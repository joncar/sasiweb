<?php
/* https://github.com/chernavin/CodeIgniter-Russian-Language */
$lang['form_validation_required']				= 'Поле &quot;%s&quot; обязательно для заполнения.';
$lang['form_validation_isset']					= 'Поле &quot;%s&quot; должно содержать значение.';
$lang['form_validation_valid_email']			= 'Поле &quot;%s&quot; должно содержать правильный адрес электронной почты.';
$lang['form_validation_valid_emails']			= 'Поле &quot;%s&quot; должно содержать правильные адреса электронной почты.';
$lang['form_validation_valid_url']				= 'Поле &quot;%s&quot; должно содержать правильный URL.';
$lang['form_validation_valid_ip']				= 'Поле &quot;%s&quot; должно содержать правильный IP.';
$lang['form_validation_min_length']				= 'Поле &quot;%s&quot; должно быть не короче %s символов.';
$lang['form_validation_max_length']				= 'Поле &quot;%s&quot; должно быть не длиннее %s символов.';
$lang['form_validation_exact_length']			= 'Поле &quot;%s&quot; должно быть точно %s символов в длину.';
$lang['form_validation_alpha']					= 'Поле &quot;%s&quot; должно содержать только буквы.';
$lang['form_validation_alpha_numeric']			= 'Поле &quot;%s&quot; должно содержать только буквы и цифры.';
$lang['form_validation_alpha_dash']				= 'Поле &quot;%s&quot; должно содержать только буквы, цифры, символы подчеркивания и тире.';
$lang['form_validation_numeric']				= 'Поле &quot;%s&quot; должно содержать правильное число.';
$lang['form_validation_is_numeric']				= 'Поле &quot;%s&quot; должно содержать правильное число.';
$lang['form_validation_integer']				= 'Поле &quot;%s&quot; должно содержать целое число.';
$lang['form_validation_regex_match']			= 'Поле &quot;%s&quot; заполнено в неверном формате.';
$lang['form_validation_matches']				= 'Поле &quot;%s&quot; должно соответствовать значению в поле &quot;%s&quot;.';
$lang['form_validation_is_unique'] 				= 'Поле &quot;%s&quot; должно содержать уникальное значение.';
$lang['form_validation_is_natural']				= 'Поле &quot;%s&quot; должно содержать положительное число.';
$lang['form_validation_is_natural_no_zero']		= 'Поле &quot;%s&quot; должно содержать число больше нуля.';
$lang['form_validation_decimal']				= 'Поле &quot;%s&quot; должно содержать десятичное число.';
$lang['form_validation_less_than']				= 'Поле &quot;%s&quot; должно содержать число меньшее чем %s.';
$lang['form_validation_greater_than']			= 'Поле &quot;%s&quot; должно содержать число большее чем %s.';
$lang['form_validation_less_than_equal_to']		= "Поле &quot;%s&quot; должно содержать число меньшее или равное %s.";
$lang['form_validation_greater_than_equal_to']	= "Поле &quot;%s&quot; должно содержать число большее или равное %s.";
/* End of file form_validation_lang.php */
/* Location: ./system/language/russian/form_validation_lang.php */