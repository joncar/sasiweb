<?php

class Api_inmovilla{	
	/* See setSucursal Function below to set credentials */
	/*protected $agencia = 278; //Producción	
	protected $password = 'Ft54_THrth';*/
	protected $idioma = 1;
	protected $json = 1;
	protected $url = 'http://apiweb.inmovilla.com/apiweb/apiweb.php';
	protected $extras = array();
	protected $telefono;
	protected $email;
	protected $clean = false;
	public $echoResultServer = false;

	protected $key;
	protected $peticiones = array();
	protected $idiomas = array();	

	function __construct(){
		$this->db = get_instance()->db;	
		$this->extras = $this->db->get('propiedades_extras');
		$this->idiomas = explode(', ',$this->db->get('ajustes')->row()->idiomas);
	}

	function rellenar_idiomas(){
		foreach($this->idiomas as $i){
			if($i!='es'){
				//Verificar por propiedad si tiene el idioma instalado de lo contrario se instala el ingles o español.				
				$this->db->select('api_descripciones.*');
				$this->db->join('api_descripciones','api_descripciones.cod_ofer = propiedades_list.cod_ofer','LEFT');
				$propiedades = $this->db->get_where('propiedades_list',array('idioma'=>$i));
				foreach($propiedades->result() as $p){
					if(empty($p->titulo)){
						//Buscarlo en ingles
						$aux = $this->db->get_where('api_descripciones',array('idioma'=>'en','cod_ofer'=>$p->cod_ofer));
						if($i!='en' && $aux->num_rows()>0){
							$aux = $aux->row();
							$this->db->insert('api_descripciones',array(
								'cod_ofer'=>$p->cod_ofer,
								'idioma'=>$i,
								'titulo'=>$aux->titulo,
								'descrip'=>$aux->descrip,
								'api_propiedades_id'=>$aux->api_propiedades_id,
								'idioma_cod'=>$aux->idioma_cod,
								'LC'=>$aux->LC
							));
						}
						//Buscarlo en español
						else{
							//Buscarlo en ingles
							$aux = $this->db->get_where('api_descripciones',array('idioma'=>'es','cod_ofer'=>$p->cod_ofer));
							if($aux->num_rows()>0){
								$aux = $aux->row();
								$this->db->insert('api_descripciones',array(
									'cod_ofer'=>$p->cod_ofer,
									'idioma'=>$i,
									'titulo'=>$aux->titulo,
									'descrip'=>$aux->descrip,
									'api_propiedades_id'=>$aux->api_propiedades_id,
									'idioma_cod'=>$aux->idioma_cod,
									'LC'=>$aux->LC
								));
							}
						}
					}
				}
			}
		}
	}

	function set_sucursal($suc){
		$ajustes = $this->db->get('ajustes')->row();
		switch($suc){
			case 'Igualada':
			case '278':
				$this->agencia = 278;				
				$this->password = 'Ft54_THrth';	
				$this->telefono = $ajustes->telefono_contacto_igualada;
				$this->email = $ajustes->email_contacto_igualada;	
				$this->direccion = $ajustes->direccion_contacto_igualada;		
			break;
			case 'Alicante':
			case '5010':
				$this->agencia = 5010;				
				$this->password = '5!CXKBcHz';
				$this->telefono = $ajustes->telefono_contacto_alicante;
				$this->email = $ajustes->email_contacto_alicante;	
				$this->direccion = $ajustes->direccion_contacto_alicante;		
			break;
			case 'Test':
			case '2':
				$this->agencia = 2;				
				$this->password = '82ku9xz2aw3';
				$this->telefono = 'TEST';
				$this->email = 'TEST';
				$this->direccion = 'TEST';
			break;
		}
		$this->cron = $this->db->get_where('cron',array('sucursal'=>$this->agencia))->row();
		$this->key = $this->agencia.';'.$this->password.';'.$this->idioma.';';
		$this->key.= "lostipos";			
	}

	/***
		Tablas maestras 
	****/	

	/****** Funcion para obtener tipos de propiedades *******/
	function getTipos(){
		$this->set('tipos',1,100,'','');
		return $this->get();
	}

	/***** Funcion para obtener provincias ******/
	function getProvincias(){
		$this->set('provincias',1,100,'','');
		return $this->get();
	}

	

	/***** Funcion para obtener provincias con ofertas *****/
	function getProvinciasOfertas(){
		$this->set('provinciasofertas',1,100,'','');
		return $this->get();
	}

	/***** Funcion para obtener ciudades*****/
	function getCiudades($provincia = ''){
		$where = !empty($provincia)?'codprov='.$provincia:'';
		$this->set('ciudades',1,100,$where,'');
		return $this->get();
	}

	/***** Funcion para obtener zonas *****/
	function getZonas($ciudad = ''){
		$where = !empty($ciudad)?'ciudad.cod_ciu='.$ciudad:'';
		$this->set('provinciasofertas',1,100,$where,'');
		return $this->get();
	}

	/***** Funcion para obtener zonas *****/
	function getTiposConservacion(){		
		$this->set('tipos_conservacion',1,100,'','');
		return $this->get();
	}

	/***** 
		Propiedades
	******/

	/****** Funcion para obtener las propiedades registradas ************/
	function getPropiedades($page = 1,$cant = 100,$where = '',$order = ''){
		$this->set('paginacion',$page,$cant,$where,$order);		
		return $this->get();
	}

	/***** Funcion para propiedades destacadas *****/
	function getDestacados(){		
		$this->set('destacados',1,100,'','');
		return $this->get();
	}

	/**** 
		Función para obtener disponibilidad de alquiler
		
	****/
	function getDisponibilidadAlquiler($cod,$apartir){		
		$where = 'codigo = '.$cod.' AND fechafin >='.$apartir;		
		$this->set('alquilerdisponibilidad',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener temporadas de alquiler registradas
		
	****/
	function getTemporadas($cod){		
		$where = "keyclave=$cod";				
		$this->set('alquilertemporada',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getPromociones($page = 1){						
		$this->set('paginacion_promociones',$page,100,'','');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getPromocion($cod,$agencia){
		$where = "codobra=$cod.$agencia";
		$this->set('ficha_promo',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getPropiedad($cod){
		$where = "cod_ofer = $cod";
		$this->set('ficha',1,100,$where,'');
		return $this->get();
	}

	/**** 
		Función para obtener promociones
		
	****/
	function getDescripciones($cod){	
		$where = "cod_ofer = $cod";	
		$this->set('descripciones',1,100);
		return $this->get();
	}



	function setIdioma($lang = 'es'){
		switch($lang){
			case 'es': //Español
				$this->idioma = 1;
			break;
			case 'en':	//Ingles
				$this->idioma = 2;
			break;
			case 'al': //Aleman
				$this->idioma = 3;
			break;
			case 'fr': //Frances
				$this->idioma = 4;
			break;
			case 'ho': //Holandes
				$this->idioma = 5;
			break;
			case 'no': //Norruego
				$this->idioma = 6;
			break;
			case 'ru': //Ruso
				$this->idioma = 7;
			break;
			case 'po': //portugues
				$this->idioma = 8;
			break;
			case 'su': //Sueco
				$this->idioma = 9;
			break;
			case 'fi': //Finlandes
				$this->idioma = 10;
			break;
			case 'ch': //Chino 
				$this->idioma = 11;
			break;
			case 'ca': //Catalan
				$this->idioma = 12;
			break;
			case 'it': //Italiano
				$this->idioma = 13;
			break;
			case 'eu': //Euskera
				$this->idioma = 14;
			break;
			case 'po': //Polaco
				$this->idioma = 15;
			break;
			case 'ga': //Gallego
				$this->idioma = 16;
			break;
		}
	}

	function getIdioma($lang = 'es'){
		switch($lang){
			case 1: //Español
				return 'es';
			break;
			case 2: //Español
				return 'en';
			break;
			case 3: //Español
				return 'al';
			break;
			case 4: //Español
				return 'fr';
			break;
			case 5: //Español
				return 'ho';
			break;
			case 6: //Español
				return 'no';
			break;
			case 7: //Español
				return 'ru';
			break;
			case 8: //Español
				return 'po';
			break;
			case 9: //Español
				return 'su';
			break;
			case 10: //Español
				return 'fi';
			break;
			case 11: //Español
				return 'ch';
			break;
			case 12: //Español
				return 'ca';
			break;
			case 13: //Español
				return 'it';
			break;
			case 14: //Español
				return 'eu';
			break;
			case 15: //Español
				return 'po';
			break;
			case 16: //Español
				return 'ga';
			break;
		}
	}




	



	function set($tipo,$posinicial,$numelementos,$where,$orden){
		$this->peticiones[]=$tipo;
	    $this->peticiones[]=$posinicial;
	    $this->peticiones[]=$numelementos;
	    if($where){
	    	$this->peticiones[]=$where;
		}
		if($orden){
	    	$this->peticiones[]=$orden;
		}
	}

	function get(){
		$texto = $this->key.';';
		foreach($this->peticiones as $p) {
	        $texto.="$p;";
	    }
	    $texto = substr($texto,0,strlen($texto)-1);	    
	    $texto = urlencode($texto);

	    if($this->json==1){
	    	$texto = "param=$texto&json=1";
	    }else{
	    	$texto = "param=$texto";
	    }
	    
	    return $this->connect($texto);
	}

	function can_sincronize(){
		$crons = $this->db->get_where('cron',array('max_iteraccion >'=>0));
        if($crons->num_rows()>0){                
            foreach($crons->result() as $cron){
                $page = $cron->current_page==0?1:$cron->current_page+1;
                $this->set_sucursal($cron->sucursalName);
                if(!$this->getPropiedades(1)){
                	correo('joncar.c@gmail.com','Sincronización interrumpida','La sucursal de '.$cron->sucursalName.' no ha podido sincronizarse '.print_r($this->getPropiedades(1),TRUE));
                	return false;
                }
            }
        }
        return true;
	}

	protected function connect($campospost){			    
	    $header[0]  = "Accept: text/xml,application/xml,application/xhtml+xml,";
	    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
	    $header[]   = "Cache-Control: max-age=0";
	    $header[]   = "Connection: keep-alive";
	    $header[]   = "Keep-Alive: 300";
	    $header[]   = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
	    $header[]   = "Accept-Language: en-us,en;q=0.5";
	    $header[]   = "Pragma: ";

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $this->url);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_POSTFIELDS,'');
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    if (strlen($campospost)>0) {
	        //los datos tienen que ser reales, de no ser asi se desactivara el servicio
	        $_SERVER["REMOTE_ADDR"] = empty($_SERVER["REMOTE_ADDR"])?'188.86.3.43':$_SERVER["REMOTE_ADDR"];
	        $campospost=$campospost . "&ia=" .$_SERVER["REMOTE_ADDR"] ."&ib=" .@$_SERVER['HTTP_X_FORWARDED_FOR'];	        
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $campospost);
	    } 
        
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_COOKIEJAR, @$cookie);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, @$cookie);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");	    
	    $page = curl_exec($ch);
	    if(strpos($page,'die(')>-1){
	    	$error = print_r($page,true);
	    	$error.= print_r($campospost,true);
	    	correo('joncar.c@gmail.com','Error en ejecución de cron',$error);
	    	throw new exception('Ha ocurrido un error en la sincronización '.$error,503);
		    return false;
		    die();  
	    }
	    if($this->echoResultServer){ 	    	
		    print_r($page);
		    die();   	    
		}
	    if(!$this->json) {	        
	        @eval($page);
	    }else{
	    	$page = json_decode($page);
	    }



	    $this->peticiones = array();
	    
	    return $page;
	}


	/***** Insertar datos en bd *********/	
	function clearPropiedades(){
		$this->clean = true;		
	}
	function triggerClearPropiedades(){
		$this->db->query('TRUNCATE _propiedades_list');					
		$this->db->query('TRUNCATE _api_propiedad_fotos');				
		$this->db->query('TRUNCATE _api_descripciones');
		$this->db->query('TRUNCATE _propiedades_videos');
		$this->db->query('TRUNCATE _propiedades_list_extras');
		$this->db->query('TRUNCATE _iteraciones');
	}

	function checkFields($table,$fields){
		foreach($fields as $n=>$v){
			$data = $this->db->field_data($table);
			$enc = false;
			foreach($data as $d){
				if($d->name==$n){
					$enc = true;
				}
			}
			if(!$enc){
				$this->db->query('ALTER TABLE '.$table.' ADD '.$n.' TEXT');
			}
		}
	}

	protected $per_page = 52;
	protected $iteraccion = 0;


	/*Si es el cron quien solicita el cambio propiedad vendra vacio por lo que se montan todas las propiedades, de lo contrario se actualiza solo la propiedad solicitada */
	function fillPropiedades($page = 1,$propiedad = '',$del = false){
		$this->iteraccion = $page==1?0:$this->iteraccion;
		$reg = (($page-1)*$this->per_page)+1;		
		if(empty($propiedad)){
			$propiedades = $this->getPropiedades($reg);
		}else{
			$propiedades = $propiedad;
		}
		
		if(!empty($propiedades->paginacion)){
			$paginacion = $propiedades->paginacion[0];
			$fragmentos = ceil($paginacion->total/$paginacion->elementos);				
			//$paginacion->elementos
			$this->db->update('cron',array('total_propiedades'=>$paginacion->total,'current_page'=>$page,'total_pages'=>$fragmentos,'per_page'=>52,'fecha_ejecucion'=>date("Y-m-d H:i:s")),array('id'=>$this->cron->id));
			$this->per_page = $paginacion->elementos;			
			foreach($propiedades->paginacion as $n=>$v){
				if($n==0){
					$v->page = $page;
					if($v->elementos!=$this->per_page){
						$this->db->update('cron',array('per_page'=>$v->elementos),array('sucursal'=>$this->agencia));
						$this->per_page = $v->elementos;
					}
					$this->db->insert('_iteraciones',array('agencia'=>$this->agencia,'text'=>print_r($v,TRUE),'fecha'=>date("Y-m-d")));
				}				
				else{
					$prod = $this->db->get_where('_propiedades_list',array('cod_ofer'=>$v->cod_ofer));
					$datos = $this->splitExtras($v);
					$data = $datos['data'];
					$this->checkFields('_propiedades_list',$data);
					if($prod->num_rows()==0 && !$del){
						$data['sucursal'] = $this->agencia;
						$this->db->insert('_propiedades_list',$data);
						$primary = $this->db->insert_id();
						$this->fillPropiedad($data['cod_ofer'],$primary);
						echo 'Metemos';

					}elseif($prod->num_rows()>0 && !empty($propiedad) && !$del){
						
						//Actualizamos
						$prod = $prod->row();
						$this->db->update('_propiedades_list',$data,array('id'=>$prod->id));
						$primary = $prod->id;
						
						$this->db->delete('_api_propiedad_fotos',array('cod_ofer'=>$prod->cod_ofer));
						$this->db->delete('_api_descripciones',array('cod_ofer'=>$prod->cod_ofer));
						$this->db->delete('_propiedades_videos',array('propiedades_list_id'=>$prod->id));
						$this->fillPropiedad($data['cod_ofer'],$primary);
						echo 'Actualizamos';
						
					}elseif($prod->num_rows()>0 && !empty($propiedad) && $del){
						//Eliminamos
						$prod = $prod->row();
						$this->db->delete('_propiedades_list',array('id'=>$prod->id));
						$this->db->delete('_api_propiedad_fotos',array('api_propiedades_id'=>$prod->id));
						$this->db->delete('_api_descripciones',array('api_propiedades_id'=>$prod->id));
						$this->db->delete('_propiedades_videos',array('propiedades_list_id'=>$prod->id));
						echo 'Eliminamos';
						
					}
				}
			}
			self::fillPropiedades($page+1);
		}
	}

	function fillPropiedad($cod,$primary){
		$propiedad = $this->getPropiedad($cod);
		$data = array();
		if(count($propiedad->ficha)>1){
			$datos = $this->splitExtras($propiedad->ficha[1]);
			$data = $datos['data'];
			$extras = $datos['extras'];
				
			$this->checkFields('_propiedades_list',$data);
			$this->db->update('_propiedades_list',$data,array('id'=>$primary));		

			//Fotos
			foreach($propiedad->fotos as $cod=>$fot){
				
				foreach($fot as $f){
					$data = array('cod_ofer'=>$cod,'foto'=>$f,'api_propiedades_id'=>$primary);
					$this->db->insert('_api_propiedad_fotos',$data);
				}
			}

			//Descripciones
			
			foreach($propiedad->descripciones as $cod=>$fot){
				
				foreach($fot as $n=>$f){
					$data = array('cod_ofer'=>$cod,'titulo'=>$f->titulo,'descrip'=>$f->descrip,'api_propiedades_id'=>$primary,'idioma_cod'=>$n,'idioma'=>$this->getIdioma($n));
					$this->db->insert('_api_descripciones',$data);
				}
			}

			//Videos
			
			foreach($propiedad->videos as $cod=>$fot){
				
				foreach($fot as $n=>$f){
					$data = array('propiedades_list_id'=>$primary,'video'=>$f);
					$this->db->insert('_propiedades_videos',$data);
				}
			}

			//Extras
			$this->addExtras($extras,$primary);
		}
	}

	function splitExtras($v){
		$datos = array(
			'data'=>array(),
			'extras'=>array()
		);

		
		foreach($v as $nn=>$d){
			//Sacar extras
			$enc = false;

			foreach($this->extras->result() as $e){
				//echo "<p>!".(string)$enc." && ".$e->codigo."==".$nn.'</p>';
				if(!$enc && $e->codigo==$nn){
					$enc = true;
					if(!empty($d)){
						$datos['extras'][] = array($e->id,$d);
					}
				}
			}
			if(!$enc){
				$datos['data'][$nn] = (string)$d;
			}
		}

		return $datos;
	}

	function addExtras($data,$primary){
		foreach($data as $d){
			$this->db->insert('_propiedades_list_extras',array('propiedades_list_id'=>$primary,'propiedades_extras_id'=>$d[0],'valor'=>$d[1]));
		}
	}




	/*** Estructuras de tablas *****/
	function getPropiedadesStruct(){
		$propiedades = $this->getPropiedades();
		$sql = 'CREATE TABLE api_propiedades(';
		$fields = '';
		foreach($propiedades->paginacion[1] as $n=>$v){
			$fields.= ','.$n.' varchar(255) COLLATE latin1_general_ci DEFAULT NULL';
		}
		$sql.= substr($fields,1);
		$sql.= ')';
		return $sql;
	}

	function getPropiedadStruct(){
		$propiedades = $this->getPropiedades();
		$fragmentos = ceil($propiedades->paginacion[0]->total/$propiedades->paginacion[0]->elementos);		

		$sql = 'CREATE TABLE api_propiedad(';
		$fields = '';
		$_SESSION['cells'] = empty($_SESSION['cells'])?array():$_SESSION['cells'];		

		$propiedades = $this->getPropiedades(19);		
		foreach($propiedades->paginacion as $n=>$v){	
			if($n>0){		
				$prop = $this->getPropiedad($v->cod_ofer);
				foreach($prop->ficha[1] as $n=>$v){
					if(!in_array($n,$_SESSION['cells'])){
						$_SESSION['cells'][] = $n;
					}
				}
			}
		}

		$propiedades = $this->getPropiedades(20);		
		foreach($propiedades->paginacion as $n=>$v){	
			if($n>0){		
				$prop = $this->getPropiedad($v->cod_ofer);
				foreach($prop->ficha[1] as $n=>$v){
					if(!in_array($n,$_SESSION['cells'])){
						$_SESSION['cells'][] = $n;
					}
				}
			}
		}


		$propiedades = $this->getPropiedades(21);		
		foreach($propiedades->paginacion as $n=>$v){	
			if($n>0){		
				$prop = $this->getPropiedad($v->cod_ofer);
				foreach($prop->ficha[1] as $n=>$v){
					if(!in_array($n,$_SESSION['cells'])){
						$_SESSION['cells'][] = $n;
					}
				}
			}
		}


		foreach($_SESSION['cells'] as $n){
			$fields.= ','.$n.' varchar(255) COLLATE latin1_general_ci DEFAULT NULL';
		}
		


		$sql.= substr($fields,1);
		$sql.= ')<br/>';
		return $sql;
	}


}