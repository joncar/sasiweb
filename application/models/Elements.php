<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function precioM2XZona($zonas_id,$tipo){
		$zonaT = $this->db->get_where('zonas',array('id'=>$zonas_id));
		$valor = 0;
		if($zonaT->num_rows()>0){
			$zonaT = $zonaT->row();
			$this->db->select('ciudades.id,comarcas.provincias_id');
			$this->db->join('comarcas','comarcas.id = ciudades.comarcas_id');
			$ciudadT = $this->db->get_where('ciudades',array('ciudades.id'=>$zonaT->ciudades_id));
			if($ciudadT->num_rows()>0){
				$ciudadT = $ciudadT->row();
				$provinciaT = $this->db->get_where('provincias',array('id'=>$ciudadT->provincias_id));
				if($provinciaT->num_rows()>0){
					$provinciaT = $provinciaT->row();

					$this->db->select('precio,m_uties,TRUNCATE((SUM(precio/m_uties)/COUNT(id)),2) as preciom',FALSE);
			        $zona = $this->db->get_where('propiedades_list',array('zonas_id'=>$zonas_id,'key_tipo'=>$tipo,'keyacci'=>1,'m_uties >'=>0));
			        $propiedades = null;
			        if($zona->num_rows()>0 && !empty($zona->row()->preciom)){
			            $propiedades = $zona;
			        }
			        //Si no se encuentran por zona buscar por ciudad
			        if(!$propiedades){
			            $this->db->select('precio,m_uties,TRUNCATE((SUM(precio/m_uties)/COUNT(id)),2) as preciom',FALSE);
			            $zona = $this->db->get_where('propiedades_list',array('ciudades_id'=>$ciudadT->id,'key_tipo'=>$tipo,'keyacci'=>1,'m_uties >'=>0));
			            if($zona->num_rows()>0 && !empty($zona->row()->preciom)){
			                $propiedades = $zona;
			            }
			        }

			        //Si no se encuentran por ciudad buscar por provincia
			        if(!$propiedades){
			            $this->db->select('precio,m_uties,TRUNCATE((SUM(precio/m_uties)/COUNT(id)),2) as preciom',FALSE);
			            $zona = $this->db->get_where('propiedades_list',array('provincias_id'=>$provinciaT->id,'key_tipo'=>$tipo,'keyacci'=>1,'m_uties >'=>0));
			            if($zona->num_rows()>0 && !empty($zona->row()->preciom)){
			                $propiedades = $zona;
			            }
			        }

			        $valor = round($propiedades->row()->preciom,0);
		    	}
	    	}
    	}
    	return $valor;
	}
}