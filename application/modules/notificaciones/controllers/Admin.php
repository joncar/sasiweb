<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');  
            $crud->set_clone();
            $this->loadView($crud->render());
        }

        function syncronize(){
            $this->db->query('truncate emails');
            $mbox = imap_open("{mallorcaislandfestival.com:143/novalidate-cert}", "reservas@mallorcaislandfestival.com", "i84%Ba5i", NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'))
                  or die("no se puede conectar: " . imap_last_error());
            $sorted_mbox = imap_sort($mbox, SORTDATE, 0); 
            $totalrows = imap_num_msg($mbox);
            
            for($i=0;$i<$totalrows;$i++){
                $data = array();
                $header = imap_fetchheader($mbox, $sorted_mbox[$i]);

                $subject = array();
                preg_match_all('/^Subject: (.*)/m', $header, $subject);
                $data['subject'] = utf8_encode($subject[1][0]);
                
                $subject = array();
                preg_match_all('/^Date: (.*)/m', $header, $subject);
                $data['fecha'] = strtotime($subject[1][0]);
                $data['fecha'] = date("Y-m-d H:i:s",$data['fecha']);

                $subject = array();
                preg_match_all('/^From: (.*)/m', $header, $subject);
                $data['email'] = $subject[1][0];

                $data['message'] = utf8_encode(imap_qprint(imap_body($mbox,$sorted_mbox[$i])));
                $this->db->insert('emails',$data);
            }

            imap_close($mbox); 
            echo 'Se han incorporado:'.$totalrows.' Emails a la bd <a href="'.base_url('notificaciones/admin/emails/success').'">Volver a los emails</a>';
        }

        function emails($x = ''){
            if($x=='syncronize'){
                $this->syncronize();
                die();
            }
            $crud = $this->crud_function('','');  
            $crud->unset_add()
                 ->unset_read()
                 ->unset_delete()
                 ->unset_print();
            $crud = $crud->render();
            $crud->output = '<div style="margin-bottom:20px;"><a href="'.base_url('notificaciones/admin/emails/syncronize').'" class="btn btn-info">Sincronizar correos</a></div>'.$crud->output;
            $this->loadView($crud);         
        }
    }
?>
