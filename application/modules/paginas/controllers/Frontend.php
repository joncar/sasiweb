<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){            
            $theme = $this->theme;
            $params = $this->uri->segments;
            //Existe?            
            if(file_exists(APPPATH.'modules/paginas/views/'.$this->theme.$url.'.php')){
                $page = $this->load->view($theme.$url,array(),TRUE);
            }else{
                $idiomas = $this->db->get('ajustes')->row()->idiomas;
                $idiomas = explode(',',$idiomas);
                foreach($idiomas as $i){
                    $i = trim($i);
                    if(file_exists(APPPATH.'modules/paginas/views/'.$i.'/'.$url.'.php')){
                        $page = $this->load->view($i.'/'.$url,array(),TRUE);
                        $_SESSION['lang'] = $i;
                        $this->theme = $i.'/';
                    }
                }
            }
            if(empty($page)){
                throw new exception('Pagina web no encontrada',404);
            }
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$page,
                    'link'=>$url,
                    'url'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                                            
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }

        function solicitar_asesor(){
            if(empty($_POST['extras']['telefono'])){
                echo $this->error('Por favor complete los datos con asterisco(*)');
                die();
            }
            if(empty($_POST['extras']['provincia'])){
                echo $this->error('Por favor complete los datos con asterisco(*)');
                die();
            }
            if(empty($_POST['extras']['cp'])){
                echo $this->error('Por favor complete los datos con asterisco(*)');
                die();
            }
            if(empty($_POST['extras']['poblacion'])){
                echo $this->error('Por favor complete los datos con asterisco(*)');
                die();
            }
            $this->contacto();
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre',l('nombre'),'required');            
            $this->form_validation->set_rules('politicas',l('acepto-las').' '.l('politicas-de-privacidad'),'required');                        
            $this->form_validation->set_rules('token','CAPTCHA','required');   
            if($this->form_validation->run()){    
                $this->load->library('recaptcha');                
                if(!$this->recaptcha->recaptcha_check_answer($_POST['token'])){
                    $_SESSION['msj'] = $this->error('Captcha es requerido');
                }else{            
                    $datos = $_POST;
                    if(!empty($_POST['extras'])){
                        unset($datos['extras']);
                        $datos['extras'] = '';
                        foreach($_POST['extras'] as $n=>$v){
                            $n = validar_acentos($n);
                            $n = str_replace('_',' ',$n);
                            $n = ucfirst($n);
                            $v = str_replace('_',' ',$v);
                            $v = ucfirst($v); 
                            if($n=='Blanco'){                           
                                $datos['extras'].= '<br/>';
                            }else{
                                $datos['extras'].= '<br/><b>'.$n.': </b> '.$v.'';
                            }                            
                        }
                    }else{
                        $datos['extras'] = '';
                    }
                    if(empty($_POST['titulo'])){
                        $datos['titulo'] = 'Correo sin titulo enviado desde la web';
                    }
                    if(empty($_POST['asunto'])){
                        $datos['asunto'] = '';
                    }
                    $email = $this->db->get('ajustes')->row()->email_contacto_igualada;
                    if(!empty($_POST['to'])){                        
                        $email = $_POST['to'];
                    }else{
                        $datos['to'] = $email;
                    }
                    $datos['extras'].= '<br/><b>Se aceptó aviso legal?</b>: SI';
                    $firma = $datos['to'] == 'info@finques-sasi.com'?19:20;
                    $datos['firma'] = $this->db->get_where('notificaciones',array('id'=>$firma))->row()->texto;
                    
                    //Viene la referencia? se adjunta el inmuble
                    if(!empty($datos['referencia'])){
                        $this->db->where('propiedades_list.ref',$datos['referencia']);
                        $aa = $this->querys->get_propiedades()->row();
                        $aa->foto = '<img src="'.$aa->foto.'" style="width:100%;">';
                        $aa->precio = moneda($aa->precio);
                        $aa->link = base_url('propiedad/'.$aa->ref);
                        if(empty($aa->titulo)){
                            $aa->titulo = $aa->nbtipo.' en '.$aa->ciudad.' zona '.$aa->zona.' con '.$aa->banyos.' baños, conservación '.$aa->nbconservacion; 
                            mb_strtolower($aa->titulo);
                        }

                        $datos['propiedad'] = $this->db->get_where('notificaciones',array('id'=>21))->row()->texto;
                        foreach($aa as $n=>$l){
                            $datos['propiedad'] = str_replace('{'.$n.'}',$l,$datos['propiedad']);
                        }
                    }else{
                        $datos['propiedad'] = '';
                    }


                    $this->enviarcorreo((object)$datos,15,$email);
                    $this->enviarcorreo((object)$datos,15,$datos['email']);
                    $_SESSION['msj'] = $this->success(l('contact_success'));                    
                }
            }else{                
               $_SESSION['msj'] = $this->error(l('contact_error'));
               $_SESSION['msj'] = str_replace('{form}',$this->form_validation->error_string(),$_SESSION['msj']);
            }

            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }
        
        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            //$this->form_validation->set_rules('politicas','Políticas','required');
            $this->form_validation->set_rules('token','CAPTCHA','required');   
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['token'])){
                    $_SESSION['msj2'] = $this->error('Captcha es requerido');
                }else{ 
                    $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                    $success = $emails->num_rows()==0?TRUE:FALSE;
                    if($success){
                        $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                        $_SESSION['msj2'] = $this->success('Subscripción exitosa');
                        $err = 'success';
                        $this->enviarcorreo((object)$_POST,14);
                    }else{
                        $_SESSION['msj2'] = $this->error('Ya el correo esta registrado');
                    }
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo $_SESSION['msj2'];
            unset($_SESSION['msj2']);            
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }

        function sitemap(){
            $pages = array(
                base_url(),
                base_url().'contacto.html',
                base_url().'empresa.html',
                base_url().'publica',
                base_url().'lowcost',
                base_url().'politicas-igualada.html',
                base_url().'aviso-legal-igualada.html',
                base_url().'politicas-alicante.html',
                base_url().'aviso-legal-alicante.html',
                base_url().'cookies.html',
                base_url().'propiedades',
                base_url().'listado'
            );

            //Listados por provincias
            foreach($this->db->get_where('provincias',array())->result() as $b){
                $pages[] = base_url().'listado/provincias_id-'.$b->id.'-p-'.toURL(mb_strtolower($b->nombre));
                $pages[] = base_url().'propiedades-en-mapa/provincias_id-'.$b->id.'-p-'.toURL(mb_strtolower($b->nombre));
                $pages[] = base_url().'propiedades-en-list/provincias_id-'.$b->id.'-p-'.toURL(mb_strtolower($b->nombre));
            }

            //Listados por comarcas
            foreach($this->db->get_where('comarcas',array())->result() as $b){
                $pages[] = base_url().'listado/provincias_id-'.$b->provincias_id.'-comarcas_id-'.$b->id.'-p-'.toURL(mb_strtolower($b->nombre));
                $pages[] = base_url().'propiedades-en-mapa/provincias_id-'.$b->provincias_id.'-comarcas_id-'.$b->id.'-p-'.toURL(mb_strtolower($b->nombre));
                $pages[] = base_url().'propiedades-en-list/provincias_id-'.$b->provincias_id.'-comarcas_id-'.$b->id.'-p-'.toURL(mb_strtolower($b->nombre));
            }

            //Listados por ciudades
            foreach($this->db->query('SELECT ciudades.*, p.provincias_id as provid,p.comarcas_id FROM propiedades_list as p INNER JOIN ciudades ON ciudades.id = p.ciudades_id WHERE ciudades_id IS NOT NULL GROUP BY ciudades_id')->result() as $b){
                $pages[] = base_url().'listado/provincias_id-'.$b->provid.'-comarcas_id-'.$b->comarcas_id.'-ciudades_id-'.$b->id.'-p-'.toURL($b->ciudad);
                $pages[] = base_url().'propiedades-en-mapa/provincias_id-'.$b->provid.'-comarcas_id-'.$b->comarcas_id.'-ciudades_id-'.$b->id.'-p-'.toURL($b->ciudad);
                $pages[] = base_url().'propiedades-en-list/provincias_id-'.$b->provid.'-comarcas_id-'.$b->comarcas_id.'-ciudades_id-'.$b->id.'-p-'.toURL($b->ciudad);
            }
            
            //Listados de propiedades
            foreach($this->db->get_where('propiedades_list')->result() as $b){
                $pages[] = base_url().'propiedad/'.$b->ref;
            }

            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }

        
    }
