<div class="modal fade" id="avisameSiBajaModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<form action="" onsubmit="insertar('propiedades/account/avisos/insert',new FormData(this),'#resultAvisoSiBaja',function(data){$('#resultAvisoSiBaja').attr('class','alert alert-success').html('Aviso almacenado con éxito')}); return false;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?= l('Alertas en tu email') ?></h4>					
				</div>
				<div class="modal-body">
					<div class="row avisocuerpo">
						<div class="col-xs-12">
							<b><?= l('Recibir alertas del Inmueble',(array)$detail) ?></b>
						</div>
						<div class="col-xs-12">&nbsp;</div>
						<div class="col-xs-12 col-md-4">
							<div class="foto " style="background:url(<?= $detail->foto ?>); background-size:cover;">
								<img src="<?= $detail->foto ?>" alt="" style="width:100%;display:none;">
							</div>
						</div>
						<div class="col-xs-12 col-md-8">
							<div><b><?= $detail->titulo ?></b></div>
							<div><?= $detail->m_uties ?>m<sup>2</sup> - <?= $detail->total_hab ?> habitaciones 
								<br/><span class="precio" style="font-size: 1.5em;"><?= moneda($detail->precio) ?></span></div>
						</div>
					</div>
					<hr>
					<b><?= l('Indícanos el email donde quieres recibir las alertas') ?></b>
					
					<div class="form-group">						
						<input type="email" name="email" class="form-control" value="<?= @$this->user->email ?>" placeholder="<?= l('escribe-tu-email') ?>">
						<input type="hidden" name="cod_ofer" value="<?= $detail->cod_ofer ?>">
						<input type="hidden" name="ultimo_precio" value="<?= $detail->precio ?>">
					</div>
					<div class="checkbox">
						<label for="">
                        	<input type="checkbox" name="politicas" value="1"> <?= l('acepto-el') ?> <a href="<?= base_url() ?>aviso-legal-igualada.html"><?= l('aviso-legal') ?></a> y <a href="<?= base_url() ?>politicas-igualada.html"><?= l('politicas-de-privacidad') ?></a>
                        </label>
                    </div>
                    <div class="checkbox">
						<label for="">
                        	<input type="checkbox" name="relacionados" value="1" style="width:auto; height:auto;"> <?= l('Recibir alertas de anuncios similares') ?></b>
                        </label>
                    </div>
					<div id="resultAvisoSiBaja"></div>
					
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success"><?= l('Recibir alertas') ?></button>
	         		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#avisameSiBajaModal"><?= l('cerrar') ?></button>
				</div>
			</form>


		</div>
	</div>
</div>