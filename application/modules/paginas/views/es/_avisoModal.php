<!------------- BANNER ------------------>
<div class="avisoModal" style="display: none;">
  <div>
    <a href="javascript:void(0)" onclick='$(".avisoModal").hide()' class="close"><i class="fa fa-times"></i></a>
    <div class="contactenosTxt"><?= l('avisomodaltitle') ?></div>
    <div class="contenido">          
      <?= l('avisomodaltitles') ?>
      <div class="cuadroRojo">
        <?= l('queharemosporti') ?>
        </div>
      </div>
      <div class="cuadroLateral">
        <?= l('nopierdasmaseltiempo') ?>
        <div class="inputBtnRef">
          <a href="#contactarAsesor" data-toggle="modal" class="btn-blue border_radius contactar uppercase"><?= l('contactar') ?></a>
        </div>
      </div>
  </div>
</div>
<?php 
  //unset($_SESSION['last']);
  //unset($_SESSION['time']);
  if(empty($_SESSION['last']) || $_SESSION['last']!=2){
    if(empty($_SESSION['time'])){
      $_SESSION['time'] = time();
    }
    $timeout = empty($_SESSION['last'])?300:480;
    $time = time()-$_SESSION['time'];
    $time = ($timeout-$time)*1000;
    if($time<0){
      $_SESSION['last'] = empty($_SESSION['last'])?1:2;
      $_SESSION['time'] = time();
      $time = 480*1000;
    }
    $onRepeat = '';
    if(empty($_SESSION['last'])){
        $onRepeat = 'setTimeout(function(){$(".avisoModal").fadeIn(400);},480000);';
    }
    echo '<script>
              setTimeout(function(){
                $(".avisoModal").fadeIn(400);
                '.$onRepeat.'
              },'.$time.');
          </script>';
  }
?>

<div class="modal fade" id="contactarAsesor">
  <form action="paginas/frontend/solicitar_asesor" onsubmit="return sendForm(this,'#responseContactarAsesorModal')">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title"><?= l('contactarasesorpersonal') ?></h4>
        </div>
        <div class="modal-body">        
            <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" name="nombre" placeholder="<?= l('nombre') ?> *" required="">
                </div>
              </div>
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <input type="tel" class="form-control" name="extras[telefono]" placeholder="<?= l('telefono') ?> *" required="">
                </div>
              </div>
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <input type="email" class="form-control" name="email" placeholder="Email *" required="">
                </div>
              </div>
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <?php 
                    $this->load->helper('html');
                    $this->db->order_by('nombre','ASC');
                    $provincias = $this->db->get('provincias');
                    $a = $this->db->get('ajustes')->row();
                  ?>
                  <select name="extras[provincia]" id="provinciaAsesorModal" class="form-control *" required="">
                    <option value=""><?= ucfirst(l('provincia')) ?> *</option>
                    <?php foreach($provincias->result() as $p): ?>
                      <option data-to="<?= $p->sucursal==278?$a->email_contacto_igualada:$a->email_contacto_alicante; ?>" value="<?= $p->nombre ?>"><?= $p->nombre ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" name="extras[cp]" placeholder="<?= l('codigopostal') ?> *" required="">
                </div>
              </div>

              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" name="extras[poblacion]" placeholder="<?= l('poblacion') ?> *" required="">
                </div>
              </div>
              
            </div>

            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" name="extras[calle]" placeholder="<?= l('calle') ?>">
                </div>
              </div>
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <textarea class="form-control" placeholder="<?= l('comentarios') ?>" name="extras[comentario]"></textarea>
                </div>
               </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-12">                   
                  <div class="form-group">
                    <input type="checkbox" name="politicas" value="1"> <?= $this->lang->line('acepto-las') ?> <a href="<?= base_url() ?>politicas-igualada.html" target="_new"><u><?= $this->lang->line('politicas-de-privacidad') ?></u></a>
                  </div>                          
              </div>
            </div>
            <div id="responseContactarAsesorModal"></div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="titulo" value="Web SASI, petición de asesor inmobiliario personal.">
          <input type="hidden" name="asesorModalTo" value="<?= $a->email_contacto_igualada ?>">
          <button type="submit" class="btn btn-success"><?= l('enviar') ?></button>
          <button type="button" onclick="$('.avisoModal').hide();" class="btn btn-danger" data-dismiss="modal"><?= l('cerrar') ?></button>
        </div>
      </div><!-- /.modal-content -->
    </div>
  </form>
</div>

<script>
  $("#provinciaAsesorModal").on('change',function(){
    $("#asesorModalTo").val($(this).find('option:selected').data('to'));
  });
</script>