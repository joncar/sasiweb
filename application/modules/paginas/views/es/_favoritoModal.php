<div class="modal fade" id="favoritoModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" onsubmit="setFav(this,'<?= $detail->cod_ofer ?>'); return false;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?= l('anadir-a-favoritos') ?></h4>					
				</div>
				<div class="modal-body">
					<div class="row avisocuerpo">
						<div class="col-xs-12">
							<b>Guarda esta propiedad en tu lista de favoritos</b>
						</div>
						<div class="col-xs-12">&nbsp;</div>
						<div class="col-xs-12 col-md-4">
							<div class="foto" style="background:url(<?= $detail->foto ?>); background-size:cover;">
								<img src="<?= $detail->foto ?>" alt="" style="width:100%;display:none;">
							</div>
						</div>
						<div class="col-xs-12 col-md-8">
							<div><b><?= $detail->titulo ?></b></div>
							<div><?= $detail->m_uties ?>m<sup>2</sup> - <?= $detail->total_hab ?> habitaciones 
								<br/><span class="precio" style="font-size: 1.5em;"><?= moneda($detail->precio) ?></span></div>
						</div>
					</div>
					<hr>
					<b>Selecciona un grupo de favoritos o crea uno nuevo</b>
					<div class="row" style="margin-top:20px;">
						<div class="col-xs-12 col-md-6">
							<div class="form-group intro">						
								<select class="" id="groupFavSelect" name="favoritos_grupos_id">				                    
				                    <option value="0"><?= l('Sin clasificar') ?></option>
				                    <?php foreach(get_instance()->db->get_where('favoritos_grupos',array('user_id'=>get_instance()->user->id))->result() as $g): ?>
				                      <option value="<?= $g->id ?>"><?= $g->nombre ?> (<?= get_instance()->db->get_Where('favoritos',array('favoritos_grupos_id'=>$g->id,'user_id'=>get_instance()->user->id))->num_rows() ?>)</option>
				                    <?php endforeach ?>                    
				                </select>
							</div>
						</div>	
						<div class="col-xs-12 col-md-6">
							<div class="form-group">						
								<input type="text" name="nombre" class="form-control" value="" placeholder="Nuevo nombre de grupo">
								<input type="hidden" name="cod_ofer" value="<?= $detail->cod_ofer ?>">
							</div>
						</div>	
					</div>			
					<div id="resultFavoritoModal"></div>
					
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success"><?= l('guardar') ?></button>
	         		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#favoritoModal"><?= l('cerrar') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	function onload(){
		alert("");
	}
</script>