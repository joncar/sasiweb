<?php 
  get_instance()->db->limit(10);
  $fotos = get_instance()->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer)); 
?>

<?php if(!empty($detail->foto) && $fotos->num_rows()==0): ?>
<div class="item">
  <div class="image" style="width:100%; height:<?= empty($height)?'258px':$height; ?>; background:url(<?= $detail->foto ?>); background-size:cover; background-position: center">
      <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
    <div class="price clearfix">
      <span class="tag"><?= $detail->acciones ?></span>
    </div>
  </div>
</div>
<?php endif ?>
<?php         
  foreach($fotos->result() as $f): 
?>
    <div class="item">
      <div class="image" style="width:100%; height:<?= empty($height)?'258px':$height; ?>; background:url(<?= $f->foto ?>); background-size:cover; background-position: center">
        <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
        <div class="price clearfix">
          <span class="tag"><?= $detail->acciones ?></span>
        </div>
      </div>
    </div>
<?php endforeach ?>