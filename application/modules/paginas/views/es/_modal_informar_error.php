<div class="modal fade" id="informarErrorModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#resultErrorModal');">
				<div class="modal-header">
					<h4 class="modal-title"><?= l('¿Tiene un error la propiedad') ?> <?= $detail->ref ?>?</h4>
				</div>
				<div class="modal-body">					
					<p><b><?= l('Indícanos el error') ?></b></p>
					<div class="form-group">						
						<input type="text" name="nombre" class="form-control" value="" placeholder="<?= l('nombre') ?>">
					</div>	
					<div class="form-group">						
						<input type="text" name="email" class="form-control" value="" placeholder="Email">
					</div>	
					<div class="form-group">						
						<textarea name="extras[error]" id="" cols="30" rows="10" placeholder="<?= ('Indícanos el error') ?>" class="form-control"></textarea>
						<input type="hidden" name="cod_ofer" value="<?= $detail->ref ?>">
						<input type="hidden" name="titulo" value="Web SASI, informar de un error Ref: <?= $detail->ref ?>">
						<input type="hidden" name="asunto" value="Un usuario a indicado que la propiedad tiene un error">
						<input type="hidden" name="to" value="<?= $detail->email_contacto ?>">
						<input type="hidden" name="referencia" value="<?= $detail->ref ?>">
					</div>	
					<div class="checkbox">
						<label for="">
                        	<input type="checkbox" name="politicas" value="1"> <?= l('acepto-el') ?> <a href="<?= base_url() ?>aviso-legal-igualada.html"><?= l('aviso-legal') ?></a> y <a href="<?= base_url() ?>politicas-igualada.html"><?= l('politicas-de-privacidad') ?></a>
                        </label>
                    </div>	
					<div id="resultErrorModal"></div>
					
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success"><?= l('enviar') ?></button>
	         		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#informarErrorModal"><?= l('cerrar') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	function onload(){
		alert("");
	}
</script>