<?php $detail->foto = str_replace('[base_url]',base_url(),$detail->foto); ?>
<div class="property_item heading_space" id="propiedad<?= 'griya'.$detail->cod_ofer ?>">
  <?php if($detail->lowcost): ?>
    <div class="lowcost">
      <span></span>
      LOW COST  
    </div>
  <?php endif ?>
  <div class="property_head text-center">
    <a href="<?= base_url('propiedad/'.$detail->ref) ?>" class=""> 
      <h3 class="captlize" style="margin: 0;">
        <?= l('tipo_'.mb_strtolower($detail->nbtipo)) ?>
      </h3>
      <p style="margin-bottom: 0"> 
        en <?= $detail->ciudad ?><br/><span class="azul">Ref: <?= $detail->ref ?></span>
      </p>
    </a>
  </div>
  
<a href="<?= base_url('propiedad/'.$detail->ref) ?>" class=""> 
  <div class="propiedadDetailFoto owl-carousel">
      
      <?php 
        get_instance()->db->limit(5);
        $fotos = get_instance()->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer)); 
      ?>

      <?php if(!empty($detail->foto) && $fotos->num_rows()==0): ?>
      <div class="item">
        <div class="image" style="background:url(<?= base_url('img/default.jpg') ?>); background-size:cover; background-position: center" data-url="<?= $detail->foto ?>">
            <img src="<?= base_url('img/default.jpg') ?>" alt="latest property" class="img-responsive">
          <div class="price clearfix">
            <span class="tag"><?= $this->lang->line('acciones_'.strtolower($detail->acciones)) ?></span>
          </div>
        </div>
      </div>
      <?php endif ?>
      <?php         
        foreach($fotos->result() as $n=>$f): if($n>0):
      ?>
          <div class="item">
            <div class="image" style="width:100%; height:<?= empty($height)?'258px':$height; ?>; background:url(<?= base_url('img/default.jpg') ?>); background-size:cover; background-position: center" data-url="<?= $f->foto ?>">
              <img src="<?= base_url('img/default.jpg') ?>" alt="latest property" class="img-responsive" style="display: none">
              <div class="price clearfix">
                <span class="tag"><?= $this->lang->line('acciones_'.strtolower($detail->acciones)) ?></span>
              </div>
            </div>
          </div>
      <?php endif; endforeach ?>
      
  </div>
</a>
  <div class="proerty_content">
    <div class="property_meta">
      <span><i class="icon-select-an-objecto-tool"></i><?= empty($detail->m_uties)?'0':$detail->m_uties ?> m<sup>2</sup></span>
      <span><i class="icon-bed"></i><?= $detail->total_hab ?> <?= $this->lang->line('hab') ?></span>
      <span><i class="icon-safety-shower"></i><?= $detail->banyos ?> <?= $this->lang->line('banos') ?></span>
    </div>
    <div class="proerty_text">
      <a href="<?= base_url('propiedad/'.$detail->ref) ?>" class="">
      <div>
        <span class="precio">
          <?= moneda($detail->precio); ?>           
        </span>
       
        <a href="javascript:showAvisoSiBaja(<?= $detail->id ?>)" class="campanaGriya">
          <i class="fa fa-bell-o"></i> 
          <span><?= $this->lang->line('avisame') ?></span>
          <span><?= $this->lang->line('si-baja') ?></span>
        </a>


      </div>
      <p class="preciobajo" style="float:none;">
        <span>
          <?php if(!empty($detail->habajado)): ?>
            <?= $this->lang->line('ha-bajado') ?> <?= moneda($detail->habajado); ?>
          <?php endif ?>   
        </span>           
        </p>

      <div class="propiedadGriyaText">
          <p class="titulo">        
            <b><?= $detail->titulo ?></b> 
          </p>
          <p class="descripcion">                
            <?= cortar_palabras($detail->descrip,40);  ?>
          </p>
      </div>
      </a>                  
    </div>
    <div class="favroute clearfix favroute2">      
      <ul>
        <?php if(!empty($detail->tourvirtual)): ?>
          <li>
            <a target="_blank" href="https://ap.apinmo.com/fotosvr/tour.php?cod=<?= $detail->cod_ofer ?>.<?= $detail->sucursal ?>&o=<?= $detail->sucursal ?>" title="Tour Virtual">
              <img src="<?= base_url('img/360.png') ?>">
            </a>
          </li>
        <?php endif ?>
        <li style="position:relative">
          <a title="<?= $this->lang->line('numero-de-fotos') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>" class="">
            <i class="icon-camera3" style="font-size:16px;display: inline-block;"></i> 
            <span style="font-size: 13px;color: rgba(227, 39, 24, 0.68);position: absolute;right: 2px;top: -14px;">
              <?= get_instance()->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer))->num_rows() ?>
                
              </span>
            </a>
          </li>
        
        <li><a title="<?= $this->lang->line('anadir-a-favoritos') ?>" href="javascript:addFav('<?= $detail->cod_ofer ?>')"><i class="icon-like fav<?= $detail->cod_ofer ?>"></i></a></li>
        <li><a title="<?= $this->lang->line('ocultar-propiedad') ?>" href="javascript:removeProp('<?= 'griya'.$detail->cod_ofer ?>')"><i class="icon-trash-can3" style="font-size: 23px;"></i></a></li>
        <li>
          <a title="<?= $this->lang->line('ver-propiedad-en-el-mapa') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>#mapa2" class="">
            <i class="icon-map-pin"></i>
          </a>
        </li>
        <li>
          <a title="<?= $this->lang->line('ver-el-street-view') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>#showStreetView" class="">
            <i class="icon-map3"></i>
          </a>
        </li>
        <li><a title="<?= $this->lang->line('contactar-con-sasi') ?>" href="javascript:contactar('<?= $detail->cod_ofer ?>')" class="share_expender"><i class="icon-envelope"></i></a></li>
      </ul>
    </div>
    <div class="toggle_share collapse" id="prop<?= $detail->cod_ofer ?>">
      <ul>
        <li><a href="#" class="facebook"><i class="icon-facebook-1"></i> Facebook</a></li>
        <li><a href="#" class="twitter"><i class="icon-twitter-1"></i> Twitter</a></li>
        <li><a href="#" class="vimo"><i class="icon-vimeo3"></i> Vimeo</a></li>
      </ul>
    </div>
  </div>
</div>