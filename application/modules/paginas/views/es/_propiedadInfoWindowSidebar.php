<?php 
	$detail = $row->datos;
	if(!empty($detail)):
?>
	<div class="propiedadDetailContent">		
		<div class="property_head text-center">
		  <?php if($detail->lowcost): ?>
		    <div class="lowcost">
		      <span></span>
		      LOW COST  
		    </div>
		  <?php endif ?>
		  <h3 class="captlize">
		  	<?= l('tipo_'.strtolower($detail->nbtipo)) ?>		  		
		  </h3>
		  <p style="margin:0">en <?= $detail->ciudad ?> <br/> <span class="azul">Ref: <?= $detail->ref ?></span></p>
		</div>
		


		<a href="<?= base_url('propiedad/'.$detail->ref) ?>" class=""> 
		  <div class="propiedadDetailFoto owl-carousel">
		      
		      <?php 
		        get_instance()->db->limit(5);
		        $fotos = get_instance()->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer)); 
		      ?>

		      <?php if(!empty($detail->foto) && $fotos->num_rows()==0): ?>
		      <div class="item">
		        <div class="image" style="width:100%; height:<?= empty($height)?'258px':$height; ?>; background:url(<?= $detail->foto ?>); background-size:cover; background-position: center">
		            <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
		          <div class="price clearfix">
		            <span class="tag"><?= $detail->acciones ?></span>
		          </div>
		        </div>
		      </div>
		      <?php endif ?>
		      <?php      
				 foreach($fotos->result() as $n=>$f): if($n>0):
		      ?>
		          <div class="item">
		            <div class="image" style="width:100%; height:<?= empty($height)?'258px':$height; ?>; background:url(<?= $f->foto ?>); background-size:cover; background-position: center">
		              <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
		              <div class="price clearfix">
		                <span class="tag"><?= $detail->acciones ?></span>
		              </div>
		            </div>
		          </div>
		      <?php endif;  endforeach ?>
		      
		  </div>
		</a>



		<div class="proerty_content">
		  <div class="property_meta">
		    <span><i class="icon-select-an-objecto-tool"></i><?= empty($detail->m_uties)?'0':$detail->m_uties ?> M<sup>2</sup></span>
		    <span><i class="icon-bed"></i><?= $detail->total_hab ?> <?= l('habitaciones') ?></span>
		    <span><i class="icon-safety-shower"></i><?= $detail->banyos ?> <?= l('banos') ?></span>
		  </div>
		  
		  <div class="favroute clearfix favroute2" style="border: 1px solid #e5e5e5;">      
	      <ul>
        	<li style="position:relative">
	          <a title="<?= $this->lang->line('numero-de-fotos') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>" class="">
	            <i class="icon-camera3" style="font-size:16px;margin-right: 13px !important;display: inline-block;"></i> 
	            <span style="font-size: 13px;color: rgba(227, 39, 24, 0.68);position: absolute;right: 7px;top: -12px;">
	              <?= $this->db->get_where('api_propiedad_fotos',array('api_propiedades_id'=>$detail->id))->num_rows() ?>
	                
	              </span>
	            </a>
            </li>
	        
	        <li><a title="<?= l('anadir-a-favoritos') ?>" href="javascript:addFav('<?= $detail->cod_ofer ?>')"><i class="icon-like fav<?= $detail->cod_ofer ?>  <?= !empty($detail->fav)?'flaticon flaticon-heart active':'' ?>"></i></a></li>
	        <li><a title="<?= l('ocultar-propiedad') ?>" href="javascript:removePropMap('<?= $detail->cod_ofer ?>')"><i class="icon-trash-can3" style="font-size: 23px;"></i></a></li>	        
	        <li>
	          <a title="<?= l('ver-el-street-view') ?>" href="javascript:showStreetView('<?= $detail->latitud ?>','<?= $detail->altitud ?>')">
	            <i class="icon-map3"></i>
	          </a>
	        </li>
	        <li><a title="<?= l('contactar-con-sasi') ?>" href="javascript:contactar('<?= $detail->cod_ofer ?>')" class="share_expender"><i class="icon-envelope"></i></a></li>
	      </ul>
	    </div>
		  <div class="toggle_share collapse" id="four<?= $detail->id ?>">
		    <ul>
		      <li><a href="#" class="facebook"><i class="icon-facebook-1"></i> Facebook</a></li>
		      <li><a href="#" class="twitter"><i class="icon-twitter-1"></i> Twitter</a></li>
		      <li><a href="#" class="vimo"><i class="icon-vimeo3"></i> Vimeo</a></li>
		    </ul>
		  </div>


		  <div class="proerty_text"  style="padding: 15px;">
		      <div>
		      	<span class="precio">
		      		<?= moneda($detail->precioinmo); ?>		      	
		      	</span>
		      	 <a href="javascript:showAvisoSiBaja(<?= $detail->id ?>)" class="campanaGriya" style="margin-top: 0">
		          <i class="fa fa-bell-o"></i> 
		          <span><?= $this->lang->line('avisame') ?></span>
		          <span><?= $this->lang->line('si-baja') ?></span>
		        </a>
		      </div>
		      <p class="preciobajo" style="float:none;">
		      	<span>
		      	 	<?php if(!empty($detail->habajado)): ?>
			            <?= $this->lang->line('ha-bajado') ?> <?= moneda($detail->habajado); ?>
		            <?php endif ?>   
		        </span>		        
	          </p>
	          <div class="propiedadGriyaText" style="height:180px">
		          <p class="titulo">        
		            <b><?= $detail->titulo ?></b> 
		          </p>
			      <p class="descripcion">        
			        <a href="<?= base_url('propiedad/'.$detail->ref) ?>" class="loadingAfterClick">
			          <?= cortar_palabras($detail->descrip,40) ?>
			        </a>
			      </p>
		      </div>
	      </div>



		  <table class="table table-striped table-responsive">
		      <tbody>		        
		        <tr>
		          <td><b><?= l('referencia') ?></b></td>
		          <td class="text-right"><?= $detail->ref ?></td>
		        </tr>
		        <tr>
		          <td><b><?= l('precio') ?></b></td>
		          <td class="text-right"><?= moneda($detail->precioinmo); ?></td>
		        </tr>
		        <tr>
		          <td><?= l('m2-utiles') ?></b></td>
		          <td class="text-right"><?= $detail->m_uties ?> m<sup>2</sup></td>
		        </tr>
		        <tr>
		          <td><b><?= l('habitaciones') ?></b></td>
		          <td class="text-right"><?= $detail->total_hab ?></td>
		        </tr>

		        <tr>
		          <td><b><?= l('banos') ?></b></td>
		          <td class="text-right"><?= $detail->banyos ?></td>
		        </tr>

		        <tr>
                  <td><b><?= l('accion') ?></b></td>
                  <td class="text-right"><?= $detail->acciones ?></td>
                </tr>
                <tr>
                  <td><b><?= l('ano-de-construccion') ?></b></td>
                  <td class="text-right"><?= $detail->antiguedad ?></td>
                </tr>
                <tr>
                  <td><b><?= l('nivel-de-conservacion') ?></b></td>
                  <td class="text-right"><?= $detail->nbconservacion ?></td>
                </tr>
                <tr>
                  <td><b><?= l('tipo-propiedad') ?></b></td>
                  <td class="text-right"><?= $detail->nbtipo ?></td>
                </tr>
                <tr>
                  <td><b><?= l('orientacion') ?></b></td>
                  <td class="text-right"><?= $detail->nborientacion ?></td>
                </tr>
                <tr>
                  <td><b><?= l('disponible-desde') ?></b></td>
                  <td class="text-right"><?= date("d/m/Y",strtotime($detail->fechacreacion)) ?></td>
                </tr>
		      </tbody>
		    </table>
		    <?php 
			      	$this->db->select('propiedades_extras.*');
			      	$this->db->join('propiedades_extras','propiedades_extras.id = propiedades_list_extras.propiedades_extras_id');
		      		$prop = $this->db->get_where('propiedades_list_extras',array('propiedades_list_id'=>$detail->id));
		      		if($prop->num_rows()>0): 
  			?>
			    <h3 class="text-center text-capitalize bottom20"><?= l('caracteristicas-generales') ?></h3>
				<div class="property-details row" style="margin-left:0; margin-right:0">
				    <ul class="pro-list col-xs-12">
				      <?php 				      	
			      		foreach($prop->result() as $e): ?>
		            	  <li> <?= l('extra_'.$e->codigo) ?></li>
		          	  <?php endforeach ?>	              
		            </ul>
				</div>
			<?php endif ?>


		  <div class="row " style="margin-left: 0px; margin-right: 0px; margin-top: 20px;">
		    
			<?php 
				$fotos = $this->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer));
				foreach($fotos->result() as $n=>$f): if($n<6):  
			?>
			    <div class="col-md-4 col-xs-4" style="padding:5px;">
			      <div class="pro-img">
			        <figure class="wpf-demo-gallery" style="background:url(<?= $f->foto ?>); background-size:cover; width:100%; height:150px;">
			          
			          <figcaption class="view-caption">    
			            <a data-fancybox-group="gallery" class="fancybox" href="<?= $f->foto ?>">
			            	<i class="icon-focus"></i>
			            </a>
			          </figcaption>
			        </figure>
			      </div>
			    </div>
			<?php endif; endforeach ?>
		    
		  </div>



		  <!---- Formulario de contatto ---->
		  <div class="top30" style="padding-left:5px;" id="contactoSideBar">
			<h3 class="text-uppercase bottom20"><?= l('solicitar-informacion-a-sasi') ?></h3>
            <p><b><?= l('referencia-del-anuncio-ref',array('ref'=>$detail->ref)) ?></b></p>
			<form class="callus noloader top20" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#responseContactMap');">
              <div class="row" style="margin-left:0; margin-right:0">
                <div class="col-xs-12 col-md-12" style="padding:0">
                  <div class="form-group">
                    <input type="text" name="nombre" class="form-control" placeholder="<?= l('nombre') ?>">
                  </div>
                </div>
                <div class="col-xs-12 col-md-12" style="padding:0">
                  <div class="form-group">
                    <input type="tel" name="extras[telefono]" class="form-control" placeholder="<?= l('telefono') ?>">
                  </div>
                </div>
                <div class="col-xs-12 col-md-12" style="padding:0">
                  <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                  </div>
                </div>
              </div>

              <div class="row" style="margin-left:0; margin-right:0">
                <div class="col-xs-12 col-md-12" style="padding:0">
                  <div class="form-group">
                    <textarea name="extras[comentario]" class="form-control" placeholder="<?= l('comentario') ?>"></textarea>
                  </div>
                 </div>
                  <div class="col-xs-12 col-md-12 bottom20" style="padding:0">
                      <?= l('indica-fecha-hora') ?>
                  </div>
                  <div class="col-xs-12 col-md-12" style="padding:0">                        
                    <div class="col-xs-12 col-sm-12 col-md-12 pl-0" style="padding:0">
                      <div class="form-group">
                          <label for="fecha"><?= l('fecha') ?></label>
                          <div class="input-group">                              
                            <input type="text" class="form-control fecha" id="fecha" name="extras[fecha_visita]" placeholder="<?= l('fecha') ?> d/m/a">                              
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                          </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 pr-0" style="padding:0">
                      <div class="form-group">
                        <label for="hora"><?= l('hora') ?></label>
                        <div class="input-group">                            
                          <input type="text" class="form-control hora" id="hora" name="extras[hora_visita]" placeholder="<?= l('hora') ?> 00:00">                              
                          <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <input type="hidden" name="extras[ref]" value="<?= $detail->ref ?>">
                  
                  <div class="col-xs-12 col-md-12" style="padding:0">
                      <div class="form-group" style="margin-bottom: 0">
                        <input type="checkbox" name="extras[recordar_mis_datos]" value="SI"> <?= l('recordar-mis-datos-en-este-equipo') ?>
                      </div>
                      <div class="form-group" style="margin-bottom: 0">
                        <input type="checkbox" name="extras[recibir_alertas_similares]" value="SI"> <?= l('recibir-alertas-de-inmuebles-similares') ?>
                      </div>
                      <div class="form-group">
                        <input type="checkbox" name="politicas" value="1"> <?= l('acepto-el') ?> <a href="<?= base_url() ?>aviso-legal-igualada.html"><?= l('aviso-legal') ?></a> y <a href="<?= base_url() ?>politicas-igualada.html"><?= l('politicas-de-privacidad') ?></a>
                      </div>                          
                  </div>  
                  <div class="col-xs-12 col-md-12" id="responseContactMap"></div>    
                  	<input type="hidden" name="to" value="<?= $detail->email_contacto ?>">            
                  	<input type="hidden" name="titulo" value="Web SASI, información sobre el inmueble: <?= $detail->ref ?>">
                  	<input type="hidden" name="referencia" value="<?= $detail->ref ?>">
                    <div class="col-xs-12 col-md-12 top10" style="padding:0">
                      <div class="form-group">
                          <input type="submit" class="btn-blue uppercase border_radius" value="<?= l('enviar') ?>">
                        </div>
                    </div>
                  
                </div>
             
          </form>
			</div>
		  <!---- Fin formulariod e contacto ----->
		  

		</div>
	</div>
	<div class="btn-group btn-group-justified" role="group" style="padding-left: 4px;">
	    <div class="btn-group" role="group">
	      <a href="<?= base_url('propiedad/'.$detail->ref) ?>" class="btn btn-blue loadingAfterClick" style="border-radius:0; background: #ffd119; color:black;"><?= l('ver-ficha') ?></a>
	    </div>
	    <div class="btn-group" role="group">
	      <a href="#contactoSideBar" class="btn btn-blue" style="border-radius:0"><?= l('contactar') ?></a>
	    </div>
	</div>
<?php endif ?>
<script>
  $(".fecha").datepicker();
  $(".hora").mask("00:00");
</script>

<script>
	$("#propiedadDetail .propiedadDetailFoto").owlCarousel({
	    autoPlay: false,
	    stopOnHover: true,
	    navigation: true,
	    navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
	    slideSpeed: 300,
	    items:1,
	    pagination: false,
	    singleItem: true
	  });
</script>