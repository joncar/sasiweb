<?php 
	$detail = $row->datos;
	if(!empty($detail)):
?>
	<div id="propiedadMiniMap">
	  <?php if($detail->lowcost): ?>
	    <div class="lowcost">
	      <span></span>
	      LOW COST  
	    </div>
	  <?php endif ?>
	  <div class="property_item heading_space">
	    <div class="property_head text-center" style="padding: 2px 0;">
	      <h3 class="captlize" id="miniMapTipo">
	      	<?= l('tipo_'.mb_strtolower($detail->nbtipo)) ?>
      	  </h3>
	      <p id="miniMapDir">en <?= $detail->ciudad ?> <br/><span class="azul">Ref: <?= $detail->ref ?></span></p>
	    </div>
	    

	    <a class="fotos" href="javascript:showModalInfo('<?= $detail->cod_ofer ?>')"  style="<?= !empty($height)?'height:'.$height.';':''; ?> display:block;"> 
		  <div class="propiedadDetailFoto owl-carousel">
		      
		      <?php 
		        get_instance()->db->limit(5);
		        $fotos = get_instance()->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer)); 
		      ?>

		      <?php if(!empty($detail->foto) && $fotos->num_rows()==0): ?>
		      <div class="item">
		        <div class="image" style="width:100%; height:<?= !empty($height)?'258px':$height; ?>; background:url(<?= $detail->foto ?>); background-size:cover; background-position: center">
		            <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
			          <div class="price clearfix">
			            <span class="tag"><?= $detail->acciones ?></span>
			          </div>
		        </div>
		      </div>
		      <?php endif ?>
		          <?php      
					 foreach($fotos->result() as $n=>$f): if($n>0):
			      ?>
		          <div class="item">
		            <div class="image" style="width:100%; <?= !empty($height)?'height:'.$height.';':''; ?> background:url(<?= $f->foto ?>); background-size:cover; background-position: center">
		              <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
		              <div class="price clearfix">
		                <span class="tag"><?= $detail->acciones ?></span>
		              </div>
		            </div>
		          </div>
		      <?php endif; endforeach ?>
		      
		  </div>
		</a>


	    <div class="proerty_content">
	      <div class="property_meta">
	          <span><i class="icon-select-an-objecto-tool"></i><?= empty($detail->m_uties)?'0':$detail->m_uties ?> m<sup>2</sup></span>
		      <span><i class="icon-bed"></i><?= $detail->total_hab ?> <?= $this->lang->line('hab') ?></span>
		      <span><i class="icon-safety-shower"></i><?= $detail->banyos ?> <?= $this->lang->line('banos') ?></span>
	      </div>

	      <div class="proerty_text"  style="padding: 15px;">
		      <div>
		      	<span class="precio">
		      		<?= moneda($detail->precioinmo); ?>		      	
		      	</span>
		      	 <a href="javascript:showAvisoSiBaja(<?= $detail->id ?>)" class="campanaGriya" style="margin-right:0; margin-top:10px">
		          <i class="fa fa-bell-o"></i> 
		          <span><?= $this->lang->line('avisame') ?></span>
		          <span><?= $this->lang->line('si-baja') ?></span>
		        </a>
		      </div>
		      <p class="preciobajo" style="float:none;">
		      	<span>
		      	 	<?php if(!empty($detail->habajado)): ?>
			            <?= $this->lang->line('ha-bajado') ?> <?= moneda($detail->habajado); ?>
		            <?php endif ?>   
		        </span>		        
	          </p>
	      </div>
	      
	      <div class="favroute clearfix favroute2">      
		      <ul>
		        <li style="position:relative">
		          <a title="<?= $this->lang->line('numero-de-fotos') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>" class="">
		            <i class="icon-camera3" style="font-size:16px;margin-right: 13px !important;display: inline-block;"></i> 
		            <span style="font-size: 13px;color: rgba(227, 39, 24, 0.68);position: absolute;right: 7px;top: -12px;">
		              <?= $this->db->get_where('api_propiedad_fotos',array('api_propiedades_id'=>$detail->id))->num_rows() ?>
		                
		              </span>
		            </a>
	            </li>
		        
		        <li><a title="<?= $this->lang->line('anadir-a-favoritos') ?>" href="javascript:addFav('<?= $detail->cod_ofer ?>')"><i class="icon-like fav<?= $detail->cod_ofer ?> <?= !empty($detail->fav)?'flaticon flaticon-heart active':'' ?>"></i></a></li>
		        <li><a title="<?= $this->lang->line('ocultar-propiedad') ?>" href="javascript:removePropMap('<?= $detail->cod_ofer ?>')"><i class="icon-trash-can3" style="font-size: 23px;"></i></a></li>		        
		        <li>
		          <a title="<?= $this->lang->line('ver-el-street-view') ?>" href="javascript:showStreetView('<?= $detail->latitud ?>','<?= $detail->altitud ?>')">
		            <i class="icon-map3"></i>
		          </a>
		        </li>
		        <li><a title="<?= $this->lang->line('contactar-con-sasi') ?>" href="javascript:contactar('<?= $detail->cod_ofer ?>')" class="share_expender"><i class="icon-envelope"></i></a></li>
		      </ul>
		    </div>
		   
	    </div>
	  </div>
	   <div style="text-align:center;margin-bottom: 15px;"><a href="javascript:showModalInfo('<?= $detail->cod_ofer ?>')" style="text-decoration: underline;"><?= l('ver-detalle') ?></a></div>
	</div>
<?php endif ?>