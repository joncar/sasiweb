<?php $detail->foto = str_replace('[base_url]',base_url(),$detail->foto); ?>
<div class=" propiedadListText media deal_media proerty_content destacadoMain destacadoMain2"  id="propiedad2<?= $detail->cod_ofer ?>">
  <div class="media-left">
    <div class="ovl">

      <?php
        get_instance()->db->limit(10);
        $fotos = get_instance()->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer));
      ?>

      <div class="ovlItem" data-url="<?= $detail->foto ?>" style="background-size:cover;">
        <a href="<?= base_url('propiedad/'.$detail->ref) ?>"></a>
      </div>
      <?php      
        foreach($fotos->result() as $n=>$f): if($n>0):
      ?>
      <div class="ovlItem" data-url="<?= $f->foto ?>"  style="background-size:cover;">
        <a href="<?= base_url('propiedad/'.$detail->ref) ?>"></a>
      </div>
      <?php endif; endforeach ?>
    </div>
  </div>
  
  <div class="media-body">
    <div class="price default_clr">
      <h4><?= l('acciones_'.mb_strtolower($detail->acciones)) ?></h4>      
      <div class="destacadoPrecioContent">
        <h3 class="precio">
          <div><?= moneda($detail->precio); ?></div>
          <div class="preciobajo">
           <?php if(!empty($detail->habajado)): ?>
            <?= l('ha-bajado') ?> <?= moneda($detail->habajado); ?>
           <?php endif ?>
          </div>
        </h3>        
        
      </div>
    </div>
    <span class="azul" style="margin-left: 10px;"> 
      <a href="javascript:showAvisoSiBaja(<?= $detail->id ?>)" style="margin:0"><i class="fa fa-bell-o"></i> <?= l('avisame-si-baja') ?></a>
    </span>
    <div class="proerty_text">
      <a href="<?= base_url('propiedad/'.$detail->ref) ?>">
        <?= l('tipo_'.mb_strtolower($detail->nbtipo)) ?> - <span class="azul">Ref: <?= $detail->ref ?></span>
      <h4 style="margin: 10px 0;">
        <?php if(empty($detail->titulo)): ?>
        <?php 
            $text = l('property_format_descr');
            foreach($detail as $n=>$v){
              $text = str_replace('{{'.$n.'}}',$v,$text);
            }
            echo $text; 
        ?>
        <?php else: $text = $detail->titulo; endif; ?>        
        <?= $text ?>
      </h4>
      <p class="descripcion descripcionPropiedad">
        <?= cortar_palabras($detail->descrip,80) ?>        
      </p>  
      </a>
    </div>
    <div class="dealer clearfix" style="margin-top: 0; padding-left: 30px;">
      <div class="author author2">
        <img src="<?= base_url() ?>img/logo-sasi-mini.jpg" alt="author">
      </div>
      <h4 style="margin-bottom: 4px;">Finques sasi <small></small></h4>
      <a href="tel:<?= $detail->telefono_contacto ?>" style="margin-left: -3px;">
        <i class="icon-phone4"></i><?= $detail->telefono_contacto ?>
      </a>
      <a href="mailto:info@sasi-alicante.com">
        <i class="icon-mail-envelope-closed4"></i><?= $detail->email_contacto ?>
      </a>      
    </div>
    <div class="favroute clearfix">
      <p class="pull-md-left"><?= $detail->zona ?>, <?= $detail->ciudad ?></p>
      <ul class="pull-right">
        <?php if(!empty($detail->tourvirtual)): ?>
          <li>
            <a href="javascript:;" title="Tour Virtual" onclick="window.open('https://ap.apinmo.com/fotosvr/tour.php?cod=<?= $detail->cod_ofer ?>.<?= $detail->sucursal ?>&o=<?= $detail->sucursal ?>','tourvirtual','fullscreen=yes');">
              <img src="<?= base_url('img/360.png') ?>">
            </a>
          </li>
        <?php endif ?>
        <li style="position:relative">
          <a title="<?= l('numero-de-fotos') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>">
            <i class="icon-camera3" style="font-size:16px;margin-right: 13px !important;display: inline-block;"></i> 
            <span style="font-size: 11px;color: rgba(227, 39, 24, 0.68);position: absolute;right: 7px;top: -12px;">
              <?= $this->db->get_where('api_propiedad_fotos',array('api_propiedades_id'=>$detail->id))->num_rows() ?>
                
              </span>
            </a>
          </li>
        <li><a title="<?= l('anadir-a-favoritos') ?>" href="javascript:addFav('<?= $detail->cod_ofer ?>')"><i class="icon-like fav<?= $detail->cod_ofer ?>"></i></a></li>
        <li><a title="<?= l('ocultar-propiedad') ?>"  href="javascript:removeProp('<?= $detail->cod_ofer ?>')"><i class="icon-trash-can3"></i></a></li>
        <li>
          <a title="<?= l('ver-propiedad-en-el-mapa') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>#mapa2">
            <i class="icon-map-pin"></i>
          </a>
        </li>
        <li><a title="<?= l('ver-el-street-view') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>#showStreetView"><i class="icon-map3"></i></a></li>
        <li><a title="<?= l('contactar-con-sasi') ?>" href="javascript:contactar('<?= $detail->cod_ofer ?>')" class="share_expender"><i class="icon-envelope"></i></a></li>
      </ul>
    </div>
    <div class="toggle_share collapse" id="seven">
      <ul>
        <li><a href="javascript:void(0)" class="facebook"><i class="icon-facebook-1"></i> Facebook</a></li>
        <li><a href="javascript:void(0)" class="twitter"><i class="icon-twitter-1"></i> Twitter</a></li>
        <li><a href="javascript:void(0)" class="vimo"><i class="icon-vimeo3"></i> Vimeo</a></li>
      </ul>
    </div>
    <div class="property_meta">
      <span><i class="icon-select-an-objecto-tool"></i><?= empty($detail->m_uties)?'0':$detail->m_uties ?> m<sup>2</sup></span>
      <span><i class="icon-bed"></i><?= $detail->total_hab ?> <?= l('hab') ?></span>
      <span><i class="icon-safety-shower"></i><?= $detail->banyos ?> <?= l('banos') ?></span>      
    </div>
  </div>
</div>