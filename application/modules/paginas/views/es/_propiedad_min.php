<div class="property_item heading_space" id="propiedad<?= $detail->cod_ofer ?>">
  <?php if($detail->lowcost=='1'): ?>
    <div class="lowcost">
      <span></span>
      LOW COST  
    </div>
  <?php endif ?>
  <div class="property_head text-center">
    <a href="<?= base_url('propiedad/'.$detail->ref) ?>"> 
      <h3 class="captlize" style="margin: 0;">
        <?= l('tipo_'.mb_strtolower($detail->nbtipo)) ?>          
      </h3>
      <p style="margin-bottom: 0"> en <?= $detail->ciudad ?><br/><span class="azul">Ref: <?= $detail->ref ?></span></p>
    </a>
  </div>
  
<a href="<?= base_url('propiedad/'.$detail->ref) ?>"> 
  <div class="propiedadDetailFoto owl-carousel">
      
      <?php 
        get_instance()->db->limit(10);
        $fotos = get_instance()->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$detail->cod_ofer)); 
      ?>

      <?php if(!empty($detail->foto) && $fotos->num_rows()==0): ?>
      <div class="item">
        <div class="image" style="width:100%; height:<?= empty($height)?'258px':$height; ?>; background:url(<?= $detail->foto ?>); background-size:cover; background-position: center">
            <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
          <div class="price clearfix">
            <span class="tag"><?= $detail->acciones ?></span>
          </div>
        </div>
      </div>
      <?php endif ?>
      <?php      
        foreach($fotos->result() as $n=>$f): if($n>0):
      ?>
          <div class="item">
            <div class="image" style="width:100%; height:<?= empty($height)?'258px':$height; ?>; background:url(<?= $f->foto ?>); background-size:cover; background-position: center">
              <img src="<?= $detail->foto ?>" alt="latest property" class="img-responsive" style="display: none">
              <div class="price clearfix">
                <span class="tag"><?= $detail->acciones ?></span>
              </div>
            </div>
          </div>
      <?php endif; endforeach ?>
      
  </div>
</a>
  <div class="proerty_content">
    <div class="property_meta">
      <span><i class="icon-select-an-objecto-tool"></i><?= empty($detail->m_uties)?'0':$detail->m_uties ?> m<sup>2</sup></span>
      <span><i class="icon-bed"></i><?= $detail->total_hab ?> <?= l('hab') ?></span>
      <span><i class="icon-safety-shower"></i><?= $detail->banyos ?> <?= l('banos') ?></span>
    </div>
     <div class="proerty_text">
      <a href="<?= base_url('propiedad/'.$detail->ref) ?>" class="">
      <div>
        <span class="precio">
          <?= moneda($detail->precio); ?>           
        </span>
       
        <a href="javascript:showAvisoSiBaja(<?= $detail->id ?>)" class="campanaGriya">
          <i class="fa fa-bell-o"></i> 
          <span><?= $this->lang->line('avisame') ?></span>
          <span><?= $this->lang->line('si-baja') ?></span>
        </a>


      </div>
      <p class="preciobajo" style="float:none;">
        <span>
          <?php if(!empty($detail->habajado)): ?>
            <?= $this->lang->line('ha-bajado') ?> <?= moneda($detail->habajado); ?>
          <?php endif ?>   
        </span>           
        </p>

      <div class="propiedadGriyaText">
          <p class="titulo">        
            <b><?= $detail->titulo ?></b> 
          </p>
          <p class="descripcion">                
            <?= cortar_palabras($detail->descrip,40);  ?>
          </p>
      </div>
      </a>                  
    </div>
    <div class="favroute clearfix favroute2">      
      <ul>
        <li class="dropdown">
          <a href="#" title="Asignar grupo" data-toggle="dropdown">
            <i class="icon-like"></i>
          </a>
          <ul class="dropdown-menu">
            <li><a href="javascript:insertar('propiedades/account/favoritos/update/<?= $detail->id ?>',{favoritos_grupos_id:0},'',function(){$('.filtering_form').trigger('submit')})"><?= l('Sin asignar') ?></a></li>
            <?php foreach(get_instance()->db->get_where('favoritos_grupos',array('user_id'=>get_instance()->user->id))->result() as $f): ?>
              <li><a href="javascript:insertar('propiedades/account/favoritos/update/<?= $detail->id ?>',{favoritos_grupos_id:<?= $f->id ?>},'',function(){$('.filtering_form').trigger('submit')})"><?= $f->nombre ?></a></li>
            <?php endforeach ?>
          </ul>
        </li>
        <li><a title="<?= $this->lang->line('Quitar de favoritos') ?>" href="<?= $detail->delete_url ?>" class="delete-row"><i class="icon-trash-can3" style="font-size: 23px;"></i></a></li>
        <li>
          <a title="<?= $this->lang->line('ver-propiedad-en-el-mapa') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>#mapa2">
            <i class="icon-map-pin"></i>
          </a>
        </li>
        <li>
          <a title="<?= $this->lang->line('ver-el-street-view') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>#showStreetView">
            <i class="icon-map3"></i>
          </a>
        </li>
        <li><a title="<?= $this->lang->line('contactar-con-sasi') ?>" href="javascript:contactar('<?= $detail->cod_ofer ?>')" class="share_expender"><i class="icon-envelope"></i></a></li>
      </ul>
    </div>
    <div class="toggle_share collapse" id="prop<?= $detail->cod_ofer ?>">
      <ul>
        <li><a href="#" class="facebook"><i class="icon-facebook-1"></i> Facebook</a></li>
        <li><a href="#" class="twitter"><i class="icon-twitter-1"></i> Twitter</a></li>
        <li><a href="#" class="vimo"><i class="icon-vimeo3"></i> Vimeo</a></li>
      </ul>
    </div>
  </div>
</div>