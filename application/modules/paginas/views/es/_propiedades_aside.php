<div class="clearfix collapse collapseFilter" id="collapseFilter"">
  <?php $datos = (object)$_GET; ?>
  <div class="form-group col-sm-12 single-query">
    <label class="text-uppercase bottom10 top15"><?= l('provincia') ?></label>
    <div class="intro">
      <input type="hidden" name="search_field[]" value="propiedades_list.provincias_id">
      <select name="search_text[]" class="provincia">
        <option value=""><?= l('todas') ?></option>
        <?php
        $this->db->select('id,UPPER(nombre) as nombre');
        $this->db->order_by('nombre','ASC');
        foreach($this->db->get('provincias')->result() as $p):
        ?>
        <option <?= @$datos->provincias_id==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
        <?php endforeach ?>
        
      </select>
    </div>
  </div>
  <div class="form-group col-sm-12 single-query">
    <label class="text-uppercase bottom10 top15"><?= l('comarca') ?></label>
    <div class="intro">
      <input type="hidden" name="search_field[]" value="propiedades_list.comarcas_id">
      <select name="search_text[]" class="comarca">
        <option value=""><?= l('todas') ?></option>
        <?php
        $this->db->select('comarcas.id,UPPER(comarcas.nombre) as nombre');
        if(!empty($datos->provincias_id)){$this->db->join('provincias','provincias.id = comarcas.provincias_id'); $this->db->where('provincias.id',$datos->provincias_id);}
        $this->db->order_by('comarcas.nombre','ASC');
        if(empty($datos->provincias_id)){
          $this->db->limit(5);
        }
        foreach($this->db->get('comarcas')->result() as $p):
        ?>
        <option <?= @$datos->comarcas_id==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
        <?php endforeach ?>
        
      </select>
    </div>
  </div>
  <div class="form-group col-sm-12 single-query">
    <label class="text-uppercase bottom10 top15"><?= l('ciudad') ?></label>
    <div class="intro">
      <input type="hidden" name="search_field[]" value="propiedades_list.ciudades_id">
      <select name="search_text[]" class="ciudad">
        <option value=""><?= l('todas') ?></option>
        <?php
        $this->db->select('ciudades.id,UPPER(ciudades.ciudad) as nombre');
        if(!empty($datos->comarcas_id)){$this->db->join('comarcas','comarcas.id = ciudades.comarcas_id'); $this->db->where('comarcas.id',$datos->comarcas_id);}
        $this->db->order_by('ciudades.ciudad','ASC');
        if(empty($datos->comarcas_id)){
          $this->db->limit(5);
        }
        foreach($this->db->get('ciudades')->result() as $p):
        ?>
        <option <?= @$datos->ciudades_id==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
        <?php endforeach ?>
        
      </select>
    </div>
  </div>
  
  <div class="form-group col-sm-12 single-query">
    <label class="text-uppercase bottom10 top15"><?= l('zona') ?></label>
    <div class="intro">
      <input type="hidden" name="search_field[]" value="propiedades_list.zonas_id">
      <select name="search_text[]" class="zona">
        
        <option value=""><?= l('todas') ?></option>
        <?php
        $this->db->select('zonas.id,UPPER(zonas.nombre) as nombre');
        if(!empty($datos->ciudades_id)){
        $this->db->where('ciudades_id',$datos->ciudades_id);
        }
        $this->db->order_by('zonas.nombre','ASC');
        if(empty($datos->ciudades_id)){
          $this->db->limit(5);
        }
        foreach($this->db->get('zonas')->result() as $p):
        ?>
        <option <?= @$datos->zonas_id==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
        <?php endforeach ?>
        
      </select>
    </div>
  </div>
  
  <div class="form-group">
    <div class="row single-query" style="margin-left: 0; margin-right: 0">
      <div class="col-xs-12">
        <label class="text-uppercase bottom10 top15"><?= l('precio') ?></label>
      </div>
      <div class="col-xs-12 col-md-12">
        <div class="intro">
          <select name="precio_min">
            <option selected="" value=""><?= l('minimo') ?></option>
            <?php for($i=60000;$i<240000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 30000; endfor; ?>
            <?php for($i=240000;$i<=420000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 60000; endfor; ?>
            <?php for($i=500000;$i<=1000000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 100000; endfor; ?>
            <?php for($i=1500000;$i<=2000000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 500000; endfor; ?>
            <option value="3000000" <?= (@$_GET['precio_min']=='3000000')?'selected="selected"':'' ?>>3.000.000€</option>
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-md-12" style="margin-top:4px;">
        <div class="intro">
          <select name="precio_max">
            <option selected="" value=""><?= l('maximo') ?></option>
            <?php for($i=60000;$i<240000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 30000; endfor; ?>
            <?php for($i=240000;$i<=420000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 60000; endfor; ?>
            <?php for($i=500000;$i<=1000000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 100000; endfor; ?>
            <?php for($i=1500000;$i<=2000000;): ?>
            <option value="<?= $i ?>" <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
            <?php $i+= 500000; endfor; ?>
            <option value="3000000" <?= (@$_GET['precio_max']=='3000000')?'selected="selected"':'' ?>>3.000.000€</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group col-sm-12 single-query">
    <label class="text-uppercase bottom10 top15"><?= l('superficie') ?></label>
    <div class="intro">
      <select name="m_uties">
        <option selected="" value=""><?= l('todas') ?></option>
        <?php for($i=50;$i<100;): ?>
        <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
        <?php $i+= 10; endfor; ?>
        <?php for($i=100;$i<200;): ?>
        <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
        <?php $i+= 25; endfor; ?>
        <?php for($i=200;$i<300;): ?>
        <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
        <?php $i+= 50; endfor; ?>
        <?php for($i=300;$i<=500;): ?>
        <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
        <?php $i+= 100; endfor; ?>
      </select>
    </div>
  </div>
  
  <div class="form-group col-sm-12 single-query">
    <label class="text-uppercase bottom10 top15"><?= l('habitaciones') ?></label>
    <div class="intro">
      <select name="total_hab">
        <option value=""><?= l('todas') ?></option>
        <option value="0">+0</option>
        <option value="1">+1</option>
        <option value="2">+2</option>
        <option value="3">+3</option>
        <option value="4">+4</option>
        <option value="5">+5</option>
      </select>
    </div>
  </div>
  <div class="form-group col-sm-12 checkboxs">
    <label class="text-uppercase bottom10 top15">
      <?= l('tipos-de-inmuebles') ?>      
    </label>
    <div>
      <?php
      $this->db->group_by('codigo_tipo');
      foreach($this->db->get_where('tipos')->result() as $t): ?>
      <label for="tipo<?= $t->codigo_tipo ?>">
        <input type="checkbox" <?= @$t->codigo_tipo==@$_GET['codigo_tipo'] || @in_array($t->codigo_tipo,$_GET['codigo_tipo'])?'checked':'' ?> id="tipo<?= $t->codigo_tipo ?>" name="codigo_tipo[]" value="<?= $t->codigo_tipo ?>"> <?= l('tipo_'.mb_strtolower($t->label)) ?>
      </label>
      <?php endforeach ?>
    </div>
  </div>
  
  <div class="form-group col-sm-12 checkboxs">
    <label class="text-uppercase bottom10 top15"><?= l('mas-caracteristicas') ?></label>
    <div>
      <?php foreach($this->db->get_where('propiedades_extras',array('mostrar_en_filtros'=>1))->result() as $t): ?>
      <label for="caract1">
        <?php $cod = str_replace('_','-',$t->codigo); ?>
        <input type="checkbox" name="propiedades_extras_id[]" id="caract<?= $t->codigo ?>" value="<?= $t->codigo ?>"  <?= @$cod==@$_GET['propiedades_extras_id'] || @in_array($cod,$_GET['propiedades_extras_id'])?'checked="checked"':'' ?> >
        <?= l('extra_'.$t->codigo) ?>
      </label>
      <?php endforeach ?>
    </div>
  </div>
  <input type="hidden" name="search_field[]" value="propiedades_list.areas_id">
  <input type="hidden" id="field-areas_id" name="search_text[]" value="<?= @$_GET['areas_id'] ?>">
  <input type="hidden" name="search_field[]" value="propiedades_list.distritos_id">
  <input type="hidden" id="field-distritos_id" name="search_text[]" value="<?= @$_GET['distritos_id'] ?>">
  <input type="hidden" name="search_field[]" value="codaccion">
  <input type="hidden" name="search_text[]" value="<?= @$_GET['acciones'] ?>">
  <input type="hidden" name="search_field[]" value="lowcost">
  <input type="hidden" name="search_text[]" value="<?= @$_GET['lowcost'] ?>">
  
  <input type="hidden" name="search_field[]" value="ref">
  <input type="hidden" name="search_text[]" value="<?= @$_GET['referencia'] ?>">
  <input type="hidden" name="search_field[]" value="nbconservacion">
  <input type="hidden" name="search_text[]" value="<?= !empty($_GET['obranueva'])?'Obra Nueva':'' ?>">
  
  <div class="col-sm-12 form-group text-center filtrarDiv">
    <button class="btn-blue btn-block filtrar" type="submit"><?= l('filtrar') ?></button>
    <a href="javascript:limpiar()" style="margin-top:10px; display: inline-block; text-decoration: underline;"><?= l('limpiar-filtros') ?></a>
  </div>
</div>