<div class="avisoRojo">
	<a href="javascript:closeAviso()" class="closeAviso">
		<i class="fa fa-times"></i>
	</a>
	<div class="row">
		<div class="col-xs-12 col-md-2">
			<i class="fa fa-whatsapp fa-3x"></i>
		</div>
		<div class="col-xs-12 col-md-10">
			<h4><?= l('te-ayudamos-a-encontrar-vivienda') ?></h4>
			
		</div>
		<div class="col-xs-12" style="text-align: center">
			<?php 
				$numero =  '34640171615'; 
				if(!empty($_GET['provincias_id'])){
					switch($_GET['provincias_id']){
						case '7':$numero = '34601712791'; break;
						case '8':$numero = '34601712791'; break;
						case '10':$numero = '34601712791'; break;
						case '13':$numero = '34601712791'; break;
						default:$numero = '34640171615'; break;
					}
				}
			?>
			<a class="visible-xs visible-sm btn btn-primary" href="https://wa.me/<?= $numero ?>/?text=Hola+deseo+contactarlos+por+informaci%C3%B3n+sobre+"><?= l('Dinos qué buscas') ?></a>
			<a class="hidden-xs hidden-sm btn btn-primary" href="https://api.whatsapp.com/send?phone=<?= $numero ?>/&text=Hola+deseo+contactarlos+por+informaci%C3%B3n+sobre+" target="_new"><?= l('Dinos qué buscas') ?></a>
			 
		</div>
	</div>
</div>
<script>
	function closeAviso(){
		$(".avisoRojo").hide();
	}
</script>