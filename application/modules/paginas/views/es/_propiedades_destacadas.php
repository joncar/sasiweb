<div class="row">
  <?php 
    
    $this->db->order_by('RAND()');
    $this->db->where('destacado !=','0');
    
    $prop = $this->db->get_where('propiedades_list');
    foreach($prop->result() as $n=>$detail): if($n<4): 
  ?>               
  	<div class="col-xs-6 col-md-3">
  		<div class="agent_wrap property_item heading_space">
        <div class="image" style="height:100%; height:174px; background:url(<?= base_url('img/default.jpg') ?>); background-size:cover; background-position: center;" data-url="<?= $detail->foto ?>">
            <a href="[base_url]propiedad/<?= $detail->cod_ofer ?>">
              <img src="<?= base_url('img/default.jpg') ?>" data-url="<?= $detail->foto ?>" alt="latest property" class="img-responsive">
            </a>
            <div class="price clearfix" style="margin-top: 50px;"> 
              <span class="tag" style="width:100%;">
                <small><?= empty($detail->m_uties)?'0':$detail->m_uties ?> m<sup>2</sup> - <?= $detail->total_hab ?> habitaciones - <?= $detail->banyos ?> baños</small>
                <br>
                <?php if($detail->codaccion==2): ?>
                  <?= moneda($detail->precioalq); ?>/<?= $detail->tipomensual ?>                    
                <?php else: ?>
                  <?= moneda($detail->precio); ?>                    
                <?php endif ?> 
              </span>
            </div>
            <span class="tag_t"><?= $detail->acciones ?></span> 
            <!--<span class="tag_l">Featured</span>-->

            <div class="img-info" style="width:100%; color:white;">
              <a href="[base_url]propiedad/<?= $detail->cod_ofer ?>">
          			<span><?= $detail->nbtipo ?></span>
      					<h4><?= $detail->precio==0?'Consultar':moneda($detail->precio) ?></h4>
      					<span><?= $detail->ciudad ?></span><br/>
      					<span><?= $detail->zona ?></span>  			     
              </a>
            </div>          
        </div>
      </div>
    </div>
  <?php endif; endforeach ?>
</div>