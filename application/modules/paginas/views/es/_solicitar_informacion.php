              <form class="callus noloader" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#contactoPropiedadResult');">
                  <div class="row">
                    <div class="col-xs-12 col-md-4">
                      <div class="form-group">
                        <input type="text" name="nombre" class="form-control" placeholder="<?= l('nombre') ?>">
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                      <div class="form-group">
                        <input type="tel" name="extras[telefono]" class="form-control" placeholder="<?= l('telefono') ?>">
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                      <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12 col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" name="extras[comentario]" placeholder="<?= l('comentario') ?>"></textarea>
                      </div>
                     </div>
                      <div class="col-xs-12 col-md-12 bottom20">
                          Indique fecha/hora que desea hacer la visita:
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-6 p0">                        
                        <div class="col-xs-12 col-sm-6 col-md-6 pl-0 px-0">
                          <div class="form-group">
                              <label for="fecha"><?= l('fecha') ?></label>
                              <div class="input-group">                              
                                <input type="text" class="form-control fecha" id="fecha" name="extras[fecha_visita]" placeholder="<?= l('fecha') ?> d/m/a">                              
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                              </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 pr-0 px-0">
                          <div class="form-group">
                            <label for="hora"><?= l('hora') ?></label>
                            <div class="input-group">                            
                              <input type="text" class="form-control hora" id="hora" name="extras[hora_visita]" placeholder="<?= l('hora') ?> 00:00">                              
                              <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <input type="hidden" name="extras[ref]" value="<?= $propiedad->ref ?>">
                      
                      <div class="col-xs-12 col-sm-6 col-md-6">
                          <div class="form-group" style="margin-bottom: 0; margin-top:13px">
                            <input type="checkbox" name="extras[recordar_datos]" value="Si" checked=""> <?= l('recordar-mis-datos-en-este-equipo') ?>
                          </div>
                          <div class="form-group" style="margin-bottom: 0">
                            <input type="checkbox" name="extras[recibir_alertas_similares]" value="Si" checked=""> <?= l('recibir-alertas-de-inmuebles-similares') ?>
                          </div>
                          <div class="form-group">
                            <input type="checkbox" name="politicas" value="1"> <?= l('acepto-el') ?> <a href="<?= base_url() ?>aviso-legal-igualada.html"><?= l('aviso-legal') ?></a> y <a href="<?= base_url() ?>politicas-igualada.html"><?= l('politicas-de-privacidad') ?></a>
                          </div>                          
                      </div>

                      <input type="hidden" name="titulo" value="Web SASI, información sobre el inmueble: <?= $propiedad->ref ?>">
                      <input type="hidden" name="to" value="<?= $propiedad->email_contacto ?>">                      
                      <input type="hidden" name="referencia" value="<?= $propiedad->ref ?>">
                      <div class="col-xs-12">
                        <div id="contactoPropiedadResult"></div>
                      </div>
                      <div class="col-xs-12 col-md-12 top30">
                        <div class="form-group">
                            <input type="submit" class="btn-blue uppercase border_radius" value="<?= l('enviar') ?>">
                          </div>
                      </div>
                      
                    </div>
                 
              </form>