<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 text-center">
            <h1 class="p-white text-uppercase">Aviso legal</h1>            
            <ol class="breadcrumb text-center">
               <li><a href="#">Inicio</a></li>               
            </ol>
         </div>
      </div>
   </div>
</section>
<!-- Page Banner End --> 
<!--Contact-->
<section>
	<div class="container">
	  	<div class="texto_contenido padding">
<div role="main" class="mainbar entry-content" id="content">


   <table id="table1" width="100%" cellspacing="1" border="0">
   <tbody>
      <tr>
         <td align="left"><strong>Información general del titular del sitio web:</strong><br>
         &nbsp;<br>
         En cumplimiento de lo dispuesto en el artículo 9 de la Ley 34/2002, de 11 de julio, de servicios de la sociedad de la información y de comercio electrónico informamos a nuestros usuarios que el titular del sitio web ubicado en el dominio <a href="http://www.sasi-alicante.com/"> www.sasi-alicante.com</a> &nbsp;es SASI REAL ESTATE S.L.<strong>&nbsp;</strong>con domicilio social en Av. Eusebio Sempere, 22&nbsp; &nbsp; 03003 ALICANTE (SPAIN),&nbsp;&nbsp; ESB42510602.<br>
         &nbsp;<br>
         La exploración del sitio web de SASI REAL ESTATE S.L., ubicado en el dominio <a href="http://www.etiqueta-adhesiva.com/"> </a><a href="http://www.sasi-alicante.com/">www.sasi-alicante.com</a>&nbsp;&nbsp;y de cualquiera de las páginas a las que a través del mismo se pueda acceder, implica que el usuario necesariamente ha leído, entiende y acepta el Aviso Legal y la política de privacidad de este sitio web. En caso de que no esté de acuerdo con los términos y condiciones descritos en dichos textos legales, el usuario debe abstenerse de utilizar este sitio web.<br>
         &nbsp;<br>
         <strong>Productos y servicios:</strong><br>
         Mediante este sitio web <a href="http://www.etiqueta-adhesiva.com/"> </a><a href="http://www.sasi-alicante.com/">www.sasi-alicante.com</a>, pone a disposición de los usuarios información gratuita respecto de los productos y servicios prestados por la sociedad. La información ofrecida no constituye una oferta comercial, siendo preciso establecer relación individual y directa con la gestora para conocer las características de sus inversiones y los requisitos de comercialización. En todo caso, la contratación de los productos y servicios de&nbsp;SASI REAL ESTATE S.L. se regirá por sus Condiciones Generales y Particulares de Contratación.<br>
         &nbsp;<br>
         <strong>Propiedad intelectual e industrial:</strong><br>
         Todos los diseños, marcas, logotipos, nombres, imágenes, gráficos, iconos y demás contenido del sitio web son titularidad de&nbsp;SASI REAL ESTATE S.L. , gozando en consecuencia de la protección propia prevista por la normativa aplicable sobre la propiedad industrial e intelectual. En ningún caso el acceso al sitio web implica cesión por parte de&nbsp;SASI REAL ESTATE S.L. de tales derechos a los usuarios. Los usuarios del sitio web pueden hacer uso privado del mismo y de su contenido. En ningún caso podrán hacer uso comercial de los mismos, ni alterarlos en cualquier manera, reproducirlos más allá de su uso privado, distribuirlos o comunicarlos públicamente. Así mismo, queda estrictamente prohibida la utilización de los contenidos del sitio web con cualquier propósito o de cualquier manera distinta de la permitida por&nbsp;SASI REAL ESTATE S.L. en el presente Aviso legal. Los contenidos, textos, logotipos, diseños, imágenes, fotografías, y, en general, cualquier creación intelectual existente en este sitio web, así como el propio sitio en su conjunto como obra artística multimedia, han sido debidamente registrados y, en consecuencia, están protegidos como derechos de autor por la legislación en materia de propiedad intelectual. En caso de que los usuarios deseen utilizar las marcas, logotipos, nombres o cualquier otro signo distintivo disponibles en el sitio web de&nbsp;SASI REAL ESTATE S.L. &nbsp;pueden solicitar la autorización necesaria poniéndose en contacto con la sociedad, dirigiéndose a la misma por los medios de contacto indicados en el presente Aviso legal.<br>
         El usuario de este sitio web se compromete a respetar los derechos mencionados y a evitar cualquier actuación que pudiera perjudicarlos. Cualquier copia o uso no autorizado del diseño o contenidos del sitio web que difiera del expresamente permitido por&nbsp;SASI REAL ESTATE S.L. , en su carácter de titular de los derechos de propiedad intelectual sobre el mismo, afectará a sus derechos y, en consecuencia, legitimará a&nbsp;SASI REAL ESTATE S.L. para el ejercicio de cuantas acciones, tanto civiles como penales, amparen sus legítimos derechos de propiedad intelectual e industrial.<br>
         &nbsp;<br>
         <strong>Acceso al sitio web:</strong><br>
         El acceso a este sitio web es libre y gratuito.<br>
         &nbsp;<br>
         <strong>Hipervínculos:</strong><br>
         Este sitio web contiene hipervínculos con otros sitios web que no son editados, controlados, mantenidos o supervisados por&nbsp;SASI REAL ESTATE S.L. &nbsp;no siendo responsable ésta del contenido de dichos sitios web. El contenido de los mismos es responsabilidad de sus respectivos titulares y&nbsp;SASI REAL ESTATE S.L. no garantiza ni aprueba dichos contenidos. La función de los links que aparecen en esta página es exclusivamente la de informar al usuario sobre la existencia de otras fuentes de información sobre la materia correspondiente en internet, donde podrá ampliar los datos ofrecidos en este sitio web.&nbsp;SASI REAL ESTATE S.L. no será en ningún caso responsable del resultado obtenido a través de dichos hiperenlaces. Aquellos usuarios que deseen establecer hipervínculos al presente sitio web deberán abstenerse de realizar manifestaciones falsas, inexactas o incorrectas sobre el mismo o su contenido. En ningún caso se declarará ni se dará a entender que&nbsp;SASI REAL ESTATE S.L. autoriza el hipervínculo, o que supervisa, aprueba o asume de cualquier forma los contenidos o servicios ofrecidos o puestos a disposición en la página web en la que se establezca el hiperenlace al sitio web. El establecimiento del hipervínculo no implica en ningún caso la existencia de relación alguna entre&nbsp;SASI REAL ESTATE S.L. y el titular de la página web en la que se establezca el mismo. Queda prohibido el establecimiento de hipervínculos al sitio web en páginas web que incluyan información o contenidos ilícitos, inmorales o contrarios a las buenas costumbres, al orden público, a los usos aceptados en internet o que de cualquier forma contravengan derechos de terceros. Los hipervínculos que, respetando las exigencias anteriormente expuestas, se establezcan al sitio web desde otras páginas web permitirán el acceso al mismo, pero no reproducirán su contenido en forma alguna. En el caso de que&nbsp;SASI REAL ESTATE S.L. , tuviera conocimiento efectivo de que la actividad o la información a la que remiten estos enlaces es ilícita, constitutiva de delito o que puede lesionar bienes o derechos de terceros susceptibles de indemnización, actuará con la diligencia necesaria para suprimir o inutilizar el enlace correspondiente a la mayor brevedad posible.<br>
         &nbsp;<br>
         <strong>Intercambio o difusión de información. </strong><br>
         SASI REAL ESTATE S.L.&nbsp;declina toda responsabilidad derivada del intercambio de información entre usuarios a través de su página web. Especialmente no se hace responsable del uso que los menores puedan hacer de ella en caso de que los contenidos a que tengan acceso puedan herir su sensibilidad.<br>
         Desde&nbsp;SASI REAL ESTATE S.L. no se garantiza la ausencia de virus y otros elementos que puedan causar daños en los sistemas informáticos, los documentos electrónicos o los ficheros de usuario de esta página web o de páginas web de terceros, y no se responsabiliza de los daños y perjuicios que se puedan llegar a producir por estos motivos.&nbsp;SASI REAL ESTATE S.L. no responderá por cualquier daño o perjuicio que se pudiera dar a causa de una circunstancia de fuerza mayor, como: error en las líneas de comunicaciones, defectos en el Hardware y Software de los usuarios, fallos en la red de Internet (de conexión, en las páginas enlazadas).<br>
         &nbsp;<br>
         <strong>Actualizaciones:</strong><br>
         SASI REAL ESTATE S.L.&nbsp;no garantiza la total actualización, exactitud y/o disponibilidad en todo momento de los contenidos de su sitio web, si bien hace todo lo posible para que así sea. A pesar que&nbsp;SASI REAL ESTATE S.L. desea prestar a sus usuarios a través del sitio web un servicio continuado, el mismo podría ser interrumpido por circunstancias de diversa índole. En tal caso,&nbsp;SASI REAL ESTATE S.L. intentaría minimizar las consecuencias de dicha interrupción para que sus usuarios se vean afectados en la menor medida posible, siempre que ello fuera posible, no responsabilizándose en ningún caso de los perjuicios que pueda suponer para el usuario la interrupción del servicio de acceso al presente sitio web.<br>
         &nbsp;<br>
         <strong>Tratamiento de datos del usuario:</strong><br>
         SASI REAL ESTATE S.L.&nbsp;aplica la normativa vigente en materia de protección de datos de carácter personal regulada por el Reglamento Europeo 2016/679 y demás leyes complementarias. El usuario puede consultar su contenido a través del enlace <strong><em>Política de Privacidad&nbsp; </em> </strong>disponible en nuestra página web.<br>
         El usuario podrá ejercitar sus derechos de acceso, rectificación, cancelación y oposición y demás reconocidos por Ley, dirigiéndose por escrito a&nbsp;SASI REAL ESTATE S.L. &nbsp;Responsable del Fichero, ubicado en la dirección facilitada en el encabezamiento o a través del email: &nbsp;<a href="mailto:info@sasi-alicante.com?subject=Modificaci%C3%B3n-Cancelaci%C3%B3n-%20GDPR">info@sasi-alicante.com</a><br>
         &nbsp;<br>
         <strong>Legislación y jurisdicción aplicable:</strong><br>
         Con carácter general, las relaciones con los usuarios, derivadas de la prestación de servicios contenidos en esta página web, están sometidas a la legislación y jurisdicción española.<br>
         &nbsp;<br>
         &nbsp;</td>
         <td align="center">&nbsp;</td>
      </tr>
      <tr>
         <td colspan="3" align="center">&nbsp;</td>
      </tr>
   </tbody>
</table>
   
   

   </div>      
	</div>
</section>
<!-- Contact End -->
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>