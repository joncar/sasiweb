<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>


<!-- Page Banner Start-->
<section class="page-banner padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 text-center">
            <h1 class="text-uppercase">Blog SASI</h1>
            <p>Las últimas noticias del mundo inmobiliario a tus servicio</p>
            <ol class="breadcrumb text-center">
               <li><a href="#.">Home</a></li>
               <li><a href="#.">Propiedades</a></li>
               <li class="active">Blog</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<!-- Page Banner End -->



<!-- News Start -->
<section id="news-section-1" class="property-details padding_top">
   <div class="container property-details">
      <div class="row">
         <div class="col-md-8">
            <div class="row">
               <div class="news-1-box clearfix">
                  <div class="col-md-5 col-sm-5 col-xs-12">
                     <div class="image-2">
                        <a href="blog_detalles.html"><img src="<?= base_url() ?>theme/theme/images/blog-1.jpg" alt="image" class="img-responsive"/></a>
                     </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 padding-left-25">
                     <h3><a href="blog_detalles.html">¿Alquilar es tirar el dinero? </a></h3>
                     <div class="news-details padding-b-10 margin-t-5">
                        <span><i class="icon-icons230"></i> Finques Sasi</span>
                        <span><i class="icon-icons228"></i> Agosto 22, 2018</span>
                     </div>
                     <p class="p-font-15">Es una creencia arraigada en España que, con la subida de los alquileres, ha vuelto a la actualidad. "Alquilar es tirar el dinero", se oye habitualmente. No en vano, según los cálculos de la OCU, cerca del 80% de los españoles prefiere comprar una vivienda que alquilarla. 

Como recoge la Radiografía del mercado de la vivienda 2017-2018 elaborada por Fotocasa, durante el último año ha ganado fuerza opción de la compra como "buena inversión" o la idea de que "alquilar es tirar el dinero", así como los convencidos de que es "la mejor herencia" que se puede dejar a un hijo....</p>
                     <div class="pro-3-link padding-t-20">
                        <a class="btn-more" href="blog_detalles.html">
                        <i>
                        <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowl.png">
                        </i>
                        <span>Leer más</span>
                        <i>
                        <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowr.png">
                        </i>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="news-1-box clearfix">
                  <div class="col-md-5 col-sm-5 col-xs-12">
                     <div class="image-2">
                       <a href="blog_detalles.html"> <img src="<?= base_url() ?>theme/theme/images/blog-2.jpg" alt="image"  class="img-responsive"/></a>
                     </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 padding-left-25">
                     <h3><a href="blog_detalles.html">Euskadi podrá obligar a alquilar viviendas</a></h3>
                     <div class="news-details padding-b-10 margin-t-5">
                        <span><i class="icon-icons230"></i> Finques Sasi</span>
                        <span><i class="icon-icons228"></i> Septiembre 22, 2018</span>
                     </div>
                     <p class="p-font-15">Euskadi ya tiene vía libre para gravar e intervenir en las viviendas vacías. El titular, dicho así, sin las matizaciones que hay que añadirle, invita a los propietarios a inquietarse y a pensar cómo movilizar ese piso que tienen cerrado a cal y canto desde hace años. Y en realidad ese es el objetivo recogido en la Ley vasca de Vivienda, que ahora ha recibido el visto bueno del Tribunal Constitucional para empezar a ponerlo en práctica. La idea defendida por el Departamento vasco....</p>
                     <div class="pro-3-link padding-t-20">
                        <a class="btn-more" href="blog_detalles.html">
                        <i><img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowl.png"></i><span>Leer más</span><i> <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowr.png"></i>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="news-1-box clearfix">
                  <div class="col-md-5 col-sm-5 col-xs-12">
                     <div id="agent-2-slider" class="owl-carousel">
                        <div class="item">
                           <div class="property_item heading_space">
                              <div class="image"><a href="blog_detalles.html"><img src="<?= base_url() ?>theme/theme/images/blog-3.jpg" alt="listin" class="img-responsive"></a></div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="property_item heading_space">
                              <div class="image"><a href="blog_detalles.html"><img src="<?= base_url() ?>theme/theme/images/blog-4.jpg" alt="listin" class="img-responsive"></a>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="property_item heading_space">
                              <div class="image"><a href="blog_detalles.html"><img src="<?= base_url() ?>theme/theme/images/blog-5.jpg" alt="listin" class="img-responsive"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 padding-left-25">
                     <h3><a href="blog_detalles.html">¿Es un buen momento para comprar o vender una vivienda?</a></h3>
                     <div class="news-details padding-b-10 margin-t-5">
                        <span><i class="icon-icons230"></i> Finques Sasi</span>
                        <span><i class="icon-icons228"></i> Septiembre 25, 2018</span>
                     </div>
                     <p class="p-font-15">El precio de la vivienda seguirá subiendo en lo que resta de 2018 y en 2019, pero a un ritmo menor por la ralentización económica que está aconteciendo en España. Es la coincidencia de varios expertos del sector inmobiliario, que prevén que el precio pueda tocar techo a finales de 2019 en ciudades como Barcelona o Madrid. Se tocará techo en 2019 Según un estudio inmobiliario publicado por la Universitat de Barcelona (UB) y la consultora Forcadell durante 2018 el precio de la ...</p>
                     <div class="pro-3-link padding-t-20">
                        <a class="btn-more" href="blog_detalles.html"><i><img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowl.png"></i><span>Leer más</span><i><img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowr.png"></i>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="news-1-box clearfix">
                  <div class="col-md-5 col-sm-5 col-xs-12">
                     <div class="image-2">
                        <a href="blog_detalles.html"><img src="<?= base_url() ?>theme/theme/images/blog-3.jpg" alt="image" class="img-responsive"/></a>
                     </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 padding-left-25">
                     <h3><a href="blog_detalles.html">Euskadi podrá obligar a alquilar viviendas</a></h3>
                     <div class="news-details padding-b-10 margin-t-5">
                        <span><i class="icon-icons230"></i> Finques Sasi</span>
                        <span><i class="icon-icons228"></i> Septiembre 22, 2018</span>
                     </div>
                     <p class="p-font-15">Euskadi ya tiene vía libre para gravar e intervenir en las viviendas vacías. El titular, dicho así, sin las matizaciones que hay que añadirle, invita a los propietarios a inquietarse y a pensar cómo movilizar ese piso que tienen cerrado a cal y canto desde hace años. Y en realidad ese es el objetivo recogido en la Ley vasca de Vivienda, que ahora ha recibido el visto bueno del Tribunal Constitucional para empezar a ponerlo en práctica. La idea defendida por el Departamento vasco....</p>
                     <div class="pro-3-link padding-t-20">
                        <a class="btn-more" href="blog_detalles.html">
                        <i><img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowl.png"></i><span>Leer más</span><i> <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowr.png"></i>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="news-1-box clearfix">
                  <div class="col-md-5 col-sm-5 col-xs-12">
                     <div class="image-2">
                        <a href="blog_detalles.html"><img src="<?= base_url() ?>theme/theme/images/blog-3.jpg" alt="image" class="img-responsive"/></a>
                     </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 padding-left-25">
                     <h3><a href="blog_detalles.html">¿Alquilar es tirar el dinero? </a></h3>
                     <div class="news-details padding-b-10 margin-t-5">
                        <span><i class="icon-icons230"></i> Finques Sasi</span>
                        <span><i class="icon-icons228"></i> Agosto 22, 2018</span>
                     </div>
                     <p class="p-font-15">Es una creencia arraigada en España que, con la subida de los alquileres, ha vuelto a la actualidad. "Alquilar es tirar el dinero", se oye habitualmente. No en vano, según los cálculos de la OCU, cerca del 80% de los españoles prefiere comprar una vivienda que alquilarla. 

Como recoge la Radiografía del mercado de la vivienda 2017-2018 elaborada por Fotocasa, durante el último año ha ganado fuerza opción de la compra como "buena inversión" o la idea de que "alquilar es tirar el dinero", así como los convencidos de que es "la mejor herencia" que se puede dejar a un hijo....</p>
                     <div class="pro-3-link padding-t-20">
                        <a class="btn-more" href="blog_detalles.html">
                        <i>
                        <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowl.png">
                        </i>
                        <span>Leer más</span>
                        <i>
                        <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowr.png">
                        </i>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="news-1-box clearfix">
                  <div class="col-md-5 col-sm-5 col-xs-12">
                     <div class="image-2">
                        <a href="blog_detalles.html"><img src="<?= base_url() ?>theme/theme/images/blog-1.jpg" alt="image" class="img-responsive"/></a>
                     </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 padding-left-25">
                     <h3><a href="blog_detalles.html">¿Alquilar es tirar el dinero? </a></h3>
                     <div class="news-details padding-b-10 margin-t-5">
                        <span><i class="icon-icons230"></i> Finques Sasi</span>
                        <span><i class="icon-icons228"></i> Agosto 22, 2018</span>
                     </div>
                     <p class="p-font-15">Es una creencia arraigada en España que, con la subida de los alquileres, ha vuelto a la actualidad. "Alquilar es tirar el dinero", se oye habitualmente. No en vano, según los cálculos de la OCU, cerca del 80% de los españoles prefiere comprar una vivienda que alquilarla. 

Como recoge la Radiografía del mercado de la vivienda 2017-2018 elaborada por Fotocasa, durante el último año ha ganado fuerza opción de la compra como "buena inversión" o la idea de que "alquilar es tirar el dinero", así como los convencidos de que es "la mejor herencia" que se puede dejar a un hijo....</p>
                     <div class="pro-3-link padding-t-20">
                        <a class="btn-more" href="blog_detalles.html">
                        <i>
                        <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowl.png">
                        </i>
                        <span>Leer más</span>
                        <i>
                        <img alt="arrow" src="<?= base_url() ?>theme/theme/images/arrowr.png">
                        </i>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row margin_bottom">
               <div class="col-md-12">
                  <ul class="pager">
                     <li><a href="#.">1</a></li>
                     <li class="active"><a href="#.">2</a></li>
                     <li><a href="#.">3</a></li>
                  </ul>
               </div>
            </div>
         </div>
         <aside class="col-md-4 col-xs-12">
            <div class="row">
               <div class="col-md-12">
                  <form class="form-search bottom40" method="get" id="news-search" action="/">
                     <div class="input-append">
                        <input type="text" class="input-medium search-query" placeholder="Search Here" value="">
                        <button type="submit" class="add-on"><i class="icon-icons185"></i></button>
                     </div>
                  </form>
               </div>
               <div class="col-md-12">
                  <h3 class="bottom20">Categories</h3>
                  <ul class="pro-list bottom20">
                     <li>
                        Air Conditioning
                     </li>
                     <li>
                        Barbeque
                     </li>
                     <li>
                        Dryer
                     </li>
                     <li>
                        Laundry
                     </li>
                     <li>
                        Refrigerator
                     </li>
                     <li>
                        Swimming Pool
                     </li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <h3 class="bottom40 margin40">Featured Properties</h3>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-sm-4 col-xs-12 p-image image bottom20">
                  <img src="<?= base_url() ?>theme/theme/images/f-p-1.png" alt="image"/>
               </div>
               <div class="col-md-8 col-sm-8 col-xs-12">
                  <div class="feature-p-text">
                     <h4><a href="#.">Get the best property in Town by our agent</a></h4>
                     <span>by Martin Moore</span>
                  </div>
               </div>
            </div>
            <div class="row padding-b-30 padding-t-30">
               <div class="col-md-4 col-sm-4 col-xs-12 p-image image bottom20">
                  <img src="<?= base_url() ?>theme/theme/images/f-p-1.png" alt="image"/>
               </div>
               <div class="col-md-8 col-sm-8 col-xs-12">
                  <div class="feature-p-text">
                     <h4><a href="#.">Get the best property in Town by our agent</a></h4>
                     <span>by Martin Moore</span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-sm-4 col-xs-12 p-image image">
                  <img src="<?= base_url() ?>theme/theme/images/f-p-1.png" alt="image"/>
               </div>
               <div class="col-md-8 col-sm-8 col-xs-12">
                  <div class="feature-p-text">
                     <h4><a href="#.">Get the best property in Town by our agent</a></h4>
                     <span>by Martin Moore</span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <h3 class="margin40 bottom20">Featured Properties</h3>
               </div>
               <div class="col-md-12 padding-t-30">
                  <div id="agent-2-slider" class="owl-carousel">
                     <div class="item">
                        <div class="property_item heading_space">
                           <div class="image">
                              <a href="#."><img src="<?= base_url() ?>theme/theme/images/slider-list2.jpg" alt="listin" class="img-responsive"></a>
                              <div class="feature"><span class="tag-2">For Rent</span></div>
                              <div class="price clearfix"><span class="tag pull-right">$8,600 Per Month - <small>Family Home</small></span></div>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="property_item heading_space">
                           <div class="image">
                              <a href="#."><img src="<?= base_url() ?>theme/theme/images/slider-list2.jpg" alt="listin" class="img-responsive"></a>
                              <div class="feature"><span class="tag-2">For Rent</span></div>
                              <div class="price clearfix"><span class="tag pull-right">$8,600 Per Month - <small>Family Home</small></span></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </aside>
      </div>
   </div>
</section>
<!-- News End -->


<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>