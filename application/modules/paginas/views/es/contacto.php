<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!--Contact-->
<section class="contacto">
  <div class="container padding">
    <div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="our-agent-box bottom30">            
            <h2><?= $this->lang->line('oficina-de-igualada-y-barcelona') ?></h2>
        </div>
        <div>
          <div id="map" style="height:300px;"></div>
        </div>
        <div class="col-xs-12 col-md-5" style="padding:0">
          
          <div class="agetn-contact-2 bottom30" style="font-size:13px">                             
               <p><i class="icon-telephone114"></i> +34 93 804 81 71</p>
               <p><i class=" icon-icons142"></i> info@finques-sasi.com</p>
               
               <p><i class="icon-world" style="font-size:12px"></i> www.finques-sasi.com</p>
               <p><i class="icon-icons74"></i> AV. Barcelona, 1 <br class="visible-md visible-lg">08700 Igualada (Barcelona)</p>
            </div>
          <ul class="social_share bottom20">
            <li><a href="javascript:void(0)" class="facebook"><i class="icon-facebook-1"></i></a></li>
            <li><a href="javascript:void(0)" class="twitter"><i class="icon-twitter-1"></i></a></li>            
            <li><a href="javascript:void(0)" class="linkden"><i class="fa fa-linkedin"></i></a></li>            
          </ul>
        </div>
        <div class="col-xs-12 col-md-7" style="padding:0">
          <div class="agent-p-form">
              <div class="our-agent-box bottom30">
                  <h2 style="font-size: 22px;">
                    <?= $this->lang->line('mandanos-un-mensaje') ?>                    
                  </h2>
              </div>
              
              <div class="row">
                <form action="paginas/frontend/contacto" class="callus noloader" onsubmit="sendForm(this,'#response1'); return false;">
                  <div class="col-md-12" style="padding:0">
                    <div class="single-query form-group">
                      <input type="text" name="nombre" placeholder="<?= $this->lang->line('tu-nombre') ?>" class="keyword-input">
                      </div>
                      <div class="single-query form-group">
                      <input type="text" name="extras[telefono]" placeholder="<?= $this->lang->line('telefono') ?>" class="keyword-input">
                    </div>
                    <div class="single-query form-group">
                      <input type="text" name="email" placeholder="Email" class="keyword-input">
                    </div>
                    <div class="single-query form-group">
                      <textarea name="extras[comentario]" placeholder="<?= $this->lang->line('mensaje') ?>" class="form-control"></textarea>
                    </div>
                     <input type="hidden" name="to" value="info@finques-sasi.com">
                     <input type="hidden" name="titulo" value="<?= $this->lang->line('web-sasi-contacto') ?>">                     
                     <div id="response1"></div>
                    <div class="form-group">
                      <input type="checkbox" name="politicas" value="1"> <label for=""> <?= $this->lang->line('acepto-las') ?> <a href="<?= base_url() ?>politicas-igualada.html" target="_new"><u><?= $this->lang->line('politicas-de-privacidad') ?></u></a></label>
                    </div>
                     <input type="submit" class="btn-blue uppercase border_radius" value="<?= $this->lang->line('enviar') ?>">
                    </div>
                </form>
                
              </div>
              
          </div>
        </div>
      </div>







  <div class="col-xs-12 col-md-6">
    <div class="our-agent-box bottom30">
        <h2><?= l('oficina-de-alicante') ?></h2>
    </div>
    <div>
        <div id="map2" style="height:300px; margin-bottom: 40px;"></div>
    </div>

    <div class="col-xs-12 col-md-5"  style="padding:0">
        <div class="agent-p-contact">
            
              <div class="agetn-contact-2 bottom30" style="font-size:13px">                           
                   <p><i class="icon-telephone114"></i> +34 865 751 305</p>
                   <p><i class=" icon-icons142"></i> info@sasi-alicante.com</p>
                   
                   <p><i class="icon-world" style="font-size:12px"></i> www.finques-sasi.com</p>
                   <p><i class="icon-icons74"></i> Avda. Eusebio 22 <br class="visible-md visible-lg">03003 Alicante (Alicante)</p>
                </div>
              <ul class="social_share bottom20">
                <li><a href="javascript:void(0)" class="facebook"><i class="icon-facebook-1"></i></a></li>
                <li><a href="javascript:void(0)" class="twitter"><i class="icon-twitter-1"></i></a></li>                    
                <li><a href="javascript:void(0)" class="linkden"><i class="fa fa-linkedin"></i></a></li>                    
              </ul>
        </div>
      </div>
      <div class="col-xs-12 col-md-7"  style="padding:0">
        <div class="agent-p-form">
            <div class="our-agent-box bottom30">
                <h2 style="font-size: 22px;"><?= $this->lang->line('mandanos-un-mensaje') ?></h2>
            </div>
            
            <div class="row">
              <form action="paginas/frontend/contacto" class="callus noloader" onsubmit="sendForm(this,'#response2'); return false;">
                <div class="col-md-12" style="padding:0">
                  <div class="single-query form-group">
                    <input type="text" name="nombre" placeholder="<?= $this->lang->line('tu-nombre') ?>" class="keyword-input">
                    </div>
                    <div class="single-query form-group">
                    <input type="text" name="extras[telefono]" placeholder="<?= $this->lang->line('telefono') ?>" class="keyword-input">
                  </div>
                  <div class="single-query form-group">
                    <input type="text" name="email" placeholder="Email" class="keyword-input">
                  </div>
                  <div class="single-query form-group">
                    <textarea name="extras[comentario]" placeholder="<?= $this->lang->line('mensaje') ?>" class="form-control"></textarea>
                  </div>
                   <input type="hidden" name="to" value="info@sasi-alicante.com">
                   <input type="hidden" name="titulo" value="Web SASI, contacto.">
                   <div id="response2"></div>
                  <div class="form-group">
                    <input type="checkbox" name="politicas" value="1"> <label for=""> <?= $this->lang->line('acepto-las') ?> <a href="<?= base_url() ?>politicas-alicante.html" target="_new"><u><?= $this->lang->line('politicas-de-privacidad') ?></u></a></label>
                  </div>
                   <input type="submit" class="btn-blue uppercase border_radius" value="<?= $this->lang->line('enviar') ?>">
                  </div>
              </form>
              
            </div>
            
        </div>
        
      </div>
</section>

<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=drawing,geometry,places"></script> 
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?= base_url() ?>theme/theme/js/gmaps.js"></script>
<script src="<?= base_url() ?>theme/theme/js/contact.js"></script>