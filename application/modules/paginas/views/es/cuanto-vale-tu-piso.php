<link href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/chosen/chosen.css" rel="stylesheet">
<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">      
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase"><?= l('quieressaberelvalordetupiso') ?></h1>
        <p></p>
        <ol class="breadcrumb text-center">
          <li><a href="<?= base_url() ?>"><?= l('inicio') ?></a></li>          
          <li class="active"><?= l('quieressaberelvalordetupiso') ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->
<!-- My Properties  -->
<section id="property" class="padding listing1">
  <div class="container">    
    <div class="row">

      
      <div class="col-sm-1 col-md-2"></div>
      <div class="col-sm-10 col-md-8">
        <img src="https://www.finques-sasi.com/img/icono-casa.jpg" alt="" class="hidden-xs hidden-sm" style="position: absolute;left: -90px;width: 80px;">
        <h2 class="text-uppercase bottom40">
          <?= l('cuantovaletut1') ?>
        </h2>

      <form action="propiedades/frontend/cuantoValeTuPiso" onsubmit="calcular(this); return false;">



              
              <h3 class="margin40 bottom15"><?= l('datos-de-contacto') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos de contacto de la propiedad"></i></h3>
              <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="provincias_id_field_box">
                          <label for="field-nombre" id="nombre_display_as_box" style="width:100%">
                              <?= l('nombre') ?> <span class="required">*</span>:
                          </label>
                          <input id="field-nombre" name="nombre" class="form-control nombre" type="text" value="" maxlength="255">                </div>
                  </div> 
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="provincias_id_field_box">
                          <label for="field-apellidos" id="apellidos_display_as_box" style="width:100%">
                              <?= l('apellidos') ?> <span class="required">*</span>:
                          </label>
                          <input id="field-apellidos" name="apellidos" class="form-control apellidos" type="text" value="" maxlength="255">                </div>
                  </div> 
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="provincias_id_field_box">
                          <label for="field-email" id="email_display_as_box" style="width:100%">
                              E-mail <span class="required">*</span>:
                          </label>
                          <input id="field-email" name="email" class="form-control email" type="text" value="" maxlength="255">                </div>
                  </div> 
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="provincias_id_field_box">
                          <label for="field-telefono" id="telefono_display_as_box" style="width:100%">
                              <?= l('telefono') ?> <span class="required">*</span>:
                          </label>
                          <input id="field-telefono" name="telefono" class="form-control telefono" type="text" value="" maxlength="255">                </div>
                  </div> 
                  
              </div>


          <h3 class="margin40 bottom15">Datos de la propiedad  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos de ubicación de la propiedad"></i></h3>
            <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="provincias_id_field_box">
                          <label for="field-provincias_id" id="provincias_id_display_as_box" style="width:100%">
                              <?= l('provincia') ?><span class="required">*</span>  :
                          </label>
                          <?php 
                            $this->db->order_by('nombre','ASC');
                            echo form_dropdown_from_query('provincia_id','provincias','id','nombre','','id="provincias_id"',TRUE,'provincia') 
                          ?>
                        </div>
                  </div> 
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="cp_field_box">
                          <label for="field-cp" id="cp_display_as_box" style="width:100%">
                              <?= l('c.p') ?><span class="required">*</span>  :
                          </label>
                          <input id="field-cp" name="cp" class="form-control cp" type="text" value="" maxlength="10">
                        </div>
                  </div> 
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="ciudades_id_field_box">
                          <label for="field-ciudades_id" id="ciudades_id_display_as_box" style="width:100%">
                              <?= l('ciudad') ?><span class="required">*</span>  :
                          </label>
                          <?php $this->db->limit(10); $this->db->where('id',-1);echo form_dropdown_from_query('ciudad','ciudades','id','ciudad','','id="ciudades_id"',TRUE,'ciudad') ?>
                        </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="ciudades_id_field_box">
                          <label for="field-ciudades_id" id="ciudades_id_display_as_box" style="width:100%">
                              <?= l('zona') ?><span class="required">*</span>  :
                          </label>
                          <?php $this->db->limit(10); $this->db->where('id',-1);echo form_dropdown_from_query('zona','zonas','id','nombre','','id="zonas_id"',TRUE,'zona') ?>
                        </div>
                  </div> 
                   
              </div>
              <div class="row"> 
                              
                  <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="form-group" id="zonaauxiliar_field_box">
                          <label for="field-zonaauxiliar" id="zonaauxiliar_display_as_box" style="width:100%">
                              <?= l('calle') ?>:
                          </label>
                          <input id="field-zonaauxiliar" name="calle" class="form-control zonaauxiliar" type="text" value="" maxlength="255">                </div>
                  </div>

                  
                  
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="numero_field_box">
                          <label for="field-numero" id="numero_display_as_box" style="width:100%">
                              <?= l('Número') ?>:
                          </label>
                          <input id="field-numero" name="numero" número="" type="text" value="" class="numero numeric form-control" maxlength="11">                </div>
                  </div> 
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="planta_field_box">
                          <label for="field-planta" id="planta_display_as_box" style="width:100%">
                              <?= l('piso') ?>:
                          </label>
                          <input id="field-planta" name="piso" planta="" type="text" value="" class="planta numeric form-control" maxlength="11">                </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="puerta_field_box">
                          <label for="field-puerta" id="puerta_display_as_box" style="width:100%">
                              <?= l('Puerta') ?>:
                          </label>
                          <input id="field-puerta" name="puerta" puerta="" type="text" value="" class="puerta numeric form-control" maxlength="11">                </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3">
                      <div class="form-group" id="calle_field_box">
                          <label for="field-calle" id="calle_display_as_box" style="width:100%">
                              M<sup>2</sup>*:
                          </label>
                          <input id="field-calle" name="m2" class="form-control calle" type="text" value="" maxlength="255">                </div>
                  </div> 
              </div>

              <h3 class="margin40 bottom15"><?= l('tipo-de-propiedad') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos generales de la propiedad"></i></h3>

              <div class="row">
                  

                    <div class="col-xs-12 col-sm-6" style="padding:0">
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group" id="tipos_id_field_box">
                              <label for="field-tipos_id" id="tipos_id_display_as_box" style="width:100%">
                                  <?= l('tipo-de-propiedad') ?> *:
                              </label>
                              <select name="tipos_id" id="field-tipos_id" class="form-control chosen-select" data-placeholder="<?= l('seleccionar-opcion') ?>">
                                  <option value=""><?= l('seleccionar-opcion') ?></option>
                                  <?php
                                      $this->db->order_by('label','ASC');
                                      $this->db->group_by('codigo_tipo');
                                      foreach($this->db->get_where('tipos')->result() as $t): 
                                  ?>
                                  
                                    <option value="<?= $t->codigo ?>"><?= l('tipo_'.mb_strtolower($t->label)) ?></option>
                                  
                                  <?php endforeach ?>
                              </select>     
                          </div>
                      </div> 
                      
                      <div class="col-xs-12 pl-0 hidden-xs">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="field-politicas" style="margin-left: 17px;">
                                  <input type="checkbox" id="field-politicas" name="politicas" value="1"> <?= l('al-pulsar-enviar') ?>
                                </label>
                            </div>
                        </div>

                        <div class="btn-group" style="margin-left: 17px;"> 
                            <button type="submit" class="btn-blue border_radius"><?= l('ver-precio') ?></button>                   
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6" style="padding:0">
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group" id="descripofertas_field_box">
                              <label for="field-descripofertas" id="descripofertas_display_as_box" style="width:100%">
                                  <?= l('descripcion-propiedad') ?>:
                              </label>
                              <textarea name="descripcion" id="field-descripofertas" cols="30" rows="10" style="width: 100%;"></textarea>
                          </div>
                      </div> 
                    </div>
                    <div class="col-xs-12 pl-0 visible-xs" style="margin-bottom: 40px">
                      <div class="form-group">
                          <div class="checkbox">
                              <label for="field-politicas" style="margin-left: 17px;">
                                <input type="checkbox" id="field-politicas" name="politicas" value="1"> <?= l('al-pulsar-enviar') ?>
                              </label>
                          </div>
                      </div>

                      <div class="btn-group" style="margin-left: 17px;"> 
                          <button type="submit" class="btn-blue border_radius"><?= l('ver-precio') ?></button>                   
                      </div>
                    </div>
                </div>
                <div class="badResponse"></div>
                <div class="row precioCalculo">
                  <div class="col-xs-12 col-md-6 text-center">
                    <div style="padding: 0 50px;background: url(https://www.finques-sasi.com/img/ticket.jpg);background-size: 100% 100%;padding: 28px 20px 42px;margin-top: 6px;">
                        <h3 class="bottom15"><?= l('tu-propiedad-vale') ?></h3>
                        <h2 class="text-uppercase valor" style="background: #ffe4e1;padding: 10px;">183.450€</h2>
                    </div>
                  </div>
                  <div class="col-xs-12 col-md-6 nota">
                    <p><?= l('cuanto-vale-nota') ?></p>
                    <a href="<?= base_url() ?>contacto.html" class="btn-blue border_radius"><?= l('contactar') ?></a>
                  </div>
                </div>




              </div>
              
        </form>        

      </div>
    </div>
    
  </div>
</section>
<!-- My Properties End -->
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>

<script>
  $(document).on('change','.provincia',function(e){
      e.stopPropagation();
      $.post(URL+'propiedades/json/ciudades/json_list',{
          'search_field[]':'jd904cb91.provincias_id',
          'search_text[]':$(this).val(),
          'per_page':'1000'
      },function(data){
          ajax = false;
          data = JSON.parse(data);          
          refreshSelect('.ciudad',data);
      });
  });

  $(document).on('change','.ciudad',function(e){
      e.stopPropagation();
      $.post(URL+'propiedades/json/zonas/json_list',{
          'search_field[]':'zonas.ciudades_id',
          'search_text[]':$(this).val(),
          'per_page':'1000'
      },function(data){
          ajax = false;
          data = JSON.parse(data);          
          refreshSelect('.zona',data);
      });
  });

  function refreshSelect(classe,data){
      var opt = '<option value="">Todas</option>';
      for(var i in data){
          opt+= '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
      }

      $(classe).html(opt);
      $(classe).chosen().trigger('liszt:updated');
  }  
  function calcular(form){
    $(".badResponse").html('');
    $(".precioCalculo").hide();
    sendForm(form,'',function(data){
      var data = JSON.parse(data);
      if(data.success){
        $(".precioCalculo .valor").html(data.msj);
        $(".precioCalculo").show();
      }else{
        $(".badResponse").html(data.msj);
      }
      
    });
  }
</script>