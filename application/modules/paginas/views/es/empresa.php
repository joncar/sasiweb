<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 text-center">
            <h1 class="p-white text-uppercase"><?= $this->lang->line('empresa') ?></h1>
            <p class="p-white"><?= $this->lang->line('quienes-somos') ?></p>
            <ol class="breadcrumb text-center">
               <li><a href="#"><?= $this->lang->line('inicio') ?></a></li>               
            </ol>
         </div>
      </div>
   </div>
</section>
<!-- Page Banner End --> 
<!--Contact-->
<section>
	<div class="container">
	  	<div class="texto_contenido padding">
         <div class="row">            
            <div class="col-xs-12 col-sm-12 col-md-6" style="font-size:16px; margin-bottom: 40px">
               <?php echo $this->lang->line('empresa-text') ?>        
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
               <img src="https://finques-sasi.com/web_anterior/img/slideshow/1.jpg" style="width:100%;">
            </div>
         </div>         
		</div>
	</div>
</section>
<!-- Contact End -->
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>