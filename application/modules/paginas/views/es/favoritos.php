<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase">Listing Style 1</h1>
        <p>Serving you since 1999. Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
        <ol class="breadcrumb text-center">
          <li><a href="#">Home</a></li>
          <li><a href="#">Pages</a></li>
          <li><a href="#">Listing</a></li>
          <li class="active">Listing - 1</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->



<!-- Listing Start -->
<section id="listing1" class="listing1 padding_top">
  <div class="container">
    <?php $this->load->view($this->theme.'/_menu_cuenta',array('favoritos'=>'active')) ?>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-9">
            <h2 class="uppercase">PROPERTY LISTINGS</h2>
            <p class="heading_space">We have Properties in these Areas View a list of Featured Properties.</p>
          </div>
          <div class="col-md-3">
          <form class="callus">
            <div class="single-query">
              <div class="intro">
                <select>
                  <option class="active">Default Order</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                </select>
              </div>
            </div>
            </form>
          </div>
        </div>
        <div class="row">
          

          <div class="row">
            <?php 
            $this->db->limit(3);
            $this->db->where('propiedades_list.precioinmo >',0);
            $prop = $this->querys->get_propiedades();
            foreach($prop->result() as $p): ?>
                <div class="col-sm-4">
                  <?php $this->load->view('_propiedad',array('detail'=>$p)); ?>
                </div>
            <?php endforeach; ?>
          </div>


          
        </div>
        
        <div class="padding_bottom text-center">
          <ul class="pager">
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
          </ul>
        </div>
      </div>
      
    </div>
  </div>
</section>
<!-- Listing end -->
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php if(!empty($js_files)):?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
<?php endif; ?>