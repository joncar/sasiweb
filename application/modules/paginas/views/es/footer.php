<footer class="footer_third">
  <div class="container padding_top">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="footer_panel bottom30">
          <a href="<?= base_url() ?>" class="logo bottom30"><img src="<?= base_url() ?>theme/theme/images/logo-white.png" alt="logo"></a>
          <p class="bottom15 nosotros">
            <?= $this->lang->line('footer-about') ?>            
          </p>        
        </div>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="footer_panel bottom30 contacts">
          <h4 class="bottom30 heading">Igualada</h4>
          
            <ul class="text-left" style="color:white">
              <li><i class="icon-icons74"></i> Av. Barcelona, 1</li>
              <li><i class="icon-icons74" style="visibility: hidden"></i> 08700 Igualada, Barcelona</li>
              <li><a href="tel:+34938048171"><i class="icon-telephone114"></i> +34 93 804 81 71</a></li>
              <li><a href="mailto:info@finques-sasi.com?subject=Web SASI, contacto"><i class="icon-icons142"></i> info@finques-sasi.com</a></li>
              <li class="visible-xs visible-sm"><a href="<?= base_url() ?>politicas-alicante.html"><i class="icon-lock2"></i> <?= $this->lang->line('politicas') ?>  </a></li>
              <li class="visible-xs visible-sm"><a href="<?= base_url() ?>aviso-legal-igualada.html"><i class="icon-user2"></i><?= $this->lang->line('aviso-legal') ?>  </a></li>
              <li>&nbsp;</li>
              <li class="hidden-xs hidden-sm">
                <a href="<?= base_url() ?>politicas-igualada.html" class="avisos">
                  <i class="icon-icons142"></i>
                  <?= $this->lang->line('politicas') ?>
                </a> | 
                <a href="<?= base_url() ?>aviso-legal-igualada.html" class="avisos">
                  <?= $this->lang->line('aviso-legal') ?>
                </a>
              </li>              
            </ul>
          
        </div>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="footer_panel bottom30 contacts">
          <h4 class="bottom30 heading">Alicante</h4>
          
            <ul class="text-left" style="color:white">
              <li><i class="icon-icons74"></i> Avda. Eusebio Sempere 22</li>
              <li><i class="icon-icons74" style="visibility: hidden"></i> 03003 Alicante (Alicante)</li>
              <li><a href="tel:+34865751305"><i class="icon-telephone114"></i> +34 865 751 305</a></li>
              <li><a href="mailto:info@sasi-alicante.com?subject=Web SASI, contacto"><i class="icon-icons142"></i> info@sasi-alicante.com</a></li>
              <li class="visible-xs visible-sm"><a href="<?= base_url() ?>politicas-alicante.html"><i class="icon-lock2"></i> <?= $this->lang->line('politicas') ?></a></li>
              <li class="visible-xs visible-sm"><a href="<?= base_url() ?>aviso-legal-igualada.html"><i class="icon-user2"></i><?= $this->lang->line('aviso-legal') ?></a></li>
              <li>&nbsp;</li>
              <li class="hidden-xs hidden-sm">
                <a href="<?= base_url() ?>politicas-alicante.html" class="avisos">
                  <i class="icon-icons142"></i> 
                  <?= $this->lang->line('politicas') ?>
                </a> | 
                <a href="<?= base_url() ?>aviso-legal-alicante.html" class="avisos">
                  <?= $this->lang->line('aviso-legal') ?>
                </a>
              </li>              
            </ul>
          
        </div>
      </div>      
      <div class="col-md-3 col-sm-6 hidden-xs hidden-sm">
        <div class="footer_panel bottom30">
          <h4 class="bottom30 heading"><?= $this->lang->line('subscribirse') ?></h4>
          <p><?= $this->lang->line('subscribirse_text') ?></p>
          <form class="top30" action="paginas/frontend/subscribir" onsubmit="sendForm(this,'#subscribeResult'); return false;">
            <div id="subscribeResult"></div>
            <input class="search" placeholder="<?= $this->lang->line('subscribirse_email') ?>" type="email" name="email">
            <button type="submit" class="button_s" href="#">
            <i class="icon-mail-envelope-open"></i>
            </button>            
          </form>
        </div>
      </div>
    </div>
    <!--CopyRight-->
    <div class="copyright_simple">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12 top20 bottom15">
          <p class="copyrightp">            
            <?= l('copyright') ?>
          </p>
          <ul class="idiomas">
            <li><a href="<?= base_url('main/traduccion/es') ?>"><span class="flag spain"></span> </a></li>
            <li><a href="<?= base_url('main/traduccion/ca') ?>"><span class="flag catalunya"></span> </a></li>
            <li><a href="<?= base_url('main/traduccion/en') ?>"><span class="flag ingles"></span> </a></li>
            <li><a href="<?= base_url('main/traduccion/al') ?>"><span class="flag aleman"></span> </a></li>
            <li><a href="<?= base_url('main/traduccion/fr') ?>"><span class="flag frances"></span> </a></li>
            <li><a href="<?= base_url('main/traduccion/it') ?>"><span class="flag italiano"></span> </a></li>
            <li><a href="<?= base_url('main/traduccion/ru') ?>"><span class="flag ruso"></span> </a></li>
            <li><a href="<?= base_url('main/traduccion/ch') ?>"><span class="flag chino"></span> </a></li>
          </ul>
          
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 text-right top15 bottom10">
          <ul class="social_share">
            <li><a href="#" class="facebook"><i class="icon-facebook-1"></i></a></li>
            <li><a href="#" class="twitter"><i class="icon-twitter-1"></i></a></li>            
            <li><a href="#" class="linkden"><i class="fa fa-linkedin"></i></a></li>            
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php $this->load->view('includes/template/scripts') ?>
