<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 text-center">
            <h1 class="text-uppercase"><?= l('propiedades-lowcost') ?></h1>            
            <p><?= l('lista-propiedades-lowcost-text') ?></p>
            <ol class="breadcrumb text-center">
               <li><a href="#"><?= l('inicio') ?></a></li>               
               <li class="active">Low Cost</li>
            </ol>
         </div>
      </div>
   </div>
</section>
<!-- Page Banner End -->

<!-- Listing Start -->
<section id="listing1" class="listing1 padding_top">
  	
  	<div class="row" style="margin-left:0; margin-right:0">      
	      <div class="col-md-8 col-sm-12 col-xs-12 lc">
	      	<div class="container" style="padding:0">
			  	<h2 class="text-uppercase"><?= l('lista-propiedades-lowcost') ?></h2>
			    <p><?= l('lista-propiedades-lowcost-text') ?></p>
			</div>
		  </div>	  
	</div>


    <div class="row">
      <div class="col-md-8 col-sm-12 col-xs-12 lc">
        <?= $output ?>
      </div>
      
      <aside class="col-md-4 col-xs-12 bottom40">
        <div class="property-query-area clearfix">
          <div class="col-md-12">
            <h3 class="text-uppercase bottom20 top15"><?= l('servicio-lowcost') ?></h3>
            <p style="color:#ffffff">
            	<?= l('servicio-lowcost-text') ?>
            </p>
          </div>
          
          <div class="col-sm-12">
            <div class="group-button-search">
              <a data-toggle="collapse" href=".search-propertie-filters" class="more-filter bottom15">
                <i class="fa fa-plus text-1" aria-hidden="true"></i><i class="fa fa-minus text-2 hide" aria-hidden="true"></i>
                <div class="text-1 hide"><?= l('pide-presupuesto') ?></div>
                <div class="text-2"><?= l('pide-presupuesto') ?></div>
              </a>
            </div>
          </div>
          <div class="search-propertie-filters collapse in">
            <div>
              
				<div class="row" style="margin-right: 0; margin-left: 0;">
					<div class="col-xs-12 col-md-12">
				
						<form class="callus noloader" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#responseLC')">
				            <div class="single-query form-group col-sm-12">
				              <label for=""><?= l('nombre') ?></label>
				              <input type="text" name="nombre" class="keyword-input" placeholder="<?= l('nombre') ?>">
				            </div>
				            <div class="single-query form-group col-sm-12">
				            	<label for=""><?= l('ciudad') ?></label>
				              <input type="text" name="extras[ciudad]" class="keyword-input" placeholder="<?= l('ciudad') ?>">
				            </div>
				            <div class="single-query form-group col-sm-12">
				            	<label for=""><?= l('telefono') ?></label>
				              	<input type="text" name="extras[telefono]" class="keyword-input" placeholder="<?= l('telefono') ?>">
				              	<input type="hidden" name="extras[blanco]" class="keyword-input" value="">
				            </div>
				            <div class="single-query form-group col-sm-12">
				            	<label for="">Email</label>
				              <input type="text" name="email" class="keyword-input" placeholder="Email">
				            </div>



				            <div class="single-query form-group col-sm-12">
			            	  <p style="color:white" class="text-uppercase top30"><?= l('servicios-que-necesitas') ?></p>
			            	</div>
				            

				            	<div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[valorar_tu_piso]" value="SI" />
				                    <span style="color:white"><?= l('valorar-tu-piso') ?></span>
				                  </div>
				                </div>
				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[realizar_fotos]" value="SI" />
				                    <span style="color:white"><?= l('realizar-fotos') ?></span>
				                  </div>
				                </div>
				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[publicar_tu_piso_en_los_mejores_portales]" value="SI" />
				                    <span style="color:white"><?= l('publicar-en-portales') ?></span>
				                  </div>
				                </div>
				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[asesorar_como_tienes_que_ensenar_y_preparar_tu_piso_para_las_futuras_visitas]" value="SI" />
				                    <span style="color:white"><?= l('asesorar-como-ensenar') ?></span>
				                  </div>
				                </div>

				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[acompanar_a_los_compradores]" value="SI" />
				                    <span style="color:white"><?= l('quieres-que-acompanemos-compradores') ?></span>
				                  </div>
				                </div>
				              
				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[documentacion]" value="SI" />
				                    <span style="color:white"><?= l('documentacion') ?></span>
				                  </div>
				                </div>
				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[acompanar_al_notario]" value="SI" />
				                    <span style="color:white"><?= l('acompanar-al-notario') ?></span>
				                  </div>
				                </div>
				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[tramitar_cedula_de_habitabilidad]" value="Si" />
				                    <span style="color:white"><?= l('tramitar-cedula') ?></span>
				                  </div>
				                </div>
				                <div class="col-sm-12 col-xs-12">
				                  <div class="form-group white">
				                    <input type="checkbox" name="extras[tramitar_certificado_energetico]" value="Si" />
				                    <span style="color:white"><?= l('tramitar-certificado-energetico') ?></span>
				                  </div>
				                </div>
				                
				            

								<div class="col-sm-12 col-xs-12 form-group white" style="margin-bottom: 0">
									<label for=""><?= l('otros-servicios') ?></label>
				                    <textarea name="extras[otros_servicios]" id="" cols="30" rows="10" placeholder="Otros" style="min-height: 20px;height: 80px; width:100%; padding:10px;"></textarea>			                    
				                </div>
				                <div class="col-sm-12 col-xs-12 form-group white" style="margin-top:20px;">
				                    <input type="checkbox" name="politicas" />
				                    <span style="color:white"><?= l('acepto-las-politicas-de-privacidad') ?></span>
				                </div>
				            	<div id="responseLC" class="col-sm-12 col-xs-12">
				            		
				            	</div>
				            	<input type="hidden" name="titulo" value="Web SASI, presupuesto LOW COST.">
					            <div class="col-sm-12 col-xs-12 form-group">
					              <button type="submit" class="btn-blue border_radius"><?= l('pedir-presupuesto') ?></button>
					            </div>
					        	</div>
				          </form>

					</div>
				</div>

            </div>
          </div>
        </div>
      </aside>







    </div>
  </div>
</section>
<!-- Listing end -->



<!-- News Details End --> 
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php if(!empty($js_files)):?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
<?php endif; ?>