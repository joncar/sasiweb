<header class="layout_boxed">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!--Slider-->
<div class="slider hidden-xs hidden-sm">
  <div class="rev_slider_wrapper">
    <div class="patternVideo">
      
    </div>
    <div id="rev_slider" class="rev_slider"  data-version="5.0">
      <ul>
        <!-- SLIDE  -->
        <li 
        data-index="rs-106" 
        data-transition="fade" 
        data-slotamount="default"  
        data-easein="default" 
        data-easeout="default" 
        data-masterspeed="default"  
        data-rotate="0" 
        data-param1="HTML5 Video" 
        data-description="">
          <img src="<?= base_url() ?>img/introPreview.png"  alt="video slide"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
          <div class="rs-background-video-layer" 
           data-volume="mute" 
           data-videowidth="100%" 
           data-videoheight="100%" 
           data-videomp4="<?= base_url() ?>theme/theme/video/video2.mp4" 
           data-videopreload="preload" 
           data-videoloop="loop" 
           data-forceCover="1" 
           data-aspectratio="16:9" 
           data-autoplay="true" 
           data-autoplayonlyfirsttime="false" 
           data-nextslideatend="false"
           data-repeat="true">
          </div>
       </li>
      </ul>
    </div>
    
  </div>
</div>
<!--Advance Search-->
<section class="buscadorPrincipalContent property-query-area queryfloat bg_light">
  <!--<p class="topper">Buscador</p>-->
  <div class="container buscadorPrincipal">

    <div class="col-xs-12 col-md-8">
    <div class="row">
      <form class="callus" action="<?= base_url() ?>propiedades/frontend/rutear" onsubmit="$('.loader').show();">

        <div class="form-group col-xs-6 col-sm-4 checkboxs">    
          <label class="text-uppercase bottom10 top15"><?= $this->lang->line('acciones') ?></label>      
          <div class="acciones">            
            <label data-label="acciones" class="active" data-val="1"><?= $this->lang->line('comprar') ?></label>
            <label data-label="acciones" data-val="2" for="2"><?= $this->lang->line('alquiler') ?></label>
            <label data-label="obranueva" data-val="1" for="4"><?= $this->lang->line('obra-nueva') ?></label>
            <label data-label="lowcost" data-val="1" for="5">Low Cost</label>
          </div>
          <div style="border:0; padding:0;margin-top: 17px; width:100%;" class="hidden-xs">
            <button type="button" class="btn btn-info btn-block buscarMapa" value="mapa"><i class="fa fa-search"></i> <?= $this->lang->line('buscar-por-mapa') ?></button>
          </div>
        </div>

        <div class="col-xs-6 visible-xs" style="height:163px;"></div>

        <div class="form-group col-xs-6 col-sm-4 checkboxs">          
          <label class="text-uppercase bottom10 top15"><?= $this->lang->line('tipos-de-inmuebles') ?></label>
          <div class="codigo_tipo">
              <label class="active" data-val="1,2,3,4,5"><?= $this->lang->line('viviendas') ?></label>              
              <label data-val="9"><?= $this->lang->line('oficina') ?></label>
              <label data-val="5"><?= $this->lang->line('local-comercial') ?></label>
              <label data-val="6"><?= $this->lang->line('industrial') ?></label>
              <label data-val="7"><?= $this->lang->line('terrenos-y-solares') ?></label>
              <label data-val="8"><?= $this->lang->line('parkings') ?></label>              
          </div>
        </div>

        <div class="form-group col-xs-6 col-sm-4 checkboxs"> 
          <label class="text-uppercase bottom10 top15"><?= $this->lang->line('ubicacion') ?></label>         
          <div class="provincias_id">            
              <label data-val="2" data-val2="16">Igualada (Anoia)</label>
              <label data-val="2">Barcelona <?= l('provincia') ?></label>
              <label data-val="2" data-val2="14" data-val3="1066">Barcelona <?= $this->lang->line('ciudad') ?></label>
              <label data-val="8">Alicante <?= l('provincia') ?></label>
              <label data-val="8" data-val2="31" data-val3="1685">Alicante <?= $this->lang->line('ciudad') ?></label>
          </div>
        </div>
  
        <div class="col-xs-12">
          <input type="hidden" id="mainSearchacciones" name="acciones" value="1">
          <input type="hidden" id="mainSearchcodigo_tipo" name="codigo_tipo[]" value="1,2,3,4,5">
          <input type="hidden" id="mainSearchprovincias_id" name="provincias_id" value="">
          <input type="hidden" id="mainSearchcomarcas_id" name="comarcas_id" value="">
          <input type="hidden" id="mainSearchciudades_id" name="ciudades_id" value="">
          <input type="hidden" name="buscar_por" value="lista">
        </div>
      
        <div class="inputRef col-xs-12 col-sm-8 col-md-8">
          <div class="single-query form-group">
            <input type="text" class="keyword-input form-control" name="referencia" placeholder="<?= $this->lang->line('buscar-por-nombre') ?>">
          </div>
        </div>
        <div class="col-xs-6 visible-xs">
          <button type="button" class="btn btn-info btn-block border_radius buscarMapa" value="mapa" style="padding: 13px 0;"><i class="fa fa-search"></i> <?= $this->lang->line('buscar-por-mapa') ?></button>
        </div>
        <div class="inputBtnRef col-xs-6 col-sm-4  col-md-4 text-right form-group">
          <button type="button" class="btn-blue border_radius buscarMapa" value="lista"><?= $this->lang->line('buscar') ?></button>
        </div>
        
      </form>

        
    </div>

  </div>    
</section>
<!--Advance Search-->
<!-- Latest Property -->
<section id="property" class="padding index2">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="uppercase"><?= $this->lang->line('ultimas-propiedades') ?></h2>
        <p class="heading_space"> 
            <?= $this->lang->line('ultimas-propiedades-text') ?>            
        </p>
      </div>
    </div>
    
    
      <?php               
        $prop = $this->querys->get_ultimas_propiedades();
        $n = 0;
        foreach($prop->result() as $n=>$p): 
          if($n==0 || $n==4):
      ?>
        <div class="row">
      <?php endif ?>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <?php $this->load->view('_propiedad',array('detail'=>$p)); ?>
          </div>
      <?php if($n==3 || $n==$prop->num_rows()-1): ?>
        </div>
      <?php endif ?>
      <?php endforeach ?>
    </div>
  </div>
</section>
<!--Latest Property Ends-->
<!--Best Deal Properties-->
<section id="deals" class="padding bg_light">
  <div class="container">
    <div class="row">
      <div class="col-sm-10">
        <h2 class="uppercase">
          <?= $this->lang->line('propiedades-destacadas') ?>          
        </h2>
        <p class="heading_space">
          <?= $this->lang->line('propiedades-destacadas-text') ?>  
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div id="full-slider" class="owl-carousel">          
          <?php             
            $prop = $this->querys->get_destacadas();
            foreach($prop->result() as $n=>$detail): ?>
          
              <div class="item" data-index="<?= $n ?>">
                <?php $this->load->view($this->theme.'_propiedad_destacada',array('detail'=>$detail),FALSE,'paginas'); ?>
              </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Parallax-->
<section id="parallax_four" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 bottom30">
        <h2 class="uppercase"><?= $this->lang->line('mas-de-2000-ofertas') ?>          </h2>
      </div>
      <div class="col-sm-12 col-md-12">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-5 margin40">
             <i class="icon-presentation"></i>
             <?= $this->lang->line('mas-de-2000-ofertas-text') ?>              
          </div>
          <div class="col-xs-12 col-sm-6 col-md-offset-1 col-md-6 margin40">
             <i class="icon-icons215"></i>
             <h4 class="bottom10"><?= $this->lang->line('clientes-satisfechos') ?></h4>
             <p><?= $this->lang->line('clientes-satisfechos-text') ?> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--About Owner ends-->

<!--Partners-->
<section id="logos">
  <div class="container partner2 padding">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2 class="uppercase"><?= $this->lang->line('colaboramos-con') ?></h2>
        <p class="heading_space">
          <?= $this->lang->line('colaboramos-con-text') ?>
        </p>
      </div>
    </div>
    <div class="row">
    <div id="partners" class="owl-carousel">
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo1.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo2.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo3.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo4.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo5.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo6.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo7.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo8.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo9.png" alt="Featured Partner">
        </div>
        <div class="item">
          <img src="<?= base_url() ?>theme/theme/images/logo10.png" alt="Featured Partner">
        </div>       
      </div>
    </div>
  </div>
</section>
<!--Partners Ends-->





<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>

<script src="<?= base_url() ?>theme/theme/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/revolution.extension.video.min.js"></script>