<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase">66.628 anuncios de propiedades <?= 'en '.$label ?></h1>
        <p>Serving you since 1999. Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
        <ol class="breadcrumb text-center">
          <li><a href="<?= base_url() ?>">Inicio</a></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->
<!-- Listing Start -->
<section id="listing1" class="listing1 padding_top">
  <div class="container">
    <div class="row">
      
      <div class="col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-12"  style="padding:0">
            <div style="margin-top:30px;">
              <form class="callus">
                <div class="col-xs-12 col-sm-3">
                  <div class="single-query">
                    <div class="intro">
                      <select>
                        
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                  <button class="btn btn-info btn-block btn-pad" type="submit">Buscar</button>
                </div>
              </form>
              <div class="col-md-3 col-md-offset-4" style="padding:0">
                <a href="<?= base_url() ?>poblaciones.html" class="btn btn-info btn-block btn-pad"> <i class="fa fa-list"></i> Ver lista de poblaciones</a>
              </div>
            </div>
          </div>
          
        </div>
        
        <div class="row property-list-area property-list-map-area" style="padding:30px 0;">
          
          
          <div class="col-xs-12 col-md-8">
            <h3 class="hidden">hiden</h3>
            
            <div class="property-list-map" style="text-align: center;">
              <img id="mapaImage" src="<?= $mapa ?>" usemap="#mapCoords" style="display:inline-block; width:100%">
              <map id="mapCoords" name="mapCoords">
              
                <?php
                  foreach($coordenadas->result() as $c): if(!empty($c->codigo_mapa_general)):
                ?>
                  <area shape="poly" data-id="<?= $c->id ?>" state="<?= $c->nombre ?>" full="<?= $c->nombre ?>" title="<?= $c->nombre ?> - <?= $c->propiedades ?> propiedades" coords="<?= $c->codigo_mapa_general ?>" href="<?= $c->url ?>" alt="">
                <?php endif; endforeach; ?>
                </map>
              </div>
            </div>
            <div class="col-xs-12 col-md-4" style="padding-right: 0;">
              <div class="agent-p-form">
                <div class="our-agent-box bottom20">
                  <h2>Contacta con nosotros</h2>
                </div>
                
                <div class="row">
                  <form action="#" class="callus" onsubmit="return false">
                    <div class="col-md-12">
                      <div class="single-query form-group">
                        <input type="text" placeholder="Nombre" class="keyword-input">
                      </div>
                      <div class="single-query form-group">
                        <input type="text" placeholder="Teléfono" class="keyword-input">
                      </div>
                      <div class="single-query form-group">
                        <input type="text" placeholder="Email" class="keyword-input">
                      </div>
                      <div class="single-query form-group">
                        <textarea placeholder="Mensaje" class="form-control" style="min-height:130px"></textarea>
                      </div>
                      <input type="submit" value="Enviar" class="btn-blue">
                    </div>
                  </form>
                  
                </div>
                
              </div>
            </div>
            
          </div>
          
          
        </div>
        
        
      </div>
      
    </div>
  </div>
</section>


<section id="listing1" class="listing1 padding_top padding_bottom bg_light">
    <div class="container">
          <h2 class="uppercase" style="margin:30px 0; ">Propiedades por <?= $label ?></h2>
        
          <div class="row">
            <?php foreach($coordenadas->result() as $n=>$v): ?>
              <div class="col-xs-12 col-sm-6 col-md-3">
                <ul class="area_search">
                  <li><a href="<?= $v->url ?>"><i class="icon-icons74"></i><?= $v->nombre ?> (<?= $v->propiedades ?>)</a></li>                  
                </ul>
              </div>
            <?php endforeach; ?>            
          </div>
      </div>
    </div>
</section>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>