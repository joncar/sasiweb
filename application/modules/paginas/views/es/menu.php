<div class="topbar">
    <div class="container">
      <div class="row">
        <div class="col-md-5 text-left">
          
          <ul class="breadcrumb_top text-left idiomasContent">            
            <li class="idiomas">
              <a href="javascript:void(0)">
                <span class="flag <?= $this->class_lang[$_SESSION['lang']][1] ?>"></span> 
                <span><?= $this->class_lang[$_SESSION['lang']][0] ?></span>
              </a>
              <ul>
                <li><a href="<?= base_url('main/traduccion/es') ?>"><span class="flag spain"></span> Español</a></li>
                <li><a href="<?= base_url('main/traduccion/ca') ?>"><span class="flag catalunya"></span> Catalá</a></li>
                <li><a href="<?= base_url('main/traduccion/en') ?>"><span class="flag ingles"></span> English</a></li>
                <li><a href="<?= base_url('main/traduccion/al') ?>"><span class="flag aleman"></span> Deutsch</a></li>
                <li><a href="<?= base_url('main/traduccion/fr') ?>"><span class="flag frances"></span> Frances</a></li>
                <li><a href="<?= base_url('main/traduccion/it') ?>"><span class="flag italiano"></span> Italiano</a></li>
                <li><a href="<?= base_url('main/traduccion/ru') ?>"><span class="flag ruso"></span> Pусский</a></li>
                <li><a href="<?= base_url('main/traduccion/ch') ?>"><span class="flag chino"></span> 中国</a></li>
              </ul>
            </li>         
            <li><p><?= $this->lang->line('me_header_slogan') ?></p></li>    
          </ul>
        </div>
        <div class="col-md-7 col-xs-12 text-right">
          <ul class="breadcrumb_top text-right">
             <?php if($this->user->log): ?>
                  <li><a href="<?= base_url() ?>mis-favoritos"><i class="icon-icons43"></i><?= $this->lang->line('mis-favoritos') ?></a></li>                                                 
                  <li><a href="<?= base_url() ?>mis-alertas"><i class="fa fa-bell-o" style="font-size: 14px;"></i><?= $this->lang->line('mis-alertas') ?></a></li>                                         
                  <!--<li><a href="<?= base_url() ?>perfil"><i class="icon-icons230"></i><?= $this->lang->line('perfil') ?></a></li>          -->
                  <li><a href="<?= base_url('main/unlog') ?>"><i class="icon-icons230"></i><?= $this->lang->line('salir') ?></a></li>      
               <?php else: ?>
                  <li><a href="javascript:showFavModal()"><i class="icon-icons43"></i><?= $this->lang->line('mis-favoritos') ?></a></li>                                                 
                  <li><a href="javascript:showAlertasModal()"><i class="fa fa-bell-o" style="font-size: 14px;"></i><?= $this->lang->line('mis-alertas') ?></a></li>                                         
                  <li><a href="#" data-toggle="modal" data-target="#modalLogin"><i class="icon-icons179"></i><?= $this->lang->line('login-registro') ?></a></li>
               <?php endif ?>
               <li class="idiomas idiomasMobile">
                  <a href="javascript:void(0)">
                    <span class="flag <?= $this->class_lang[$_SESSION['lang']][1] ?>"></span>                     
                  </a>
                  <ul>
                    <li><a href="<?= base_url('main/traduccion/es') ?>"><span class="flag spain"></span></a></li>
                    <li><a href="<?= base_url('main/traduccion/ca') ?>"><span class="flag catalunya"></span></a></li>
                    <li><a href="<?= base_url('main/traduccion/en') ?>"><span class="flag ingles"></span></a></li>
                    <li><a href="<?= base_url('main/traduccion/al') ?>"><span class="flag aleman"></span></a></li>
                    <li><a href="<?= base_url('main/traduccion/fr') ?>"><span class="flag frances"></span></a></li>
                    <li><a href="<?= base_url('main/traduccion/it') ?>"><span class="flag italiano"></span></a></li>
                    <li><a href="<?= base_url('main/traduccion/ru') ?>"><span class="flag ruso"></span></a></li>
                    <li><a href="<?= base_url('main/traduccion/ch') ?>"><span class="flag chino"></span></a></li>
                  </ul>
                </li>  
          </ul>
        </div>
      </div>
    </div>
  </div>

  
  <div class="header-upper dark hidden-xs hidden-sm">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-12">
          <div class="logo">
          <a href="<?= base_url() ?>">
            <img alt="" src="<?= base_url() ?>theme/theme/images/logo-white.png" class="img-responsive">
          </a>
          </div>
        </div>
      <!--Info Box-->
        <div class="col-md-9 col-sm-12 right">
          
          <div class="cubespinner">
              <div class="face1">

                  <div class="info-box first">
                    <div class="icons"><i class="icon-icons74"></i></div>
                      <ul>
                        <li><strong>Av. de Barcelona, 1</strong></li>
                        <li>08700 Igualada, Barcelona</li>
                      </ul>
                  </div>

                  <div class="info-box">
                      <div class="icons"><i class="icon-telephone114"></i></div>
                      <ul>
                        <li><strong><?= $this->lang->line('telefono') ?></strong></li>
                        <li><a href="tel:+34938048171">+34 93 804 81 71</a></li>
                      </ul>                      
                  </div>
                  
                  <div class="info-box">
                    <div class="icons"><i class="icon-icons142"></i></div>
                    <ul>
                      <li><strong>E-mail</strong></li>
                      <li><a href="mailto:info@finques-sasi.com?subject=Web SASI, contacto">info@finques-sasi.com</a></li>
                    </ul>
                  </div>

              </div>

              <div class="face2">

                  <div class="info-box first">
                    <div class="icons"><i class="icon-icons74"></i></div>
                      <ul>

                        <li><strong>Avda. Eusebio Sempere 22</strong></li>
                        <li>03003 Alicante (Alicante)</li>
                      </ul>
                  </div>

                  <div class="info-box">
                      <div class="icons"><i class="icon-telephone114"></i></div>
                      <ul>
                        <li><strong><?= $this->lang->line('telefono') ?></strong></li>
                        <li><a href="tel:+34865751305">+34 865 751 305</a></li>
                      </ul>                      
                  </div>

                  <div class="info-box">
                    <div class="icons"><i class="icon-icons142"></i></div>
                    <ul>
                      <li><strong>E-mail</strong></li>
                      <li><a href="mailto:info@sasi-alicante.com?subject=Web SASI, contacto">info@sasi-alicante.com</a></li>
                    </ul>
                  </div>

              </div>
          </div>


        </div>
      </div>
    </div>
</div>
  <nav class="navbar navbar-default navbar-sticky bootsnav" >
    
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="attr-nav">
            <a href="<?= base_url('lowcost') ?>" class="btn-touch btn-touch-triangle btntriangle2 uppercase">Low Cost</a>
          </div>
          <div class="attr-nav">
            <a href="<?= base_url('publica') ?>" class="btn-touch btn-touch-triangle uppercase"><?= $this->lang->line('publica') ?></a>
          </div>
          <div class="attr-nav">
            <ul class="social_share clearfix">
              <li><a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="javascript:void(0)" class="twitter"><i class="fa fa-twitter"></i></a></li>
            </ul>
          </div>
          <!-- Start Header Navigation -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
              <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand sticky_logo" href="<?= base_url() ?>">
              <img src="<?= base_url() ?>theme/theme/images/logo.png" class="logo" alt="">
            </a>
          </div>
          <!-- End Header Navigation -->
          <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav" data-in="fadeIn" data-out="fadeOut" style="float:right">
              <li>
                <a href="<?= base_url() ?>"><?= $this->lang->line('inicio') ?> </a>                
              </li>
              <li><a href="<?= base_url('empresa') ?>.html"><?= $this->lang->line('empresa') ?></a></li>                   
              <li class="visible-xs visible-sm"><a href="<?= base_url('lowcost') ?>">Low Cost</a></li>              
              <li class="visible-xs visible-sm"><a href="<?= base_url('publica') ?>"><?= $this->lang->line('publica') ?></a></li>                       
              <li><a href="<?= base_url('contacto') ?>.html"><?= $this->lang->line('contactanos') ?></a></li>              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </nav>