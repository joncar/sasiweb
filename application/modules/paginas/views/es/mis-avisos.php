<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase"><?= l('avisos') ?></h1>
        <p><?= l('avisos-text') ?></p>
        <ol class="breadcrumb text-center">
          <li><a href="<?= base_url() ?>"><?= l('inicio') ?></a></li>          
          <li class="active"><?= l('avisos') ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->



<!-- Listing Start -->
<section id="listing1" class="listing1 padding_top">
  <div class="container">    
    <div class="row bottom30">
      <div class="col-md-12 text-center">
        <h2 class="text-uppercase"><?= l('avisos') ?></h2>
      </div>
    </div>
  </div>


  <!-- Propiedades cruds list-fav --->
  <?= $output ?>
</section>
<!-- Listing end -->
<?php $this->load->view($this->theme.'_propiedades_aviso',array(),FALSE,'paginas'); ?>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>  