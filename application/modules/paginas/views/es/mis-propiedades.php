<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase">Listing Style 1</h1>
        <p>Serving you since 1999. Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
        <ol class="breadcrumb text-center">
          <li><a href="#">Home</a></li>
          <li><a href="#">Pages</a></li>
          <li><a href="#">Listing</a></li>
          <li class="active">Listing - 1</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->



<!-- Listing Start -->
<section id="listing1" class="listing1 padding_top">
  <div class="container">
    <?php $this->load->view($this->theme.'/_menu_cuenta',array('mispropiedades'=>'active')) ?>  
    <div class="row bottom30">
      <div class="col-md-12 text-center">
        <h2 class="text-uppercase">Mis propiedades</h2>
      </div>
    </div>
  </div>


  
  <div class="container  list-t-border margin_bottom">
    <div class="row bg-hover">
      <div class="my-pro-list">
        <div class="col-md-2 col-sm-2 col-xs-12">
          <img src="https://fotos6.apinmo.com/2/4269359/1-1.jpg" style="width:100%;" alt="image"/>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="feature-p-text">
            <h4>Ático</h4>
            <p>Ciudad, zona calle 08700</p>
            <span><b>Status:</b>  For Sale</span><br>
            <div class="button-my-pro-list">
              <a href="#.">130.000€</a>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12">
          <div class="select-pro-list">
            <a href="#"><i class="icon-pen2"></i></a>
            <a href="#"><i class="icon-cross"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row bg-hover bg-color-gray">
      <div class="my-pro-list">
        <div class="col-md-2 col-sm-2 col-xs-12">
          <img src="https://fotos6.apinmo.com/2/4269359/1-1.jpg" style="width:100%;" alt="image"/>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="feature-p-text">
            <h4>Ático</h4>
            <p>Ciudad, zona calle 08700</p>
            <span><b>Status:</b>  For Sale</span><br>
            <div class="button-my-pro-list">
              <a href="#.">130.000€</a>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12">
          <div class="select-pro-list">
            <a href="#"><i class="icon-pen2"></i></a>
            <a href="#"><i class="icon-cross"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row bg-hover">
      <div class="my-pro-list">
        <div class="col-md-2 col-sm-2 col-xs-12">
          <img src="https://fotos6.apinmo.com/2/629678/2-1.jpg" style="width:100%;" alt="image"/>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="feature-p-text">
            <h4>Ático</h4>
            <p>Ciudad, zona calle 08700</p>
            <span><b>Status:</b>  For Sale</span><br>
            <div class="button-my-pro-list">
              <a href="#.">130.000€</a>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12">
          <div class="select-pro-list">
            <a href="#"><i class="icon-pen2"></i></a>
            <a href="#"><i class="icon-cross"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Listing end -->
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>