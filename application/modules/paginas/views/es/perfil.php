<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>

<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase">Perfil</h1>
        <!--<p>Serving you since 1999. Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>-->
        <ol class="breadcrumb text-center">
          <li><a href="<?= base_url() ?>">Inicio</a></li>          
          <li class="active">Perfil</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->




<!-- Profile Start -->
<section id="agent-2-peperty" class="profile padding">
  <!-- propiedades/cruds/perfil --->
  <?= $output ?>

</section>
<!-- Profile end -->

<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php if(!empty($js_files)):?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
<?php endif; ?>