<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>


<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase">Viviendas por ciudades</h1>
        <p>Serving you since 1999. Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
        <ol class="breadcrumb text-center">
          <li><a href="<?= base_url() ?>">Inicio</a></li>
          <li><a href="#">Listado de poblaciones</a></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->



<section style="padding-bottom: 40px;" class="bg_light">
  <div class="container">    
  	<h2 class="uppercase" style="margin:30px 0; ">Propiedades en provincias de españa</h2>
    <?php
    $listado = $this->db->get('provincias');
    $por_columna = ceil($listado->num_rows()/4);
    ?>
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3">
        <ul class="area_search">
          <?php foreach($listado->result() as $n=>$v): ?>
          <?php if($n<$por_columna): ?>
          <li><a href="<?= base_url() ?>propiedades2.html"><i class="icon-icons74"></i><?= $v->nombre ?> (20)</a></li>
          <?php endif; ?>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <ul class="area_search">
          <?php $x = 0; foreach($listado->result() as $n=>$v):?>
          <?php if($n>$por_columna && $x<$por_columna): ?>
          <li><a href="<?= base_url() ?>propiedades2.html"><i class="icon-icons74"></i><?= $v->nombre ?> (20)</a></li>
          <?php $x++; endif;  ?>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <ul class="area_search">
          <?php $x = 0; foreach($listado->result() as $n=>$v):?>
          <?php if($n>$por_columna*2 && $x<$por_columna): ?>
          <li><a href="<?= base_url() ?>propiedades2.html"><i class="icon-icons74"></i><?= $v->nombre ?> (20)</a></li>
          <?php $x++; endif;  ?>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <ul class="area_search">
          <?php $x = 0; foreach($listado->result() as $n=>$v):?>
          <?php if($n>$por_columna*3 && $x<$por_columna): ?>
          <li><a href="<?= base_url() ?>propiedades2.html"><i class="icon-icons74"></i><?= $v->nombre ?> (20)</a></li>
          <?php $x++; endif;  ?>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
  
</section>


<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>