<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 text-center">
            <h1 class="p-white text-uppercase">POLíTICA PRIVACIDAD</h1>            
            <ol class="breadcrumb text-center">
               <li><a href="#">Inicio</a></li>               
            </ol>
         </div>
      </div>
   </div>
</section>
<!-- Page Banner End --> 
<!--Contact-->
<section>
   <div class="container">
      <div class="texto_contenido padding">
         <div role="main" class="mainbar entry-content" id="content">

    <div class="rpgdPrivacid" style="margin-bottom: 20px;">
                <p><b>Responsable:</b> SASI REAL ESTATE S.L.</p>
                <p><b>Finalidad:</b> Gestión de las solicitudes de información a través de la página web con la finalidad de prestarles servicios profesionales inmobiliarios y así facilitarles información sobre lo que solicite.</p>
        <p><b>Legitimación:</b> La legitimación se basa en el consentimiento que usted nos otorga al clicar el botón "Acepto la política de protección de datos".</p>
        <p><b>Destinatarios:</b> Sus datos no se cederán a nadie, salvo obligación legal.</p>
        <p><b>Derechos:</b> Podrá ejercitar los derechos de acceso, rectificación, supresión, oposición, portabilidad y retirada de consentimiento de sus datos personales en la dirección de correo electrónico info@sasi-alicante.com</p>
    </div>
   <table id="table1" width="100%" cellspacing="1" border="0">
   <tbody>
      <tr>
         <td align="center">&nbsp;</td>
         <td align="left">&nbsp;<br>
         <strong>Política de privacidad</strong><br>
         &nbsp;<br>
         En cumplimiento de lo dispuesto en el artículo 9 de la Ley 34/2002, de 11 de julio, de servicios de la sociedad de la información y de comercio electrónico informamos a nuestros usuarios que el titular del sitio web ubicado en el dominio <a href="http://www.sasi-alicante.com"> www.sasi-alicante.com</a>&nbsp;&nbsp;&nbsp; es SASI REAL ESTATE S.L., con domicilio social en Av. Eusebio Sempere, 22,&nbsp; &nbsp; 03003 ALICANTE (SPAIN), &nbsp;&nbsp;&nbsp; ESB66459231.<br>
         &nbsp;<br>
         Cuando el usuario facilita sus datos de carácter personal utilizando el formulario de contacto expuesto en la web o a través del envío de su CV, está autorizando expresamente a&nbsp;SASI REAL ESTATE S.L. al tratamiento de sus datos personales para poder gestionar nuestra relación con Ud.<br>
         &nbsp;<br>
         Finalidades del tratamiento de sus datos:<br>
         &nbsp;
         <ul>
            <li>- Responder a sus solicitudes y consultas sobre nuestros productos y servicios.</li>
            <li>- Gestionar su acceso al apartado privado de clientes en nuestra página web.</li>
            <li>- Mantenerle informado sobre ofertas y novedades de los productos y servicios que ofrecemos, en este caso previa obtención de su consentimiento para enviarle este tipo de comunicaciones comerciales.</li>
         </ul>
         &nbsp;<br>
         Estos datos se incluirán en un fichero titularidad de&nbsp;SASI REAL ESTATE S.L. debidamente inscrito en el RAEPD, y contará con todas las medidas de seguridad, necesarias y exigidas por la normativa vigente en materia de Protección de Datos de acuerdo con lo establecido en el Reglamento (UE) 2016/679 relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos<br>
         SASI REAL ESTATE S.L.&nbsp;&nbsp;no cederá o comunicará a terceros los datos recabados sin previo consentimiento expreso del usuario, excepto en el caso que la comunicación sea necesaria para prestarle el servicio que el usuario nos haya solicitado.<br>
         &nbsp;<br>
         El usuario responderá, en cualquier caso, de la veracidad de los datos facilitados, y de su exactitud y pertinencia, reservándose&nbsp;SASI REAL ESTATE S.L. el derecho a excluir de los servicios registrados a todo usuario que haya facilitado datos falsos, inadecuados, inexactos, sin perjuicio de las demás acciones que procedan en Derecho.<br>
         &nbsp;<br>
         El usuario podrá ejercitar sus derechos de acceso, rectificación, cancelación y oposición y demás reconocidos por Ley, dirigiéndose por escrito a&nbsp;SASI REAL ESTATE S.L. Responsable del Fichero, ubicado en la dirección facilitada en el encabezamiento o a través del email:&nbsp; <a href="mailto:info@sasi-alicante.com?subject=Modificaci%C3%B3n-Cancelaci%C3%B3n-%20GDPR">info@sasi-alicante.com</a> &nbsp;<br>
         &nbsp;</td>
         <td align="center">&nbsp;</td>
      </tr>
      <tr>
         <td colspan="3" align="center">&nbsp;</td>
      </tr>
   </tbody>
</table>
   
   

   </div>      
      </div>
   </div>
</section>
<!-- Contact End -->
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>