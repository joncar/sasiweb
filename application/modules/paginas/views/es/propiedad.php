<?php   
  $detail = $propiedad;
  $videoArray = $this->db->get_where('propiedades_videos',array('propiedades_list_id'=>$detail->propid));
  if($detail->energiarecibido==2){
    $energiaValor = l('en-tramites');
  }
  if($detail->energiarecibido==3){
    $energiaValor = l('exento');
  }
  $detail->energiavalor = empty($detail->energiavalor)?'0':$detail->energiavalor;
  $detail->emisionesvalor = empty($detail->emisionesvalor)?'0':$detail->emisionesvalor;
?>
  <header class="layout_default onMap">
    <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
    <div class="topbar topbarn hidden-xs hidden-sm dark">
      <div style="background: #053972;">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-8" style="height:100px; display: table; text-align: left;">
              <h2 id="tit" class="text-uppercase">
                <span style="color: #e31f22;"><?= 'Ref: '.$detail->ref ?></span>
                <?php echo $detail->titulo; ?>
              </h2>
            </div>
            <div class="col-md-4 text-right propiedadPrecio" style="">
              <h1 class="propiedadPrecioTop" style=" display:inline-block; margin-right:10px">
                <span class="rojo">                
                    <?php if($detail->codaccion==2): ?>
                      <?= moneda($detail->precioalq); ?>/<?= $detail->tipomensual ?>                    
                    <?php else: ?>
                      <?= moneda($detail->precio); ?>                    
                    <?php endif ?>                    
                </span>
                <div class="propiedadLinkBajo">
                  <span style="font-size:16px;">
                    <?php if(!empty($detail->outlet) && $detail->outlet > $detail->precio): ?>              
                        <?= $this->lang->line('ha-bajado') ?> <?= moneda($detail->habajado); ?>              
                    <?php else: ?>
                      &nbsp;
                    <?php endif ?>
                  </span>
                </div>

              </h1>
              <div class="campanaPropiedad">
                <a href="javascript:showAvisoSiBaja(<?= $detail->propid ?>)">
                  <i class="fa fa-bell-o"></i>
                  <span><?= $this->lang->line('avisame') ?></span>
                  <span><?= $this->lang->line('si-baja') ?></span>
                </a>
              </div>  
                         
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>



<section class="mostrarSlider propiedadFotos">
  <?php if($detail->lowcost): ?>
    <div class="lowcost">
      <span></span>
      LOW COST  
    </div>
  <?php endif ?>
  <div>
    <div class="lupa portada">
      <div style="background:url(<?= $detail->foto ?>); background-size:cover;"></div>
    </div>
    <div class="miniaturas hidden-xs hidden-sm">
      <?php 
        foreach($detail->fotos->result() as $n=>$p): if($n>0 && $n<5): 
      ?>
        <div class="lupa">
          <div style="background:url(<?= $p->foto ?>); background-size:cover;"></div>
        </div>
      <?php endif; endforeach; ?>
    </div>
  </div>
</section>

<section id="property" class="bg_light padding_bottom_half">
  <div class="container">
    <div class="row">


      <!--- Botonera --->
      <div class="col-md-12 listing1 property-details">
        <div class="row proerty_content">
          

          <div class="col-xs-12 col-sm-8 col-md-9 bottom40">
            <div class="favroute clearfix bottom20" style="border:0">          
              <ul class="pull-left propiedadBotonera">                
                <?php if(!empty($detail->tourvirtual)): ?>
                  <li>
                    <a target="_blank" href="https://ap.apinmo.com/fotosvr/tour.php?cod=<?= $detail->cod_ofer ?>.<?= $detail->sucursal ?>&o=<?= $detail->sucursal ?>" title="Tour Virtual">
                      <img src="<?= base_url('img/360.png') ?>">
                    </a>
                  </li>
                <?php endif ?>
                <?php if($videoArray->num_rows()>0): ?>
                  <li><a href="#video" title="<?= l('ver-video') ?>"><i class="icon-video2"></i> <span style="font-size: 14px;color: #6f6f6b;"><?= $detail->video; ?></span></a></li>
                <?php endif ?>
                <li style="position:relative">
                  <a title="<?= $this->lang->line('numero-de-fotos') ?>" href="<?= base_url('propiedad/'.$detail->ref) ?>" class="">
                    <i class="icon-camera3" style="font-size:16px;display: inline-block;"></i> 
                    <span style="font-size: 13px;color: rgba(227, 39, 24, 0.68);position: absolute;right: 2px;top: -14px;">
                        <?= $detail->fotos->num_rows() ?>
                    </span>
                  </a>
                </li>
                <li><a title="<?= $this->lang->line('ver-propiedad-en-el-mapa') ?>" href="#mapa2"><i class="icon-map-pin"></i></a></li>
                <li><a title="<?= $this->lang->line('ver-el-street-view') ?>" href="#mapa2" id="streetView"><i class="icon-map3"></i></a></li>
                <li><a title="<?= $this->lang->line('anadir-a-favoritos') ?>" href="javascript:addFav('<?= $detail->cod_ofer ?>')"><i class="icon-like fav<?= $detail->cod_ofer ?>"></i></a></li>
                <li><a title="<?= $this->lang->line('ocultar-propiedad') ?>" href="javascript:removeProp('<?= $detail->cod_ofer ?>')"><i class="icon-trash-can" style="font-size: 23px;"></i></a></li>                
                <li><a title="<?= $this->lang->line('contactar-con-sasi') ?>" href="#saberSobre" class="share_expender"><i class="icon-envelope"></i></a></li>
                <li><a title="<?= l('imprimir') ?>" href="javascript:printe()" class="share_expender"><i class="icon-printer2" style="font-size:23px;"></i></a></li>                
              </ul>
              <div class="pull-left botonamarillo propBotonAmarillo">                
                  <a href="javascript:history.back();" style="margin: 0 30px;"><i class="fa fa-arrow-left"></i> <?= l('volver-al-listado') ?></a>                                  
              </div>

              <ul class="pull-right botonesNextPrev">                  
                <?php if(!empty($prev)): ?>
                  <li class="beforeBtn"><a style="width:100%" title="Anterior propiedad" href="<?= base_url('propiedad/'.$prev) ?>"><i class="fa fa-arrow-left" style="font-size:14px;"></i> <small><?= l('anterior-propiedad') ?></small></a></li>
                <?php endif ?>
                <?php if(!empty($next)): ?>
                  <li class="afterBtn"><a style="width:100%" title="Siguiente propiedad" href="<?= base_url('propiedad/'.$next) ?>"><small><?= l('siguiente-propiedad') ?></small> <i class="fa fa-arrow-right" style="font-size:14px;"></i></a></li>
                <?php endif ?>
              </ul>  
            </div>

            <p  style="font-size: 20px;"><i class="fa fa-map-marker"></i> <?= $detail->provincia.', '.$detail->ciudad.' en '.$detail->zona ?></p>
            <div class="bottom80" style="font-size: 18px;">
              <span style="margin-right: 30px"><?= $detail->m_uties ?>m<sup>2</sup></span>
              <span style="margin-right: 30px"><?= $detail->total_hab ?> <?= l('hab') ?></span>
              <span style="margin-right: 30px"><?= $detail->banyos ?>  <?= l('banos') ?></span>
              <span><?= number_format($detail->precio_m_uties,0,',','.') ?> €/m<sup>2</sup></span> 
            </div>
            <div class="bottom20"></div>
            <div style="text-align:justify;"><?= str_replace('~','<br/><br/>',$detail->descrip) ?></div>
          </div>
          <!--- Cuadro subscribe --->
          <aside class="col-md-3 col-sm-4 col-xs-12 newsletterlist" style="margin-bottom: 0">            
            <p class="topper" style="width:100%;"><?= l('recibe-alertas-por-email-de-anuncions-similares') ?></p>
            <div style="background: white; padding:10px;">
              
              <form action="propiedades/account/busquedas" onsubmit="return sendForm(this,'#responseBusqueda')" style="border:2px solid black; padding:10px;">
                  <div class="form-group">
                    <div class="intro">
                      <input type="email" name="email" class="form-control" placeholder="<?= l('escribe-tu-email') ?>" value="<?= @$_SESSION['email'] ?>">
                    </div>
                    <input type="checkbox" name="politicas" value="1"> <span style="font-size:13px"><?= l('acepto-las-politicas-de-privacidad') ?></span>
                    <input type="hidden" name="string" value="<?= 'provincias_id-'.$detail->provincias_id.'-comarcas_id-'.$detail->comarcas_id.'-ref-'.str_replace('-','_',$detail->ref)  ?>">
                  </div>
                  <div id="responseBusqueda"></div>
                  <div>
                    <button type="submit" class="btn btn-primary btn-block"><?= l('guardar') ?></button>
                  </div>
              </form>      

            </div>
          </aside>
          <!--- Fin Cuadro subscribe --->
          
        </div>
      </div>
      <!--- Fin Botonera --->

      


    </div>
    <div class="padding_top"></div>

        
        
        
        



        
        <div class="row property-d-table bottom40">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <h2 class="text-uppercase bottom20"><?= l('caracteristicas') ?></h2>
            <table class="table table-striped table-responsive">
              <tbody>                
                <tr>
                  <td><b><?= l('referencia') ?></b></td>
                  <td class="text-right"><?= $detail->ref ?></td>
                </tr>
                <tr>
                  <td><b><?= l('precio') ?></b></td>
                  <td class="text-right">
                    <?php if($detail->codaccion==2): ?>
                      <?= moneda($detail->precioalq); ?>/<?= $detail->tipomensual ?>                    
                    <?php else: ?>
                      <?= moneda($detail->precio); ?>                    
                    <?php endif ?>
                  </td>
                </tr>
                <tr>
                  <td><b><?= l('m2-utiles') ?></b></td>
                  <td class="text-right"><?= $detail->m_uties ?> m<sup>2</sup></td>
                </tr>
                <tr>
                  <td><b><?= l('habitaciones') ?></b></td>
                  <td class="text-right"><?= $detail->total_hab ?></td>
                </tr>

                <tr>
                  <td><b><?= l('banos') ?></b></td>
                  <td class="text-right"><?= $detail->banyos ?></td>
                </tr>

                <tr>
                  <td><b><?= l('accion') ?></b></td>
                  <td class="text-right"><?= l('acciones_'.strtolower($detail->acciones)) ?></td>
                </tr>
                <tr>
                  <td><b><?= l('ano-de-construccion') ?></b></td>
                  <td class="text-right"><?= $detail->antiguedad ?></td>
                </tr>
                <tr>
                  <td><b><?= l('nivel-de-conservacion') ?></b></td>
                  <td class="text-right"><?= l('cons_'.$detail->nbconservacion) ?></td>
                </tr>
                <tr>
                  <td><b><?= l('tipo-propiedad') ?></b></td>
                  <td class="text-right"><?= l('tipo_'.mb_strtolower($detail->nbtipo)) ?></td>
                </tr>
                <tr>
                  <td><b><?= l('orientacion') ?></b></td>
                  <td class="text-right"><?= $detail->nborientacion ?></td>
                </tr>
                <tr>
                  <td><b><?= l('disponible-desde') ?></b></td>
                  <td class="text-right"><?= date("d/m/Y",strtotime($detail->fechacreacion)) ?></td>
                </tr>
                            
              </tbody>
            </table>
          </div>
          <div class=" col-sm-offset-1 col-sm-5 col-xs-12">
              <?php 
                      $this->db->select('propiedades_extras.*');
                      $this->db->join('propiedades_extras','propiedades_extras.id = propiedades_list_extras.propiedades_extras_id');
                      $prop = $this->db->get_where('propiedades_list_extras',array('propiedades_list_id'=>$detail->propid));
                      if($prop->num_rows()>0): 
                ?>
                  <h2 class="text-uppercase bottom20"><?= l('caracteristicas-generales') ?></h2>
                  <div class="detalleGeneral property-details row" style="margin-left:0; margin-right:0">
                      
                        <?php                 
                          foreach($prop->result() as $e): ?>
                            <ul class="pro-list col-xs-12">
                              <li> <?= l('extra_'.$e->codigo) ?></li>
                            </ul>
                        <?php endforeach ?>               
                          
                  </div>
                <?php endif ?>
                
          </div>
        </div>
        
        <?php if($videoArray->num_rows()>0): ?>
        <?php 
          
          if($videoArray->num_rows()>0):  $video = $videoArray->row();
        ?>
          <div class="padding_top"></div>
          <h2 class="text-uppercase bottom20" id="video"><?= l('video-presentacion') ?></h2>
          <div class="row bottom40" style="height:300px;">
            <div class="col-md-12 padding-b-20">
              <div class="pro-video">                  
                <iframe width="100%" height="515" src="https://www.youtube.com/embed/<?= $video->video ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="width:100%; height:300px;"></iframe>
              </div>
            </div>
          </div>
          <div class="padding_top"></div>
        <?php endif ?>
      <?php endif ?>




        
        <?php if(!empty($detail->altitud)): ?>
            <h2 class="text-uppercase bottom20" id="mapa2" style="padding-top:140px; margin-top:-140px; position:relative;">
              <?= l('ubicacion-geografica') ?>
            </h2>
            <div class="row bottom40">
              <div class="col-xs-12 col-md-12">
                <div class="property-list-map">
                  <div id="property-listing-map2" 
                      class="multiple-location-map" style="width:100%;height:100%;"
                      data-lat="<?= str_replace(',','.',$detail->latitud) ?>"
                      data-lon="<?= str_replace(',','.',$detail->altitud) ?>"
                      data-zoom="16"
                  >
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-12 social-networks">
                <div class="cercanias">                  
                  <label for="cercania1"><img src="<?= base_url('img/iconosMapa/school.png') ?>" style="width:20px; vertical-align: top"> <?= l('colegios') ?> <input type="checkbox" class="cercaniasProp" id="cercania1" value="school"> </label>
                  <label for="cercania2"> <img src="<?= base_url('img/iconosMapa/subway_station.png') ?>" style="width:20px; vertical-align: top"> <?= l('metro') ?> <input type="checkbox" class="cercaniasProp" id="cercania2" value="subway_station"></label>
                  <label for="cercania3"> <img src="<?= base_url('img/iconosMapa/bus_station.png') ?>" style="width:20px; vertical-align: top"> <?= l('autobus') ?> <input type="checkbox" class="cercaniasProp" id="cercania3" value="bus_station"></label>
                  <label for="cercania4"> <img src="<?= base_url('img/iconosMapa/supermarket.png') ?>" style="width:20px; vertical-align: top"> <?= l('supermercados') ?> <input type="checkbox" class="cercaniasProp" id="cercania4" value="supermarket"></label>
                  <label for="cercania5"> <img src="<?= base_url('img/iconosMapa/hospital.png') ?>" style="width:20px; vertical-align: top"> <?= l('hospitales') ?> <input type="checkbox" class="cercaniasProp" id="cercania5" value="hospital"></label>
                  <label for="cercania6"> <img src="<?= base_url('img/iconosMapa/pharmacy.png') ?>" style="width:20px; vertical-align: top"> <?= l('farmacias') ?> <input type="checkbox" class="cercaniasProp" id="cercania6" value="pharmacy"></label>
                  <label for="cercania7"> <img src="<?= base_url('img/iconosMapa/gas_station.png') ?>" style="width:20px; vertical-align: top"> <?= l('gasolineras') ?> <input type="checkbox" class="cercaniasProp" id="cercania7" value="gas_station"></label>
                  <label for="cercania8"> <img src="<?= base_url('img/iconosMapa/park.png') ?>" style="width:20px; vertical-align: top"> <?= l('parques') ?> <input type="checkbox" class="cercaniasProp" id="cercania8" value="park"></label>
                </div>
              </div>
            </div>
        <?php endif ?>

        <!-- Property Detail Start -->
        <section id="property" class="cuadros3 bg_light padding_bottom_half top30">
          
            <div class="row">
              <div class="col-md-12 listing1 property-details">
                <div class="property_meta bg-black bottom40">
                  <span><i class="icon-creditcards"></i> <?= l('precio-del-inmueble') ?>: <b><?= moneda($detail->precio) ?> </b></span>
                  <span><i class="icon-calculator2"></i> <?= l('precio-m2-del-inmueble') ?>: <b><?= number_format($detail->precio_m_uties,0,',','.') ?> €/m<sup>2</sup></b></span>
                  <span><i class="icon-calculator2"></i> <?= l('precio-m2-de-la-zona') ?>:
                  <b><?php 
                    echo moneda($this->elements->precioM2XZona($detail->zonas_id,$detail->key_tipo));
                  ?>  
                  /m<sup>2</sup></b></span>
                </div>
              </div>
            </div>
          
        </section>
  

        <div class="row bottom40">
          <div class="col-md-6 col-sm-6 bottom40">
            <h2 class="bottom30"><?= l('calcular-hipoteca') ?></h2>
            <table class="calculadoraHipoteca table table-striped table-responsive">
              <tbody>
                <tr>
                  <td style="width: 80%;"><b><?= l('precio-del-inmueble') ?></b></td>
                  <td class="text-right"><?= moneda($detail->precio) ?></td>
                </tr>
                <tr>
                  <td><b><?= l('impuestos-y-gastos') ?></b></td>
                  <td class="text-right">
                      <?php
                        $impuestos = $detail->precio*0.13;
                        echo moneda($impuestos);
                      ?>
                  </td>
                </tr>
                <tr>
                  <td><b><?= l('precio-i-gastos') ?></b></td>
                  <td class="text-right"><?= 
                    moneda($detail->precio+$impuestos);
                  ?></td>
                </tr>

                <tr>
                  <td style="vertical-align: middle;"><b><?= l('ahorro-aportado') ?></b></td>
                  <td>
                    <input type="number" class="ahorro form-control" value="30.000€">
                  </td>
                </tr>
                <tr>
                  <td style="vertical-align: middle;"><b><?= l('plazo-en-anos') ?></b></td>
                  <td>
                    <input type="number" class="plazo form-control" value="30">
                  </td>
                </tr>
                <tr>
                  <td style="vertical-align: middle;"><b><?= l('tipo-de-interes') ?></b></td>
                  <td>
                    <input type="hidden" class="precioTotal" value="<?= $detail->precio+$impuestos ?>">
                    <input type="number" class="interes form-control" value="3">
                  </td>
                </tr>                
                <tr>
                  <td><b><?= l('cuota-mensual') ?></b></td>
                  <td class="text-right" style="font-size:18px"><b id="totalCostoEstimado">0€</b></td>
                </tr>
                <tr>
                  <td colspan="2">
                    <small><?= l('nota-costes-hipoteca') ?></small>
                  </td>
                </tr>
                <tr>
                  <td style="vertical-align: middle">                    
                    <div class="botonesRojosPropiedad">
                      <a href="#informarErrorModal" data-toggle="modal" class="btn btn-danger"><i class="fa fa-exclamation"></i> <?= l('informar-error') ?> </a>
                      <a href="#proponerPrecioModal" data-toggle="modal" class="btn btn-danger"><i class="fa fa-money"></i> <?= l('proponer-un-precio') ?></a>
                    </div>                    
                  </td>
                  <td class="text-right" style="font-size:18px; vertical-align: middle">
                    <div>
                      <a href="javascript:limpiarcalculo()" class="btn-blue uppercase border_radius" style="padding: 8px 35px; overflow: inherit;"><?= l('reiniciar') ?> </a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            
            
            
          </div>
          <div class="col-xs-11 col-lg-5 col-lg-offset-1 col-md-6 col-sm-6" style="position:relative;">
            <h2 class="bottom30"><?= l('certificacion-energetica') ?></h2>
            <!--<img src="<?= base_url() ?>theme/theme/images/certificacionEnergetica.png">-->
            <?php
              if(!empty($energiaValor)): 
            ?>
              <div class="tramiteDiv"><?= $energiaValor ?></div>
            <?php endif ?>
            <table class="tableene <?= !empty($energiaValor)?l('tramite'):'' ?>">
              <thead>
                <tr>
                  <th  style="width:30%"><?= l('calificacion-energetica') ?></th>
                  <th  style="width:15%; text-align:center"><?= l('consumo') ?> <br/><small>KW h/m<sup>2</sup> <?= l('anio') ?></small></th>
                  <th  style="width:15%; text-align:center"><?= l('emisiones') ?> <br/><small>Kg CO<sup>2</sup>/m<sup>2</sup> <?= l('anio') ?></small></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><div class="ene enea">A</div></td>
                  <td class="valorenergia"><span><?= $detail->energialetra=='A'?$detail->energiavalor:'' ?></span></td>
                  <td class="valorenergia"><span><?= $detail->emisionesletra=='A'?$detail->emisionesvalor:'' ?></span></td>
                </tr>
                <tr>
                  <td><div class="ene eneb">B</div></td>
                  <td class="valorenergia"><span><?= $detail->energialetra=='B'?$detail->energiavalor:'' ?></span></td>
                  <td class="valorenergia"><span><?= $detail->emisionesletra=='B'?$detail->emisionesvalor:'' ?></span></td>
                </tr>
                <tr>
                  <td><div class="ene enec">C</div></td>
                  <td class="valorenergia"><span><?= $detail->energialetra=='C'?$detail->energiavalor:'' ?></span></td>
                  <td class="valorenergia"><span><?= $detail->emisionesletra=='C'?$detail->emisionesvalor:'' ?></span></td>
                </tr>
                <tr>
                  <td><div class="ene ened">D</div></td>
                  <td class="valorenergia"><span><?= $detail->energialetra=='D'?$detail->energiavalor:'' ?></span></td>
                  <td class="valorenergia"><span><?= $detail->emisionesletra=='D'?$detail->emisionesvalor:'' ?></span></td>
                </tr>
                <tr>
                  <td><div class="ene enee">E</div></td>
                  <td class="valorenergia"><span><?= $detail->energialetra=='E'?$detail->energiavalor:'' ?></span></td>
                  <td class="valorenergia"><span><?= $detail->emisionesletra=='E'?$detail->emisionesvalor:'' ?></span></td>
                </tr>
                <tr>
                  <td><div class="ene enef">F</div></td>
                  <td class="valorenergia"><span><?= $detail->energialetra=='F'?$detail->energiavalor:'' ?></span></td>
                  <td class="valorenergia"><span><?= $detail->emisionesletra=='F'?$detail->emisionesvalor:'' ?></span></td>
                </tr>
                <tr>
                  <td><div class="ene eneg">G</div></td>
                  <td class="valorenergia"><span><?= $detail->energialetra=='G'?$detail->energiavalor:'' ?></span></td>
                  <td class="valorenergia"><span><?= $detail->emisionesletra=='G'?$detail->emisionesvalor:'' ?></span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>


</div>
</div>
</div>
</section>
<section class="ocultoPrint padding_top padding_bottom_half" id="saberSobre">
  <div class="container">
    <div class="row">
      <div class="col-md-12 listing1 property-details">

        
  
        <div class="row">
            
            <div class="col-xs-12 col-md-9 bottom40">
              <h2 class="text-uppercase bottom20"><?= l('solicitar-informacion-a-sasi') ?></h2>
              <p><b><?= l('referencia-del-anuncio-ref',array('ref'=>$detail->ref)) ?></b></p>
              <?php $this->load->view('_solicitar_informacion'); ?>
            </div>
            <div class="col-xs-12 col-md-3 p0">

                  <div class="col-xs-12 col-sm-4 col-md-12">
                    <div class="bottom40">
                      <img src="<?= base_url() ?>theme/theme/images/logo.png" class="logo" alt="">
                    </div>
                    <div class="bottom40" style="background: white;">
                      <div>
                        <a href="tel:<?= $detail->telefono_contacto ?>" class="btn btn-danger btn-block"><?= l('llamar') ?></a>
                      </div>    
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-8 col-md-12">
                    <div class="col-xs-12 col-sm-6 col-md-12 agetn-contact-2 bottom30">
                       <p><i class="icon-telephone114"></i> <?= $detail->telefono_contacto ?></p>
                       <p><i class=" icon-icons142"></i> <?= $detail->email_contacto ?></p>
                       
                       <p><i class="icon-browser"></i> www.finques-sasi.com</p>
                       <p><i class="icon-icons74"></i> <?= $detail->direccion_contacto ?></p>
                    </div>
                    <div  class="col-xs-12 col-sm-6 col-md-12">
                      <div>
                        <img src="<?= base_url('img/logo-api.png') ?>" style="width:120px" alt="">
                      </div>
                      <div class="top10" style="font-size:12px">
                        <?= l('registro-de-agentes-inmobiliarios') ?>
                      </div>
                    </div>
                  </div>

            </div>
            
        </div>
      </div>
    </div>
  </div>
</section>





<!--------- MODALES --------->
<?php $this->load->view($this->theme.'_modal_informar_error',array('detail'=>$detail),FALSE,'paginas'); ?>
<?php $this->load->view($this->theme.'_modal_proponer_precio',array('detail'=>$detail),FALSE,'paginas'); ?>
<!--------- FIN MODALES --------->

<?php

  $this->db->limit(4);
  $this->db->where('ciudades_id',$detail->ciudades_id);
  $this->db->where('propiedades_list.cod_ofer !=',$detail->cod_ofer);
  $prop = $this->querys->get_propiedades();
  if($prop->num_rows()>0):

?>
<section class="ocultoPrint bg_light padding_top padding_bottom_half">
  <div class="container">
    <div class="row index2">
      <div class="col-xs-12 col-md-12 bottom40">
            <h2 class="text-uppercase top20"><?= l('propiedades-recomendadas'); ?></h2>          
          </div>

          <?php             
            foreach($prop->result() as $p): 
          ?>
             <div class="col-md-3 col-xs-12 col-sm-6">
                <?php $this->load->view('_propiedad',array('detail'=>$p)); ?>
            </div>
          <?php endforeach ?>
        </div>
      
    </div>
  </div>
</section>
<?php endif ?>
<!-- Property Detail End -->

<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=drawing,geometry,places"></script> 
<script src="<?= base_url() ?>theme/theme/js/google-map.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src='<?= base_url() ?>js/lightgallery/js/lightgallery.js'></script>
<script>
  function printe(){
    window.print();
  }
  var fotos = <?php 
    $fotos = array();
    foreach($detail->fotos->result() as $p){
      $fotos[] = array('src'=>$p->foto,'thumb'=>$p->foto,'title'=>'Hola');
    }
    echo json_encode($fotos);
  ?>;

  var prevScrollpos = window.pageYOffset;
  window.onscroll = function() {
    if($(".bootsnav2").css('position')=='fixed'){
      var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        $(".bootsnav2").css('top',"0px");
      } else {
        $(".bootsnav2").css('top',"-50px");
      }
      prevScrollpos = currentScrollPos;
    }
  } 

  $(".ahorro,.plazo,.interes").on('change',function(){calcularPrecioEstimado()});
  function calcularPrecioEstimado(){
    var ahorro = parseFloat($(".ahorro").val());
    var plazos = parseFloat($(".plazo").val());
    plazos*=12;
    var interes = parseFloat($(".interes").val())/12;    
    var precio = parseFloat($(".precioTotal").val())-ahorro;
    var $I = interes/100 ;
    var $I2 = $I + 1 ;
    $I2 = Math.pow($I2,-plazos) ;
    var total = ($I * precio) / (1 - $I2) ; 
    total = total.toFixed(2);
    $("#totalCostoEstimado").html(total+'€');
  }

  function limpiarcalculo(){
    $(".ahorro").val('');
    $(".plazo").val('30');
    $(".interes").val('3');
    $("#totalCostoEstimado").html('0€');
  }
</script>
<script>
  fillImages();
</script>