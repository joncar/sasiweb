<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="" style="background: #173471; padding-top:40px;">
  <div class="container">
    <div class="row ">
      <aside class="col-md-3 callus col-xs-12 newsletterlist">
        <p class="topper">Recibe alertas por email de anuncions similares</p>
        <div style="background: white; padding:10px;">
          <form action="<?= base_url() ?>propiedades.html" style="border:2px solid black; padding:10px;">
              <div class="form-group">
                <div class="intro">
                  <input type="email" class="form-control" placeholder="Escribe tu email">
                </div>
                <input type="checkbox" name="politicas" value="1"> <span style="font-size:13px">Acepto las politícas de privacidad</span>
              </div>
              <div>
                <a href="#" class="btn btn-primary btn-block">Guardar</a>
              </div>
          </form>      
        </div>
      </aside>
      <div class="col-md-9 col-xs-12">
        <div class="row headerPropiedadList">
            <div class="col-xs-4 col-md-2">
              <div class="single-query form-group">
                <div class="intro">
                  <select>
                    <option class="active">Venta</option>
                    <option>Todas</option>
                    <option>Comprar </option>
                    <option>Alquiler</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="col-xs-4 col-md-2">
              <div class="single-query form-group">
                <div class="intro">
                  <select>
                    <option class="active">Viviendas</option>
                    <option>Todas</option>
                    <option>Comprar </option>
                    <option>Alquiler</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="col-xs-4 col-md-2">
              <div class="single-query form-group">
                <div class="intro">
                  <select>
                    <option class="active">Barcelona</option>
                    <option>Todas</option>
                    <option>Comprar </option>
                    <option>Alquiler</option>
                  </select>
                </div>
              </div>
            </div>
        </div>
        <div class="row" style="color:white; margin-top: 30px;">
          <div class="col-xs-12">
            <h1>Viviendas en barcelona</h1>
          </div>
        </div>
        <div class="row" style="color:white; margin-top:10px;  padding: 17px 0;">
          <div class="col-xs-4 col-md-2">
            <a href="#">Comprar</a>
          </div>
          <div class="col-xs-4 col-md-2">
            <a href="#">Alquiler</a>
          </div>
          <div class="col-xs-4 col-md-2">
            <a href="#">Obra nueva</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->

<!-- Property Search area Start -->
<section class="bg_light" style="">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-9 col-md-offset-3" style="margin-bottom: 40px;">
        <div class="row" style="background: #ffffff; padding:20px; margin-left: 0px; margin-right: 0px;">
          <div class="col-xs-6 col-md-2">
            <a title="Mapa" href="<?= base_url('propiedades') ?>" style="margin-left:10px; font-size:18px"><i class="fa fa-th-large"></i></a>
            <a title="Listado" href="<?= base_url('propiedades-en-list') ?>" style="margin-left:10px; font-size:18px"><i class="fa fa-list"></i></a>
            <a title="Cuadricula" href="<?= base_url('propiedades-en-mapa') ?>" style="margin-left:10px; font-size:18px"><i class="fa fa-map-marker"></i></a>
          </div>
          <div class="col-xs-6 col-md-7" style="text-align: right">
            <span class="hidden-xs" style="margin-right: 70px;">10-20 de 100 resultados</span>            
          </div>
          <div class="col-xs-12 col-md-3">
            <div class="single-query accionesBar">
              <div class="col-xs-3" style="align:right">
                Por 
              </div>
              <div class="intro col-xs-9" style="margin-top: -12px; padding: 0; margin-bottom: -22px;">                
                <select>
                  <option class="active">Acción</option>
                  <option>Todas</option><option>Comprar </option>
                  <option>Alquiler</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Property Search area End -->
<!-- Listing Start -->

<section id="listing1" class="listing1 bg_light">
  <div class="container">
    <div class="row">
      




      <?php $this->load->view($this->theme.'_propiedades_aside',array()); ?>







      <div class="col-md-9 col-sm-12 col-xs-12">
        



        <!------ Destacados -------->
        <?php $this->load->view($this->theme.'_propiedades_destacadas'); ?>
        <!---- Fin destacados ----->




        <div id="deals" class="row" style="margin-top:40px;">
          <?php 
            $this->db->limit(8);
            $prop = $this->querys->get_propiedades();
            foreach($prop->result() as $p): 
          ?>               
          <?php $this->load->view('_propiedad_list',array('detail'=>$p)); ?>
          <?php endforeach ?>
        </div>
        
        <div class="padding_bottom text-center padding_top">
          <ul class="pager">
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
          </ul>
        </div>
      </div>
      
    </div>
  </div>
</section>
<!-- Listing end -->
<?php $this->load->view($this->theme.'_propiedades_aviso'); ?>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>