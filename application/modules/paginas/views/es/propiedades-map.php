<div class="mapContent">
  <header class="layout_default onMap">
    <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
  </header>





<section style="position: relative;">
  <div id="property-listing-map" data-lat="<?= $center[0] ?>" data-lon="<?= $center[1] ?>" data-zoom="<?= $center[2] ?>" data-region="<?= @$datos['provincias_id'] ?>" data-comarca="<?= @$datos['comarcas_id'] ?>" data-ciudad="<?= @$datos['ciudades_id'] ?>" class="multiple-location-map"></div>
  <div id="pano"></div>
</section>
<!-- Property Search area Start -->
<section class="bg_light" style="">
  <div class="container">
    <div class="row">
      
    </div>
  </div>
</section>
<!-- Property Search area End -->
<!-- Listing Start -->

<section id="listing1" class="listing1 mapaPropiedadList">
  
    <div class="" style="margin-left:0; margin-right: 0; ">
      




      
      <aside class="col-md-2 col-xs-12 bg_light filterMap">
        <div class="clearfix">
            <a href="javascript:slideFilters()" class="visible-xs visible-sm" style="position: absolute;right: 30px;top: 24px;font-size: 22px;"><i class="fa fa-times"></i></a>
            <div style="margin:20px 0">
              <h3><?= l('inmuebles-en') ?></h3>
              <h3><?= ucfirst($lugar) ?></h3>
            </div>
          <form action="<?= base_url('propiedades/frontend/rutear') ?>" id="filtros">

            

            <div class="form-group single-query">
            <label class="text-uppercase bottom10 top15"><?= l('provincia') ?></label>
              <div class="intro">                
                <select name="provincias_id" class="provincia">                  
                  <option value=""><?= l('todas') ?></option>
                  <?php 
                      $this->db->select('id,UPPER(nombre) as nombre');
                      $this->db->order_by('nombre','ASC');
                      foreach($this->db->get('provincias')->result() as $p): 
                  ?>
                      <option <?= @$_GET['provincias_id']==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
                  <?php endforeach ?>
                  
                </select>
              </div>
            </div>

            <div class="form-group single-query">
            <label class="text-uppercase bottom10 top15"><?= l('comarca') ?></label>
              <div class="intro">                
                <select name="comarcas_id" class="comarca">                  
                  <option value=""><?= l('todas') ?></option>
                  <?php 
                      $this->db->select('comarcas.id,UPPER(comarcas.nombre) as nombre');
                      if(!empty($_GET['provincias_id'])){$this->db->join('provincias','provincias.id = comarcas.provincias_id'); $this->db->where('provincias.id',$_GET['provincias_id']);}
                      $this->db->order_by('comarcas.nombre','ASC');
                      if(empty($_GET['provincias_id'])){
                        $this->db->limit(5);
                      }
                      foreach($this->db->get('comarcas')->result() as $p): 
                  ?>
                      <option <?= @$_GET['comarcas_id']==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
                  <?php endforeach ?>
                  
                </select>
              </div>
            </div>

            <div class="form-group single-query">
            <label class="text-uppercase bottom10 top15"><?= l('ciudad') ?></label>
              <div class="intro">                
                <select name="ciudades_id" class="ciudad">                  
                  <option value=""><?= l('todas') ?></option>
                  <?php 
                      $this->db->select('ciudades.id,UPPER(ciudades.ciudad) as nombre');
                      if(!empty($_GET['comarcas_id'])){$this->db->join('comarcas','comarcas.id = ciudades.comarcas_id'); $this->db->where('comarcas.id',$_GET['comarcas_id']);}
                      $this->db->order_by('ciudades.ciudad','ASC');
                      if(empty($_GET['comarcas_id'])){
                        $this->db->limit(5);
                      }
                      foreach($this->db->get('ciudades')->result() as $p): 
                  ?>
                      <option <?= @$_GET['ciudades_id']==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
                  <?php endforeach ?>
                  
                </select>
              </div>
            </div>
            

            <div class="form-group single-query">
            <label class="text-uppercase bottom10 top15"><?= l('zona') ?></label>
              <div class="intro">              
                <select name="zonas_id" class="zona">                  
                  <option value=""><?= l('todas') ?></option>
                  <?php 
                      $this->db->select('zonas.id,UPPER(zonas.nombre) as nombre');
                      if(!empty($_GET['ciudades_id'])){$this->db->join('ciudades','ciudades.id = zonas.ciudades_id'); $this->db->where('ciudades.id',$_GET['ciudades_id']);}
                      $this->db->order_by('zonas.nombre','ASC');
                      if(empty($_GET['ciudades_id'])){
                        $this->db->limit(5);
                      }
                      foreach($this->db->get('zonas')->result() as $p): 
                  ?>
                      <option <?= @$_GET['zonas_id']==$p->id?'selected="selected"':'' ?> value="<?= $p->id ?>"><?= $p->nombre ?></option>
                  <?php endforeach ?>
                  
                </select>
              </div>
            </div>


              <div class="form-group single-query">
                  <label class="text-uppercase bottom10 top15" style=" padding:0"><?= l('precio') ?></label>
                    
                  <div class="intro">
                    <select name="precio_min">
                      <option selected="" value=""><?= l('minimo') ?></option>
                      <?php for($i=60000;$i<240000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 30000; endfor; ?>                          
                      <?php for($i=240000;$i<=420000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 60000; endfor; ?>                          
                      <?php for($i=500000;$i<=1000000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 100000; endfor; ?>                          
                      <?php for($i=1500000;$i<=2000000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_min']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 500000; endfor; ?>                          
                      <option value="3000000">3.000.000€</option>
                    </select>
                  </div>
                </div>

                <div class="form-group single-query">
                  <div class="intro">
                    <select name="precio_max">
                      <option selected="" value=""><?= l('maximo') ?></option>
                      <?php for($i=60000;$i<240000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 30000; endfor; ?>                          
                      <?php for($i=240000;$i<=420000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 60000; endfor; ?>                          
                      <?php for($i=500000;$i<=1000000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 100000; endfor; ?>                          
                      <?php for($i=1500000;$i<=2000000;): ?>
                        <option value="<?= $i ?>"  <?= (@$_GET['precio_max']==$i)?'selected="selected"':'' ?>><?= number_format($i,0,',','.') ?>€</option>
                      <?php $i+= 500000; endfor; ?>                          
                      <option value="3000000">3.000.000€</option>
                    </select>
                  </div>
                </div>

            <div class="form-group single-query" style=" padding:0">
            <label class="text-uppercase bottom10 top15"><?= l('superficie') ?></label>
              <div class="intro">
                <select name="m_uties">
                  <option selected="" value=""><?= l('todas') ?></option>
                  <?php for($i=50;$i<100;): ?>
                    <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
                  <?php $i+= 10; endfor; ?>                          
                  <?php for($i=100;$i<200;): ?>
                    <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
                  <?php $i+= 25; endfor; ?>                          
                  <?php for($i=200;$i<300;): ?>
                    <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
                  <?php $i+= 50; endfor; ?>                          
                  <?php for($i=300;$i<=500;): ?>
                    <option value="<?= $i ?>" <?= (@$_GET['m_uties']==$i)?'selected="selected"':'' ?>>Desde <?= $i ?>m<sup>2</sup></option>
                  <?php $i+= 100; endfor; ?>                                            
                </select>
              </div>
            </div>
            

            <div class="form-group single-query" style=" padding:0">
            <label class="text-uppercase bottom10 top15"><?= l('habitaciones') ?></label>
              <div class="intro">
                <select name="total_hab">
                  <option value="0">+0</option>
                  <option value="1">+1</option>
                  <option value="2">+2</option>
                  <option value="3">+3</option>
                  <option value="4">+4</option>
                  <option value="5">+5</option>
                </select>
              </div>
            </div>

            <div class="form-group checkboxs" style=" padding:0">
              <label class="text-uppercase bottom10 top15"><?= l('tipos-de-inmuebles') ?></label>
              <div>
                <?php 
                  $this->db->group_by('codigo_tipo');
                  foreach($this->db->get_where('tipos')->result() as $t): ?>
                  <label for="tipo<?= $t->codigo_tipo ?>">
                    <input type="checkbox" <?= @$t->codigo_tipo==@$_GET['codigo_tipo'] || @in_array($t->codigo_tipo,$_GET['codigo_tipo'])?'checked':'' ?> id="tipo<?= $t->codigo_tipo ?>" name="codigo_tipo[]" value="<?= $t->codigo_tipo ?>"> <?= l('tipo_'.mb_strtolower($t->label)) ?>                    
                  </label>
                <?php endforeach ?>
              </div>
            </div>
            

            <div class="form-group checkboxs" style=" padding:0">
              <label class="text-uppercase bottom10 top15"><?= l('mas-caracteristicas') ?></label>
              <div>
                <?php foreach($this->db->get_where('propiedades_extras',array('mostrar_en_filtros'=>1))->result() as $t): ?>
                  <label for="caract1">
                    <?php $cod = str_replace('_','-',$t->codigo); ?>
                    <input type="checkbox" name="propiedades_extras_id[]" id="caract<?= $t->codigo ?>" value="<?= $t->codigo ?>"  <?= @$cod==@$_GET['propiedades_extras_id'] || @in_array($cod,$_GET['propiedades_extras_id'])?'checked="checked"':'' ?> >
                      <?= l('extra_'.$t->codigo) ?>
                  </label>
                <?php endforeach ?>
              </div>
            </div>

            <input type="hidden" name="acciones" value="<?= @$_GET['acciones'] ?>">
            <input type="hidden" name="areas_id" value="<?= @$_GET['areas_id'] ?>">
            <input type="hidden" name="distritos_id" value="<?= @$_GET['distritos_id'] ?>">
            <input type="hidden" name="grupos_favoritos_id" value="<?= @$_GET['grupo'] ?>">
            <input type="hidden" name="favoritos" value="<?= @$_GET['favoritos'] ?>">
            <input type="hidden" name="avisos" value="<?= @$_GET['avisos'] ?>">
            <input type="hidden" name="lowcost" value="<?= @$_GET['lowcost'] ?>">

            <input type="hidden" name="obranueva" value="<?= @$_GET['obranueva'] ?>">
            <input type="hidden" name="avisos" value="<?= @$_GET['avisos'] ?>">
            <input type="hidden" name="referencia" value="<?= @$_GET['referencia'] ?>">
            <input type="hidden" name="buscar_por" value="mapa">

            <div class="col-sm-12 form-group text-center filtrarDiv mapaBtnFiltroSubmit">              
              <button class="btn-blue btn-block" type="submit"><?= l('filtrar') ?></button>
              <a href="javascript:limpiar()" style="margin-top:10px; display: inline-block; text-decoration: underline;"><?= l('limpiar-filtros') ?></a>
            </div>
          </form>
         </div>
      </aside>


      <!--Advance Search-->
      <section class="botonesSobreElMapa">
        <p class="topper">
          <a href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?= l('volver-al-listado') ?></a>
        </p><p class="topper dibuja">
          <a href="javascript:initDrag()"><i id="drawButton" class="fa fa-edit"></i> <?= l('dibuja-tu-area-de-busqueda') ?> </a>
        </p>
        
      </section>

      <div class="col-xs-12 col-md-4 sideMapInfo">

        <div id="propiedadDetail">
          <a href="javascript:closeDetail()" class="closeAviso" style="position: absolute;top:20px;right: 20px;background: #fff;z-index: 10000;width: 20px;height: 20px;text-align: center;border: 1px solid black;"><i class="fa fa-times"></i></a>
          <div class="property_item heading_space" style="padding: 0 0 0 8px; margin-bottom: 0">
              <!-- _propiedadInfoWindowSidebar.php -->
          </div>
          
        </div>
      </div>
    </div>
  
</section>
<!-- Listing end -->
</div>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=drawing,geometry,places"></script> 
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<?php $this->load->view('includes/template/scripts'); ?>
<script>
  var ur = '<?= $filtros ?>';
  var onCache = undefined;
  <?php if(strpos($filtros,'saved')): ?>    
    onCache = localStorage.cacheMap && typeof(localStorage.cacheMap!='undefined')?JSON.parse(localStorage.cacheMap):{};
  <?php else: ?>    
    localStorage.cacheMap && typeof(localStorage.cacheMap!='undefined')?localStorage.removeItem('cacheMap'):undefined;
  <?php endif ?>
  $("html,body").css({'height':'100vh','overflow':'hidden','position':'relative'});
</script>
<script src="<?= base_url() ?>theme/theme/js/google-map.js"></script>