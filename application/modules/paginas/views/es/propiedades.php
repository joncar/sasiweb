<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase"> <?= $datos['titulo'] ?></h1>        
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->
<!-- Listing Start -->
<section id="listing1" class="listing1">
  <div class="row">
    <div class="col-xs-12 col-md-6">      
      <?php require_once('img/mapas/'.$datos['mapa']); ?>
      <div class="bottom20"></div>
      
      <?php if(!empty($datos['cercanias'])): ?>
        <div class="container  hidden-xs hidden-sm">
            <h2 class="text-uppercase bottom20" id="mapa2">
                <?= $datos['cercanias']['titulo'] ?> <?= $this->lang->line('cercanas'); ?>
             </h2>
             <div class="comarcasMapa property-details">
              <ul class="pro-list row" style="margin:10px 0">
                  <?php foreach($datos['cercanias']['datos'] as $n=>$c): ?>
                  <li class="col-xs-12 col-md-6 item">
                    <a href="<?= $c[1] ?>" class="loadingAfterClick"><?= $n ?> <span>(<?= $c[0] ?>)</span></a>
                  </li>
                  <?php endforeach ?>
              </ul>
             </div>
         </div>
      <?php endif ?>
    </div>    
    <div class="col-xs-12 col-md-6 propiedadInfo">
      <div class="container">
          <div class="botonesamarillos">
            <div class="hidden-xs hidden-sm">
                <div class="intro">
                   <select name="selectCiudad" id="selectCiudad">                  
                    <option value=""><?= $this->lang->line('seleccion-rapida-de-poblacion'); ?> <i class="fa fa-chevron-down"></i></option>
                    
                    <?php foreach($datos['datos']->result() as $c): if(!empty($c->datos) && $c->datos->num_rows()>0 && $c->cant>0): ?>
                      <option value="<?= $c->link ?>"><?= ucfirst($c->nombre) ?></option>             
                    <?php endif; endforeach ?>         
                  </select>
                </div>
              </div>
              <div class="botonamarillo">                
                  <a href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?= $this->lang->line('volver-al-listado') ?></a>                                  
              </div>
              <div class="botonamarillo">                
                  <a href="<?= base_url() ?>propiedades-en-mapa/<?= $filtros ?>"><i class="fa fa-map-marker"></i> <?= l('ver-propiedades-en-el-mapa'); ?></a>
              </div>
          </div>
           <div class="clearfix"></div>
           <h2 class="text-uppercase bottom20" id="mapa2" style="padding-top:30px;">
              <?php $datos['tipo'] = l($datos['tipo']); echo l('viviendas-por-tipo-en-lugar',$datos) ?>
           </h2>
           <div class="comarcasMapa property-details" style="padding:0 20px;">
            <?php foreach($datos['datos']->result() as $n=>$c): if(!empty($datos['showAll']) || ($c->cant>0 && !empty($c->datos) && $c->datos->num_rows()>0 || $c->cant>0)):  ?>
               
               <div <?= empty($c->datos)?'class="col-xs-12 col-sm-6"':'' ?> style="border-top:1px solid lightgray; white-space: nowrap">
                 <h4><a href="<?= $c->link ?>" class="colorenlaceazul loadingAfterClick"><?= ucfirst($c->nombre) ?> <span <?= $c->cant==0?'style="color:lightgray"':'' ?>>(<?= $c->cant ?>)</span></a></h4>
                 <ul class="pro-list row">

                   <?php if(!empty($c->datos)): foreach($c->datos->result() as $ci): if(!empty($datos['showAllChilds']) || $ci->cant>0): ?>
                    <li class="col-xs-12 col-sm-6 item">
                      <a href="<?= $ci->link ?>" class="loadingAfterClick" style="font-size:13px;"><?= $ci->nombre ?> <span>(<?= @$ci->cant ?>)</span></a>
                    </li>
                   <?php endif; endforeach; endif; ?>

                 </ul>             
              </div>
            <?php endif; endforeach ?>



            <?php if(!empty($datos['otros'])): ?>
              <div class="" style="border-top:1px solid lightgray; white-space: nowrap">
               <h4><a href="#" class="colorenlaceazul">Otros (<?= $datos['otroscant'] ?>)</a></h4>
               <ul class="pro-list row">

                 <?php foreach($datos['otros']->result() as $ci): ?>
                  <li class="col-xs-12 col-sm-6 item">
                    <a href="<?= $ci->link ?>" class="loadingAfterClick" style="font-size:13px;">
                      <?= $ci->nombre ?> <span>(<?= @$ci->cant ?>)</span>
                    </a>
                  </li>
                 <?php endforeach; ?>

               </ul>             
            </div>
            <?php endif ?>
           </div>
        </div>
    </div>
  </div>
  <div class="row padding_top" style="text-align: center">
    <div class="botonamarillo">                
        <a href="<?= base_url() ?>listado/<?= $datos['filtros'] ?>" class="loadingAfterClick">
          <i class="fa fa-eye"></i> <?= l('ver-todas-las-propiedades'); ?> (<?= $datos['total'] ?>)
        </a>
    </div>
  </div>
  
<div class="padding_top"></div>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>

<script>  

  function addEvents(id,title,content,link){
    
    if ($(window).width() > 800){
      $(id).popover({
        html:true,
        content:content,
        title:title,
        container:'body',
        placement:'right'
      });
    
      $(id).on('mouseleave',function(){
        $(this).popover('hide');
      });
      $(id).on('mouseenter',function(){
        $(this).popover('show');
      });
      $(id).on('click',function(){
        document.location.href=link;
      });
    }else{
      $(id).on('click',function(){
        document.location.href=link;
      });
    }
  }
  

  <?php foreach($datos['datos']->result() as $d): 
        if(!empty($d->datos) && !empty($d->id_mapa)):
        $html = '<div class=\'property-details\'><ul class=\'pro-list\'>';
        $cant = 0;
        foreach($d->datos->result() as $dd){
          if($dd->cant>0 && (empty($dd->id_mapa) || empty($_GET['comarcas_id']))){
            $html.= '<li style=\'margin-bottom: 5px; color:#787373; font-size:13px; font-family:robotoregular\'>'.$dd->nombre.' <span style=\'color: #72afdd;\'>('.$dd->cant.')</span> </li>';
            $cant+= $dd->cant;
          }
        } 
        $html .= '</ul></div>';
        $d->id_mapa = str_replace(',',',#',$d->id_mapa);
  ?>
    addEvents(
      '#<?= $d->id_mapa ?>',
      "<?= ucfirst($d->nombre) ?> (<?= $d->cant ?>)",
      "<?= $html ?>",
      "<?= $d->link ?>"
    );

    <?php foreach($d->datos->result() as $dd): if(!empty($dd->id_mapa)): ?>
        addEvents(
          '#<?= $dd->id_mapa ?>',
          "<?= ucfirst($dd->nombre) ?> (<?= $dd->cant ?>)",
          "",
          "<?= $dd->link ?>",

        );
    <?php endif; endforeach; ?>
  
  <?php endif; endforeach ?>


  $("#selectCiudad").on('change',function(){
    if($(this).val()!==''){
      document.location.href = $(this).val();
    }
  });

  
</script>
<script>localStorage.removeItem('crud_page');</script>