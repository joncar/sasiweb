<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="" style="background: #024a99; padding-top:40px;">
  <div class="container">
    <div class="row ">
      <div class="col-md-9 col-xs-12">        
        <div class="row" style="color:white;">
          <div class="col-xs-12">
            <h1><?= l('Inmuebles-en-lugar',array('lugar'=>ucfirst(mb_strtolower($lugar)))) ?></h1>
          </div>
        </div>
        <div class="row accionesBar">
          <div class="col-xs-3 col-lg-1">
            <?php
              $urlventa = $_GET;
              $urlventa['acciones'] = '1';
              unset($urlventa['lowcost']);
              $urlventa = base_url($url).'/'.get_instance()->encodeFiltros($urlventa);
            ?>
            <a href="<?= $urlventa ?>" class="acciones <?= @$_GET['acciones']=='1'?'active':'' ?>"><?= l('comprar') ?></a>
          </div>
          <div class="col-xs-3 col-lg-1">
             <?php
              $urlventa = $_GET;
              $urlventa['acciones'] = '2';
              unset($urlventa['lowcost']);
              $urlventa = base_url($url).'/'.get_instance()->encodeFiltros($urlventa);
            ?>
            <a href="<?= $urlventa ?>" class="acciones <?= @$_GET['acciones']=='2'?'active':'' ?>"><?= l('alquiler') ?></a>
          </div>
          <div class="col-xs-3 col-lg-1" style="white-space: nowrap;">
             <?php
              $urlventa = $_GET;
              $urlventa['acciones'] = '4';
              unset($urlventa['lowcost']);
              unset($urlventa['acciones']);
              $urlventa['propiedades_extras_id'] = 'obranueva';
              $urlventa = base_url($url).'/'.get_instance()->encodeFiltros($urlventa);
            ?>
            <a href="<?= $urlventa ?>" class="acciones <?= @$_GET['propiedades_extras_id']=='obranueva'?'active':'' ?>"><?= l('obra-nueva') ?></a>
          </div>
          <div class="col-xs-3 col-md-3">
             <?php
              $urlventa = $_GET;
              $urlventa['lowcost'] = '1';
              unset($urlventa['acciones']);
              $urlventa = base_url($url).'/'.get_instance()->encodeFiltros($urlventa);
            ?>
            <a href="<?= $urlventa ?>" class="acciones <?= @$_GET['lowcost']=='1'?'active':'' ?>"><span class="hidden-xs"><?= l('con-comision') ?> </span>Low Cost</a>
          </div>
        </div>
      </div>

      <?php if(!empty($filtros)): ?>
        <aside class="col-md-3 col-xs-12 newsletterlist hidden-xs hidden-sm">
          <p class="topper"><?= l('recibe-alertas-por-email-de-anuncions-similares') ?></p>
          <div style="background: white; padding:10px;">          
            <form action="propiedades/account/busquedas" onsubmit="return sendForm(this,'#responseBusqueda')" style="border:2px solid black; padding:10px;">
                <div class="form-group">
                  <div class="intro">
                    <input type="email" name="email" class="form-control" placeholder="<?= l('escribe-tu-email') ?>" value="<?= @$_SESSION['email'] ?>">
                  </div>
                  <input type="checkbox" name="politicas" value="1"> <span style="font-size:13px"><?= l('acepto-las-politicas-de-privacidad') ?></span>
                  <input type="hidden" name="string" value="<?= $filtros ?>">
                </div>
                <div id="responseBusqueda"></div>
                <div>
                  <button type="submit" class="btn btn-primary btn-block"><?= l('guardar') ?></button>
                </div>
            </form>      
          </div>
        </aside>
      <?php endif ?>
    </div>
  </div>
</section>
<!-- Page Banner End -->

<?php
  //app modules propiedades views cruds listado-[griya,list]
  echo $output 
?>

<?php $this->load->view($this->theme.'_propiedades_aviso',array(),FALSE,'paginas'); ?>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>  
<script>
  function limpiar(){
    $("form input[type='checkbox'],form select").val('');
    $("form input[type='checkbox'],form select").prop('checked',false);
    $("form .zelect").remove();
    $("form .intro select").zelect();
  }
</script>