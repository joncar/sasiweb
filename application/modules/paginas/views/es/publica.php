<header class="layout_default">
  <?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
</header>
<!-- Page Banner Start-->
<section class="page-banner padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="text-uppercase"><?= l('envia-tu-propiedad') ?></h1>
        <p><?= l('envia-tu-propiedad-text') ?></p>
        <ol class="breadcrumb text-center">
          <li><a href="<?= base_url() ?>"><?= l('inicio') ?></a></li>          
          <li class="active"><?= l('envia-tu-propiedad') ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Page Banner End -->
<!-- My Properties  -->
<section id="property" class="padding listing1">
  <div class="container">    
    <div class="row">
      
      <div class="col-sm-1 col-md-2"></div>
      <div class="col-sm-10 col-md-8">

        <div class="alert alert-info">
          <?= l('envia-tu-propiedad-text2') ?>          
        </div>
        <h2 class="text-uppercase bottom40"><?= l('envia-tu-propiedad') ?></h2>
        <?= $output ?>
      </div>
    </div>
    
  </div>
</section>
<!-- My Properties End -->
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div id="politicas" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= l('politica-privacidad') ?></h4>
      </div>
      <div class="modal-body">
        <p><?= $this->db->get('ajustes')->row()->politicas ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= l('cerrar') ?></button>        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  $(document).on('ready',function(){
    $("#field-politicas").on('change',function(){
      if($(this).prop('checked')){
        $("#politicas").modal('toggle');
      }
    });
  })
</script>
<?php if(!empty($js_files)):?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
<?php endif; ?>
<script>

  function refreshSelect(classe,data){
      var opt = '<option value="">Todas</option>';
      for(var i in data){
          opt+= '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
      }

      $(classe).html(opt);
      $(classe).removeClass('chzn-done');
      $(classe).parents('.form-group').find('.chzn-container').remove();
      $(classe).chosen({"search_contains": true, allow_single_deselect:true});
      $(classe).parents('.form-group').find('.chzn-container').css('width','100%');
  }

  $(document).on('change','#field-provincias_id',function(e){
        e.stopPropagation();
        $.post(URL+'propiedades/json/ciudades/json_list',{
            'search_field[]':'jd904cb91.provincias_id',
            'search_text[]':$(this).val(),
            'per_page':'1000'
        },function(data){
            ajax = false;
            data = JSON.parse(data);
            refreshSelect('#field-ciudades_id',data);             
        });
    });
</script>