<form method="get">
	<select name="idioma" class="form-control">
		<option value="">Seleccione un idioma</option>
		<?php foreach(explode(',',$this->db->get('ajustes')->row()->idiomas) as $i): ?>
			<option value="<?= trim($i) ?>"><?= $i ?></option>
		<?php endforeach ?>
	</select>
	<button type="submit" class="btn btn-info">Cargar</button>
</form>