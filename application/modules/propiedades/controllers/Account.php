<?php 
require_once APPPATH.'/controllers/Panel.php';    
class Account extends Main{
    function __construct() {
        parent::__construct();
        $this->load->library('grocery_crud');
        $this->load->library('ajax_grocery_crud');
    }

    protected function crud_function($x,$y,$controller = ''){
        $crud = new ajax_grocery_CRUD($controller);        
        $crud->set_theme('bootstrap2');
        $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
        $crud->set_table($table);
        $crud->set_subject(ucfirst($this->router->fetch_method()));
        if(!empty($this->norequireds)){
            $crud->norequireds = $this->norequireds;
        }
        $crud->required_fields_array();
        
        if(method_exists('panel',$this->router->fetch_method()))
         $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
        return $crud;
    }     

    function encodeFiltros($datos){
        $dat = '';
        foreach($datos as $n=>$v){
            if(!empty($v)){
                if(is_array($v)){
                    $v = implode(',',$v);
                }
                $v = str_replace('-','_',$v);
                $dat.='-'.$n.'-'.$v;
            }
        }            
        $dat = substr($dat,1);            
        return $dat;
    }  

    function decodeFiltros($filtros){

        if(!empty($filtros)){
            $filtros = explode('-',$filtros);
            $data = array();

            if(count($filtros)%2==0){
                for($i=0;$i<count($filtros);){                        
                    $filtros[$i+1] = str_replace('_','-',$filtros[$i+1]);
                    $data[$filtros[$i]] = urldecode($filtros[$i+1]);
                    $i = $i+2;
                }
            }

            foreach($data as $n=>$v){
                if(strpos($v,',')>-1){
                    $data[$n] = explode(',',$v);
                }
            }


            $_GET = $data;                
            return $data;
        }
    }
    

    

    function perfil($x = ''){
        $this->as['perfil'] = 'user';
        $this->norequireds = array('status','admin','password','email');
        $crud = $this->crud_function("","");           
        $crud->set_theme('perfil');
        $crud->set_primary_key_value($this->user->id);      
        $crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'215px','height'=>'215px'));
        $crud->set_url('propiedades/account/perfil/');
        $crud->unset_add()->unset_read()->unset_delete()->unset_list()->unset_export()->unset_print();
        $crud->callback_before_update(function($post,$primary){
            $post['password'] = empty($post['password'])?$this->db->get_where('user',array('id'=>$this->user->id))->row()->password:$post['password'];

            if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password']){
                $post['password'] = md5($post['password']);                
            }

            return $post;
        });
        $crud->callback_after_update(function($post,$primary){
            $this->user->login_short($primary);
        });
        if(empty($x)){
            $crud = $crud->render(3,'application/modules/propiedades/views/cruds/');
        }else{
            $crud = $crud->render();
        }
        $crud->view = 'perfil';
        $this->loadView($crud);
    }

    function trash(){
        $crud = $this->crud_function("","");                         
        $crud->callback_before_insert(function($post){
            $user = $this->user->log?$this->user->id:$_SESSION['user_id_temp'];
            get_instance()->db->delete('trash',array('cod_ofer'=>$post['cod_ofer'],'user_id'=>$user));
            $post['user_id'] = $user;
            return $post;
        });        
        $crud = $crud->render();
        $this->loadView($crud);
    }

    function avisos($x = ''){
        if(empty($x) || $x=='ajax_list' || $x=='ajax_list_info'){       
            $this->as['avisos'] = 'view_avisos'; 
        }
        $this->norequireds = array('user_id');                      
        $crud = $this->crud_function("","");   
        $crud->set_theme('listado-aviso');
        $crud->set_primary_key('id');   
        $crud->field_type('lowcost','string');       
        $crud->callback_before_insert(function($post){
            $post['user_id'] = $this->user->id;
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            $this->db = get_instance()->db;            
            $this->db->select('avisos.id as avid,propiedades_list.*,avisos.email, api_descripciones.titulo');            
            $this->db->join('propiedades_list','propiedades_list.cod_ofer = avisos.cod_ofer');
            $this->db->join('api_descripciones','api_descripciones.cod_ofer = propiedades_list.cod_ofer AND idioma = \''.$_SESSION['lang'].'\'');
            $avisos = $this->db->get_where('avisos',array('avisos.id'=>$primary));
            foreach($avisos->result() as $a){
                $this->db->where('propiedades_list.id',$a->id);
                $aa = $this->querys->get_propiedades()->row();
                $aa->foto = '<img src="'.$aa->foto.'" style="width:100%;">';
                $aa->email = $a->email;   
                $precio =   $aa->precio;
                $aa->precio = moneda($aa->precio);
                $aa->link = base_url('propiedad/'.$aa->ref);
                $firma = $aa->email_contacto == 'info@finques-sasi.com'?19:20;
                $aa->firma = $this->db->get_where('notificaciones',array('id'=>$firma))->row()->texto;

                if(empty($aa->titulo)){
                    $aa->titulo = $aa->nbtipo.' en '.$aa->ciudad.' zona '.$aa->zona.' con '.$aa->banyos.' baños, conservación '.$aa->nbconservacion; 
                    mb_strtolower($aa->titulo);
                }

                get_instance()->enviarcorreo($aa,16,$aa->email);
                get_instance()->enviarcorreo($aa,16,$aa->email_contacto);                
            }
        });
        $crud->unset_back_to_list();
        $crud->set_url('propiedades/account/avisos/');        
        $crud->set_rules('email','Email','required|callback_validateavisoname');
        $crud->where('view_avisos.email',$this->user->email);
        $crud->where('view_avisos.idioma',$_SESSION['lang']);
        $_GET['avisos'] = 1;
        $crud = $crud->render('','application/modules/propiedades/views/cruds/');
        $crud->view = 'avisos';
        $crud->title = 'Mis alertas';
        $this->loadView($crud);
    }

    function busquedas(){
        $this->form_validation->set_rules('email','STRING','required|valid_email')
                              ->set_rules('string','STRING','required')
                              ->set_rules('politicas','Políticas','required');
        if($this->form_validation->run()){
            $existe = $this->db->get_where('busquedas',array('email'=>$_POST['email'],'string'=>$_POST['string']));
            if($existe->num_rows()>=0){
                //$this->db->insert('busquedas',array('email'=>$_POST['email'],'string'=>$_POST['string']));
                $filtros = $this->decodeFiltros($_POST['string']);
                $datos = $filtros;
                $datos['extras'] = '';                                    
                if(!empty($filtros['ref'])){
                    $propiedad = $this->db->get_where('propiedades_list',array('ref'=>$filtros['ref']));
                    $datos['extras'].= '<p><b>Activación de alertas para inmuebles similares a:  </b>'.$filtros['ref'].'</p>';
                    $datos['extras'].= '<p><b>Politícas de privacidad:</b> Aceptadas</p>';                    
                    $datos['to'] = $propiedad->row()->email_contacto; 
                    $datos['titulo'] = 'Web sasi, Enviar similares a ref: '.$filtros['ref'];                    
                    $datos['extras'].= '<ul>';
                    //Viene la referencia? se adjunta el inmuble
                    $this->db->where('propiedades_list.ref',$filtros['ref']);
                    $aa = $this->querys->get_propiedades()->row();
                    $aa->foto = '<img src="'.$aa->foto.'" style="width:100%;">';
                    $aa->precio = moneda($aa->precio);
                    $aa->link = base_url('propiedad/'.$aa->ref);
                    $datos['propiedad'] = $this->db->get_where('notificaciones',array('id'=>21))->row()->texto;
                    foreach($aa as $n=>$l){
                        $datos['propiedad'] = str_replace('{'.$n.'}',$l,$datos['propiedad']);
                    }
                    $ref = ' a REF: '.$filtros['ref'];
                }elseif(!empty($filtros['provincias_id'])){                                        
                    $datos['extras'].= '<p><b>Activación de alertas para inmuebles similares a:</p><ul>';
                    $datos['extras'].= '</ul>';
                    $datos['propiedad'] = '';
                    $ref = '';
                    $provincia = $this->db->get_Where('provincias',array('id'=>$filtros['provincias_id']))->row()->sucursal;
                    switch($provincia){                        
                        case '5010': 
                            $datos['to'] = $this->db->get('ajustes')->row()->email_contacto_alicante;
                        break;
                        default:
                            $datos['to'] = $this->db->get('ajustes')->row()->email_contacto_igualada;
                        break;
                    }
                }else{
                    $datos['to'] = $this->db->get('ajustes')->row()->email_contacto_igualada;
                    $datos['extras'].= '<p><b>Activación de alertas para inmuebles similares a:</p><ul>';                    
                }
                    
                    if(!empty($filtros['provincias_id'])){
                        $datos['extras'].= '<li><b>Provincia</b>: '.$this->db->get_where('provincias',array('id'=>$filtros['provincias_id']))->row()->nombre.'</li>';
                    }
                    if(!empty($filtros['comarcas_id'])){
                        $datos['extras'].= '<li><b>Comarca</b>: '.$this->db->get_where('comarcas',array('id'=>$filtros['comarcas_id']))->row()->nombre.'</li>';
                    }
                    if(!empty($filtros['ciudades_id'])){
                        $datos['extras'].= '<li><b>Ciudad</b>: '.$this->db->get_where('ciudades',array('id'=>$filtros['ciudades_id']))->row()->ciudad.'</li>';
                    }
                    if(!empty($filtros['areas_id'])){
                        $datos['extras'].= '<li><b>Area</b>: '.$this->db->get_where('areas',array('id'=>$filtros['areas_id']))->row()->nombre.'</li>';
                    }

                    if(!empty($filtros['distritos_id'])){
                        $datos['extras'].= '<li><b>Distrito</b>: '.$this->db->get_where('distritos',array('id'=>$filtros['distritos_id']))->row()->nombre.'</li>';
                    }

                    if(!empty($filtros['zonas_id'])){
                        $datos['extras'].= '<li><b>Zona</b>: '.$this->db->get_where('zonas',array('id'=>$filtros['zonas_id']))->row()->nombre.'</li>';
                    }
                    if(!empty($filtros['acciones'])){
                        $accion = '';
                        switch($filtros['acciones']){
                            case '1':$accion = 'Comprar'; break;
                            case '2':$accion = 'Alquilar'; break;
                        }
                        $datos['extras'].= '<li><b>Acción</b>: '.$accion.'</li>';
                    }
                    if(!empty($filtros['total_hab'])){
                        $datos['extras'].= '<li><b>Habitaciones</b>: +'.$filtros['total_hab'].'</li>';
                    }
                    if(!empty($filtros['m_uties'])){
                        $datos['extras'].= '<li><b>Superficie M<sup>2</sup></b>: +'.$filtros['m_uties'].'</li>';
                    }
                    if(!empty($filtros['precio_min'])){
                        $datos['extras'].= '<li><b>Precio Min</b>: '.$filtros['precio_min'].'</li>';
                    }
                    if(!empty($filtros['precio_max'])){
                        $datos['extras'].= '<li><b>Precio Max</b>: '.$filtros['precio_max'].'</li>';
                    }
                    if(!empty($filtros['lowcost'])){
                        $datos['extras'].= '<li><b>Lowcost</b>: Activado'.'</li>';
                    }
                    if(!empty($filtros['obranueva'])){
                        $datos['extras'].= '<li><b>Conservación</b>: Obra nueva'.'</li>';
                    }

                    if(!empty($filtros['codigo_tipo'])){
                        $w = array();
                        foreach($filtros['codigo_tipo'] as $f){
                            $w[] = $this->db->get_where('tipos',array('codigo_tipo'=>$f))->row()->nombre;
                        }
                        $datos['extras'].= '<li><b>Tipos de inmuebles</b>: '.implode(',',$w).'</li>';
                    }
                    if(!empty($filtros['propiedades_extras_id'])){
                        $w = array();
                        foreach($filtros['propiedades_extras_id'] as $f){
                            $w[] = $this->db->get_where('propiedades_extras',array('id'=>$f))->row()->nombre;
                        }
                        $datos['extras'].= '<li><b>Más características</b>: '.implode(',',$w).'</li>';
                    }
                $firma = $datos['to'] == 'info@finques-sasi.com'?19:20;
                $datos['firma'] = $this->db->get_where('notificaciones',array('id'=>$firma))->row()->texto;
                $datos['extras'].= '</ul>';
                $datos['asunto'] = '';
                $datos['nombre'] = '';
                $datos['titulo'] = 'Web sasi, Envio de inmuebles similares'.$ref;              
                $datos['email'] = $_POST['email'];
                $firma = $datos['to'] == 'info@finques-sasi.com'?19:20;
                $datos['firma'] = $this->db->get_where('notificaciones',array('id'=>$firma))->row()->texto;
                
                $this->enviarcorreo((object)$datos,15,$datos['to']);
                $this->enviarcorreo((object)$datos,15,$_POST['email']);

            }
            echo $this->success('Datos almacenados con éxito');
        }else{
            echo $this->error($this->form_validation->error_String());
        }
    }

    function validateavisoname($name){
        if(empty($_POST['politicas'])){
            $this->form_validation->set_message('validateavisoname','Debe aceptar nuestras politícas de privacidad para realizar esta acción');
            return false;
        }
        if($this->db->get_where('avisos',array('email'=>$name,'cod_ofer'=>$_POST['cod_ofer']))->num_rows()>0){
            $this->form_validation->set_message('validateavisoname','El aviso ya se encuentra registrado');
            return false;
        }
        return true;    
    }

    function avisamesibaja($propid){
        if(is_numeric($propid)){
            $this->db->select('propiedades_list.*, api_descripciones.titulo, api_descripciones.descrip');
            $this->db->where(array('propiedades_list.id'=>$propid));
            $prop = $this->querys->get_propiedades();
            if($prop->num_rows()>0){
                $this->load->view($this->theme.'_avisameSiBajaModal',array('detail'=>$prop->row()),FALSE,'paginas');
            }
        }
    }

    function favoritosModal($propid = ''){
        if(!empty($propid)){
            $this->db->select('propiedades_list.*, api_descripciones.titulo, api_descripciones.descrip');
            $this->db->where(array('propiedades_list.cod_ofer'=>$propid));
            $prop = $this->querys->get_propiedades();                
            if($prop->num_rows()>0){
                $this->load->view($this->theme.'_favoritoModal',array('detail'=>$prop->row()),FALSE,'paginas');
            }
        }
    }

    function favoritos($x = '',$propid = ''){
        if(empty($x) || $x=='ajax_list' || $x=='ajax_list_info'){       
            $this->as['favoritos'] = 'view_favoritos'; 
        }
        if(!empty($_POST['nombre'])){
            $grupo = $this->db->insert('favoritos_grupos',array('user_id'=>$this->user->id,'nombre'=>$_POST['nombre']));
            $_POST['favoritos_grupos_id'] = $this->db->insert_id();
        }
        $user = $this->user->log?$this->user->id:$_SESSION['user_id_temp'];
        $crud = $this->crud_function("","");     
        $crud->set_primary_key('id');
        $crud->set_theme('listado');   
        $crud->field_type('lowcost','string');              
        $crud->set_url('propiedades/account/favoritos/');        
        $crud->edit_fields('favoritos_grupos_id');
        $crud->unset_read()->unset_export()->unset_print();        
        $crud->callback_before_insert(function($post){
            $user = $this->user->log?$this->user->id:$_SESSION['user_id_temp'];
            get_instance()->db->delete('favoritos',array('cod_ofer'=>$post['cod_ofer'],'user_id'=>$user));            
            $post['user_id'] = $user;
            return $post;
        });
        $crud->callback_column('fotos',function($val,$row){
            $fot = array();
            $fotos = $this->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$row->cod_ofer));
            foreach($fotos->result() as $f){
                $fot[] = $f;
            }
            return $fot;
        });
        $crud->where('view_favoritos.user_id',$user);
        $crud->where('view_favoritos.idioma',$_SESSION['lang']);
        if(isset($_POST['favoritos_grupos_id'])){
            $crud->where('favoritos_grupos_id = '.$_POST['favoritos_grupos_id'],'ESCAPE');
        }
        $crud->group_by('id');
        $_GET['favoritos'] = 1;
        $crud = $crud->render('','application/modules/propiedades/views/cruds/');
        $crud->view = 'favoritos';
        $crud->title = 'Lista de favoritos';
        $this->loadView($crud);
    }

    function propiedades($x = ''){
        $user = $this->user->log?$this->user->id:$_SESSION['user_id_temp'];
        $crud = $this->crud_function("","");             
        $crud->set_theme('propiedades');                                        
        $crud->set_url('propiedades/account/propiedades/');
        get_instance()->extras = array();
        $this->db->order_by('nombre','ASC');
        foreach($this->db->get_where('propiedades_extras',array('mostrar_en_form'=>1))->result() as $p){
            get_instance()->extras[$p->codigo] = $p->nombre;
        }
        $crud->callback_before_insert(function($post){
            $post['propiedades_extras'] = implode(',',$post['propiedades_extras']);
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            get_instance()->sendMail($primary);
        });
        $crud->field_type('propiedades_extras','multi',get_instance()->extras)
             ->field_type('user_id','hidden',$user)
             ->field_type('descripofertas','editor',array('type'=>'textarea'))
             ->field_type('comunidadincluida','checkbox')
             ->field_type('opcioncompra','checkbox')
             ->callback_field('propiedades_extras',function(){
                return json_encode(get_instance()->extras);
             })
             ->field_type('foto1','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto2','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto3','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto4','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto5','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto6','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto7','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto8','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto9','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto10','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto11','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'))
             ->field_type('foto12','image',array('path'=>'img/propiedades','width'=>'1064px','height'=>'800px'));        

        $crud->set_relation('ciudades_id','ciudades','ciudad');
        $crud->unset_jquery();
        $crud->disable_ftp();
        $crud->display_as('provincias_id',l('provincia'))
             ->display_as('ciudades_id',l('ciudad'))
             ->display_as('auxizona',l('Zona'))
             ->display_as('zonaauxiliar',l('Zona Auxiliar'))
             ->display_as('auxizona',l('zona'))
             ->display_as('calle',l('Domicilio Propiedad'))
             ->display_as('numero',l('Número'))
             ->display_as('planta',l('Planta'))
             ->display_as('puerta',l('Puerta'))
             ->display_as('propiedades_extras','Extras');        
        if(empty($x)){
            $crud = $crud->render('2','application/modules/propiedades/views/cruds/');     
        }else{
            $crud = $crud->render('','application/modules/propiedades/views/cruds/');     
        }        
        $crud->view = 'publica'; 
        $crud->title = 'Publica tu propiedad';               
        $this->loadView($crud);
    }

    function sendMail($id){
        $this->db->select('propiedades.*, provincias.nombre as provincia,provincias.sucursal as provsuc,ciudades.ciudad,tipos.label as tipo, conservacion.nombre as conservacion');
        $this->db->join('provincias','provincias.id = propiedades.provincias_id');
        $this->db->join('ciudades','ciudades.id = propiedades.ciudades_id');
        $this->db->join('tipos','tipos.codigo = propiedades.tipos_id','left');
        $this->db->join('conservacion','conservacion.codigo = propiedades.conservacion_id','left');
        $id = $this->db->get_where('propiedades',array('propiedades.id'=>$id));
        if($id->num_rows()>0){            
            $prop = $id->row();
            get_instance()->load->library('mailer');
            for($i=1;$i<=12;$i++){
                if(!empty($prop->{'foto'.$i})){                    
                    get_instance()->mailer->mail->addAttachment('img/propiedades/'.$prop->{'foto'.$i},'Foto '.$i);
                }
            }        

            //Transformar correo
            $prop->titulo = 'Web SASI, propiedad para publicar.';                        
            $prop->extras = explode(',',$prop->propiedades_extras);
            $extras = array();
            foreach($prop->extras as $e){
                $extras[] = $this->db->get_where('propiedades_extras',array('codigo'=>$e))->row()->nombre;
            }      
            $prop->extras = '<ul>';
            foreach($extras as $e){
                $prop->extras .= '<li>'.$e.'</li>';
            }  
            $prop->extras .= '</ul>';   
            // $to = $prop->provincias_id==8?'info@sasi-alicante.com':'info@finques-sasi.com';            
            $ajustes = $this->db->get('ajustes')->row();
            if($prop->provsuc=='5010'){
                $to = $ajustes->email_contacto_alicante;
                $prop->firma = $this->db->get_where('notificaciones',array('id'=>20))->row()->texto;
            }else{
                $to = $ajustes->email_contacto_igualada;
                $prop->firma = $this->db->get_where('notificaciones',array('id'=>19))->row()->texto;
            }
            $this->enviarcorreo($prop,18,$to);
            get_instance()->mailer->mail->clearAttachments();
        }
    }

    function propiedades_fotos($x = ''){  
        $this->load->library('image_crud');      
        $crud = new image_crud();                     
        $crud->set_table('propiedades_fotos')            
             ->set_image_path('img/propiedades')
             ->set_url_field('foto');
        $crud->module = 'propiedades';
        $crud = $crud->render();
        $crud->view = 'publica';   
        return $crud;
    }

    function favoritos_grupos(){
        if(!empty($this->user->id)){
            $this->norequireds = array('user_id');
            $crud = $this->crud_function("","");             
            $crud->set_theme('propiedades');            
            $crud->callback_before_insert(function($post){
                $post['user_id'] = $this->user->id;
                return $post;
            });
            $crud->callback_before_delete(function($post){
                get_instance()->db->update('favoritos',array('favoritos_grupos_id'=>0),array('favoritos_grupos_id'=>$post));
                return true;
            });
            $crud->unset_back_to_list();
            $crud->set_rules('nombre','Nombre','required|callback_validategroupfavname');
            $crud = $crud->render();
            $crud->view = 'publica';   
            return $crud;
        }else{
            throw new exception('La página que desea consultar no se encuentra disponible',404);
        }
    }

    function validategroupfavname($name){
        if($this->db->get_where('favoritos_grupos',array('nombre'=>$name,'user_id'=>$this->user->id))->num_rows()>0){
            $this->form_validation->set_message('validategroupfavname','El nombre del grupo ya se encuentra registrado');
            return false;
        }
        return true;    
    }
}