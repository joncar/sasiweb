<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function codigos_tipos($x = '',$y = ''){
            $crud = $this->crud_function("","");                                     
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function propiedades_list($x = '',$y = ''){
            $crud = $this->crud_function("","");    
            $crud->columns('id','cod_ofer','ref','precio');                                 
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function cron($x = '',$y = ''){
            $crud = $this->crud_function("","");                         
            $crud->unset_add()->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->columns('current_page','total_pages','per_page','sucursalName','max_iteraccion','fecha_ejecucion','status');
            $crud->unset_searchs('status');
            $crud->callback_column('status',function($val,$row){
                if($row->current_page<$row->total_pages){
                    return '<span class="label label-info">Ejecutandose</span>';
                }elseif($row->max_iteraccion==0){
                    return '<span class="label label-default">Inactivo</span>';
                }elseif($row->total_pages==0){
                    return '<span class="label label-warning">Pendiente</span>';
                }else{
                    return '<span class="label label-success">Completado</span>';
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function provincias($x = '',$y = ''){
            $crud = $this->crud_function("","");                         
            $crud->set_field_upload('mapa','img/mapas')
            	 ->field_type('codigo','editor',array('type'=>'textarea'))
                 ->field_type('codigo_mapa_general','editor',array('type'=>'textarea'))
            	 ->add_action('comarcas','',base_url('propiedades/admin/comarcas/').'/')
                 ->columns('nombre','sucursal');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function comarcas($x = '',$y = ''){
            $crud = $this->crud_function("","");       
            if(is_numeric($x)){
            	$crud->field_type('provincias_id','hidden',$x)
            		 ->where('provincias_id',$x);
            }
            if($crud->getParameters()=='add'){
                $crud->set_rules('nombre','Nombre','required|is_unique[comarcas.nombre]');
            }
            $crud->columns('provincias_id','nombre');
            $crud->set_field_upload('mapa','img/mapas')
            	 ->field_type('codigo','editor',array('type'=>'textarea'))
                 ->add_action('ciudades','',base_url('propiedades/admin/ciudades/').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function areas($x = '',$y = ''){
            $crud = $this->crud_function("","");       
            if(is_numeric($x)){
                $crud->field_type('comarcas_id','hidden',$x)
                     ->where('comarcas_id',$x);
            }
            if($crud->getParameters()=='add'){
                $crud->set_rules('nombre','Nombre','required|is_unique[areas.nombre]');
            }
            $crud->set_relation('comarcas_id','comarcas','nombre');
            $crud->columns('comarcas_id','nombre');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function ciudades($x = '',$y = ''){
            $crud = $this->crud_function("","");       
            if(is_numeric($x)){
                $crud->field_type('comarcas_id','hidden',$x)
                     ->where('ciudades.comarcas_id',$x);
                $crud->columns('ciudad','mapa');
            }else{
                $crud->set_relation('comarcas_id','comarcas','nombre');
            }
            if($crud->getParameters()=='add'){
                $crud->set_rules('nombre','Nombre','required|is_unique[ciudades.nombre]');
            }

            $crud->set_relation('areas_id','areas','nombre');
            
            $crud->set_field_upload('mapa','img/mapas')
                 ->field_type('codigo','editor',array('type'=>'textarea'))
                 ->add_action('distritos','',base_url('propiedades/admin/distritos/').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function distritos($x = '',$y = ''){
            $crud = $this->crud_function("","");       
            if(is_numeric($x)){
                $crud->field_type('ciudades_id','hidden',$x)
                     ->where('ciudades_id',$x);
            }
            if($crud->getParameters()=='add'){
                $crud->set_rules('nombre','Nombre','required|is_unique[distritos.nombre]');
            }
            $crud->set_relation('ciudades_id','ciudades','ciudad')
                 ->add_action('zonas','',base_url('propiedades/admin/zonas/'.$x.'/').'/');
            $crud->columns('ciudades_id','nombre');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function zonas($x = '',$y = ''){
            $crud = $this->crud_function("","");       
            if(is_numeric($x)){
                $crud->field_type('distritos_id','hidden',$y)
                     ->field_type('ciudades_id','hidden',$x)
                     ->where('distritos_id',$y);
                $crud->columns('nombre','mapa');            
            }else{
                $crud->set_relation('ciudades_id','ciudades','ciudad');
            }
            if($crud->getParameters()=='add'){
                $crud->set_rules('nombre','Nombre','required|is_unique[zonas.nombre]');
            }
            
            $crud->set_field_upload('mapa','img/mapas')
                 ->field_type('codigo','editor',array('type'=>'textarea'));
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function zonas_inmovilla($x = '',$y = ''){
            $this->as['zonas_inmovilla'] = 'barrios';
            $crud = $this->crud_function("","");
            $crud->set_relation('comarcas_id','comarcas','nombre');
            $crud->set_relation('ciudades_id','ciudades','ciudad');
            $crud = $crud->render();
            $crud->output = '<a href="'.base_url('propiedades/admin/SanearPropiedades').'" target="_new" class="btn btn-info">Actualizar zonas de propiedades</a>'.$crud->output;
            $this->loadView($crud);
        }

        function tipos($x = '',$y = ''){
            $crud = $this->crud_function("","");                   
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function mapas($x = '',$y = ''){
            $crud = $this->crud_function("","");                   
            $crud = $crud->render();
            $this->loadView($crud);
        }


        function propiedades_extras($x = '',$y = ''){
            $crud = $this->crud_function("","");                   
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function  SanearPropiedades(){
            $this->db->query('select SanearPropiedades(1)');
            echo 'true';
        }
    }
?>
