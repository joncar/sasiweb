<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        function comarcas($x = '',$y = ''){
        	$this->load->library('grocery_crud')
        			   ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('comarcas')
            	 ->set_subject('bootstrap2')
            	 ->set_theme('bootstrap2');       
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function encodeFiltros($datos){
            $dat = '';
            foreach($datos as $n=>$v){
                if(!empty($v)){
                    if(is_array($v)){
                        $v = implode(',',$v);
                    }
                    $v = str_replace('-','_',$v);
                    $dat.='-'.$n.'-'.$v;
                }
            }            
            $dat = substr($dat,1);            
            return $dat;
        }

        function encodeFiltrosGet($datos){
            $dat = '';
            foreach($datos as $n=>$v){
                if(!empty($v)){
                    if(is_array($v)){
                        $v = implode(',',$v);
                    }
                    $v = str_replace('-','_',$v);
                    $dat.='&'.$n.'='.$v;
                }
            }
            $dat = substr($dat,1);
            return $dat;
        }

        function decodeFiltros($filtros){

            if(!empty($filtros)){
                $filtros = explode('-',$filtros);
                $data = array();

                if(count($filtros)%2==0){
                    for($i=0;$i<count($filtros);){                        
                        $filtros[$i+1] = str_replace('_','-',$filtros[$i+1]);
                        $data[$filtros[$i]] = urldecode($filtros[$i+1]);
                        $i = $i+2;
                    }
                }

                foreach($data as $n=>$v){
                    if(strpos($v,',')>-1){
                        $data[$n] = explode(',',$v);
                    }
                }


                $_GET = $data;                
                return $data;
            }
        }



        function rutear(){
            $datos = (object)$_GET;
            $_SESSION['propiedades_list'] = empty($_SESSION['propiedades_list'])?array():array();
            //Evitar el paso a los mapas            
            if(isset($_GET['buscar_por']) && $_GET['buscar_por']=='mapa'){                
                header('Location:'.base_url('propiedades-en-mapa/'.$this->encodeFiltros($datos)));
                echo "<html><script>localStorage.removeItem('crud_page');</script></html>";
                die();
            }
            if(!empty($_GET['referencia'])){
                header('Location:'.base_url('listado').'/referencia-'.str_replace('-','_',$_GET['referencia']));
                echo "<html><script>localStorage.removeItem('crud_page');</script></html>";
                die();
            }else{
                $this->load->model('Mapassvg_model');
                if(!$this->Mapassvg_model->getMapInfo()){
                    header('Location:'.base_url('listado/'.$this->encodeFiltros($datos)));
                    echo "<html><script>localStorage.removeItem('crud_page');</script></html>";
                    die();
                }else{
                    header('Location:'.base_url('propiedades/?'.$this->encodeFiltrosGet($datos)));
                    echo "<html><script>localStorage.removeItem('crud_page');</script></html>";
                    die();
                }

            }
            

            $this->loadView(array(
                'view'=>'mapas',                
                'mapa'=>$mapa,
                'coordenadas'=>$coordenadas,
                'label'=>$label
            ));
        }

        
        

        function propiedadesMapa(){
           $prop = array();
           //Filtros
           if(!empty($_POST)){
                $filtros = $_POST;                
                if(!empty($filtros['provincias_id'])){
                    $this->db->where('provincias_id',$filtros['provincias_id']);
                }
                if(!empty($filtros['comarcas_id'])){
                    $this->db->where('comarcas_id',$filtros['comarcas_id']);
                }
                if(!empty($filtros['ciudades_id'])){
                    $this->db->where('ciudades_id',$filtros['ciudades_id']);
                }
                if(!empty($filtros['areas_id'])){
                    $this->db->where('areas_id',$filtros['areas_id']);
                }

                if(!empty($filtros['distritos_id'])){
                    $this->db->where('distritos_id',$filtros['distritos_id']);
                }

                if(!empty($filtros['zonas_id'])){
                    $this->db->where('zonas_id',$filtros['zonas_id']);
                }
                if(!empty($filtros['acciones'])){
                    $this->db->where('codaccion',$filtros['acciones']);
                }
                if(!empty($filtros['total_hab'])){
                    $this->db->where('total_hab >=',$filtros['total_hab']);
                }
                if(!empty($filtros['m_uties'])){
                    $this->db->where('m_uties >=',$filtros['m_uties']);
                }
                if(!empty($filtros['precio_min'])){
                    $this->db->where('( precio >='.$filtros['precio_min'].')',NULL,FALSE);
                }
                if(!empty($filtros['precio_max'])){
                    $this->db->where('( precio <='.$filtros['precio_max'].')',NULL,FALSE);
                }
                if(!empty($filtros['lowcost'])){
                    $this->db->where('lowcost',1);
                }
                if(!empty($filtros['obranueva'])){
                    $this->db->where('nbconservacion','Obra Nueva');
                }

                if(!empty($filtros['codigo_tipo'])){
                    $w = '';
                    foreach($filtros['codigo_tipo'] as $f){
                        $w.= "OR codigo_tipo='".$f."' ";
                    }
                    $w = '('.substr($w,3).')';
                    $this->db->where($w,NULL,FALSE);
                }
                if(!empty($filtros['propiedades_extras_id'])){
                    $w = '';
                    foreach($filtros['propiedades_extras_id'] as $f){
                        $w.= "AND propiedades_list.id IN ( SELECT propiedades_list_id from propiedades_list_extras JOIN propiedades_extras ON propiedades_extras.id = propiedades_list_extras.propiedades_extras_id WHERE codigo ='".$f."')";
                    }

                    $w = substr($w,3);
                    $this->db->where($w,NULL,FALSE);
                }

                if(!empty($filtros['referencia'])){
                    $this->db->where('ref',$filtros['referencia']);
                }
           }
           if(!$this->user->log || (empty($filtros['avisos']) && empty($filtros['favoritos']))){
               $propiedades = $this->querys->get_propiedades();
               foreach($propiedades->result() as $p){
                    if($p->codaccion==2){
                      $p->precio = moneda($p->precioalq).'/'.$p->tipomensual;
                    }else{
                      $p->precio = moneda($p->precio);
                    }
                    $fav = 0;
                    $fav = $this->user->log && $this->db->get_where('view_favoritos',array('cod_ofer'=>$p->cod_ofer,'user_id'=>$this->user->id))->num_rows()>0?1:0;

                    $pp = array(
                        'cod_ofer'=>$p->cod_ofer,
                        'altitud'=>$p->altitud,
                        'latitud'=>$p->latitud,
                        'precio'=>$p->precio,                        
                        'codaccion'=>$p->codaccion,
                        'tipomensual'=>$p->tipomensual,
                        'showModals'=>1,
                        'provincia'=>$p->provincia,
                        'ciudad'=>$p->ciudad,
                        'favorito'=>$fav
                    );
                    $prop[] = $pp;
               }
            }
            elseif(!empty($filtros['favoritos'])){
               if(!empty($filtros['grupos_favoritos_id'])){
                    $this->db->where('favoritos_grupos_id',$filtros['grupos_favoritos_id']);
               }
               $propiedades = $this->db->get_where('view_favoritos',array('user_id'=>$this->user->id));
               foreach($propiedades->result() as $p){
                    if($p->codaccion==2){
                      $p->precio = moneda($p->precioalq).'/'.$p->tipomensual;
                    }else{
                      $p->precio = moneda($p->precio);
                    }
                    $fav = 0;
                    $pp = array(
                        'cod_ofer'=>$p->cod_ofer,
                        'altitud'=>$p->altitud,
                        'latitud'=>$p->latitud,
                        'precio'=>$p->precio,                        
                        'codaccion'=>$p->codaccion,
                        'tipomensual'=>$p->tipomensual,
                        'showModals'=>1,
                        'provincia'=>$p->provincia,
                        'ciudad'=>$p->ciudad,
                        'favorito'=>1
                    );
                    $prop[] = $pp;
               }
            }

            elseif(!empty($filtros['avisos'])){
               $propiedades = $this->db->get_where('view_avisos',array('email'=>$this->user->email));
               foreach($propiedades->result() as $p){
                    if($p->codaccion==2){
                      //$p->precio = moneda($p->precioalq).'/'.$p->tipomensual;
                      $p->precio = moneda($p->precio);
                    }else{
                      $p->precio = moneda($p->precio);
                    }
                    $fav = 0;
                    $pp = array(
                        'cod_ofer'=>$p->cod_ofer,
                        'altitud'=>$p->altitud,
                        'latitud'=>$p->latitud,
                        'precio'=>$p->precio,                        
                        'codaccion'=>$p->codaccion,
                        'tipomensual'=>$p->tipomensual,
                        'showModals'=>1,
                        'provincia'=>$p->provincia,
                        'ciudad'=>$p->ciudad,                        
                    );
                    $prop[] = $pp;
               }
            }
           echo json_encode($prop);
        }

        function propiedades(){            
            $this->load->library('grocery_crud')
                       ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('propiedades_list')
                 ->set_subject('bootstrap2')
                 ->set_theme('bootstrap2')
                 ->set_primary_key('cod_ofer');              
            $crud->columns('cod_ofer','altitud','latitud','datos','content','contentExpanded');
            $crud->callback_column('altitud',function($val,$row){
                return str_replace(',','.',$val);
            })->callback_column('latitud',function($val,$row){
                return str_replace(',','.',$val);
            })->callback_column('datos',function($val,$row){                
                $this->db->select('propiedades_list.*, api_descripciones.titulo, api_descripciones.descrip');
                $this->db->where(array('propiedades_list.cod_ofer'=>$row->cod_ofer));
                $detail = $this->querys->get_propiedades();
                if($detail->num_rows()>0){
                    $detail = $detail->row();
                    if(!empty($this->user->id)){
                        $detail->fav = $this->db->get_where('view_favoritos',array('user_id'=>$this->user->id,'cod_ofer'=>$detail->cod_ofer))->num_rows()>0?1:0;
                    }else{
                        $detail->fav = 0;
                    }
                }else{
                    $detail = null;
                }
                return $detail;
            });

            if(!empty($_POST['cod_ofer'])){
                $crud->callback_column('content',function($val,$row){
                    return get_instance()->load->view($this->theme.'_propiedadInfowindow',array('row'=>$row),TRUE,'paginas');
                })->callback_column('contentExpanded',function($val,$row){
                    return get_instance()->load->view($this->theme.'_propiedadInfoWindowSidebar',array('row'=>$row),TRUE,'paginas');
                });
                $crud->where('cod_ofer',$_POST['cod_ofer']);
            }

            //$crud->like('ciudad','barcelona');                      
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render();
            $crud->view = 'listado';            
            $this->loadView($crud);
        }

        function listado($theme = 'listado-list',$filtros = ''){
            if(!empty($filtros)){
                $this->decodeFiltros($filtros);
            }


            
            $this->load->library('grocery_crud')
                       ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('propiedades_list')
                 ->set_subject('bootstrap2')
                 ->set_theme($theme)
                 ->callback_column('foto',function($val){return str_replace('[base_url]',base_url(),$val);})
                 ->set_primary_key('id')
                 ->set_primary_key('propiedades_list_id','propiedades_list_extras');            
            $crud->columns('id','cod_ofer','precio','precioalq','m_uties','total_hab','banyos','propiedades_list_extras','datos','titulo','descrip');
            $crud->callback_column('datos',function($val,$row){return get_instance()->db->get_where('api_descripciones',array('cod_ofer'=>$row->cod_ofer,'idioma'=>$_SESSION['lang']))->row();});
            $crud->callback_column('titulo',function($val,$row){return !empty($row->datos->titulo)?$row->datos->titulo:'';});
            $crud->callback_column('descrip',function($val,$row){return !empty($row->datos->titulo)?$row->datos->descrip:'';});
            if(!empty($_POST)){
                if(!empty($_POST['precio_min'])){
                    $crud->where('( precio >='.$_POST['precio_min'].')','ESCAPE',FALSE);
                }
                if(!empty($_POST['precio_max'])){
                    $crud->where('( precio <='.$_POST['precio_max'].')','ESCAPE',FALSE);
                }
                if(!empty($_POST['m_uties'])){
                    $crud->where('( m_uties >='.$_POST['m_uties'].')','ESCAPE',FALSE);
                }
                if(!empty($_POST['total_hab'])){
                    $crud->where('( total_hab >='.$_POST['total_hab'].')','ESCAPE',FALSE);
                }

                $crud->where('my_province(propiedades_list.provincias_id) = 1','ESCAPE',FALSE);

                if(!empty($_POST['codigo_tipo'])){
                    $w = '';
                    foreach($_POST['codigo_tipo'] as $f){
                        $w.= "OR codigo_tipo='".$f."' ";
                    }
                    $w = '('.substr($w,3).')';
                    $this->db->where($w,NULL,FALSE);
                }
                if(!empty($_POST['propiedades_extras_id'])){
                    $w = '';
                    foreach($_POST['propiedades_extras_id'] as $f){
                        $w.= "AND propiedades_list.id IN ( SELECT propiedades_list_id from propiedades_list_extras JOIN propiedades_extras ON propiedades_extras.id = propiedades_list_extras.propiedades_extras_id WHERE codigo ='".$f."')";
                    }

                    $w = substr($w,3);
                    $this->db->where($w,NULL,FALSE);
                }
                $_SESSION['filtros'] = $_POST;
            }

            if(!empty($_GET['retorno']) && !empty($_SESSION['filtros'])){
                $_GET = $_SESSION['filtros'];                
                $datos = array();
                foreach($_GET['search_field'] as $n=>$v){
                    $datos[str_replace('propiedades_list.','',$v)] = $_GET['search_text'][$n];
                }
                $_GET = array_merge($_GET,$datos);
                unset($_GET['search_field']);
                unset($_GET['search_text']);
                unset($_GET['per_page']);
                unset($_GET['order_by']);
                unset($_GET['operator']);
                unset($_GET['page']);
                $_GET['acciones'] = $_GET['codaccion'];                
                header('Location:'.base_url().'listado/'.$this->encodeFiltros($_GET));
                die();
            }
            if(isset($_POST) && empty($_POST['order_by'][0]) && empty($_POST['order_by'][1])){
                $crud->order_by('precio','DESC');
            }
            $crud->set_url('propiedades/frontend/listado/'.$theme.'/');    
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render('','application/modules/propiedades/views/cruds/');
            $crud->view = 'listado';
            $crud->filtros = $filtros;
            $crud->lugar = $this->getLugar();
            $crud->url = $theme=='listado-griya'?'propiedades-en-list':'listado';
            $crud->title = 'Lista de propiedades en '.$this->getLugar();
            $this->loadView($crud);
        }

        function getLugar(){
            $label = 'España';
            if(!empty($_GET['provincias_id'])){
                $com = $this->db->get_where('provincias',array('id'=>$_GET['provincias_id']));
                if($com->num_rows()>0){
                    $label = $com->row()->nombre;
                }
            }

            if(!empty($_GET['comarcas_id'])){
                $com = $this->db->get_where('comarcas',array('id'=>$_GET['comarcas_id']));
                if($com->num_rows()>0){
                    $label = $com->row()->nombre;
                }
            }

            if(!empty($_GET['areas_id'])){
                $com = $this->db->get_where('areas',array('id'=>$_GET['areas_id']));
                if($com->num_rows()>0){
                    $label = $com->row()->nombre;
                }
            }

            if(!empty($_GET['ciudades_id'])){
                $com = $this->db->get_where('ciudades',array('id'=>$_GET['ciudades_id']));
                if($com->num_rows()>0){
                    $label = $com->row()->ciudad;
                }
            }            

            if(!empty($_GET['distritos_id'])){
                $com = $this->db->get_where('distritos',array('id'=>$_GET['distritos_id']));
                if($com->num_rows()>0){
                    $label = $com->row()->nombre;
                }
            }

            if(!empty($_GET['zonas_id'])){
                $com = $this->db->get_where('zonas',array('id'=>$_GET['zonas_id']));
                if($com->num_rows()>0){
                    $label = $com->row()->nombre;
                }
            }
            return $label;
        }

        function mapa($filtros = ''){ 
            $datos = $this->decodeFiltros($filtros);
            $lugar = $this->getLugar();
            if(!empty($datos['provincias_id'])){
                
                switch($datos['provincias_id']){
                    case '2': //Barcelona
                        $center = array('41.4634083','2.0708714','10');
                    break;
                    case '8': //Alicante
                        $center = array('38.6317414','-0.4567562','9.73');
                    break;   
                    default:     
                        $center = array('39.7196566','-3.6262126','7');        
                    break;             
                }

                if(!empty($datos['ciudades_id'])){
                    switch($datos['ciudades_id']){
                        case '1066': //Barcelona
                            $center = array('41.3788807','2.1671226','13');
                        break;
                        case 'IGUALADA':
                            $center = array('41.6154986','1.529334','11.19');
                        break;
                        
                        case '1685': //Alicante
                            $center = array('38.3526063','-0.4971124','13.75');
                        break;
                        default:     
                            $center = array('39.7196566','-3.6262126','7');        
                        break;
                    }
                }
            }else{
                $center = array('39.7196566','-3.6262126','7');
            }   

            
            $this->loadView(array('view'=>'mapa','filtros'=>$filtros,'datos'=>$datos,'center'=>$center,'lugar'=>$lugar));
        }


        function ver($propiedad = ''){

            if(empty($propiedad)){
                redirect('propiedades');
                die();
            }       

            $prev = 0;
            $next = 0;     

            if(!empty($_SESSION['propiedades_list'])){                
                $listado = $_SESSION['propiedades_list'];                
                $entradas = array();
                foreach($listado as $n=>$b){
                    $entradas[] = $b;
                }
                foreach($entradas as $n=>$b){
                    if($b->ref==$propiedad){
                        if(!empty($entradas[$n-1])){
                            $bb = $entradas[$n-1];
                            $prev = $entradas[$n-1]->ref;
                        }

                        if(!empty($entradas[$n+1])){
                            $bb = $entradas[$n+1];
                            $next = $entradas[$n+1]->ref;
                        }
                    }
                }
            }   

            $propiedad = urldecode($propiedad);
            
            $this->db->select('*, propiedades_list.id as propid');
            $this->db->where(array('propiedades_list.ref'=>$propiedad));
            $propiedad = $this->querys->get_propiedades();
            if($propiedad->num_rows()>0){
                $propiedad = $propiedad->row();
                $propiedad->fotos = $this->db->get_where('api_propiedad_fotos',array('cod_ofer'=>$propiedad->cod_ofer));
                $this->loadView(array('view'=>'propiedad','propiedad'=>$propiedad,'title'=>'Propiedad #'.$propiedad->ref,'prev'=>$prev,'next'=>$next));
            }else{
                //redirect('propiedades');
                die();
            }

        }

        function contactar($cod){
            $this->load->view('contactarModal',array('cod'=>$cod));
        }


        function lowcost(){            
            $this->load->library('grocery_crud')
                       ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('propiedades_list')
                 ->set_subject('bootstrap2')
                 ->set_theme('listado3x3')
                 ->where('lowcost',1)
                 ->callback_column('foto',function($val){return str_replace('[base_url]',base_url(),$val);});
            $crud->set_url('propiedades/frontend/lowcost/');
            $crud->columns('id','ref','banyos','acciones','total_hab','precio','ha_bajado','datos','titulo','descrip');
            $crud->callback_column('datos',function($val,$row){return get_instance()->db->get_where('api_descripciones',array('cod_ofer'=>$row->cod_ofer,'idioma'=>$_SESSION['lang']))->row();});
            $crud->callback_column('titulo',function($val,$row){return $row->datos->titulo;});
            $crud->callback_column('descrip',function($val,$row){return $row->datos->descrip;});
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render('','application/modules/propiedades/views/cruds/');
            $crud->view = 'lowcost';               
            $crud->title = 'Propiedades Low Cost';                     
            $this->loadView($crud);
        }

        function cuantoValeTuPiso(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('provincia_id','Provincia','required');
            $this->form_validation->set_rules('ciudad','Ciudad','required');
            $this->form_validation->set_rules('zona','Zona','required');
            $this->form_validation->set_rules('cp','Código postal','required');
            $this->form_validation->set_rules('m2','Metros cuadrados','required|is_numeric');
            $this->form_validation->set_rules('tipos_id','Tipo','required|is_numeric');
            if($this->form_validation->run()){                
                $valor = $this->elements->precioM2XZona($_POST['zona'],$_POST['tipos_id']);                
                $valor = $valor*$_POST['m2'];

                $provincia = $this->db->get_where('provincias',array('id'=>$_POST['provincia_id']));
                $aj = $this->db->get('ajustes')->row();
                $_POST['to'] = $provincia->num_rows()>0 && $provincia->row()->sucursal==278?$aj->email_contacto_igualada:$aj->email_contacto_alicante;
                $_POST['zona'] = @$this->db->get_where('zonas',array('id'=>$_POST['zona']))->row()->nombre;
                //enviar correos                
                $_POST['extras'] = '';
                foreach($_POST as $n=>$v){
                    //$n = validar_acentos($n);
                    $n = str_replace('_',' ',$n);
                    $n = ucfirst($n);
                    $v = str_replace('_',' ',$v);
                    $v = ucfirst($v); 
                    if($n!='Nombre' && $n!='Email' && $n!='Apellidos' && $n!='Teléfono'){
                        if($n=='Blanco'){                           
                            $_POST['extras'].= '<br/>';
                        }
                        elseif($n=='Provincia id'){
                            $n = l('provincia');
                            $v = $provincia->row()->nombre;
                            $_POST['extras'].= '<br/><b>'.$n.': </b> '.$v.'';
                        }
                        elseif($n=='Ciudad'){
                            $n = l('ciudad');
                            $v = $this->db->get_where('ciudades',array('id'=>$_POST['ciudad']))->row()->ciudad;
                            $_POST['extras'].= '<br/><b>'.$n.': </b> '.$v.'';
                        }
                        elseif($n=='Tipos id'){
                            $n = l('tipo-de-propiedad');
                            $v = $this->db->get_where('tipos',array('codigo'=>$v))->row()->label;
                            $_POST['extras'].= '<br/><b>'.$n.': </b> '.$v.'';
                        }

                        elseif($n=='Politicas'){
                            $n = '¿Se acepto las políticas de privacidad?';
                            $v = 'SI';
                            $_POST['extras'].= '<br/><b>'.$n.': </b> '.$v.'';
                        }
                        elseif($n=='Extras'){

                        }
                        else{
                            $n = l($n);
                            $n = l(mb_strtolower($n));
                            $n = validar_acentos($n);
                            $n = ucfirst($n);
                            $_POST['extras'].= '<br/><b>'.$n.': </b> '.$v.'';
                        }
                    }
                }      

                $notif = $this->db->get_where('notificaciones',array('id'=>15))->row();
                $notif->texto = str_replace('<span style="font-family: helvetica, arial, sans-serif;"><strong>Email</strong>:</span>','',$notif->texto);



                $email = $_POST['email'];
                $_POST['email'] = '<b>'.l('apellidos').': </b> '.$_POST['apellidos'];
                $_POST['email'].= '<br/><b>Email: </b> '.$email;
                $_POST['email'].= '<br/><b>'.l('telefono').': </b> '.$_POST['telefono'];

                $_POST['extras'].= '<br/><h2><b>'.l('Valor de tu piso').'</b>:'.moneda($valor).'</h2>';
                $_POST['propiedad'] = '';
                $firma = $_POST['to'] == 'info@finques-sasi.com'?19:20;
                $_POST['firma'] = $this->db->get_where('notificaciones',array('id'=>$firma))->row()->texto;

                $_POST['asunto'] = 'Alguien ha valorado su piso. en al web de sasi';
                $_POST['titulo'] = 'web sasi, alguien ha valorado su piso.';                
                $this->enviarcorreo((object)$_POST,$notif,$_POST['to']);
                $notif = $this->db->get_where('notificaciones',array('id'=>15))->row();
                $notif->texto = str_replace('<span style="font-family: helvetica, arial, sans-serif;"><strong>Email</strong>:</span>','',$notif->texto);
                $_POST['asunto'] = l('asunto-calculo-email');
                $_POST['titulo'] = l('title-calculo-email');
                $this->enviarcorreo((object)$_POST,$notif,$email);
                echo json_encode(array('success'=>true,'msj'=>moneda($valor)));
                
            }else{
                echo json_encode(array('success'=>false,'msj'=>$this->error($this->form_validation->error_string())));
            }
        }



    }
?>
