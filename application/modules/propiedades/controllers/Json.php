<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Json extends Main{
        function __construct() {
            parent::__construct();
        }

        function provincias($x = '',$y = ''){
        	$this->load->library('grocery_crud')
        			   ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('provincias')
            	 ->set_subject('bootstrap2')
            	 ->set_theme('bootstrap2');  
            $crud->columns('id','nombre'); 
            $crud->order_by('nombre','ASC');    
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function comarcas($x = '',$y = ''){
        	$this->load->library('grocery_crud')
        			   ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('comarcas')
            	 ->set_subject('bootstrap2')
            	 ->set_theme('bootstrap2');   
            $crud->columns('id','nombre');  
            $crud->order_by('nombre','ASC');  
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function ciudades($x = '',$y = ''){
        	$this->load->library('grocery_crud')
        			   ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('ciudades')
            	 ->set_subject('bootstrap2')
            	 ->set_theme('bootstrap2')
            	 ->display_as('ciudad','nombre');  
            $crud->columns('id','ciudad');   
            $crud->order_by('ciudad','ASC');  
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function zonas($x = '',$y = ''){
        	$this->load->library('grocery_crud')
        			   ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('zonas')
            	 ->set_subject('bootstrap2')
            	 ->set_theme('bootstrap2');   
            $crud->columns('id','nombre'); 
            $crud->order_by('nombre','ASC');   
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print()->unset_export();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function getSelectFavGroupToList($selected = ''){
            $str = '<option class="active" value="0">Grupo</option>';
            $str.= '<option value="0">Sin clasificar</option>';
            foreach(get_instance()->db->get_where('favoritos_grupos',array('user_id'=>get_instance()->user->id))->result() as $g){
                    $selectedLabel = $g->id==$selected?'class="active" selected="selected"':'';
                    $str.= '<option '.$selectedLabel.' value="'.$g->id.'">'.$g->nombre.'('.get_instance()->db->get_Where('favoritos',array('favoritos_grupos_id'=>$g->id,'user_id'=>get_instance()->user->id))->num_rows().')</option>';
            }
            echo $str;
        }
    }
?>
