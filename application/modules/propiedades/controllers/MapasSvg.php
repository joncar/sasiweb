<?php 
require_once APPPATH.'/controllers/Panel.php';    
class MapasSvg extends Main{
    function __construct() {
        parent::__construct();
    }

    function listado(){
    	$this->load->model('Mapassvg_model');
    	$datos = $this->Mapassvg_model->getMapInfo();
    	if(!$datos){
    		header('Location:'.base_url().'listado/'.$this->Mapassvg_model->encodeFiltros($_GET));
            echo "<html></html>";
            exit;
    	}        
        $_SESSION['propiedades_list'] = empty($_SESSION['propiedades_list'])?array():array();
        $title = $datos['titulo'];
        $filtros = $this->Mapassvg_model->encodeFiltros($_GET);
    	$this->loadView(array('view'=>'mapas','datos'=>$datos,'title'=>$title,'filtros'=>$filtros));
    }


}