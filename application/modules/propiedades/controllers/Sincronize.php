<?php   
    require_once APPPATH.'/controllers/Panel.php';    
    class Sincronize extends Main{
        function __construct() {
            parent::__construct();
        }

        function clearAll(){
            $this->load->library('api_inmovilla');
            if($this->api_inmovilla->can_sincronize()){
                $this->api_inmovilla->triggerClearPropiedades();
                $this->db->update('cron',array('current_page'=>0,'total_pages'=>0));
                $this->sincr();
            }
        }

        function test($page = ''){
            header('Content-Type: application/json');
            $this->load->library('api_inmovilla');            
            /*$this->api_inmovilla->echoResultServer = true;*/
            $this->api_inmovilla->set_sucursal('Igualada');
            //$this->api_inmovilla->fillPropiedades($page);
            $this->api_inmovilla->echoResultServer = true;
            //$this->api_inmovilla->getPropiedades(1,100,'ref=89553');
            $this->api_inmovilla->getPropiedad('8488058');
            //$this->api_inmovilla->rellenar_idiomas();
        }

        function getPropiedad(){
            $this->load->library('api_inmovilla');
            $this->api_inmovilla->set_sucursal('Igualada');
            $prop = $this->api_inmovilla->getPropiedades(1,100,"");
            echo print_r($prop,TRUE);
            echo '<hr>';
            //echo print_r($this->api_inmovilla->splitExtras($prop->ficha[1]),TRUE);
        }

        function sincr(){
            $this->load->library('api_inmovilla');
            echo '<p>Sincronizando</p>';
            
            //$this->api_inmovilla->echoResultServer = true;
            $this->db->where('max_iteraccion > 0',NULL,FALSE);            
            $crons = $this->db->get_where('cron');
            if($crons->num_rows()>0){                
                foreach($crons->result() as $cron){
                    $page = 1;
                    $this->api_inmovilla->set_sucursal($cron->sucursalName);
                    $this->api_inmovilla->fillPropiedades($page);                    
                }
            }

            //Actualizamos id de zonas
            $this->db->query('SELECT SanearPropiedades()');      
            $this->db->query('CALL moverPropiedades');
        }

        function sanear(){
            $crons = $this->db->get_where('ajustes')->row();
            if($crons->correr_saneado==1){                
                $this->db->query('SELECT SanearPropiedades()');
                $this->db->update('ajustes',array('correr_saneado'=>0));
            }
        }

        function webhook(){
            //$_POST = file_get_contents("php://input");            
            $prop = file_get_contents("php://input");            
            //$prop = '{"adaptadominus":"0","agua":"0","aire_con":"0","airecentral":"0","alarma":"0","alarmaincendio":"0","alarmarobo":"0","alta_exclusiva":null,"altillo":"0","alturatecho":"0.0","antesydespues":"0","antiguedad":"0","apartseparado":"0","arboles":"0","arma_empo":"0","ascensor":"0","aseos":"0","baja_exclusiva":null,"balcon":"0","banyos":"0","bar":"0","barbacoa":"0","buhardilla":"0","cajafuerte":"0","calefaccion":"0","calefacentral":"0","calle":"oeste","cesioncom":"0.0","chimenea":"0","cocina_inde":"0","cod_ofer":"7336448","comision":"0","comunidadincluida":"0","conservacion":"0","cp":"08788","depoagua":"0","descalcificador":"0","descripciones":{"1":{"titulo":"SOLAR EN VILANOVA DEL CAMI EN ZONA PARC FLUVIAL 1123","descripcion":"Solar esquinero de 205 m2 situado en la ronda oeste de Vilanova, vallado, con muchas posibilidades para la construcci\u00f3n de una casa . Situado en zona muy tranquila, a un paso de todos los servicios y con muy buena conexi\u00f3n con la ronda direcci\u00f3n autov\u00eda A-2. 11 1 13"},"12":{"titulo":"SOLAR A VILANOVA ZONA PARC FLUVIAL","descripcion":"Solar cantoner de 205 m2 situat a la ronda oest de Vilanova, tancat, amb moltes possibilitats per la construcci\u00f3 d\'una casa. Situat a una zona molt tranquil\u00b7la, a un pas de tots els serveis i amb molt bona connexi\u00f3 amb la ronda direcci\u00f3 autov\u00eda A-2."}},"despensa":"0","destacado":"0","diafano":"0","distmar":"0","edificio":"","electro":"0","emisionesletra":"G","emisionesvalor":"0","energialetra":"G","energiarecibido":"3","energiavalor":"0","eninternet":"1","entidadbancaria":"0","escalera":"S","estadoficha":"1","exclu":"0","fecha":"2018-11-23 12:48:41","fechaact":"2018-12-03 15:25:00","galeria":"0","garajedoble":"0","gasciudad":"0","gastos_com":"0","gimnasio":"0","habdobles":"0","habitaciones":"0","habjuegos":"0","haycartel":"0","hidromasaje":"0","hilomusical":"0","jacuzzi":"0","jardin":"0","key_acci":"1","key_acci_text":"vender","key_loca":"134599","key_loca_text":"Vilanova del Cami","key_tipo":"9099","key_tipo_text":"Terreno urbanizable","key_zona":"2043711","key_zona_text":"Can Tit\u00f3","keycarpin":"0","keycarpinext":"0","keyelectricidad":"0","keyfachada":"0","keyori":"0","keysuelo":"0","keytecho":"0","latitud":"41.5706571305532","lavanderia":"0","linea_tlf":"0","longitud":"1.63513708207319","luminoso":"0","luz":"0","m_altillo":"0","m_cocina":"0","m_comedor":"0","m_cons":"0","m_fachada":"0.0","m_parcela":"205","m_sotano":"0","m_terraza":"0","m_utiles":"0","mirador":"0","montacargas":"0","muebles":"0","nodisponible":"0","numero":"29","numllave":"","numplanta":"0","observacion":null,"ojobuey":"0","opcioncompra":"0","outlet":"0","panos":"0","parking":"0","patio":"0","pergola":"0","piscina_com":"0","piscina_prop":"0","planta":"0","plaza_gara":"0","porceniva":"0","precioalq":"0","precioinmo":"80000","precioiva":"0","preinstaacc":"0","preinsthmusi":"0","primera_linea":"0","prospecto":"0","puerta":"LO","puerta_blin":"0","puertasauto":"0","rcatastral":"6233609CG8063S0001SD","ref":"89416","registrod":null,"rfolio":"0","riegoauto":"0","rlibro":"0","rnumero":"0","r tomo":"0","salon":"0","satelite":"0","sauna":"0","solarium":"0","sotano":"0","tbano":"","tcocina":"","tenis":"0","teniscom":"0","terraza":"0","terrazaacris":"0","tfachada":"","tinterior":null,"tipomensual":"MES","todoext":"0","trastero":"0","trifasica":"0","tv":"0","vallado":"0","vestuarios":"0","video_port":"0","vistasalmar":"0","media":{"fotos":{"1":{"url":"https:\/\/fotos.apinmo.com\/278\/7336448\/0-1.jpg"}}}} ';
            $propjson = $prop;
            if(!empty($prop)){
                $prop = json_decode($prop);
                if(!empty($prop->cod_ofer)){
                    //Se consulta si esta
                    $this->load->library('api_inmovilla');
                    $pro = $this->db->get_where('propiedades_list',array('cod_ofer'=>$prop->cod_ofer));
                    
                    if($pro->num_rows()>0){
                        $pro = $pro->row();
                        $this->api_inmovilla->set_sucursal($pro->numagencia);
                    }else{
                        $this->api_inmovilla->set_sucursal($prop->idagencia);
                    }
                    
                    $p = $this->api_inmovilla->getPropiedades(1,100,'cod_ofer='.$prop->cod_ofer);
                    //$del = $prop->nodisponible==0?true:false; //Desactivar si se comprueba que llega en nodisponible = 1
                    $del = false;
                    $this->api_inmovilla->fillPropiedades(1,$p,$del);
                    //Actualizamos id de zonas
                    $this->db->update('ajustes',array('correr_saneado'=>1));
                    //$this->db->query('SELECT SanearPropiedades(\'\')');
                    //correo('joncar.c@gmail.com','Webhook',print_r($propjson,TRUE));
                }                
            }
            
        }

        function sendAvisos(){
            $this->db->select('avisos.id as avid,propiedades_list.id,avisos.email,propiedades_list.habajado, propiedades_list.cod_ofer, propiedades_list.ref');
            $this->db->join('propiedades_list','propiedades_list.cod_ofer = avisos.cod_ofer');
            $avisos = $this->db->get_where('avisos',array('avisar'=>1));
            foreach($avisos->result() as $a){
                $this->db->where('propiedades_list.id',$a->id);
                $aa = $this->querys->get_propiedades()->row();
                $aa->foto = '<img src="'.$aa->foto.'" style="width:100%;">';
                $aa->email = $a->email;   
                $precio =   $aa->precio;
                $aa->precio = moneda($aa->precio);
                $aa->link = base_url('propiedad/'.$aa->ref);
                $firma = $aa->email_contacto == 'info@finques-sasi.com'?19:20;
                $aa->firma = $this->db->get_where('notificaciones',array('id'=>$firma))->row()->texto;
                $aa->habajado = moneda($aa->habajado);

                $this->enviarcorreo($aa,11,$a->email);
                $this->db->update('avisos',array('avisar'=>0,'ultimo_precio'=>$precio),array('id'=>$a->avid));

            }
        }

        function processPhotos(){
            $this->load->library('api_inmovilla');
            echo '<p>Sincronizando</p>';

            $imgUrl = $_POST['imgUrl'];
            $imgInitW = $_POST['imgInitW'];
            $imgInitH = $_POST['imgInitH'];
            $imgW = $_POST['imgW'];
            $imgH = $_POST['imgH'];
            $imgY1 = $_POST['imgY1'];
            $imgX1 = $_POST['imgX1'];
            $cropW = $_POST['cropW'];
            $cropH = $_POST['cropH'];
            $jpeg_quality = 100;
            $name = explode("/",$imgUrl);
            $name = $name[count($name)-1];
            $output_filename = $_POST['url'].'/'.$name;
            $output_filename = explode(".",$output_filename);
            $output_filename = $output_filename[0].rand();
            $what = getimagesize($imgUrl);
            switch(strtolower($what['mime']))
            {
                case 'image/png':
                    $img_r = imagecreatefrompng($imgUrl);
                            $source_image = imagecreatefrompng($imgUrl);
                            $type = '.png';
                    break;
                case 'image/jpeg':
                    $img_r = imagecreatefromjpeg($imgUrl);
                            $source_image = imagecreatefromjpeg($imgUrl);
                            $type = '.jpeg';
                    break;
                case 'image/gif':
                    $img_r = imagecreatefromgif($imgUrl);
                            $source_image = imagecreatefromgif($imgUrl);
                            $type = '.gif';
                    break;
                default: die('image type not supported');
            }
            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW,$imgH, $imgInitW, $imgInitH);    
            $dest_image = imagecreatetruecolor($cropW, $cropH);
            imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW,$cropH, $cropW, $cropH);    
            imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
            $response = array("status" => 'success',"url" => base_url($output_filename.$type ));
        }

        function validarImportacion(){
            $propiedades = $this->db->get('propiedades_list')->num_rows();
            if($propiedades==0){
                correo('gerente@cuservi.com','Web sasi, importación fallida','Ocurrió un error en la importación de propiedades desde inmovilla, por lo que esta vacia la tabla propiedades');
            }
        }



    }
?>
