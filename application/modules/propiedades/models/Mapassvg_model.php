<?php

class Mapassvg_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function encodeFiltros($datos){
        $dat = '';
        foreach($datos as $n=>$v){
            if(!empty($v)){
                if(is_array($v)){
                    $v = implode(',',$v);
                }
                $v = str_replace('-','_',$v);
                $dat.='-'.$n.'-'.$v;
            }
        }            
        $dat = substr($dat,1);            
        return $dat;
    }

    function encodeFiltrosGet($datos){
        $dat = '';
        foreach($datos as $n=>$v){
            if(!empty($v)){
                if(is_array($v)){
                    $v = implode(',',$v);
                }
                $v = str_replace('-','_',$v);
                $dat.='&'.$n.'='.$v;
            }
        }
        $dat = substr($dat,1);
        return $dat;
    }

    function decodeFiltros($filtros){

        if(!empty($filtros)){
            $filtros = explode('-',$filtros);
            $data = array();

            if(count($filtros)%2==0){
                for($i=0;$i<count($filtros);){                        
                    $filtros[$i+1] = str_replace('_','-',$filtros[$i+1]);
                    $data[$filtros[$i]] = urldecode($filtros[$i+1]);
                    $i = $i+2;
                }
            }

            foreach($data as $n=>$v){
                if(strpos($v,',')>-1){
                    $data[$n] = explode(',',$v);
                }
            }


            $_GET = $data;                
            return $data;
        }
    }

    function get_propiedades($where){
    	if(!empty($_GET)){
    		if(!empty($_GET['acciones'])){
    			$this->db->where('pl.codaccion',$_GET['acciones']);
    		}
            if(!empty($_GET['lowcost'])){
                $this->db->where('pl.lowcost',1);
            }
            if(!empty($_GET['obranueva'])){
                $this->db->where('pl.nbconservacion','Obra Nueva');
            }
    		if(!empty($_GET['codigo_tipo']) && is_string($_GET['codigo_tipo'])){
                $w = '';
                foreach(explode(',',$_GET['codigo_tipo']) as $f){
                    $w.= "OR codigo_tipo='".$f."' ";
                }
                $w = '('.substr($w,3).')';
                $this->db->where($w,NULL,FALSE);
            }
    	}
        $this->db->where('my_province(provincias_id) = 1',NULL,TRUE);
    	$e = $this->db->get_where('propiedades_list as pl',$where);
    	return $e;
    }

    function getUrl($get,$name,$replace = ''){
        if(is_array($name)){
            foreach($name as $n=>$v){
                $get[$n] = $v;
            }
        }else{
            $get[$name] = $replace;        
        }
        return base_url().'propiedades/?'.$this->encodeFiltrosGet($get);
    }

    function getUrlListado($get,$name,$replace){
        if(is_array($name)){
            foreach($name as $n=>$v){
                $get[$n] = $v;
            }
        }else{
            $get[$name] = $replace;
        }
        return base_url().'listado/'.$this->encodeFiltros($get);
    }


	function getMapInfo(){
        $get = $_GET;
        $datos = array();        
        //Si mostramos el mapa general de españa?
        if(empty($get['provincias_id'])){
        	$datos['mapa'] = 'espana.svg';
        	$datos['tipo'] = 'provincias';
        	$datos['lugar'] = 'España';
        	$datos['total'] = 0;
            
            //$datos['showAll'] = 1;
            $datos['cercanias'] = array();
        	$this->db->order_by('nombre','ASC');
            $datos['datos'] = $this->db->get_where('provincias');
            foreach($datos['datos']->result() as $n=>$p){
            	$this->db->select('comarcas.*');  
                $this->db->order_by('nombre','ASC');          	
                $d = $this->db->get_where('comarcas',array('provincias_id'=>$p->id));
                $datos['datos']->row($n)->cant = 0;
                foreach($d->result() as $nn=>$vv){
                	$d->row($nn)->link = base_url().'propiedades?provincias_id='.$p->id.'&comarcas_id='.$vv->id.'&'.$this->encodeFiltrosGet($get);                	
                	$d->row($nn)->cant = $this->get_propiedades(array('provincias_id'=>$p->id,'comarcas_id'=>$vv->id))->num_rows();
                	$datos['datos']->row($n)->cant += $d->row($nn)->cant;
                }
                $datos['datos']->row($n)->datos = $d;
                $datos['datos']->row($n)->link = base_url().'propiedades?provincias_id='.$p->id.'&'.$this->encodeFiltrosGet($get);
                if($datos['datos']->row($n)->cant==0 || $datos['datos']->row($n)->datos->num_rows()==0){
                    $datos['datos']->row($n)->cant = $this->get_propiedades(array('provincias_id'=>$p->id))->num_rows();
                }
                $datos['total']+= @$datos['datos']->row($n)->cant;
            }

            $datos['filtros'] = $this->encodeFiltros($get);
            $datos['titulo'] = l('mapa-titulo-default',$datos);
            return $datos;
        }

        if(!empty($get['provincias_id']) && empty($get['comarcas_id'])){        	
        	$datos['tipo'] = 'comarcas';   
        	$this->db->order_by('comarcas.nombre','ASC');     	            
            $datos['datos'] = $this->db->get_where('comarcas',array('provincias_id'=>$get['provincias_id']));
            $datos['total'] = 0;
            $datos['showAll'] = 1;
            foreach($datos['datos']->result() as $n=>$p){
            	$this->db->select('ciudades.*, ciudades.ciudad as nombre');                 	       
                $d = $this->db->get_where('ciudades',array('comarcas_id'=>$p->id));                
                $datos['datos']->row($n)->cant = 0;
                foreach($d->result() as $nn=>$vv){
                	$d->row($nn)->link = base_url().'propiedades?comarcas_id='.$p->id.'&ciudades_id='.$vv->id.'&'.$this->encodeFiltrosGet($get);
                	$d->row($nn)->cant = $this->get_propiedades(array('provincias_id'=>$get['provincias_id'],'comarcas_id'=>$p->id,'ciudades_id'=>$vv->id))->num_rows();
                	$datos['datos']->row($n)->cant += $d->row($nn)->cant;
                }
                $datos['datos']->row($n)->datos = $d;
                $datos['datos']->row($n)->link = base_url().'propiedades?comarcas_id='.$p->id.'&'.$this->encodeFiltrosGet($get);
                $datos['total']+= @$datos['datos']->row($n)->cant;
            }

            switch($get['provincias_id']){
                case '2': //Barcelona Provincia
                    $datos['mapa'] = 'barcelona.svg';
                    $datos['lugar'] = 'Barcelona';
                    $datos['titulo'] = l('anuncios-en-la-provincia-de-barcelona',$datos);
                    $datos['cercanias'] = array(
                        'titulo'=>l('provincias'),
                        'datos'=>array(
                            'Girona'=>array($this->get_propiedades(array('provincias_id'=>'3'))->num_rows(),$this->getUrl($get,'provincias_id','3')),
                            'Lleida'=>array($this->get_propiedades(array('provincias_id'=>'4'))->num_rows(),$this->getUrl($get,'provincias_id','4')),
                            'Tarragona'=>array($this->get_propiedades(array('provincias_id'=>'5'))->num_rows(),$this->getUrl($get,'provincias_id','5')),
                        )
                    );
                break;
                case '8': //Alicante Provincia
                    $datos['mapa'] = 'alicante.svg';
                    $datos['lugar'] = 'Alicante';
                    $datos['titulo'] = l('anuncios-en-la-provincia-de-alicante',$datos);
                    $datos['cercanias'] = array(
                        'titulo'=>l('provincias'),
                        'datos'=>array(
                            'Albacete'=>array($this->get_propiedades(array('provincias_id'=>'13'))->num_rows(),$this->getUrl($get,'provincias_id','13')),
                            'Murcia'=>array($this->get_propiedades(array('provincias_id'=>'10'))->num_rows(),$this->getUrl($get,'provincias_id','10')),
                            'Valencia'=>array($this->get_propiedades(array('provincias_id'=>'7'))->num_rows(),$this->getUrl($get,'provincias_id','7')),
                        )
                    );
                break;
                default:
                    return false;
                break;
            }

            $datos['filtros'] = $this->encodeFiltros($get);
            return $datos;
        }

        if(!empty($get['provincias_id']) && !empty($get['comarcas_id']) && empty($get['ciudades_id'])){
        	switch($get['comarcas_id']){
        		case '16': //Anoia

                    $datos['mapa'] = 'anoia.svg';
                    $datos['lugar'] = 'Anoia';
                    $datos['tipo'] = 'ciudades';                      
                    $datos['total'] = 0;
                    $datos['showAll'] = 1;
                    $datos['showAllChilds'] = 1;
                    $this->db->order_by('areas.nombre','ASC');                                      
                    $datos['datos'] = $this->db->get_where('areas',array('comarcas_id'=>$get['comarcas_id']));
                    foreach($datos['datos']->result() as $n=>$p){
                        $this->db->select('ciudades.*, ciudades.ciudad as nombre');                       
                        $d = $this->db->get_where('ciudades',array('areas_id'=>$p->id));
                        $datos['datos']->row($n)->cant = 0;
                        foreach($d->result() as $nn=>$vv){
                            $d->row($nn)->link = base_url().'propiedades?areas_id='.$p->id.'&ciudades_id='.$vv->id.'&'.$this->encodeFiltrosGet($get);
                            $d->row($nn)->cant = $this->get_propiedades(array('provincias_id'=>$get['provincias_id'],'comarcas_id'=>$get['comarcas_id'],'areas_id'=>$p->id,'ciudades_id'=>$vv->id))->num_rows();                            
                            $datos['datos']->row($n)->cant += $d->row($nn)->cant;
                        }
                        $datos['datos']->row($n)->datos = $d;
                        $d = $get;
                        $d['ciudades_id'] = $p->id==6?886:null;
                        $datos['datos']->row($n)->link = $p->id==6?'?'.$this->encodeFiltrosGet($d):$this->getUrlListado($get,'areas_id',$p->id);
                        $datos['total']+= @$datos['datos']->row($n)->cant;
                    }

                    //en otros
                    $this->db->select('ciudades_id as id,ciudad as nombre, count(ciudad) as cant');
                    $this->db->where('comarcas_id = 16 AND areas_id IS NULL',NULL,TRUE);
                    $this->db->group_by('ciudad');
                    $datos['otros'] = $this->get_propiedades(array());
                    $datos['otroscant'] = 0;
                    foreach($datos['otros']->result() as $n=>$o){
                        $datos['otros']->row($n)->link = $this->getUrlListado($get,'ciudades_id',$o->id);
                        $datos['otros']->row($n)->nombre = empty(trim($o->nombre))?'Sin Zona':$o->nombre;
                        $datos['total']+=$o->cant;
                        $datos['otroscant']+=$o->cant;
                    }

                    $datos['filtros'] = $this->encodeFiltros($get);

                    $datos['cercanias'] = array(
                        'titulo'=>l('comarcas'),
                        'datos'=>array(                            
                            'Alt Penedès'=>array($this->get_propiedades(array('comarcas_id'=>'17'))->num_rows(),$this->getUrl($get,'comarcas_id','17')),
                            'Baix Llobregat'=>array($this->get_propiedades(array('comarcas_id'=>'15'))->num_rows(),$this->getUrl($get,'comarcas_id','15')),
                            'Vallès Occidental'=>array($this->get_propiedades(array('comarcas_id'=>'13'))->num_rows(),$this->getUrl($get,'comarcas_id','13'))
                        )
                    );    
                    $datos['titulo'] = l('anuncios-en-la-comarca-de-anoia',$datos);              
		            return $datos;
        		break;
        		default:
        			return false;
        		break;
        	}
        }

        if(!empty($get['provincias_id']) && !empty($get['comarcas_id']) && !empty($get['ciudades_id']) && empty($get['zonas_id'])){
        	switch($get['ciudades_id']){
        		case '1685': //Alicante
        			$datos['mapa'] = 'alicante-ciudad.svg';
        			$datos['lugar'] = 'Alicante';
        			$datos['tipo'] = 'distritos';
        			$datos['total'] = 0;                    
        			$datos['datos'] = $this->db->get_where('distritos',array('ciudades_id'=>$get['ciudades_id']));
                    $datos['showAll'] = 1;
                    $datos['showAllChilds'] = 1;
		            foreach($datos['datos']->result() as $n=>$p){
		            	$this->db->select('zonas.*');		            	
		                $d = $this->db->get_where('zonas',array('distritos_id'=>$p->id));
		                $datos['datos']->row($n)->cant = 0;
		                foreach($d->result() as $nn=>$vv){
		                	$d->row($nn)->link = base_url().'propiedades?distritos_id='.$p->id.'&zonas_id='.$vv->id.'&'.$this->encodeFiltrosGet($get);
		                	$d->row($nn)->cant = $this->get_propiedades(array('provincias_id'=>$get['provincias_id'],'comarcas_id'=>$get['comarcas_id'],'ciudades_id'=>$get['ciudades_id'],'zonas_id'=>$vv->id))->num_rows();
                			$datos['datos']->row($n)->cant += $d->row($nn)->cant;
		                }
		                $datos['datos']->row($n)->datos = $d;
		                $datos['datos']->row($n)->link = $this->getUrlListado($get,'distritos_id',$p->id);
		                $datos['total']+= @$datos['datos']->row($n)->cant;
		            }

                    //en otros
                    $this->db->select('zona as nombre, count(zona) as cant');
                    $this->db->where('ciudades_id = 1685 AND (zonas_id IS NULL OR distritos_id IS NULL)',NULL,TRUE);
                    $this->db->group_by('zona');
                    $datos['otros'] = $this->get_propiedades(array());
                    $datos['otroscant'] = 0;
                    foreach($datos['otros']->result() as $n=>$o){
                        $datos['otros']->row($n)->link = $this->getUrlListado($get,'ciudades_id','1685');
                        $datos['otros']->row($n)->nombre = empty(trim($o->nombre))?'Sin Zona':$o->nombre;
                        $datos['total']+=$o->cant;
                        $datos['otroscant']+=$o->cant;
                    }

		            $datos['filtros'] = $this->encodeFiltros($get);

                    $datos['cercanias'] = array(
                        'titulo'=>l('poblaciones'),
                        'datos'=>array(                            
                            'Sant Joan d\'Alacant'=>array($this->get_propiedades(array('ciudades_id'=>'1690'))->num_rows(),$this->getUrl($get,'ciudades_id','1690')),
                            'Torrevieja'=>array($this->get_propiedades(array('comarcas_id'=>35,'ciudades_id'=>'1816'))->num_rows(),$this->getUrl($get,array('comarcas_id'=>35,'ciudades_id'=>'1816'))),
                            'El Campello'=>array($this->get_propiedades(array('ciudades_id'=>'1687'))->num_rows(),$this->getUrl($get,'ciudades_id','1687')),
                            'San Vicente del Raspeig'=>array($this->get_propiedades(array('ciudades_id'=>'1691'))->num_rows(),$this->getUrl($get,'ciudades_id','1691')),
                            'Mutxamel'=>array($this->get_propiedades(array('ciudades_id'=>'1689'))->num_rows(),$this->getUrl($get,'ciudades_id','1689')),
                            'Santa pola'=>array($this->get_propiedades(array('ciudades_id'=>'1822','comarcas_id'=>29))->num_rows(),$this->getUrl($get,array('comarcas_id'=>29,'ciudades_id'=>'1822'))),
                        )
                    );
                    $datos['titulo'] = l('anuncios-en-la-ciudad-de-alicante',$datos);
		            return $datos;
        		break;

                case '886': //Igualada
                    $datos['mapa'] = 'igualada.svg';
                    $datos['lugar'] = 'Igualada';
                    $datos['tipo'] = 'barrios';                    
                    $datos['total'] = 0;
                    $datos['datos'] = $this->db->get_where('distritos',array('ciudades_id'=>$get['ciudades_id']));
                    $datos['showAll'] = 1;
                    $datos['showAllChilds'] = 1;
                    foreach($datos['datos']->result() as $n=>$p){
                        $this->db->select('zonas.*');                       
                        $d = $this->db->get_where('zonas',array('distritos_id'=>$p->id));
                        $datos['datos']->row($n)->cant = 0;
                        foreach($d->result() as $nn=>$vv){
                            $d->row($nn)->link = base_url().'propiedades?distritos_id='.$p->id.'&zonas_id='.$vv->id.'&'.$this->encodeFiltrosGet($get);
                            $d->row($nn)->cant = $this->get_propiedades(array('provincias_id'=>$get['provincias_id'],'comarcas_id'=>$get['comarcas_id'],'ciudades_id'=>$get['ciudades_id'],'zonas_id'=>$vv->id))->num_rows();
                            $datos['datos']->row($n)->cant += $d->row($nn)->cant;
                        }
                        $datos['datos']->row($n)->datos = $d;

                        $datos['datos']->row($n)->link = $this->getUrlListado($get,'distritos_id',$p->id);
                        $datos['total']+= @$datos['datos']->row($n)->cant;
                    }

                    //en otros
                    $this->db->select('zona as nombre, count(zona) as cant');
                    $this->db->where('ciudades_id = 886 AND (zonas_id IS NULL OR distritos_id IS NULL)',NULL,TRUE);
                    $this->db->group_by('zona');
                    $datos['otros'] = $this->get_propiedades(array());
                    $datos['otroscant'] = 0;
                    foreach($datos['otros']->result() as $n=>$o){
                        $datos['otros']->row($n)->link = $this->getUrlListado($get,'ciudades_id','886');
                        $datos['otros']->row($n)->nombre = empty(trim($o->nombre))?'Sin Zona':$o->nombre;
                        $datos['total']+=$o->cant;
                        $datos['otroscant']+=$o->cant;
                    }

                    $datos['filtros'] = $this->encodeFiltros($get);
                    
                    $datos['cercanias'] = array(
                        'titulo'=>l('poblaciones'),
                        'datos'=>array(
                            'Òdena'=>array($this->get_propiedades(array('ciudades_id'=>'891'))->num_rows(),$this->getUrl($get,'ciudades_id','891')),
                            'Vilanova del Camí'=>array($this->get_propiedades(array('ciudades_id'=>'907'))->num_rows(),$this->getUrl($get,'ciudades_id','907')),
                            'Jorba'=>array($this->get_propiedades(array('ciudades_id'=>'887'))->num_rows(),$this->getUrl($get,'ciudades_id','887')),
                            'Capellades'=>array($this->get_propiedades(array('ciudades_id'=>'881'))->num_rows(),$this->getUrl($get,'ciudades_id','881')),
                            'La Pobla de Claramunt'=>array($this->get_propiedades(array('ciudades_id'=>'895'))->num_rows(),$this->getUrl($get,'ciudades_id','895')),                            
                            'Sant Martí de Tous'=>array($this->get_propiedades(array('ciudades_id'=>'900'))->num_rows(),$this->getUrl($get,'ciudades_id','900')),                            
                            'Piera'=>array($this->get_propiedades(array('ciudades_id'=>'893'))->num_rows(),$this->getUrl($get,'ciudades_id','893')),                            
                            'Santa Margarida de Montbui'=>array($this->get_propiedades(array('ciudades_id'=>'902'))->num_rows(),$this->getUrl($get,'ciudades_id','902')),
                        )
                    );
                    $datos['titulo'] = l('anuncios-en-la-ciudad-de-igualada',$datos);
                    return $datos;
                break;

        		case '1066': //Barcelona
        			$datos['mapa'] = 'barcelona-ciudad.svg';
        			$datos['lugar'] = 'Barcelona';
        			$datos['tipo'] = 'distritos';                    
        			$datos['total'] = 0;
                    $datos['showAll'] = 1;
                    $datos['showAllChilds'] = 1;
        			$datos['datos'] = $this->db->get_where('distritos',array('ciudades_id'=>$get['ciudades_id']));
		            foreach($datos['datos']->result() as $n=>$p){
		            	$this->db->select('zonas.*');		            	
		                $d = $this->db->get_where('zonas',array('distritos_id'=>$p->id));
		                $datos['datos']->row($n)->cant = 0;
		                foreach($d->result() as $nn=>$vv){
		                	$d->row($nn)->link = base_url().'propiedades?distritos_id='.$p->id.'&zonas_id='.$vv->id.'&'.$this->encodeFiltrosGet($get);
		                	$d->row($nn)->cant = $this->get_propiedades(array('provincias_id'=>$get['provincias_id'],'comarcas_id'=>$get['comarcas_id'],'ciudades_id'=>$get['ciudades_id'],'distritos_id'=>$p->id,'zonas_id'=>$vv->id))->num_rows();
                			$datos['datos']->row($n)->cant += $d->row($nn)->cant;
		                }
		                $datos['datos']->row($n)->datos = $d;
		                $datos['datos']->row($n)->link = $this->getUrlListado($get,'distritos_id',$p->id);
		                $datos['total']+= @$datos['datos']->row($n)->cant;
		            }

                    //en otros
                    $this->db->select('zona as nombre, count(zona) as cant');
                    $this->db->where('ciudades_id = 1066 AND (zonas_id IS NULL OR distritos_id IS NULL)',NULL,TRUE);
                    $this->db->group_by('zona');
                    $datos['otros'] = $this->get_propiedades(array());
                    $datos['otroscant'] = 0;
                    foreach($datos['otros']->result() as $n=>$o){
                        $datos['otros']->row($n)->link = $this->getUrlListado($get,'ciudades_id','1066');
                        $datos['otros']->row($n)->nombre = empty(trim($o->nombre))?'Sin Zona':$o->nombre;
                        $datos['total']+=$o->cant;
                        $datos['otroscant']+=$o->cant;
                    }


		            $datos['filtros'] = $this->encodeFiltros($get);
                    
                    $datos['cercanias'] = array(
                        'titulo'=>l('poblaciones'),
                        'datos'=>array(
                            'Badalona'=>array($this->get_propiedades(array('ciudades_id'=>'1065'))->num_rows(),$this->getUrl($get,'ciudades_id','1065')),
                            'Sant Adrià de Besòs'=>array($this->get_propiedades(array('ciudades_id'=>'1068'))->num_rows(),$this->getUrl($get,'ciudades_id','1068')),
                            'L\'Hospital de Llobregat'=>array($this->get_propiedades(array('ciudades_id'=>'1067'))->num_rows(),$this->getUrl($get,'ciudades_id','1067')),
                            'El Prat de Llobregat'=>array($this->get_propiedades(array('comarcas_id'=>15,'ciudades_id'=>'1037'))->num_rows(),$this->getUrl($get,array('ciudades_id'=>'1037','comarcas_id'=>15))),
                            'Sta Coloma de Gramenet'=>array($this->get_propiedades(array('ciudades_id'=>'1069'))->num_rows(),$this->getUrl($get,'ciudades_id','1069')),                            
                        )
                    );
                    $datos['titulo'] = l('anuncios-en-la-ciudad-de-barcelona',$datos);
		            return $datos;
        		break;
        		default:
        			return false;
        		break;
        	}	
        }

        return false;
    }
}