<?php 

$propiedad = $this->db->get_where('propiedades_list',array('cod_ofer'=>$cod));
if($propiedad->num_rows()>0): 
	$detail = $propiedad->row();
?>
<form action="paginas/frontend/contacto" onsubmit="sendForm(this,'#responseContactFormInfo'); return false;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <h4 class="modal-title"><?= l('Pedir información sobre el inmueble') ?> <span style="color: #e31f22;">Ref: <?= $detail->ref ?></span></h4>
		  </div>
		  <div class="modal-body">		    
	          <div class="row">
	            <div class="col-xs-12 col-md-12">
	              <div class="form-group">
	                <input type="text" class="form-control" name="nombre" placeholder="<?= l('nombre') ?>">
	              </div>
	            </div>
	            <div class="col-xs-12 col-md-12">
	              <div class="form-group">
	                <input type="tel" class="form-control" name="extras[telefono]" placeholder="<?= l('telefono') ?>">
	              </div>
	            </div>
	            <div class="col-xs-12 col-md-12">
	              <div class="form-group">
	                <input type="email" class="form-control" name="email" placeholder="Email">
	              </div>
	            </div>
	          </div>

	          <div class="row">
	            <div class="col-xs-12 col-md-12">
	              <div class="form-group">
	                <textarea class="form-control" placeholder="<?= l('comentario') ?>" name="extras[comentario]"></textarea>
	              </div>
	             </div>
	              <div class="col-xs-12 col-md-12 bottom20">
	                  <?= l('indica-fecha-hora') ?>
	              </div>
	              <div class="col-xs-12 col-md-12">                        
	                <div class="col-xs-12 col-md-6" style="padding-left:0">
	                  <div class="form-group">
	                      <label for="fecha"><?= l('fecha') ?></label>
	                      <div class="input-group">                              
	                        <input type="text" class="form-control fecha" id="fecha" name="extras[fecha_visita]" placeholder="<?= l('fecha') ?> d/m/a">                              
	                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
	                      </div>
	                    </div>
	                </div>
	                <div class="col-xs-12 col-md-6" style="padding-left:0;">
	                  <div class="form-group">
	                    <label for="hora"><?= l('hora') ?></label>
	                    <div class="input-group">                            
	                      <input type="text" class="form-control hora" id="hora" name="extras[hora_visita]" placeholder="<?= l('hora') ?> 00:00">                              
	                      <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
	                    </div>
	                  </div>
	                </div>
	              </div>

	              <input type="hidden" name="extras[ref]" value="<?= $detail->ref ?>" >
	              
	              <div class="col-xs-12 col-md-12">	                  
	                  <div class="form-group">
	                    <input type="checkbox" name="politicas" value="1"> <?= l('acepto-el') ?> <a href="<?= base_url() ?>aviso-legal-igualada.html"><?= l('aviso-legal') ?></a> y <a href="<?= base_url() ?>politicas-igualada.html"><?= l('politicas-de-privacidad') ?></a>
	                  </div>                          
	              </div>
	              
	              
	            </div>
	            <div id="responseContactFormInfo"></div>
		  </div>
		  <div class="modal-footer">
		  	<input type="hidden" class="form-control" name="titulo" value="Web SASI, información sobre el inmueble: <?= $detail->ref ?>">
		  	<input type="hidden" class="form-control" name="to" value="<?= $detail->email_contacto ?>">		  	
		  	<input type="hidden" class="form-control" name="extras[ref]" value="<?= $detail->ref ?>">
		  	<input type="hidden" name="referencia" value="<?= $detail->ref ?>">
		    


		    <button type="submit" class="btn btn-success"><?= l('enviar') ?></button>
     		<button type="button" class="btn btn-danger" data-dismiss="modal"><?= l('cerrar') ?></button>
		  </div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</form>
<?php else: ?>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <h4 class="modal-title"><?= l('propiedad-no-encontrada') ?></h4>
		  </div>
		  <div class="modal-body">
		    <p><?= l('propiedad-no-encontrada-text') ?></p>
		  </div>
		  <div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal"><?= l('cerrar') ?></button>		    
		  </div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
<?php endif ?>
<script>
  $(".fecha").datepicker();
  $(".hora").mask("00:00");
</script>