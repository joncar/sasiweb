<?php foreach ($list as $num_row =>$row): ?>
 <div class="row bg-hover">
	  <div class="my-pro-list">
	    <div class="col-md-2 col-sm-2 col-xs-12">


		  <div class="propiedadDetailFoto propiedadDetailFav owl-carousel">

			<?php         
		        foreach($row->fotos as $f): 
		    ?>
		      <a href="<?= base_url('propiedad/'.$row->cod_ofer) ?>">
			      <div style="width:100%; height:130px; background:url(<?= $row->foto ?>); background-size:cover;">
			      	<img src="<?= $row->foto ?>" style="width:100%; display: none;" alt="image"/>
			      </div>
		  	  </a>
			<?php endforeach ?>
		  </div>

	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="feature-p-text">
	        <h4><?= $row->nbtipo ?></h4>
	        <p><?= $row->cp.', '.$row->zona.' '.$row->ciudad.' '.$row->provincia ?></p>
	        <span><b>Acción:</b>  <?= $row->acciones ?></span><br>
	        <span><b>Ref:</b>  <?= $row->ref ?></span><br>
	        <div class="button-my-pro-list">
	          <a href="<?= base_url('propiedad/'.$row->cod_ofer) ?>"><?= moneda($row->precioinmo) ?></a>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-2 col-sm-2 col-xs-12">
	      <div class="select-pro-list">
	        <!--<a href="#"><i class="icon-pen2"></i></a>-->
	        <a href="<?= $row->delete_url ?>" class="delete-row" title="Eliminar favorito"><i class="icon-cross"></i></a>
	      </div>
	    </div>
	  </div>
	</div>
<?php endforeach ?>

<?php if(count($list)==0): ?>
<div class="row">
	<div class="col-xs-12 col-md-12">
		<?= l('sin-propiedades-en-esta-zona') ?>
	</div>
</div>
<?php endif ?>