<?php
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js('js/cruds/cookies.js',TRUE);
$this->set_js('js/cruds/listados.js',TRUE);
$this->set_js('js/cruds/jquery.form.js',TRUE);
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js('js/cruds/jquery.printElement.min.js',true);
$this->set_js('js/cruds/pagination.js',true);
//$this->set_css($this->default_css_path . 'jquery_plugins/fancybox/jquery.fancybox.css');
//$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
//$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');
$this->load_js_jqueryui();
$filtros = get_instance()->encodeFiltros($_GET);
$default_per_page = 12;
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    if(typeof(localStorage.crud_page)!=='undefined'){
        var crud_pagin = parseInt(localStorage.crud_page);
    }else{
        var crud_pagin = 1;
    }
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<!-- Property Search area Start -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-9" style="padding:0">
        <div class="row barraListadoBotones">
          <div class="col-xs-8 col-sm-6 col-md-6">
            <a title="<?= l('mostrar-en-lista') ?>" href="<?= base_url('listado/'.$filtros.'/') ?>" style="font-size:18px"><i class="fa fa-list"></i></a>
            <a title="<?= l('mostrar-en-cuadricula') ?>" href="<?= base_url('propiedades-en-list/'.$filtros.'/') ?>" style="margin-left:10px; font-size:18px"><i class="fa fa-th-large"></i></a>            
            <a title="<?= l('mostrar-propiedades-en-el-mapa') ?>" href="<?= base_url('propiedades-en-mapa/'.$filtros.'/') ?>" style="margin-left:10px; font-size:18px"><i class="fa fa-map-marker"></i>  <small><?= l('ver-propiedades-en-el-mapa'); ?></small></a>            
          </div>
          <div class="col-xs-4 col-sm-6 col-md-5" style="text-align: right">
            <a class="visible-xs visible-sm" title="Filtrar propiedades" data-toggle="collapse" data-target=".collapseFilter" href="javascript:void(0)" style="float:right; font-size: 18px;"><i class="fa fa-filter"></i>  <small><?= l('filtrar') ?></small></a>            
            <span class="totalResultsList hidden-xs"><?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
                <?php $paging_total_results = "<span id='total_items' class='total_items'>".number_format($total_results,0,',','.')."</span>" ?>
                <?php
                echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results), $this->l('list_displaying')
                );
                ?>
            </span>            
          </div>
          
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Property Search area End -->
<!-- Listing Start -->

<section id="listing1" class="listing1 bg_light" style="padding-top:70px;">
  <div class="container">
     <div class="row">        
           <aside class="col-md-2 col-xs-12 bg_light">
                <?php get_instance()->load->view(get_instance()->theme.'_propiedades_aside',array(),FALSE,'paginas'); ?>    
            </aside>

            <div class="col-md-10 col-sm-12 col-xs-12">

            <div class="botonesamarillos">
              <div>
                <div class="intro" style="padding: 0;">                
                  <select class="field-sorting">
                    <option class="active" value=""><?= l('ordenar-por') ?></option>
                    <option><?= l('todas') ?></option>
                    <option value="1" data-field="precio" data-type="ASC"><?= l('precio-mas-bajo') ?></option>
                    <option value="2" data-field="precio" data-type="DESC"><?= l('precio-mas-alto') ?></option>
                    <option value="3" data-field="precio_m_uties" data-type="ASC"><?= l('precio-m2-mas-bajo') ?></option>
                    <option value="4" data-field="precio_m_uties" data-type="DESC"><?= l('precio-m2-mas-alto') ?></option>
                    <option value="5" data-field="outlet" data-type="DESC"><?= l('ha-bajado-de-precio') ?></option>
                    <option value="9" data-field="lowcost" data-type="DESC">Low Cost</option>
                    <option value="6" data-field="m_uties" data-type="DESC"><?= l('mas-m2') ?></option>
                    <option value="7" data-field="total_hab" data-type="DESC"><?= l('mas-habitaciones') ?></option>
                    <option value="8" data-field="total_hab" data-type="ASC"><?= l('menos-habitaciones') ?></option>
                  </select>
                </div>
              </div>

              
              <div class="botonamarillo">                
                  <a href="<?= base_url('propiedades-en-mapa/'.$filtros.'/') ?>">
                    <i class="fa fa-map-marker"></i> <?= l('ver-propiedades-en-el-mapa') ?>
                  </a>
              </div>
              <div class="botonamarillo">                
                  <a href="javascript:history.back();"><i class="fa fa-arrow-left"></i> <?= l('volver-al-listado') ?></a>                                  
              </div>
            </div>


            <div class="flexigrid" style="margin-top:40px;">
                <div class="ajax_list index2" id="deals">
                    <?= l('cargando-propiedades') ?>... 
                </div>
                <div class="padding_bottom text-center" style="margin-top:40px;">
                  <ul class="pager">
                  </ul>
                </div>
                <input type="hidden" class="per_page" name="per_page" id="per_page" value="12">
                <input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
                <input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
            </div>
     </div>
      
    </div>
  </div>
</section>
<!-- Listing end -->
<?php echo form_close() ?>
