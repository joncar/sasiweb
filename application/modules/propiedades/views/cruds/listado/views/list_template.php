<?php
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js('js/cruds/cookies.js',TRUE);
$this->set_js('js/cruds/listados-fav.js',TRUE);
$this->set_js('js/cruds/jquery.form.js',TRUE);
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js('js/cruds/jquery.printElement.min.js',true);
$this->set_js('js/cruds/pagination.js',true);

$this->load_js_jqueryui();
$filtros = get_instance()->encodeFiltros($_GET);
$default_per_page = 12;
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<!-- Property Search area Start -->

<!-- Property Search area End -->
<!-- Listing Start -->

<section id="listing1" class="listing1 bg_light" style="padding-top:70px;">
  <div class="container">
     <div class="row">        
           

            <div class="col-md-12 col-sm-12 col-xs-12">
                
                
            

            <div class="botonesamarillos toolsSearchOrder">
              
              <div>
                <div class="intro">
                    <select class="field-sorting">
                      <option class="active" value=""><?= l('ordenar-por') ?></option>
                      <option><?= l('todas') ?></option>
                      <option value="1" data-field="precio" data-type="ASC"><?= l('precio-mas-bajo') ?></option>
                      <option value="2" data-field="precio" data-type="DESC"><?= l('precio-mas-alto') ?></option>
                      <option value="3" data-field="precio_m_uties" data-type="ASC"><?= l('precio-m2-mas-bajo') ?></option>
                      <option value="4" data-field="precio_m_uties" data-type="DESC"><?= l('precio-m2-mas-alto') ?></option>
                      <option value="5" data-field="outlet" data-type="DESC"><?= l('ha-bajado-de-precio') ?></option>
                      <option value="9" data-field="lowcost" data-type="DESC">Low Cost</option>
                      <option value="6" data-field="m_uties" data-type="DESC"><?= l('mas-m2') ?></option>
                      <option value="7" data-field="total_hab" data-type="DESC"><?= l('mas-habitaciones') ?></option>
                      <option value="8" data-field="total_hab" data-type="ASC"><?= l('menos-habitaciones') ?></option>
                    </select>
                  </div>
              </div>

              <div>
                <div class="botonamarillo">                
                    <a href="<?= base_url('propiedades-en-mapa/'.$filtros.'/') ?>"><i class="fa fa-map-marker"></i> <?= l('ver-propiedades-en-el-mapa') ?></a>
                </div>
              </div>
              <div> 
                <div class="intro" >
                  <select class="field-sorting" id="groupFavSelect" name="favoritos_grupos_id">
                    <option class="active" value="0"><?= l('grupo') ?></option>
                    <option value="0"><?= l('Sin clasificar') ?></option>
                    <?php foreach(get_instance()->db->get_where('favoritos_grupos',array('user_id'=>get_instance()->user->id))->result() as $g): ?>
                      <option value="<?= $g->id ?>"><?= $g->nombre ?> (<?= get_instance()->db->get_Where('view_favoritos',array('favoritos_grupos_id'=>$g->id,'user_id'=>get_instance()->user->id,'idioma'=>$_SESSION['lang']))->num_rows() ?>)</option>
                    <?php endforeach ?>                    
                  </select>
                  <div>
                    <a id="deleteFavButton" href="javascript:void(0)" onclick="javascript:remoteConnection(favDelete,{},function(){document.location.reload()})" style="display: inline-blocK; width:49%;">
                      <span style="display: none;"><i class="fa fa-minus-circle "></i> <?= l('Eliminar grupo') ?></span>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#nuevoGrupo" style="display: inline-blocK; width:49%; text-align: right">
                      <i class="fa fa-plus-circle "></i> <?= l('Agregar nuevo') ?>
                    </a>
                  </div>
                </div>                
                
              </div>
            </div>


            <div class="flexigrid" style="margin-top:40px;">
                <div id="property" class="ajax_list" id="deals">
                    <?= l('cargando-propiedades') ?>... 
                </div>
                <div class="padding_bottom text-center" style="margin-top:40px;">
                  <ul class="pager">
                  </ul>
                </div>
                <input type="hidden" class="per_page" name="per_page" id="per_page" value="12">
                <input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
                <input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
            </div>
     </div>
      
    </div>
  </div>
</section>
<!-- Listing end -->
<?php echo form_close() ?>
<script>
  var mapaUrl = '<?= base_url().'propiedades-en-mapa/'.$filtros ?>';
  favDelete = 'propiedades/account/favoritos_grupos/delete/1';
  $("#groupFavSelect").on('change',function(){
    favDelete = 'propiedades/account/favoritos_grupos/delete/'+$(this).val();
    if($(this).val()!=='0'){
      $("#deleteFavButton span").show();
      $(".botonamarillo a").attr('href',mapaUrl+'-grupo-'+$(this).val());
    }else{
      $(".botonamarillo a").attr('href',mapaUrl);
      $("#deleteFavButton span").hide();
    }
  });
</script>
<div id="nuevoGrupo" class="modal fade">
  <form action="" onsubmit="insertar('propiedades/account/favoritos_grupos/insert',new FormData(this),'#responseFavGroup',function(data){$('#responseFavGroup').addClass('alert alert-success').html(data.success_message); setTimeout(function(){document.location.reload()},1000)}); return false;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="form-group">
            <label for=""><?= l('nombre') ?>: </label>
                        <input type="text" name="nombre" class="form-control" placeholder="<?= l('nombre-grupo-text') ?>">
          </div>
          <div id="responseFavGroup"></div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success"><?= l('guardar') ?></button>
          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#nuevoGrupo"><?= l('cerrar') ?></button>
        </div>
      </div>
    </div>
  </form>
</div>