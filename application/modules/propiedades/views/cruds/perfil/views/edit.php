<?php		
	$this->set_js('js/cruds/jquery.form.js',TRUE);
	$this->set_js('js/cruds/flexigrid-edit.js',TRUE);	
	$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
?>



<div class="flexigrid crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
	<?php echo form_open( $update_url, 'method="post" class="callus" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>

			
	      <div class="container-3">

	      	<div id='report-error' style="display:none" class='alert alert-danger'></div>
            <div id='report-success' style="display:none" class='alert alert-success'></div>

	        <div class="row">
	          <div class="col-md-4 col-sm-6 col-xs-12">
	            <h2 class="text-uppercase bottom30">Mi perfil</h2>
	            <div class="agent-p-img">
	              <?php echo $input_fields['foto']->input; ?>
	              <button class="btn-blue border_radius"><?php echo $this->l('form_save'); ?></button>	              
	            </div>
	          </div>
	          <div class="col-md-8">
	            <div class="profile-form">
	              <div class="row">
	                
	                  <div class="col-sm-4">
	                    <div class="single-query">
	                      <label>Nombre:</label>
	                    </div>
	                  </div>
	                  <div class="col-sm-8">
	                    <div class="single-query form-group">
	                      <?php echo $input_fields['nombre']->input; ?>
	                    </div>
	                  </div>
	                  <div class="col-sm-4">
	                    <div class="single-query">
	                      <label>Apellido:</label>
	                    </div>
	                  </div>
	                  <div class="col-sm-8">
	                    <div class="single-query form-group">
	                      <?php echo $input_fields['apellido_paterno']->input; ?>
	                    </div>
	                  </div>
	                  <div class="col-sm-4">
	                    <div class="single-query">
	                      <label>Teléfono:</label>
	                    </div>
	                  </div>
	                  <div class="col-sm-8">
	                    <div class="single-query form-group">
	                     <?php echo $input_fields['telefono']->input; ?>
	                    </div>
	                  </div>
	                  <div class="col-sm-4">
	                    <div class="single-query">
	                      <label>Movil:</label>
	                    </div>
	                  </div>
	                  <div class="col-sm-8">
	                    <div class="single-query form-group">
	                      <?php echo $input_fields['movil']->input; ?>
	                    </div>
	                  </div>
	                  <div class="col-sm-4">
	                    <div class="single-query">
	                      <label>E-mail:</label>
	                    </div>
	                  </div>
	                  <div class="col-sm-8">
	                    <div class="single-query form-group">
	                      <input type="text" readonly="" placeholder="<?= get_instance()->user->email ?>" value="<?= get_instance()->user->email ?>" class="keyword-input">
	                    </div>
	                  </div>
	                  <div class="col-md-12 col-sm-12 col-xs-12 text-center">
	                  <button class="btn-blue border_radius"><?php echo $this->l('form_save'); ?></button>
	                </div>
	                
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="container">
	        <div class="row">
	          <div class="col-md-5 col-sm-6 col-xs-12 profile-form margin40">
	            <h3 class="bottom30 margin40">Mis redes</h3>
	            <div class="row">
	              
	                <div class="col-sm-4">
	                  <div class="single-query">
	                    <label>Facebook:</label>
	                  </div>
	                </div>
	                <div class="col-sm-8">
	                  <div class="single-query form-group">
	                    <?php echo $input_fields['facebook']->input; ?>
	                  </div>
	                </div>
	                <div class="col-sm-4">
	                  <div class="single-query">
	                    <label>Twitter:</label>
	                  </div>
	                </div>
	                <div class="col-sm-8">
	                  <div class="single-query form-group">
	                    <?php echo $input_fields['twitter']->input; ?>
	                  </div>
	                </div>
	                <div class="col-sm-4">
	                  <div class="single-query">
	                    <label>Google Plus:</label>
	                  </div>
	                </div>
	                <div class="col-sm-8">
	                  <div class="single-query form-group">
	                    <?php echo $input_fields['google']->input; ?>
	                  </div>
	                </div>
	                <div class="col-sm-4">
	                  <div class="single-query">
	                    <label>Linkedin:</label>
	                  </div>
	                </div>
	                <div class="col-sm-8">
	                  <div class="single-query form-group">
	                    <?php echo $input_fields['linkedin']->input; ?>
	                  </div>
	                </div>
	                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
	                  <button class="btn-blue border_radius"><?php echo $this->l('form_save'); ?></button>
	                </div>
	              
	            </div>
	          </div>
	          <div class="col-md-2 hidden-xs"></div>
	          <div class="col-md-5 col-sm-6 col-xs-12 profile-form margin40">
	            <h3 class=" bottom30 margin40">Cambiar contraseña</h3>
	            <div class="row">
	             
	                <div class="col-sm-4">
	                  <div class="single-query">
	                    <label>Contraseña actual:</label>
	                  </div>
	                </div>
	                <div class="col-sm-8">
	                  <div class="single-query form-group">
	                    <input type="password" class="keyword-input" name="password_before">
	                  </div>
	                </div>
	                <div class="col-sm-4">
	                  <div class="single-query">
	                    <label>Nueva contraseña:</label>
	                  </div>
	                </div>
	                <div class="col-sm-8">
	                  <div class="single-query form-group">
	                    <input type="password" class="keyword-input" name="password">
	                  </div>
	                </div>
	                <div class="col-sm-4">
	                  <div class="single-query">
	                    <label>Confirmar contraseña:</label>
	                  </div>
	                </div>
	                <div class="col-sm-8">
	                  <div class="single-query form-group">
	                    <input type="password" class="keyword-input" name="password2">
	                  </div>
	                </div>
	                <div class="col-sm-12 text-center">
	                  <button class="btn-blue border_radius"><?php echo $this->l('form_save'); ?></button>
	                </div>
	              
	            </div>
	          </div>
	        </div>
	      </div>
	<?php echo form_close(); ?>
</div>


<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
        $("input[type='text'],input[type='password']").addClass('form-control');
</script>
