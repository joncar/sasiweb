<?php		
	$this->set_js('js/cruds/jquery.form.js',TRUE);
	$this->set_js('js/cruds/flexigrid-add.js',TRUE);	
	$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
?>



<div class="flexigrid crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
	<?php echo form_open( $insert_url, 'method="post" class="" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>



        
        <h3 class="margin40 bottom15"><?= l('datos-de-contacto') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos de contacto de la propiedad"></i></h3>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="provincias_id_field_box">
                    <label for='field-nombre' id="nombre_display_as_box" style="width:100%">
                        <?= l('nombre') ?> <span class='required'>*</span>:
                    </label>
                        <?php echo $input_fields['nombre']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="provincias_id_field_box">
                    <label for='field-apellidos' id="apellidos_display_as_box" style="width:100%">
                        <?= l('apellidos') ?> <span class='required'>*</span>:
                    </label>
                        <?php echo $input_fields['apellidos']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="provincias_id_field_box">
                    <label for='field-email' id="email_display_as_box" style="width:100%">
                        E-mail <span class='required'>*</span>:
                    </label>
                        <?php echo $input_fields['email']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="provincias_id_field_box">
                    <label for='field-telefono' id="telefono_display_as_box" style="width:100%">
                        <?= l('telefono') ?> <span class='required'>*</span>:
                    </label>
                        <?php echo $input_fields['telefono']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="provincias_id_field_box">
                    <label for='field-telefono' id="telefono_display_as_box" style="width:100%">
                        <?= l('movil') ?> <span class='required'>*</span>:
                    </label>
                        <?php echo $input_fields['movil']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="provincias_id_field_box">
                    <label for='field-movil' id="movil_display_as_box" style="width:100%">
                        <?= l('otro-telefono') ?> <span class='required'>*</span>:
                    </label>
                        <?php echo $input_fields['otro_telefono']->input ?>
                </div>
            </div> 
            
        </div>


		<h3 class="margin40 bottom15"><?= l('datos-localizacion') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos de ubicación de la propiedad"></i></h3>
	    <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="provincias_id_field_box">
                    <label for='field-provincias_id' id="provincias_id_display_as_box" style="width:100%">
                        <?php echo $input_fields['provincias_id']->display_as; ?><?php echo ($input_fields['provincias_id']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['provincias_id']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="ciudades_id_field_box">
                    <label for='field-ciudades_id' id="ciudades_id_display_as_box" style="width:100%">
                        <?php echo $input_fields['ciudades_id']->display_as; ?><?php echo ($input_fields['ciudades_id']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['ciudades_id']->input ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="auxizona_field_box">
                    <label for='field-auxizona' id="auxizona_display_as_box" style="width:100%">
                        <?php echo $input_fields['auxizona']->display_as; ?><?php echo ($input_fields['auxizona']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['auxizona']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="cp_field_box">
                    <label for='field-cp' id="cp_display_as_box" style="width:100%">
                        <?php echo $input_fields['cp']->display_as; ?><?php echo ($input_fields['cp']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['cp']->input ?>
                </div>
            </div>  
        </div>
        <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class='form-group' id="calle_field_box">
                    <label for='field-calle' id="calle_display_as_box" style="width:100%">
                        <?php echo $input_fields['calle']->display_as; ?><?php echo ($input_fields['calle']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['calle']->input ?>
                </div>
            </div>             
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="zonaauxiliar_field_box">
                    <label for='field-zonaauxiliar' id="zonaauxiliar_display_as_box" style="width:100%">
                        <?php echo $input_fields['zonaauxiliar']->display_as; ?><?php echo ($input_fields['zonaauxiliar']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['zonaauxiliar']->input ?>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="numero_field_box">
                    <label for='field-numero' id="numero_display_as_box" style="width:100%">
                        <?php echo $input_fields['numero']->display_as; ?><?php echo ($input_fields['numero']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['numero']->input ?>
                </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="planta_field_box">
                    <label for='field-planta' id="planta_display_as_box" style="width:100%">
                        <?php echo $input_fields['planta']->display_as; ?><?php echo ($input_fields['planta']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['planta']->input ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="puerta_field_box">
                    <label for='field-puerta' id="puerta_display_as_box" style="width:100%">
                        <?php echo $input_fields['puerta']->display_as; ?><?php echo ($input_fields['puerta']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['puerta']->input ?>
                </div>
            </div>
        </div>

        <h3 class="margin40 bottom15"><?= l('datos-de-la-propiedad') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos generales de la propiedad"></i></h3>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="tipos_id_field_box">
                    <label for='field-tipos_id' id="tipos_id_display_as_box" style="width:100%">
                        <?= l('tipo-de-propiedad') ?><?php echo ($input_fields['tipos_id']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <select name="tipos_id" id="field-tipos_id" class="form-control chosen-select" data-placeholder="<?= l('seleccionar-opcion') ?>">
                        <option value=""><?= l('seleccionar-opcion') ?></option>
                        <?php 
                            $this->db = get_instance()->db;
                            $this->db->order_by('label','ASC'); 
                            $tipos = $this->db->get_where('tipos');
                            foreach($tipos->result() as $t): 
                        ?>
                            <option value="<?= $t->codigo ?>"><?= l('tipo_'.mb_strtolower($t->label)) ?></option>
                        <?php endforeach ?>
                    </select>                                            
                </div>
            </div> 
            
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="conservacion_id_field_box">
                    <label for='field-conservacion_id' id="conservacion_id_display_as_box" style="width:100%">
                        <?= l('estado-propiedad') ?><?php echo ($input_fields['conservacion_id']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <select name="conservacion_id" id="field-conservacion_id" class="form-control chosen-select" data-placeholder="<?= l('seleccionar-opcion') ?>">
                        <option value=""><?= l('seleccionar-opcion') ?></option>
                        <?php 
                            $this->db = get_instance()->db;
                            $this->db->order_by('nombre','ASC'); 
                            $tipos = $this->db->get_where('conservacion');
                            foreach($tipos->result() as $t): 
                        ?>
                            <option value="<?= $t->codigo ?>"><?= l('cons_'.$t->nombre) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class='form-group' id="m_uties_field_box">
                    <label for='field-m_uties' id="m_uties_display_as_box" style="width:100%">
                        <?= l('m-utiles') ?><?php echo ($input_fields['m_uties']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['m_uties']->input ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class='form-group' id="m_cons_field_box">
                    <label for='field-m_cons' id="m_cons_display_as_box" style="width:100%">
                        <?= l('m-const') ?><?php echo ($input_fields['m_cons']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['m_cons']->input ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class='form-group' id="m_cons_field_box">
                    <label for='field-m_parcela' id="m_parcela_display_as_box" style="width:100%">
                        <?= l('m-parcela') ?><?php echo ($input_fields['m_parcela']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['m_parcela']->input ?>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="habitaciones_field_box">
                    <label for='field-habitaciones' id="habitaciones_display_as_box" style="width:100%">
                        <?= l('habitaciones') ?><?php echo ($input_fields['habitaciones']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['habitaciones']->input ?>
                </div>
            </div> 
            
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="habdobles_field_box">
                    <label for='field-habdobles' id="habdobles_display_as_box" style="width:100%">
                        <?= l('hab-doble') ?><?php echo ($input_fields['habdobles']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['habdobles']->input ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="banyos_field_box">
                    <label for='field-banyos' id="banyos_display_as_box" style="width:100%">
                        <?= l('banos') ?><?php echo ($input_fields['banyos']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['banyos']->input ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="aseos_field_box">
                    <label for='field-aseos' id="aseos_display_as_box" style="width:100%">
                        <?= l('aseos') ?><?php echo ($input_fields['aseos']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['aseos']->input ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="carpinteria_field_box">
                    <label for='field-carpinteria' id="carpinteria_display_as_box" style="width:100%">
                        <?= l('carpinteria-interior') ?><?php echo ($input_fields['carpinteria']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['carpinteria']->input ?>
                </div>
            </div> 
            
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="carpinext_field_box">
                    <label for='field-carpinext' id="carpinext_display_as_box" style="width:100%">
                        <?= l('carpinteria-exterior') ?><?php echo ($input_fields['carpinext']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['carpinext']->input ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="suelo_field_box">
                    <label for='field-suelo' id="suelo_display_as_box" style="width:100%">
                        <?= l('suelo') ?><?php echo ($input_fields['suelo']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['suelo']->input ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class='form-group' id="vistas_field_box">
                    <label for='field-vistas' id="vistas_display_as_box" style="width:100%">
                        <?= l('vistas') ?><?php echo ($input_fields['vistas']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['vistas']->input ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class='form-group' id="descripofertas_field_box">
                    <label for='field-descripofertas' id="descripofertas_display_as_box" style="width:100%">
                        <?= l('Descripción Propiedad') ?><?php echo ($input_fields['descripofertas']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <textarea name="descripofertas" id="field-descripofertas" cols="30" rows="6" style="width: 100%;"></textarea>
                </div>
            </div>             
        </div>

        <h3 class="margin40 bottom15"><?= l('Datos económicos') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos de ubicación de la propiedad"></i></h3>

        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class='form-group' id="precioalq_field_box">
                    <label for='field-precioalq' id="precioalq_display_as_box" style="width:100%">
                        <?= l('Precio Alquiler') ?><?php echo ($input_fields['precioalq']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['precioalq']->input ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class='form-group' id="descripofertas_field_box">
                    <label for='field-descripofertas' id="precio_display_as_box" style="width:100%">
                        <?= l('Precio de venta') ?><?php echo ($input_fields['precio']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <input id="field-precio" name="precio" type="text" value="" class="numeric form-control" maxlength="11">
                </div>
            </div> 
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class='form-group' id="gastos_com_field_box">
                    <label for='field-gastos_com' id="gastos_com_display_as_box" style="width:100%">
                        <?= l('Gastos Com.') ?><?php echo ($input_fields['gastos_com']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input_fields['gastos_com']->input ?>
                </div>
            </div>            
        </div>

        <div class="row">
            

            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="checkbox" id="comunidadincluida_field_box">
                    <label for='field-comunidadincluida' id="comunidadincluida_display_as_box">
                      <input type="checkbox" id="field-comunidadincluida" name="comunidadincluida" value="1"> <?= l('Comunidad Incluida') ?> <?php echo ($input_fields['comunidadincluida']->required)? "<span class='required'>*</span> " : ""; ?>
                    </label>
                  </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="checkbox" id="opcioncompra_field_box">
                    <label for='field-opcioncompra' id="opcioncompra_display_as_box" style="width:100%">
                      <input type="checkbox" id="field-opcioncompra" name="opcioncompra" value="1"><?= l('Opción a Compra') ?><?php echo ($input_fields['opcioncompra']->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                  </div>
            </div>
        </div>

        <h3 class="margin40 bottom15"><?= l('Datos extras') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade los datos extras"></i></h3>

        <div class="row">
            <?php $extras = json_decode($input_fields['propiedades_extras']->input) ?>
            <?php foreach($extras as $n=>$v): ?>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="checkbox">
                        <label for="field-politicas">
                            <input type="checkbox" name="propiedades_extras[]" value="<?= $n ?>"> 
                            <?= l('extra_'.$n) ?>
                        </label>
                    </div>
                </div>
            <?php endforeach ?>
            
        </div>

        <h3 class="margin40 bottom15"><?= l('Fotos') ?>  <i class="fa fa-info-circle help" data-toggle="tooltip" title="" data-original-title="Añade las fotos de la propiedad"></i></h3>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto1']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto2']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto3']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto4']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto5']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto6']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto7']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto8']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto9']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto10']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto11']->input ?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <?php echo $input_fields['foto12']->input ?>
            </div>
        </div>


        <div id='report-error' style="display:none" class='alert alert-danger'></div>
        <div id='report-success' style="display:none" class='alert alert-success'></div>
        
        <?php
            foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
            }
        ?>

        <div class="form-group">
            <div class="checkbox">
                <label for="field-politicas">
                    <input type="checkbox" id="field-politicas" name="politicas"> <?= l('publicar-aviso') ?>
                </label>
            </div>
        </div>

		<div class="btn-group">	
            <button type="submit" class="btn-blue border_radius margin40"><?php echo $this->l('form_save'); ?></button>		                
		</div>
	<?php echo form_close(); ?>
</div>


<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
        $("input[type='text'],input[type='password']").addClass('form-control');
</script>
