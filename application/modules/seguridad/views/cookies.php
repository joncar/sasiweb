
<link href="<?= base_url() ?>js/stocookie/js/colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
    <div class="container">
    <h1>StoCookie setup manager</h1>
    <button class="btn btn-default reset">Reset options</button>

      <div class="row">
        <div class="col-sm-6">

          <h3>Banner</h3>
            <div class="row">
            	<div class="col-sm-12">
                <div class="form-group">
                  <label>Message</label>
                  <textarea name="message" class="form-control setval" data-option="message" rows="4">
Our website uses cookies necessary for essential purposes: functionality and analytics cookies, content sharing and social networking cookies.<br>
By using our website and agreeing to <a href="#">this policy</a>, you consent to our use of cookies.</textarea>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Background color</label>
                  <div id="pick-background" class="input-group colorpicker-component"> 
                    <input type="text" value="#ffffff" class="form-control setval" data-option="background" name="background"> 
                    <span class="input-group-addon"><span class="glyphicon glyphicon-tint" aria-hidden="true"></span>
                  </div> 
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label>Text color</label>
                  <div id="pick-color" class="input-group colorpicker-component"> 
                    <input type="text" value="#333333" class="form-control setval" data-option="color" name="color"> 
                    <span class="input-group-addon"><span class="glyphicon glyphicon-font" aria-hidden="true"></span>
                  </div>
                </div>
              </div>

            </div> <!-- row -->

            <div class="row">

              <div class="col-sm-6">
                <div class="form-group">
                  <label>Link color</label>
                  <div id="pick-link" class="input-group colorpicker-component"> 
                    <input type="text" value="#1b98ff" class="form-control setval" data-option="linkColor" name="linkColor"> 
                    <span class="input-group-addon"><span class="glyphicon glyphicon-link" aria-hidden="true"></span>
                  </div> 
                </div>
              </div>
              
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Border radius</label>
                  <div class="input-group">
                    <input class="form-control pixval" data-option="bannerCorner" name="bannerCorner" type='number'>
                    <span class="input-group-addon">px</span>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Max width</label>
                  <div class="input-group">
                    <input class="form-control pixval" data-option="maxw" name="maxw" type='number' min="100">
                    <span class="input-group-addon">px</span>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label>Margin</label>
                  <div class="input-group">
                    <input class="form-control pixval" data-option="margin" name="margin" type='number'>
                    <span class="input-group-addon">px</span>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label>Font family</label>
                  <input class="form-control setval" data-option="fontFamily" name="fontFamily" type='text'>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label>Font size</label>
                  <div class="input-group">
                    <input class="form-control pixval" data-option="fontSize" name="fontSize" type='number'>
                    <span class="input-group-addon">px</span>
                  </div>
                </div>
              </div>

            </div>
<!-- end BANNER section -->

            <h3>Effects</h3>
            <div class="row">

              <div class="col-sm-6">
                <div class="checkbox">
                  <label><input type="checkbox" value="y" name="scrollClose" class="radioval" data-option="scrollClose">Hide on window scroll</label>
                </div>
                <label>Animation</label>
                <div class="form-group">
                  <label class="checkbox-inline">
                    <input type="checkbox" value="y" name="fade" class="radioval" data-option="fade" checked>Fade
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="y" name="slide" class="radioval" data-option="slide" checked>Slide
                  </label>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label>position</label>
                  <select class="form-control setval setpos" data-option="position" name="position" id="pick-position">
                    <option>top</option>
                    <option>bottom</option>
                  </select>
                </div>
              </div>

            </div> <!-- row -->
<!-- end EFFECTS section -->

          </div> <!-- end left col -->
          
          <div class="col-sm-6">
          

            <h3>Button</h3>
            <div class="row">

              <div class="col-sm-6">
                <div class="form-group">
                  <label>Background color</label>
                  <div id="pick-button-background" class="input-group colorpicker-component"> 
                    <input type="text" value="#333333" class="form-control setval" data-option="buttonBg" name="buttonBg"> 
                    <span class="input-group-addon"><span class="glyphicon glyphicon-tint" aria-hidden="true"></span>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <label>Text color</label>x
                  <div id="pick-button-color" class="input-group colorpicker-component"> 
                    <input type="text" value="#ffffff" class="form-control setval" data-option="buttonColor" name="buttonColor"> 
                    <span class="input-group-addon"><span class="glyphicon glyphicon-font" aria-hidden="true"></span>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <label>"I Agree"</label>
                <div class="form-group">
                    <input class="form-control setval" data-option="btnText" name="btnText" type='text' value="OK">
                </div>
              </div>
              
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Border radius</label>
                  <div class="input-group">
                    <input class="form-control pixval" data-option="buttonCorner" name="buttonCorner" type='number'>
                    <span class="input-group-addon">px</span>
                  </div>
                </div>
              </div>

              <div class="col-sm-12">
                <label>Align button</label>
                <div class="form-group">
                  <label class="radio-inline">
                    <input type="radio" value="left" name="buttonAlign" class="radioval" data-option="buttonAlign">left
                  </label>
                  <label class="radio-inline">
                    <input type="radio" value="center" name="buttonAlign" class="radioval" data-option="buttonAlign">center
                  </label>
                  <label class="radio-inline">
                    <input type="radio" value="right" name="buttonAlign" class="radioval" data-option="buttonAlign">right
                  </label>
                </div>
              </div>
            </div> <!-- row -->
<!-- end BUTTON section -->

            <h3>Mode</h3>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Display banner</label>
                  <select class="form-control setval setmode" data-option="mode" name="mode">
                    <option value="cookie">Only one time (set a cookie)</option>
                    <option value="session" selected>Every new session</option>
                    <option value="always">Always</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 collapse">
                <div class="row">
                  <div class="col-sm-12">
                    <p><em>Optional values: the default cookie is called <strong>stoCookie</strong> and lasts <strong>365</strong> days</em></p>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Cookie name</label>
                      <input name="cookieName" class="form-control setval" data-option="cookieName" type="text">
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Cookie duration</label>
                      <div class="input-group">
                        <input class="form-control setval" data-option="cookieLife" name="cookieLife" type='number'/>
                        <span class="input-group-addon">Días</span>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

            </div> <!-- row -->
<!-- end MODE section -->

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <a class="btn btn-lg btn-info btn-block" id="getresults">Actualizar</a>
                </div>

					<pre class="result">
					jQuery(document).ready(function($){
					    $('body').stoCookie();
					});</pre>
					<div class="form-group">
                  <a class="btn btn-lg btn-info btn-block" id="saveresults">Guardar</a>
                </div>
              </div>
            </div> <!-- row -->
<!-- end update config -->

          </div> <!-- col-sm-6 -->
      </div> <!-- row -->
    </div>  <!-- container -->


    <script src="<?= base_url() ?>js/stocookie/js/colorpicker/js/bootstrap-colorpicker.js"></script>

    <script src="<?= base_url() ?>js/stocookie/stoCookie.js"></script>

    <script type="text/javascript" src="<?= base_url() ?>js/stocookie/js/configurator.min.js"></script>

    <script type="text/javascript">

      $(document).ready(function($){

        $('.colorpicker-component').each(function(){
          var input = $(this).find('.setval');
            var picker = $(this).colorpicker();
            picker.on('changeColor', function(e) {
                updateBanner(input.data('option'), input.val())
            });
        });

        $(document).on('click','#saveresults',function(){
            $(this).attr('disabled',true).html('Guardando por favor espere.');
            var code = '<script>'+$("pre").html()+'<\/script>';
        		$.post('<?= base_url('seguridad/ajustes/update/1') ?>',{
              'cookies':code
            },function(data){
                $("#saveresults").attr('disabled',false).html('Guardar');
            });

        });

        $('body').stoCookie({
          mode: 'always',
          btnText: 'OK',
          fade: true,
          slide: true,
          added: function(){
            checkVals();
          }
        });

      });
    </script>
