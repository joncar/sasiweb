##FUNCTION SanearPropiedades 
BEGIN
#Colocar provincias_id
UPDATE _propiedades_list,(
	SELECT 
	*
	FROM 
	provincias
) as cd
SET provincias_id = cd.id
WHERE (UPPER(cd.nombre) = UPPER(_propiedades_list.provincia) OR cd.codigo_inmo = _propiedades_list.keyprov);

#Colocar ciudades_id y comarcas_id
UPDATE _propiedades_list,(
SELECT
	ciudades.id,
    ciudades.comarcas_id,
    ciudades.ciudad
    FROM 
    _propiedades_list
    JOIN ciudades ON ciudades.ciudad = _propiedades_list.ciudad
) as cd
SET _propiedades_list.comarcas_id = cd.comarcas_id, _propiedades_list.ciudades_id = cd.id
WHERE _propiedades_list.ciudad = cd.ciudad;

#Crear zonas nuevas
INSERT INTO zonas SELECT NULL as id,ciudades_id,distritos_id,zona,NULL as nombre_habitaclia,NULL,NULL,NULL from _propiedades_list where zona NOT IN (SELECT nombre from zonas where ciudades_id = _propiedades_list.ciudades_id) AND zona IS NOT NULL AND zona != '' GROUP BY zona,ciudades_id;

#Colocar zonas_id
UPDATE _propiedades_list, zonas SET _propiedades_list.zonas_id = zonas.id, _propiedades_list.distritos_id = zonas.distritos_id WHERE _propiedades_list.zona = zonas.nombre AND _propiedades_list.ciudades_id = zonas.ciudades_id;

#Colocar zonas_id con variantes
UPDATE _propiedades_list,(SELECT zonas.*, barrios.zona_habitaclia from barrios INNER JOIN zonas ON zonas.id = barrios.zona_habitaclia where zona_habitaclia != 0) as cd SET _propiedades_list.zonas_id = cd.zona_habitaclia, _propiedades_list.distritos_id = cd.distritos_id WHERE _propiedades_list.zona = cd.nombre AND _propiedades_list.ciudades_id = cd.ciudades_id AND _propiedades_list.zonas_id IS NULL;

#Colocar areas_id
UPDATE _propiedades_list,(SELECT id,areas_id from ciudades) as cd SET _propiedades_list.areas_id = cd.areas_id WHERE _propiedades_list.ciudades_id = cd.id;

#Colocar Coordenadas geográficas al formato estándar
UPDATE _propiedades_list,(
SELECT _propiedades_list.id, REPLACE(_propiedades_list.altitud,',','.') as altitud, REPLACE(_propiedades_list.latitud,',','.') as latitud from _propiedades_list
) as cd SET _propiedades_list.altitud = cd.altitud, _propiedades_list.latitud = cd.latitud WHERE _propiedades_list.id = cd.id;

#Colocar las propiedades Lowcost
UPDATE (SELECT 1 as lc,api_propiedades_id as id FROM _api_descripciones WHERE SUBSTRING(titulo,1,3) = 'LC-') AS cd, _propiedades_list SET _propiedades_list.lowcost = cd.lc WHERE _propiedades_list.id = cd.id;
##Colocar extras lowcosts
INSERT INTO _propiedades_list_extras SELECT NULL, _propiedades_list.id, 62 as codigo, 1 FROM _propiedades_list WHERE lowcost = 1 AND NOT EXISTS (SELECT * FROM _propiedades_list_extras WHERE propiedades_list_id = _propiedades_list.id AND propiedades_extras_id = 62);
##Limpiar campo lowcost
UPDATE _api_descripciones, (SELECT id,api_propiedades_id,titulo, SUBSTRING(titulo,4) AS org FROM _api_descripciones WHERE SUBSTRING(titulo,1,3) = 'LC-') AS cd SET _api_descripciones.titulo = cd.org, _api_descripciones.LC = 1 WHERE _api_descripciones.id = cd.id;
##Colocar extras precios rebajados
INSERT INTO _propiedades_list_extras SELECT NULL, id, 61, 1 FROM _propiedades_list WHERE outlet > 0 AND NOT EXISTS (SELECT * FROM _propiedades_list_extras WHERE propiedades_list_id = _propiedades_list.id AND propiedades_extras_id = 61);
##Colocar extras Obra nueva
INSERT INTO _propiedades_list_extras SELECT NULL, id, 63, 1 FROM _propiedades_list WHERE nbconservacion = 'Obra Nueva' AND NOT EXISTS (SELECT * FROM _propiedades_list_extras WHERE propiedades_list_id = _propiedades_list.id AND propiedades_extras_id = 63);
##Colocar tipo de propiedad
UPDATE _propiedades_list, tipos SET _propiedades_list.codigo_tipo = tipos.codigo_tipo WHERE _propiedades_list.key_tipo = tipos.codigo;

##Colocar foto
UPDATE _propiedades_list set foto = '[base_url]img/default.jpg' where foto IS NULL;
##Colocar el habajado
UPDATE _propiedades_list,(SELECT p.id, IF(p.habajado>0,p.habajado,0) AS habajado FROM (
SELECT 
_propiedades_list.id,
(outlet - precioinmo) as habajado
FROM _propiedades_list WHERE outlet > 0 AND precioinmo > 0 AND keyacci = 1) as p) as cd SET _propiedades_list.habajado = cd.habajado WHERE _propiedades_list.id = cd.id;
##Colocar precio_m_uties
UPDATE _propiedades_list,(SELECT id,
(CASE WHEN m_uties > 0 
THEN TRUNCATE(IF(keyacci=1,precioinmo/m_uties,precioalq/m_uties),0)
ELSE 0
END) AS precio_m_uties
FROM _propiedades_list) AS cd SET _propiedades_list.precio_m_uties = cd.precio_m_uties WHERE _propiedades_list.id = cd.id;
#Colocar id regionales resagados
UPDATE _propiedades_list,(SELECT
_propiedades_list.id,                         
comarcas.id as comarcas_id,
areas.id as areas_id,
ciudades.id as ciudades_id,
distritos.id as distritos_id,
provincias.id as provincias_id
FROM _propiedades_list INNER JOIN distritos ON distritos.id = _propiedades_list.distritos_id INNER JOIN ciudades ON ciudades.id = distritos.ciudades_id INNER JOIN comarcas ON comarcas.id = ciudades.comarcas_id INNER JOIN provincias ON provincias.id = comarcas.provincias_id LEFT JOIN areas ON areas.id = ciudades.areas_id where _propiedades_list.comarcas_id IS NULL AND _propiedades_list.distritos_id IS NOT NULL) as cd SET _propiedades_list.comarcas_id = cd.comarcas_id,_propiedades_list.areas_id = cd.areas_id,_propiedades_list.ciudades_id = cd.ciudades_id, _propiedades_list.provincias_id = cd.provincias_id WHERE _propiedades_list.id = cd.id;
##Update precio 
UPDATE _propiedades_list set precio = precioalq WHERE keyacci = 2; ##Alq
UPDATE _propiedades_list set precio = precioinmo WHERE keyacci = 1 OR keyacci = 4; ## Venta

#Ejecutar update de avisos
UPDATE avisos,(SELECT avisos.*,_propiedades_list.precio FROM `avisos` INNER JOIN _propiedades_list ON _propiedades_list.cod_ofer = avisos.cod_ofer WHERE _propiedades_list.precio < avisos.ultimo_precio AND avisos.avisar = 0) as cd SET avisos.avisar = 1 WHERE cd.id = avisos.id;

#Colocar información de contacto
UPDATE _propiedades_list, (
SELECT 
_propiedades_list.id,
CASE WHEN provincias.sucursal = '5010' THEN (SELECT email_contacto_alicante from ajustes) ELSE (SELECT email_contacto_igualada FROM ajustes) END as email_contacto,
CASE WHEN provincias.sucursal = '5010' THEN (SELECT telefono_contacto_alicante from ajustes) ELSE (SELECT telefono_contacto_igualada FROM ajustes) END as telefono_contacto,
CASE WHEN provincias.sucursal = '5010' THEN (SELECT direccion_contacto_alicante from ajustes) ELSE (SELECT direccion_contacto_igualada FROM ajustes) END as direccion_contacto
FROM _propiedades_list 
INNER JOIN provincias ON provincias.id = _propiedades_list.provincias_id) AS cd SET _propiedades_list.email_contacto = cd.email_contacto, _propiedades_list.direccion_contacto = cd.direccion_contacto, _propiedades_list.telefono_contacto = cd.telefono_contacto WHERE _propiedades_list.id = cd.id;

##Rellenar idiomas faltantes
## CAT
INSERT INTO _api_descripciones SELECT NULL,apd.api_propiedades_id,apd.titulo,apd.descrip,apd.cod_ofer,12,'ca',apd.lc
FROM _api_descripciones as apd 
LEFT JOIN _api_descripciones as apdh ON apdh.cod_ofer = apd.cod_ofer AND apdh.idioma = 'ca'
WHERE apd.idioma = 'es' AND apdh.titulo IS NULL;
## ENG
INSERT INTO _api_descripciones SELECT NULL,apd.api_propiedades_id,apd.titulo,apd.descrip,apd.cod_ofer,2,'en',apd.lc
FROM _api_descripciones as apd 
LEFT JOIN _api_descripciones as apdh ON apdh.cod_ofer = apd.cod_ofer AND apdh.idioma = 'en'
WHERE apd.idioma = 'es' AND apdh.titulo IS NULL;
## FRA
INSERT INTO _api_descripciones SELECT NULL,apd.api_propiedades_id,apd.titulo,apd.descrip,apd.cod_ofer,4,'fr',apd.lc
FROM _api_descripciones as apd 
LEFT JOIN _api_descripciones as apdh ON apdh.cod_ofer = apd.cod_ofer AND apdh.idioma = 'fr'
WHERE apd.idioma = 'en' AND apdh.titulo IS NULL;
## IT
INSERT INTO _api_descripciones SELECT NULL,apd.api_propiedades_id,apd.titulo,apd.descrip,apd.cod_ofer,13,'it',apd.lc
FROM _api_descripciones as apd 
LEFT JOIN _api_descripciones as apdh ON apdh.cod_ofer = apd.cod_ofer AND apdh.idioma = 'it'
WHERE apd.idioma = 'en' AND apdh.titulo IS NULL;
## AL
INSERT INTO _api_descripciones SELECT NULL,apd.api_propiedades_id,apd.titulo,apd.descrip,apd.cod_ofer,3,'al',apd.lc
FROM _api_descripciones as apd 
LEFT JOIN _api_descripciones as apdh ON apdh.cod_ofer = apd.cod_ofer AND apdh.idioma = 'al'
WHERE apd.idioma = 'en' AND apdh.titulo IS NULL;
## RU
INSERT INTO _api_descripciones SELECT NULL,apd.api_propiedades_id,apd.titulo,apd.descrip,apd.cod_ofer,7,'ru',apd.lc
FROM _api_descripciones as apd 
LEFT JOIN _api_descripciones as apdh ON apdh.cod_ofer = apd.cod_ofer AND apdh.idioma = 'ru'
WHERE apd.idioma = 'en' AND apdh.titulo IS NULL;
## CH
INSERT INTO _api_descripciones SELECT NULL,apd.api_propiedades_id,apd.titulo,apd.descrip,apd.cod_ofer,11,'ch',apd.lc
FROM _api_descripciones as apd 
LEFT JOIN _api_descripciones as apdh ON apdh.cod_ofer = apd.cod_ofer AND apdh.idioma = 'ch'
WHERE apd.idioma = 'en' AND apdh.titulo IS NULL;
RETURN 1;
END


##  my_province
## _provincia
## RETURN BOOLEAN

BEGIN
DECLARE my TINYINT;

SELECT sino INTO my FROM(SELECT 
CASE _provincia
WHEN 1 THEN 1 ##Andorra
WHEN 2 THEN 1 ##Barcelona
WHEN 3 THEN 1 ##Girona
WHEN 4 THEN 1 ##Lerida
WHEN 5 THEN 1 ##Tarragona
WHEN 7 THEN 1 ##Valencia
WHEN 8 THEN 1 ##Alicante
WHEN 9 THEN 1 ##Castellon
WHEN 10 THEN 1 ##Murcia
ELSE 0 
END as sino) as cd;

RETURN my;
END



##  moverPropiedades
## RETURN BOOLEAN
BEGIN
TRUNCATE propiedades_list;	
TRUNCATE api_propiedad_fotos;
TRUNCATE api_descripciones;
TRUNCATE propiedades_videos;
TRUNCATE propiedades_list_extras;
INSERT INTO propiedades_list SELECT * FROM _propiedades_list;	
INSERT INTO api_propiedad_fotos SELECT * FROM _api_propiedad_fotos;
INSERT INTO api_descripciones SELECT * FROM _api_descripciones;
INSERT INTO propiedades_videos SELECT * FROM _propiedades_videos;
INSERT INTO propiedades_list_extras SELECT * FROM _propiedades_list_extras;
RETURN 1;
END