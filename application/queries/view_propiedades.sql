DROP VIEW IF EXISTS view_propiedades;
CREATE VIEW view_propiedades AS 
SELECT 
api_propiedades.*
FROM api_propiedades
LEFT JOIN api_propiedad ON api_propiedad.api_propiedades_id = api_propiedades.id
LEFT JOIN api_descripciones on api_descripciones.api_propiedades_id = api_propiedades.id;




DROP VIEW IF EXISTS view_propiedades_ciudad;
CREATE VIEW view_propiedades_ciudad AS
SELECT 
ciudades.*,
propiedades_list.ciudad as cd
FROM ciudades
INNER JOIN propiedades_list ON UPPER(propiedades_list.ciudad) = UPPER(ciudades.ciudad);






DROP VIEW IF EXISTS view_propiedades_zona;
CREATE VIEW view_propiedades_zona AS
SELECT 
zonas.*,
propiedades_list.zona
FROM zonas
INNER JOIN propiedades_list ON UPPER(propiedades_list.zona) = UPPER(zonas.nombre)




DROP VIEW IF EXISTS view_favoritos;
CREATE VIEW view_favoritos AS
SELECT
favoritos.id,
favoritos.user_id,
favoritos.cod_ofer,
favoritos.favoritos_grupos_id,
api_descripciones.titulo,
api_descripciones.descrip,
api_descripciones.idioma,
propiedades_list.foto,
propiedades_list.precio,
propiedades_list.provincia,
propiedades_list.ciudad,
propiedades_list.zona,
propiedades_list.cp,
propiedades_list.acciones,
propiedades_list.precioinmo,
propiedades_list.nbtipo,
propiedades_list.ref,
propiedades_list.m_uties,
propiedades_list.total_hab,
propiedades_list.banyos,
propiedades_list.codaccion,
propiedades_list.nbconservacion,
propiedades_list.habajado,
propiedades_list.outlet,
propiedades_list.precio_m_uties,
propiedades_list.lowcost,
propiedades_list.altitud,
propiedades_list.latitud,
propiedades_list.tipomensual,
'Add' as fotos #Añadir fotos como un json
FROM favoritos
INNER JOIN propiedades_list ON propiedades_list.cod_ofer = favoritos.cod_ofer
INNER JOIN api_descripciones ON api_descripciones.cod_ofer = propiedades_list.cod_ofer;
#GROUP BY favoritos.id




DROP VIEW IF EXISTS view_avisos;
CREATE VIEW view_avisos AS
SELECT
avisos.id,
avisos.email,
avisos.cod_ofer,
api_descripciones.titulo,
api_descripciones.descrip,
api_descripciones.idioma,
propiedades_list.foto,
propiedades_list.precio,
propiedades_list.provincia,
propiedades_list.ciudad,
propiedades_list.zona,
propiedades_list.cp,
propiedades_list.acciones,
propiedades_list.precioinmo,
propiedades_list.nbtipo,
propiedades_list.ref,
propiedades_list.m_uties,
propiedades_list.total_hab,
propiedades_list.banyos,
propiedades_list.codaccion,
propiedades_list.nbconservacion,
propiedades_list.habajado,
propiedades_list.outlet,
propiedades_list.precio_m_uties,
propiedades_list.lowcost,
propiedades_list.altitud,
propiedades_list.latitud,
propiedades_list.tipomensual,
'Add' as fotos #Añadir fotos como un json
FROM avisos
INNER JOIN propiedades_list ON propiedades_list.cod_ofer = avisos.cod_ofer
INNER JOIN api_descripciones ON api_descripciones.cod_ofer = propiedades_list.cod_ofer;
#GROUP BY favoritos.id