DROP VIEW IF EXISTS view_provincias_propiedades;
CREATE VIEW view_provincias_propiedades AS
SELECT 
provincias.*,
propiedades_list.provincia
FROM provincias
INNER JOIN propiedades_list ON UPPER(propiedades_list.provincia) = UPPER(provincias.nombre);



DROP VIEW IF EXISTS propiedades_list_ciudad;
CREATE VIEW propiedades_list_ciudad AS
SELECT 
ciudades.*,
propiedades_list.ciudad as cd
FROM ciudades
INNER JOIN propiedades_list ON UPPER(propiedades_list.ciudad) = UPPER(ciudades.ciudad);


DROP VIEW IF EXISTS propiedades_list_zona;
CREATE VIEW propiedades_list_zona AS
SELECT 
ciudades.*,
propiedades_list.ciudad as cd
FROM ciudades
INNER JOIN propiedades_list ON UPPER(propiedades_list.ciudad) = UPPER(ciudades.ciudad);