DROP VIEW IF EXISTS view_regiones;
CREATE VIEW view_regiones AS 
SELECT 
provincias.id,
provincias.nombre,
provincias.mapa,
provincias.codigo,
provincias.codigo_mapa_general,
comarcas.id as comarca_id,
comarcas.nombre as comarca,
comarcas.mapa as comarca_mapa,
comarcas.codigo as comarca_codigo,
ciudades.id as ciudad_id,
ciudades.ciudad as ciudad,
ciudades.mapa as ciudad_mapa,
ciudades.codigo as ciudad_codigo,
zonas.id as zona_id,
zonas.nombre as zona,
zonas.mapa as zona_mapa,
zonas.codigo as zona_codigo
FROM provincias
LEFT JOIN comarcas ON comarcas.provincias_id = provincias.id
LEFT JOIN ciudades ON ciudades.comarcas_id = comarcas.id
LEFT JOIN zonas ON zonas.ciudades_id = ciudades.id