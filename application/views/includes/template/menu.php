<ul id="menu" class="clearfix">
                  <li class="<?= $this->router->fetch_class()=='main'?'current':'' ?>">
                    <a href="<?= site_url() ?>">Inici</a>
                    <!--sub menu-->
                    <!--<div class="sub-menu-wrap">
                      <ul>
                        <li><a href="index.html">Homepage Layout 1</a></li>
                        <li class="current"><a href="home_2.html">Homepage Layout 2</a></li>
                        <li><a href="home_3.html">Homepage Layout 3</a></li>
                      </ul>
                    </div>-->
                  </li>
                  <li class="<?= @$link=='nosaltres'?'current':'' ?>">
                    <a href="<?= base_url('p/nosaltres') ?>">Nosaltres</a>
                    <!--sub menu-->
                    <!--<div class="sub-menu-wrap mega-menu flex-row">
                      <ul>
                        <li><a href="practice_area_v1.html">Practice Areas Overview v1</a></li>
                        <li><a href="practice_area_v2.html">Practice Areas Overview v2</a></li>
                        <li><a href="practice_area_v3.html">Practice Areas Overview v3</a></li>
                        <li><a href="practice_single.html">Single Service Page</a></li>
                      </ul>
                      <ul>
                        <li><a href="practice_area_v1.html">Personal Injury</a></li>
                        <li><a href="practice_area_v2.html">Estate Planning</a></li>
                        <li><a href="practice_area_v3.html">Maritime & Offshore Injury</a></li>
                        <li><a href="practice_single.html">Insurance Claims</a></li>
                      </ul>
                      <ul>
                        <li><a href="practice_area_v1.html">Business & Real Estate</a></li>
                        <li><a href="practice_area_v2.html">Medical/Pharmaceutical Injury</a></li>
                        <li><a href="practice_area_v3.html">Elder Law</a></li>
                        <li><a href="practice_single.html">Criminal Defense</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="dropdown"><a href="#">Attorneys</a>
                    <!--sub menu-->
                    <!--<div class="sub-menu-wrap">
                      <ul>
                        <li><a href="attorneys_list_v1.html">Attorneys List v1</a></li>
                        <li><a href="attorneys_list_v2.html">Attorneys List v2</a></li>
                        <li><a href="attorney_profile_v1.html">Attorney Profile Page v1</a></li>
                        <li><a href="attorney_profile_v2.html">Attorney Profile Page v2</a></li>
                      </ul>
                    </div>-->
                  </li>
                  <li class="<?= @$link=='serveis'?'current':'' ?>">
                    <a href="<?= base_url('serveis') ?>">Serveis</a>
                    <!--sub menu-->
                    <!--<div class="sub-menu-wrap">
                      <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="testimonials.html">Testimonials</a></li>
                        <li><a href="case_v1.html">Case Result v1</a></li>
                        <li><a href="case_v2.html">Case Result v2</a></li>
                        <li><a href="shortcodes.html">Shortcodes</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="404_page.html">404 Page</a></li>
                        <li><a href="coming_soon.html">Coming Soon Page</a></li>
                      </ul>
                    </div>-->
                  </li>
                  <li class="<?= @$link=='blog'?'current':'' ?>">
                    <a href="<?= base_url('blog') ?>">Blog</a>
                    <!--sub menu-->
                    <!--<div class="sub-menu-wrap">
                      <ul>
                        <li><a href="blog_classic.html">Classic</a></li>
                        <li><a href="blog_masonry.html">Masonry</a></li>
                        <li><a href="blog_single.html">Single Blog Post</a></li>
                      </ul>
                    </div>-->
                  </li>
                  <li class="<?= @$link=='contacte'?'current':'' ?>"><a href="<?= base_url('p/contacte') ?>">Contacte</a>
                    <!--sub menu-->
                    <!--<div class="sub-menu-wrap">
                      <ul>
                        <li><a href="contact_v1.html">Contact Us v1</a></li>
                        <li><a href="contact_v2.html">Contact Us v2</a></li>
                      </ul>
                    </div>-->
                  </li>
                  <li class="phoneonlysm">
                    <a href="#" style="font-size:18px;">
                      <b><i class="licon-telephone"></i> 987.654.3210</b>
                    </a>
                  </li>
                </ul>