
<div class="modal fade" tabindex="-1" role="dialog" id="modalContacto">
  
</div><!-- /.modal -->

<?php if(!$this->user->log): ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="modalLogin">
    <form class="callus" onsubmit="return loginShort(this)">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
              <?= l('Indícanos tu email para almacenar tus favoritos y alertas') ?>              
            </h4>
          </div>
          <div class="modal-body">    
              <div class="row" style="margin-bottom: 30px;">
                <div class="col-xs-12">
                  <p><b><?= l('¿Porqué crear un usuario?') ?></b></p>
                  <ul>
                    <?= l('pasos-registor') ?>
                  </ul>
                </div>
              </div>    
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div id="response"></div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control" placeholder="E-mail">
                  <input type="hidden" name="redirect" value="" id="redirect">
                </div>
              </div>          
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success"><?= l('guardar') ?></button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><?= l('cerrar') ?></button>              
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </form>
  </div><!-- /.modal -->
<?php endif ?>
<script>
  var $owl;
</script>
<script src="<?= base_url() ?>js/plugins.js"></script>
<script src="<?= base_url() ?>theme/theme/js/functions_custom.js"></script>
<script src="<?= base_url() ?>js/frame.js"></script>
<script>

  var onLogin = undefined;
  var log = <?= $this->user->log?1:0 ?>;
  function addFav(cod){
    if(log){
      $.post(URL+'propiedades/account/favoritosModal/'+cod,{},function(data){
          if($(document).find("#favoritoModal").length>0){
            $("#favoritoModal").replaceWith(data);          
          }else{
            $("body").append(data);          
          }
          $('#groupFavSelect').zelect({})
          $("#favoritoModal").modal('toggle');
      });
    }else{
      onLogin = function(){
        $("#modalLogin").modal('toggle');
        log = 1;
        onLogin = undefined;
        addFav(cod);        
      }
      $("#modalLogin").modal('toggle');
    }
  }

  function setFav(data,cod){
      data = new FormData(data);
      remoteConnection('propiedades/account/favoritos/insert',data,function(){
        $(".fav"+cod).addClass('flaticon flaticon-heart active');  
        $("#resultFavoritoModal").addClass('alert alert-success').html('Favorito añadido con éxito');
        if(typeof(onClickFav)){
          onClickFav();
        }
      });
      return false;
  }

  function showAvisoSiBaja(cod){
    if(log){
      $.post(URL+'propiedades/account/avisamesibaja/'+cod,{},function(data){
          if($(document).find("#avisameSiBajaModal").length>0){
            $("#avisameSiBajaModal").replaceWith(data);          
          }else{
            $("body").append(data);          
          }        
          $("#avisameSiBajaModal").modal('toggle');
      });
    }else{
      onLogin = function(){
        $("#modalLogin").modal('toggle');
        log = 1;
        onLogin = undefined;
        showAvisoSiBaja(cod);        
      }
      $("#modalLogin").modal('toggle');
    }
  }

  function showAlertasModal(){
    $("#modalLogin #redirect").val('mis-alertas');
    $("#modalLogin").modal('toggle');
  }

  function showFavModal(){
    $("#modalLogin #redirect").val('mis-favoritos');
    $("#modalLogin").modal('toggle');
  }

  function removeProp(cod){
    var index = $('#propiedad2'+cod).parent().attr('data-index');
    console.log(index);
    $owl.trigger('refresh.owl.carousel');
    $owl.trigger('remove.owl.carousel',index).trigger('refresh.owl.carousel');
    $('#propiedad'+cod).parent().remove();
    $('#propiedad2'+cod).remove();
    
  }

  function removePropMap(cod){
    removeMark(cod);
  }

  function contactar(cod){
    $.post(URL+'propiedades/frontend/contactar/'+cod,{},function(data){
      $("#modalContacto").html(data);
      $("#modalContacto").modal('toggle');
    });
  }

  function loginShort(f){
    var dat = new FormData(f);
    remoteConnection('main/loginShort',dat,function(data){
      if(data=='success'){
        if(onLogin==undefined){
          document.location.reload();
        }else{
          onLogin(data);
        }
      }else{
        $("#modalLogin #response").html(data);
      }
    });
    return false;
  }


  $(document).on('ready',function(){
    var loc = document.location.href;
    if(loc.search('#')>-1){
      loc = loc.split('#');
      loc = loc[1];
      if($("#"+loc).length>0){
        $('html,body').animate({scrollTop:$("#"+loc).offset().top},600);
      }else if(loc==='showStreetView'){
        //Si estamos en el detalle y se necesita mostrar el streetview
        $('html,body').animate({scrollTop:$("#mapa2").offset().top},600);
        showStreetViewOnDetail();
      }
    }

    $(document).on('click','.buscarMapa',function(){
        $("input[name='buscar_por']").val($(this).val());
        $(".callus").submit();
    });

    $(document).on('click touchstart','.acciones > label,.codigo_tipo > label,.provincias_id > label',function(){
      $(this).parent().find('label').removeClass('active');
      var name = $(this).parent().attr('class');
      var val = $(this).data('val');
      var label = $(this).data('label');
      var val2 = $(this).data('val2');
      var val3 = $(this).data('val3');      
      $("#mainSearch"+name).val(val);      
      if(typeof(label)!=='undefined'){        
        $("#mainSearch"+name).attr('name',label);
      }
      if(typeof(val2)!=='undefined'){
        $("input[name='comarcas_id']").val(val2);
      }else{
        $("input[name='comarcas_id']").val('');
      }

      if(typeof(val3)!=='undefined'){
        $("input[name='ciudades_id']").val(val3);
      }else{
        $("input[name='ciudades_id']").val('');
      }

      $(this).addClass('active');
    });
    $(".hora").mask("00:00");

     $(document).on('click','a .owl-next,a .owl-prev',function(e){
      e.stopPropagation();
      e.preventDefault();
    });



    

    $('.mostrarSlider').on('click', function() {
     
        var $lg = $(this).lightGallery({
            mode: 'lg-fade',
            cssEasing: 'easeOutCubic',
            speed: 400,
            dynamic: true,            
            hash: false,
            download: false,
            dynamicEl: fotos, 
            thumbnail:true,           
        });

        $lg.on('onAfterSlide.lg',function(event, index, fromTouch, fromThumb){
            if($(".lg-descr").length==0){
              $(".lg-toolbar").append('<span class="lg-descr">'+$("#tit").html()+'</span>');
              $(".lg-toolbar").append('<span class="lg-icon rojo">'+$(".rojo").html()+'</span>');
            }
        });
     
    });






  });
</script>

<?php $this->load->view('predesign/datepicker'); ?>
<script>
  $(".fecha").datepicker();
</script>

<script>
  $(".ahorro,.plazo,.interes").on('change',function(){calcularPrecioEstimado()});
  function calcularPrecioEstimado(){
    var ahorro = parseInt($(".ahorro").val());
    var plazos = parseInt($(".plazo").val());
    plazos*=12;
    var interes = parseInt($(".interes").val())/12;
    var precio = parseInt($(".precioTotal").val())-ahorro;
    

    var $I = interes/100 ;
    var $I2 = $I + 1 ;
    $I2 = Math.pow($I2,-plazos) ;

    var total = ($I * precio) / (1 - $I2) ; 
    total = total.toFixed(2);

    $("#totalCostoEstimado").html(total+'€');
  }
</script>
<script>
  function fillImages(){

     $("img[data-url]").each(function(){
      $(this).attr('src',$(this).data('url'));
    });
    $("div[data-url]").each(function(){
      $(this).css('background-image','url('+$(this).data('url')+')');
    });
    
  }
  window.onload = function(){
    fillImages();
  }

  window.onresize = function(){
    if($(".filtrarDiv").length>0){
      sticktothebottom();
    }
  }

  function sticktothebottom() {
    console.log(window.innerWidth);
    if(window.innerWidth>1100){
      var e = $("aside.bg_light")[0];
      x = $(e).offset().left;
      w = $($(e).find('.form-group')[0]).innerWidth()-30;
      $(".filtrarDiv").css({'width':$(e).width(),'left':$(e).offset().left+15});
    }else{      
      var e = $("aside.bg_light")[0];
      x = $(e).offset().left;
      w = $($(e).find('.form-group')[0]).innerWidth()-30;
      $(".filtrarDiv").css({'width':$(e).width(),'left':'-0.767px'});
    }
  }

  if($(".filtrarDiv").length>0){
    
    $(function() {
        $(window).scroll(sticktothebottom);
        sticktothebottom();
    });

  }

  $(".loadingAfterClick").on('click',function(){
    $(".loader").show();
  });
</script>