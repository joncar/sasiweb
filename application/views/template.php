<!doctype html>
<html lang="<?= $_SESSION['lang'] ?>">

	<!-- Google Web Fonts
	================================================== -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'SASI' : $title ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/editor.css">
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<script>window.wURL = window.URL; var URL = '<?= base_url() ?>'; var lang = '<?= $_SESSION['lang'] ?>';</script>
	<?php if(!empty($css_files)):?>
	      <?php foreach($css_files as $file): ?>
	      <link href="<?= $file ?>" rel="stylesheet">
	      <?php endforeach; ?>                
	<?php endif; ?>

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/reality-icon.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/flaticon.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/bootsnav.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/cubeportfolio.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/owl.transitions.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/settings.css">
	
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/range-Slider.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/search.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/style_custom.css?v1.2">
	<!-- Print css --->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/style_print.css" media="print">


	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/preload.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>js/lightgallery/css/lightgallery.css">
 	
 	<style>
		.icon-like.active::before{
			color:#e31f22 !important;font-family: "Flaticon" !important;font-size: 20px !important;font-style: normal !important; content:"\f100" !important;
		}
 		<?php 
 			$user = $this->user->log?$this->user->id:$_SESSION['user_id_temp'];
	 		$favoritos = $this->db->get_where('favoritos',array('user_id'=>$user));
    		foreach($favoritos->result() as $f=>$v){
    			echo '.icon-like.fav'.$v->cod_ofer.'::before{color:#e31f22 !important;font-family: "Flaticon" !important;font-size: 20px !important;font-style: normal !important; content:"\f100" !important;}';
    		}
 		?>
 	</style>

 	<script src="<?= base_url() ?>theme/theme/js/jquery-2.1.4.js"></script> 
 	<script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
 	<script src="https://www.google.com/recaptcha/api.js?render=6LeXgLUUAAAAAJr-5UFLv8xXlLkLQ2_jbqm-hjuk"></script>
</head>

<body>

	<!--Loader-->
	<div class="loader">
	  <div class="span">
	    <div class="location_indicator"></div>
	  </div>
	  <div id="loadingProp" class="span">
		  <span>Cargando propiedades</span>
	  </div>
	  <div id="loadingDraw" class="span">
		  <span>Marcando propiedades</span>
	  </div>
	</div>
	 <!--Loader--> 

 
	<?php 
		if(empty($editor)){
			$this->load->view($view); 
		}else{
			echo $view;
		}
		$this->load->view('_avisoModal',array(),FALSE,'paginas');
	?>	

<!-- Default Statcounter code for Finques-sasi.com - Web
Principal https://www.finques-sasi.com -->
<script type="text/javascript">
var sc_project=12003454;
var sc_invisible=1;
var sc_security="cb6a802f";
</script>
<script type="text/javascript"
src="https://www.statcounter.com/counter/counter.js"
async></script>
<noscript><div class="statcounter"><a title="Web Analytics"
href="https://statcounter.com/" target="_blank"><img
class="statcounter"
src="https://c.statcounter.com/12003454/0/cb6a802f/1/"
alt="Web Analytics"></a></div></noscript>
<!-- End of Statcounter Code -->

<a href="<?= base_url() ?>cuanto-vale-tu-piso.html" class="cuantoValeTuPisoBtn uppercase">
  <img src="<?= base_url() ?>img/icono-casa.jpg">
  <span><?= l('quieressaberelvalordetupiso') ?></span>
</a>
</body>
</html>