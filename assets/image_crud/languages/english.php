<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['upload_button'] = 'Carga los archivos aquí';
$lang['upload-drop-area'] = 'Borra los archivos';
$lang['upload-cancel'] = 'Cancelar';
$lang['upload-failed'] = 'Fallo';
$lang['upload-retry'] = 'Reintentar';

$lang['format-progress'] = '{percent}% of {total_size}';

$lang['loading'] = 'Cargando, por favor espere...';
$lang['deleting'] = 'Borrando, por favor espere...';
$lang['saving_title'] = 'Guardando titulo...';

$lang['list_delete'] = 'Borrar';
$lang['alert_delete'] = 'Estas seguro que deseas eliminar esta imagen?';

/* End of file english.php */
/* Location: ./assets/image_crud/languages/english.php */