$(function(){
        $('.quickSearchButton').click(function(){
                $(this).closest('.flexigrid').find('.quickSearchBox').slideToggle('normal');
        });

        $('.ptogtitle').click(function(){
                if ($(this).hasClass('vsble')) {
                        $(this).removeClass('vsble');
                        $(this).closest('.flexigrid').find('.main-table-box').slideDown("slow");
                } else {
                        $(this).addClass('vsble');
                        $(this).closest('.flexigrid').find('.main-table-box').slideUp("slow");
                }
        });

        //call_fancybox();
        //add_edit_button_listener();
        //displaying_and_pages();            
        
        $(document).on('keyup','.flexigrid input',function(e){
            if(e.which==13){
                $(document).find('.filtering_form').trigger('submit');
            }
        })

    $('.crud_search').click(function(){
            $(this).closest('.flexigrid').find('.crud_page').val('1');
            $(this).closest('.flexigrid').find('.filtering_form').trigger('submit');
    });

    $('.per_page').change(function(){                

            $(this).closest('.flexigrid').find('.crud_page').val('1');
            $(this).parents('.filtering_form').trigger('submit');
    });

    $('.ajax_refresh_and_loading').click(function(){

            $(this).parents('.filtering_form').trigger('submit');
    });


    $('.ajax_list').on('click','.delete-row', function(){
            var delete_url = $(this).attr('href');
            var this_container = $(this).parents('.flexigrid');
            if( confirm( message_alert_delete ) )
            {
                    $.ajax({
                            url: delete_url,
                            dataType: 'json',
                            success: function(data)
                            {
                                    if(data.success)
                                    {
                                            

                                            success_message(data.success_message);
                                    }
                                    else
                                    {
                                            error_message(data.error_message);

                                    }

                                    $('.filtering_form').submit(); 
                            }
                    });
            }

            return false;
    });

    $('.export-anchor').click(function(){
            var export_url = $(this).attr('data-url');
            var form_input_html = '';
            $.each($(this).closest('.flexigrid').find('.filtering_form').serializeArray(), function(i, field) {
                form_input_html = form_input_html + '<input type="hidden" name="'+field.name+'" value="'+field.value+'">';
            });
            var form_on_demand = $("<form/>").attr("id","export_form").attr("method","post").attr("target","_blank").attr("action",export_url).html(form_input_html);
            $(this).closest('.flexigrid').find('.hidden-operations').html(form_on_demand);
            $(this).closest('.flexigrid').find('.hidden-operations').find('#export_form').submit();
    });

    $('.print-anchor').click(function(){
            var print_url = $(this).attr('data-url');

            var form_input_html = '';
            $.each($(this).closest('.flexigrid').find('.filtering_form').serializeArray(), function(i, field) {
                form_input_html = form_input_html + '<input type="hidden" name="'+field.name+'" value="'+field.value+'">';
            });

            var form_on_demand = $("<form/>").attr("id","print_form").attr("method","post").attr("action",print_url).html(form_input_html);

            $(this).closest('.flexigrid').find('.hidden-operations').html(form_on_demand);

            var _this_button = $(this);

            $(this).closest('.flexigrid').find('#print_form').ajaxSubmit({
                    beforeSend: function(){
                            _this_button.find('.fbutton').addClass('loading');
                            _this_button.find('.fbutton>div').css('opacity','0.4');
                    },
                    complete: function(){
                            _this_button.find('.fbutton').removeClass('loading');
                            _this_button.find('.fbutton>div').css('opacity','1');
                    },
                    success: function(html_data){
                            $("<div/>").html(html_data).printElement();
                    }
            });
    });

    $('.crud_page').numeric();


    if ($('.flexigrid').length == 1) {	 //disable cookie storing for multiple grids in one page
            var cookie_crud_page = readCookie('crud_page_'+unique_hash);
            var cookie_per_page  = readCookie('per_page_'+unique_hash);
            var hidden_ordering  = readCookie('hidden_ordering_'+unique_hash);
            var hidden_sorting  = readCookie('hidden_sorting_'+unique_hash);
            var cookie_search_text  = readCookie('search_text_'+unique_hash);
            var cookie_search_field  = readCookie('search_field_'+unique_hash);

            if(cookie_crud_page !== null && cookie_per_page !== null)
            {
                    $('#crud_page').val(cookie_crud_page);
                    $('#per_page').val(cookie_per_page);
                    $('#hidden-ordering').val(hidden_ordering);
                    $('#hidden-sorting').val(hidden_sorting);
                    $('#search_text').val(cookie_search_text);
                    $('#search_field').val(cookie_search_field);

                    if(cookie_search_text !== '')
                            $('#quickSearchButton').trigger('click');

                    $('#filtering_form').trigger('submit');
            }
    }

});

function displaying_and_pages(this_container)
{
        var crud_page 		= parseInt(crud_pagin, 10) ;
        localStorage.crud_page = crud_page;
        var per_page	 	= parseInt($(".flexigrid").find('.per_page').val(), 10 );
        var total_items     = parseInt(total_results, 10 );
        this_container.find(".pager").paging(total_items, {
                format: '[< nnnn >]',
                page: crud_page,
                perpage: per_page,
                onSelect: function (page) {
                       if(crud_pagin!=page){
                            crud_pagin = page;
                            console.log(this_container);
                            this_container.trigger('submit');
                            $('html,body').animate({scrollTop:$('#listing1').offset().top},0);
                            //$(document).find('.filtering_form').trigger('submit');
                        }
                },
                onFormat: function (type) {
                        switch (type) {
                        case 'block': // n and c
                                cl = crud_page==this.value?'active':'';
                                return '<li class="paginate_button '+cl+'"><a style="cursor:pointer">' + this.value + '</a></li>';
                        case 'next': // >
                                return '<li class="paginate_button"><a style="cursor:pointer">&gt;</a></li>';
                        case 'prev': // <
                                return '<li class="paginate_button"><a style="cursor:pointer">&lt;</a></li>';
                        case 'first': // [
                                return '<li class="paginate_button"><a style="cursor:pointer">&laquo;</a></li>';
                        case 'last': // ]
                                return '<li class="paginate_button"><a style="cursor:pointer">&raquo;</a></li>';
                        }
                }
        });                      
}

var call_fancybox = function(){
        $('.image-thumbnail').fancybox({
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'speedIn'		:	600,
                'speedOut'		:	200,
                'overlayShow'	:	false
        });
};

function getData(flexigrid,formdatos,crud_pagin){
    d = new FormData(formdatos);
    d.append('page',crud_pagin);
    d.append('operator','where');
    console.log('paginado');
    $.ajax({
        url:ajax_list,
        data:d,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:function(data){
            flexigrid.find('.ajax_list').html(data);
            $(".ajax_refresh_and_loading").removeClass('loading');
            var actualPage = parseInt(crud_pagin);
            var perpage = parseInt($(".flexigrid").find('.per_page').val());

            $("#page-starts-from").html((crud_pagin-1)*perpage+1);
            $("#page-ends-to").html(((crud_pagin-1)*perpage)+perpage);

             $(".propiedadDetailFoto, #agent-slider, #agent-2-slider").owlCarousel({
                autoPlay: false,
                stopOnHover: true,
                navigation: true,
                navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                slideSpeed: 300,
                pagination: false,
                items:1,
                singleItem: true,
                dots:false,
              });

             //For Single Slide
              $(".ovl").owlCarousel({
                autoPlay: false,
                stopOnHover: true,
                navigation: true,
                navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                slideSpeed: 300,
                pagination: false,
                singleItem: true,
                items: 1,
                dots:false
              });

              fillImages();
        },
        error:function(data){                    
            //error_message('Ha ocurrido un error consultando los datos, ERROR: '+data.status);
            //$(".ajax_refresh_and_loading").removeClass('loading');
        }
    });
}

var working = false;
var paginado = false;
function filterSearchClick(datos){
	if(!working){
		working = true;
	    var contentForm = $(datos);
	    var flexigrid = $(datos).find('.flexigrid');
        
	    var formdatos = datos;
	    $(".ajax_refresh_and_loading").addClass('loading');
	    var ajax_list_info_url = $(datos).attr('data-ajax-list-info-url');
	    var ajax_list = $(datos).attr('action');
	    d = new FormData(formdatos);
	    d.append('page',crud_pagin);
        d.append('operator','where');
	    flexigrid.find('.ajax_list').html('Cargando propiedades... ');
	    if(!paginado){
            $.ajax({
    	        url:ajax_list_info_url,
    	        data:d,
    	        cache: false,
    	        contentType: false,
    	        processData: false,
    	        type: 'POST',
    	        success:function(data){
                    paginado = true;
    	        	working = false;
    	            data = JSON.parse(data);
    	            $('#total_items').html( data.total_results);                    
    	            total_results = data.total_results;         
    	            displaying_and_pages(flexigrid);
    	            getData(flexigrid,formdatos,crud_pagin);
    	        },
    	        error:function(data){                    
    	            //error_message('Ha ocurrido un error consultando la información de los datos, ERROR: '+data.status);
    	            //$(".ajax_refresh_and_loading").removeClass('loading');
    	        }
    	    });
    	}else{   
            working = false;
            displaying_and_pages(flexigrid);         
            getData(flexigrid,formdatos,crud_pagin);
        }
    }
    return false;
}

$(document).on('click','.searchActionClick', function(){
        var row = $(this).parents('.filtering_form').find('.searchRow');
        if(row.hasClass('hide')){
            row.removeClass('hide');
        }else{
            row.addClass('hide');
        }
});
$(document).on('change','.field-sorting', function(){
        var val = $(this).find('option:selected').data('field');
        var type = $(this).find('option:selected').data('type');
        var valor = $(this).val();
        if(valor!=''){
            localStorage.ordenarPor = valor;        
        }
        $(".hidden-sorting").val(val);
        $(".hidden-ordering").val(type);

        $('.filtering_form').submit();
});

$(document).on('zelectedInit',function(){
    
    $(document).on('change','.provincia',function(e){
        e.stopPropagation();
        $.post(URL+'propiedades/json/comarcas/json_list',{
            'search_field[]':'provincias_id',
            'search_text[]':$(this).val(),
            'per_page':'1000'
        },function(data){
            ajax = false;
            data = JSON.parse(data);
            refreshSelect('.comarca',data); 
            refreshSelect('.ciudad',{});
            $("#field-areas_id").val('');
            $("#field-distritos_id").val('');
        });
    });

    $(document).on('change',".comarca",function(e){
        e.stopPropagation();
        if($(this).val()!==''){
            $.post(URL+'propiedades/json/ciudades/json_list',{
                'search_field[]':'ciudades.comarcas_id',
                'search_text[]':$(this).val(),
                'per_page':'1000'
            },function(data){
                ajax = false;
                data = JSON.parse(data);
                refreshSelect('.ciudad',data);
                refreshSelect('.zona',{}); 
                $("#field-areas_id").val('');
                $("#field-distritos_id").val('');
            });
        }
    });

    $(document).on('change',".ciudad",function(e){
        e.stopPropagation();
        if($(this).val()!==''){
            $.post(URL+'propiedades/json/zonas/json_list',{
                'search_field[]':'zonas.ciudades_id',
                'search_text[]':$(this).val(),
                'per_page':'1000'
            },function(data){
                ajax = false;
                data = JSON.parse(data);
                refreshSelect('.zona',data);                
                $("#field-distritos_id").val('');
            });
        }
    });

    $(document).on('change',".zona",function(e){
        e.stopPropagation();
        //$(".filtering_form").submit();
    });

    $(document).on('click','.filtrar',function(){
        $(".filtering_form").attr('onsubmit','');
        $(".filtering_form").attr('action',URL+'listado?retorno=1');
        $(".filtering_form").trigger('submit');
        $(".loader").show();
    });

    if(typeof(localStorage.ordenarPor)!='undefined'){        
        $(".field-sorting").val(localStorage.ordenarPor);        
        $(".field-sorting").parent().find('.zelect .zelected').html($(".field-sorting option:selected").html());        
        $(".field-sorting").trigger('change');
    }
});



function refreshSelect(classe,data){
    var opt = '<option value="">Todas</option>';
    for(var i in data){
        opt+= '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
    }

    $(classe).html(opt);
    $(classe).parent().find('.zelect').remove();
    $(classe).zelect();
}

var init = false;//1ebea5
$(document).on('ready',function(){
    if(!init){
        init = true;

        //$('.filtering_form').submit();  
        
    }
});