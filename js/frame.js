function emergente(data,xs,ys,boton,header){


	var x = (xs==undefined)?(window.innerWidth/2)-325:xs;
	var y = (ys==undefined)?(window.innerHeight/2):ys;
	var b = (boton==undefined || boton)?true:false;
	var h = (header==undefined)?'Mensaje':header;
	if($(".modal").html()==undefined){
	$('body,html').animate({scrollTop: 0}, 800);
	var str = '';
            var str = '<!-- Modal -->'+
            '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog">'+
            '<div class="modal-content">'+
            '<div class="modal-body">'+
            data+
            '</div>'+
            '<div class="modal-footer">'+
            '<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'+
            '</div>'+
            '</div><!-- /.modal-content -->'+
            '</div><!-- /.modal-dialog -->'+
            '</div><!-- /.modal -->';
            $("body").append(str);
            $("#myModal").modal("toggle");
	}
	else
	{
		$(".modal-body").html(data);
                    if($("#myModal").css('display')=='none')
                        $("#myModal").modal("toggle");
	}
}

function remoteConnection(url,data,callback){        
    var r;
    if(typeof(grecaptcha)!=='undefined'){
        grecaptcha.ready(function() {
            var r = grecaptcha.execute('6LeXgLUUAAAAAJr-5UFLv8xXlLkLQ2_jbqm-hjuk', {action: 'action_name'})
            .then(function(token) {
                // Verifica el token en el servidor.
                    data.append('token',token);
                    return $.ajax({
                    url: URL+url,
                    data: data,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:callback
                });
            });
        });

    }else{
        r = $.ajax({
            url: URL+url,
            data: data,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:callback
        });
    }
    return r;
};

function insertar(url,datos,resultId,callback,callbackError){
    $(document).find(resultId).removeClass('alert alert-danger').html('');
    $("button[type=submit]").attr('disabled',true);
    var uri = url.replace('insert','insert_validation');
    uri = uri.replace('update','update_validation');    
    console.log(datos instanceof HTMLFormElement);
    if(!(datos instanceof FormData) && (datos instanceof HTMLFormElement)){
        datos = new FormData(datos);
    }else if(!(datos instanceof FormData)){
        var d = new FormData();
        for(var i in datos){
            d.append(i,datos[i]);
        }
        datos = d;
        console.log(datos);
    }


    remoteConnection(uri,datos,function(data){
        $("button[type=submit]").attr('disabled',false);
        data = $(data).text();
        data = JSON.parse(data);
        
        if(data.success){
          remoteConnection(url,datos,function(data){
            data = $(data).text();
            data = JSON.parse(data);
            if(typeof(callback)=='function'){
                callback(data);
            }else{
                $(document).find(resultId).addClass('alert alert-success').html(data.success_message);
            }
          });
        }else{
          $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
          if(typeof(callbackError)!=='undefined'){
            callbackError(data);
          }else{
            $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
          }
        }
    });
}

function sendForm(form,divResponseId,callback){
    divResponseId = typeof(divResponseId)==='undefined'?'#response':divResponseId;
    var url = $(form).attr('action');
    var response = $(form).find(divResponseId);
    var data = new FormData(form);
    remoteConnection(url,data,function(data){
        if(typeof(callback)==='undefined'){
            if(response!==undefined){
                response.html(data);
            }else{
                console.log(response);
            }
        }else{
            callback(data);
        }
    });
    return false;
}