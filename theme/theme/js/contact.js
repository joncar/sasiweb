
   var map;
    map = new GMaps({
        el: '#map',
        lat: 41.5864096,
        lng: 1.6145387,
        scrollwheel: false
    });

    map.addMarker({
        lat: 41.5864096,
        lng: 1.6145387,
        title: 'Finques Sasi Igualada',
        infoWindow: {
            content: '<p>Finques Sasi Igualada</p>'
        }
    });    

    map = new GMaps({
        el: '#map2',
        lat: 38.3398319,
        lng: -0.4936062,
        scrollwheel: false
    });

    map.addMarker({
        lat: 38.3398319,
        lng: -0.4936062,
        title: 'Finques Sasi Alicante',
        infoWindow: {
            content: '<p>Finques Sasi Alicante</p>'
        }
    });    