var map;
var markers = [];
var markersOnMap = [];
var styleMap = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}];
var bounds;
var infoWindow;
var onDrag = false;
var drawingManager;
var afterOverlay = undefined;
var total = 0;
var region = '';
var comarca = '';
var ciudad = '';
var contenidos = [];
var contenidosSidebar = [];
var markerCluster = undefined;
var onClickFav = function(){};
var regionDraw = undefined;
var loading = false; 
var onCached =  typeof(onCache)!='undefined'?onCache:{};
var erased = [];
var shaped = [];
var polygon = undefined;
var styleCluster = 
[
{url:URL+'img/clusters/m1.png',textColor:'white',width:'53',height:'52'},
{url:URL+'img/clusters/m2.png',textColor:'white',width:'56',height:'55'},
{url:URL+'img/clusters/m3.png',textColor:'white',width:'66',height:'65'},
{url:URL+'img/clusters/m4.png',textColor:'white',width:'78',height:'77'},
{url:URL+'img/clusters/m5.png',textColor:'white',width:'90',height:'89'}
];
$(document).ready(function(){
	// Property listing map page load map
	function property_listing_map_initialize() {
	    $.post(URL+'propiedades/frontend/propiedades/ajax_list_info',{},function(data){
	    	total = JSON.parse(data);
	    	total = total.total_results;
	    	initMap();
	    });
	    
	}


	if(document.getElementById('property-listing-map') != null ){
		google.maps.event.addDomListener(window, 'load', property_listing_map_initialize);
	}

	//Init map detail
	if($("#property-listing-map2").length>0){
		initMapProperty();
	}
});

$(document).on('zelectedInit',function(){

	$(".provincia").on('change',function(e){    	
		$.post(URL+'propiedades/json/comarcas/json_list',{
			'search_field[]':'provincias_id',
			'search_text[]':$(this).val(),
			'per_page':'1000'
		},function(data){
			ajax = false;
			data = JSON.parse(data);
			refreshSelect('.comarca',data);
		});    	
	});

	$(".comarca").on('change',function(e){	
		$.post(URL+'propiedades/json/ciudades/json_list',{
			'search_field[]':'ciudades.comarcas_id',
			'search_text[]':$(this).val(),
			'per_page':'1000'
		},function(data){
			ajax = false;
			data = JSON.parse(data);
			refreshSelect('.ciudad',data);
		});    	
	});

	$(".ciudad").on('change',function(e){		
		$.post(URL+'propiedades/json/zonas/json_list',{
			'search_field[]':'zonas.ciudades_id',
			'search_text[]':$(this).val(),
			'per_page':'1000'
		},function(data){
			ajax = false;
			data = JSON.parse(data);
			refreshSelect('.zona',data);
		});    	
	});
});

var s = false;
var loadingAn = undefined;


function search(){
	if(!s){
		s = true;
		var form = document.getElementById('filtros');
		form = new FormData(form);
		loadingAn = setTimeout(function(){$(".loader, #loadingProp").show();},2000);		
		$.ajax({
		    url: URL+'propiedades/frontend/propiedadesMapa/json_list',
		    data: form,
		    context: document.body,
		    cache: false,
		    contentType: false,
		    processData: false,
		    type: 'POST',
		    success:function(data){
		    	$("#loadingProp").hide();
		    	$("#loadingDraw").show();
		    	s = false;
		    	data = JSON.parse(data);
		    	markers = [];
		    	for(i in data){
		    		markers.push(data[i]);
		    	}

		    	for( var i = 0; i < markers.length; i++ ) {
		        	//si no esta eliminado desde el cache
		        	if(!erased || erased.indexOf(markers[i].cod_ofer)<0){
		        		addMarker(markers[i],i);
		        	}

			    }
			    
				markerCluster = new MarkerClusterer(map, markersOnMap,{imagePath: URL+'img/clusters/m','styles':styleCluster});
				markerCluster.resetViewport(true);
				if(regionDraw!=undefined){
			    	showMarkersOnRegion(regionDraw);
			    }
			    loading = true;			    
			    if(typeof(onCached.shape)!=='undefined' && onCached.shape.length>0){
			    	loadShape();
				}
				$("#loadingDraw").hide();
				$(".loader").hide();
				clearTimeout(loadingAn);
	    	}
		});
	}
}

function loadShape(){
	polygon = new google.maps.Polygon({
	    path: shaped,
	    strokeWeight: 0,
        fillOpacity: 0.45,
        editable: true,
        draggable: true    
	});
	polygon.setMap(map);	
	showMarkersOnRegion(polygon);
	google.maps.event.addListener(polygon,'dragend',function(event,attr){
		showMarkersOnRegion(polygon);
		shaped = polygon.getPath().getArray();
		saveHistory();		
	});
	google.maps.event.addListener(polygon.getPath(),'set_at',function(event,attr){
		showMarkersOnRegion(polygon);
		shaped = polygon.getPath().getArray();
		saveHistory();		
	});
	google.maps.event.addListener(polygon.getPath(),'insert_at',function(event,attr){
		showMarkersOnRegion(polygon);
		shaped = polygon.getPath().getArray();
		saveHistory();		
	});
	google.maps.event.addListener(polygon.getPath(),'remove_at',function(event,attr){
		showMarkersOnRegion(polygon);
		shaped = polygon.getPath().getArray();
		saveHistory();		
	});
}

function showInfo(cod,marker){
	var contenido = contenidos.find(contenido=>contenido.cod === cod);
	if(typeof(contenido) == 'undefined'){
		//infoWindow.setContent('<div style="height:500px">Cargando datos por favor espere.</div>');
	    
		$.post(URL+'propiedades/frontend/propiedades/json_list',{    	
	    	cod_ofer:cod
	    },function(data){
	    	data = JSON.parse(data);
	    	datos = data[0];
	    	infoWindow.setContent(datos.content); 
	    	infoWindow.open(map, marker);  
	    	contenidos.push({cod:cod,content:datos.content}); 
	    	setTimeout(function(){startowl();},300);
	    });
	}else{
		infoWindow.setContent(contenido.content);   
		infoWindow.open(map, marker);		
		startowl();
	}
	var pos = marker.getPosition();
	var newLat = pos.lat();
	console.log(pos);
	map.panTo({lat:newLat,lng:pos.lng()});
}

function startowl(){
	$(".propiedadDetailFoto, #agent-slider, #agent-2-slider").owlCarousel({
	    autoPlay: false,
	    stopOnHover: true,
	    navigation: true,
	    navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
	    slideSpeed: 300,
	    items:1,
	    pagination: false,
	    singleItem: true
	 });
}

function showModalInfo(cod){
	$.post(URL+'propiedades/frontend/propiedades/json_list',{    	
    	cod_ofer:cod
    },function(data){
    	data = JSON.parse(data);
    	datos = data[0];
    	$("#propiedadDetail .property_item").html(datos.contentexpanded);
    	$("#propiedadDetail").addClass('envista');  	
    	infoWindow.close();    
    	contenidosSidebar.push({cod:cod,content:datos.contentexpanded});     
    });
}

function closeDetail(){
	$("#propiedadDetail").removeClass('envista');  
}

function loadHistory(){
	localStorage.cacheMap = !localStorage.cacheMap || typeof(localStorage.cacheMap)=='undefined'?'{}':localStorage.cacheMap;	
	onCached = JSON.parse(localStorage.cacheMap);
}

function saveHistory(){
	localStorage.cacheMap = !localStorage.cacheMap || typeof(localStorage.cacheMap)=='undefined'?'{}':localStorage.cacheMap;
	
	onCached = JSON.parse(localStorage.cacheMap);
	var pos = map.getCenter();
    	onCached.lat = pos.lat();
    	onCached.lng = pos.lng();
    	onCached.zoom = map.getZoom();
		onCached.erased = erased;
		onCached.shape = shaped;
	localStorage.cacheMap = JSON.stringify(onCached);		
	//Refresh URL
	var loc = document.location.href;
	loc = loc.split('/');
	loc = loc[loc.length-1];
	if(loc.indexOf('saved')<0){
		loc+= '-saved-1';
	}

	history.replaceState({},'',loc);
	console.log(history);
}


function initMap(){
	loadHistory();
	var lat =  !onCached || typeof(onCached)=='undefined'?$("#property-listing-map").data('lat'):onCached.lat;
    var lng =  !onCached || typeof(onCached)=='undefined'?$("#property-listing-map").data('lon'):onCached.lng;
    var zoom =  !onCached || typeof(onCached)=='undefined'?$("#property-listing-map").data('zoom'):onCached.zoom;

    lat =  isNaN(parseFloat(lat))?$("#property-listing-map").data('lat'):lat;
    lng =  isNaN(parseFloat(lng))?$("#property-listing-map").data('lon'):lng;
    zoom =  isNaN(parseFloat(zoom))?$("#property-listing-map").data('zoom'):zoom;
    erased = !onCached.erased?[]:onCached.erased;
    shaped = !onCached.shape?[]:onCached.shape;    
    pyrmont = {lat:lat,lng:lng};
    region = $("#property-listing-map").data('region');
    comarca = $("#property-listing-map").data('comarca');
    ciudad = $("#property-listing-map").data('ciudad');
    var mapOptions = {
        mapTypeId: 'roadmap',
        center:pyrmont,
        zoom:zoom,
        scrollwheel: false,
		styles: styleMap,
		streetViewControl: false,
		zoomControl: true,
		scrollwheel: true,
	    zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
	    }
	};
    map = new google.maps.Map(document.getElementById("property-listing-map"), mapOptions);
    map.setTilt(45);   
    search(); 
    infoWindow = new google.maps.InfoWindow({Width: 365});

    map.addListener('zoom_changed',function(){
    	saveHistory();
    });
    map.addListener('dragend',function(){    	
    	saveHistory();
    });


    //Boton filtrar
    // constructor passing in this DIV.
    var centerControlDiv = document.createElement('div');
    centerControlDiv.innerHTML = '<div class="visible-xs visible-sm" style="background:#fff;padding: 15px;font-size: 15px; cursor:pointer" onclick="slideFilters()"><i class="fa fa-filter"></i> Filtrar</div>';

    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.LEFT_CENTER].push(centerControlDiv);
    
}

function slideFilters(){
	if($(".filterMap").hasClass('active')){
		$(".filterMap").removeClass('active');
	}else{
		$(".filterMap").addClass('active');
	}
}





function initEmptyMap(){
	var lat =  41.5864096;
    var lng =  1.6145387;

    var mapOptions = {
        mapTypeId: 'roadmap',
        scrollwheel: false,
		styles: styleMap,
		center:new google.maps.LatLng(lat,lng),
		zoom:16,
		zoomControl: true,
	    zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
	    },


    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("property-listing-map"), mapOptions);
}

function cleanMarkers(){
	if(loading && markerCluster!=undefined){markerCluster.clearMarkers();}
	for(var i in markersOnMap){		
		markersOnMap[i].setMap(null);
	}
	markersOnMap = [];
}

function removeMark(cod){
	for(var i in markersOnMap){
		if(markersOnMap[i].datos.cod_ofer==cod){
			markersOnMap[i].setMap(null);
			markersOnMap.splice(i,1);
			erased = typeof(erased)=='undefined'?[]:erased;
			erased.push(cod);
			saveHistory();
			closeDetail();
		}
	}
}
var xx = false;
function addMarker(mark,id){
	var enc = false;
	for(var i in markersOnMap){
		//console.log(markersOnMap[i].lat+'=='+mark[1]+'&&'+markersOnMap[i].lng+'=='+mark[2]);
		if(markersOnMap[i].lat==mark.latitud && markersOnMap[i].lng==mark.altitud){
			enc = true;
		}
	}
	
	if(!enc){
		var position = new google.maps.LatLng(mark.latitud, mark.altitud);
			var icon = mark.favorito==1?'img/map_marker2.png':'img/map_marker.png';
	    	marker = new MarkerWithLabel({
		       position: position,
		       lat:mark.latitud,
		       lng:mark.altitud,	      
		       map: map,
		       icon:URL+icon,
		       labelContent: mark.precio,
		       labelAnchor: new google.maps.Point(-3, 35),
		       labelClass: "labels", // the CSS class for the label
		       labelStyle: {opacity: 1},
		       datos:mark,
		     });
	        markersOnMap.push(marker);
	        
	        google.maps.event.addListener(marker, 'click', (function(marker, i) {    		
	            return function(){
	            	var datos = marker.datos;
	            	showInfo(datos.cod_ofer,marker);
	            	closeDetail();
	            	onClickFav = function(){
	            		marker.setIcon(URL+'img/map_marker2.png');
	            		console.log(marker);	            		
	            		contenidos = [];
	            	}
	            }
	        })(marker, id));
    }
}




function initDrag(){
	if(!onDrag){
		$("#drawButton").removeClass('fa-edit').addClass('fa-pencil-square');		
		var polyOptions = {
            strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true,
            draggable: true            
        };
        
		drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            polygonOptions: polyOptions,
            map: map
        });
		onDrag = true;
		drawingManager.setMap(map);
		google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event,attr) {								
			if(afterOverlay!==undefined){
				afterOverlay.setMap(null);
				afterOverlay = undefined;
			}
			if(polygon!==undefined){
				polygon.setMap(null);
			}
			var shape = event.overlay;
			regionDraw = shape;
			showMarkersOnRegion(shape);
			shaped = shape.getPath().getArray();
			saveHistory();
			google.maps.event.addListener(event.overlay.getPath(),'set_at',function(event,attr){
				showMarkersOnRegion(shape);
			});
			google.maps.event.addListener(event.overlay.getPath(),'insert_at',function(event,attr){
				showMarkersOnRegion(shape);
			});
			google.maps.event.addListener(event.overlay.getPath(),'remove_at',function(event,attr){
				showMarkersOnRegion(shape);
			});
			afterOverlay = event.overlay;
			initDrag();						
		});
	}else{
		$("#drawButton").removeClass('fa-pencil-square').addClass('fa-edit');
		onDrag = false;
		drawingManager.setMap(null);
	}

}

function showMarkersOnRegion(shape){
	cleanMarkers();	
	for(var i in markers){
		if(google.maps.geometry.poly.containsLocation(new google.maps.LatLng(markers[i].latitud,markers[i].altitud), shape)){
			addMarker(markers[i],i);
		}
	}
	
	markerCluster = new MarkerClusterer(map, markersOnMap,{imagePath: URL+'img/clusters/m','styles':styleCluster});		
	//markerCluster = new MarkerClusterer(map, markersOnMap,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});		
}

function showStreetView(lat,lng){	
  var fenway = new google.maps.LatLng(lat,lng);  
  panorama = map.getStreetView();
  panorama.setPosition(fenway);
  /*panorama.setPov(({
    heading: 265,
    pitch: 0
  }));*/
  toggleStreetView();
}

function toggleStreetView() {
  var toggle = panorama.getVisible();
  if (toggle == false) {
    panorama.setVisible(true);
  } else {
    panorama.setVisible(false);
  }
}






function initEmptyMapDetail(){
		var lat =  41.5864096;
        var lng =  1.6145387;

        var mapOptions = {
	        mapTypeId: 'roadmap',
	        scrollwheel: false,
			styles: styleMap,
			center:new google.maps.LatLng(lat,lng),
			zoom:16
	    };
	                    
	    // Display a map on the page
	    map = new google.maps.Map(document.getElementById("property-listing-map2"), mapOptions);
	    var position = new google.maps.LatLng(lat, lng);
	    marker = new google.maps.Marker({
            position: position,
            map: map,
            title: 'Nombre de propiedad',
            icon : URL+'theme/theme/images/map_marker.png',            
        });
}

function removePropMap(cod){
	removeMark(cod);
}






/******** Mapa de propiedad ********/
var cercanias = [];
var pyrmont = [];
var cercaniasMarcas = [];
function initMapProperty(){

	var lat =  $("#property-listing-map2").data('lat');
    var lng =  $("#property-listing-map2").data('lon');
    pyrmont = {lat:lat,lng:lng};
    var zoom = $("#property-listing-map2").data('zoom');
    var position = new google.maps.LatLng(lat,lng);
    var mapOptions = {
        mapTypeId: 'roadmap',
        scrollwheel: false,
		styles: styleMap,
		center:position,
		zoom:zoom,
		zoomControl: true,
		zoomControl: true,
		scrollwheel: true,
	    zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
	    },


    };

    // Create the DIV to hold the control and call the CenterControl()
    
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("property-listing-map2"), mapOptions);

    // constructor passing in this DIV.
    var centerControlDiv = document.createElement('div');
    var centerControl = new createStreetViewMap(centerControlDiv, map);

    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(centerControlDiv);

    marker = new google.maps.Marker({
        position: position,
        map: map,
        title: 'Ubicación geográfica de propiedad',
        icon : URL+'theme/theme/images/map_marker.png',        
    });
}

consulta = false;

function triggerCercanias(){
	if(!consulta){
		consulta = true;
		for(var i in cercaniasMarcas){
			cercaniasMarcas[i].setMap(null);
		}
		cercaniasMarcas = [];
		if(cercanias.length>0){

			for(var i in cercanias){
				var tipo = cercanias[i];								
				var service = new google.maps.places.PlacesService(map);
		        service.nearbySearch({
		          location: pyrmont,
		          radius: 500,
		          type: tipo
		        },function(results, status){		        	
		        	if (status === google.maps.places.PlacesServiceStatus.OK) {		        		
			          for (var i = 0; i < results.length; i++) {
			          	console.log(results);
			          	var place = results[i];
			            var placeLoc = place.geometry.location;
				        var marker = new google.maps.Marker({
				          map: map,
				          position: place.geometry.location,
				          title:place.name,
				          icon:URL+'img/iconosMapa/'+results[i].types[0]+'.png'
				        });
				        cercaniasMarcas.push(marker);
			          }
			        }
		        });
	    	}


    	}
    	consulta = false;
	}
}

function createStreetViewMap(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '0px';
        controlUI.style.marginRight = '10px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click to recenter the map';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '8px';
        controlText.style.paddingRight = '8px';
        controlText.innerHTML = '<i class="icon-map3"></i>';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          showStreetViewOnDetail();
        });

      }



function showStreetViewOnDetail(){
  panorama = map.getStreetView();
  panorama.setPosition(pyrmont);
  panorama.setPov(/** @type {google.maps.StreetViewPov} */({
    heading: 265,
    pitch: 0
  }));
  panorama.setVisible(true);
}


$(document).on('ready',function(){
	$(".cercaniasProp").on('click',function(e){

		if($(this).prop('checked')){
			if(cercanias.indexOf($(this).val())<0){
				cercanias.push($(this).val());
			}
		}else{
			var pos = cercanias.indexOf($(this).val());
			if(pos>=0){
				cercanias.splice(pos,1);
			}
		}

		triggerCercanias();

		//console.log(cercanias);
	});

	//Mostrar streetview
	
	$("#streetView").on('click',function(){		  
		  panorama = map.getStreetView();
		  panorama.setPosition(pyrmont);
		  panorama.setPov(/** @type {google.maps.StreetViewPov} */({
		    heading: 265,
		    pitch: 0
		  }));
		  panorama.setVisible(true);
	});
});	

function limpiar(){
	$("form input[type='checkbox'],form select").val('');
	$("form input[type='checkbox'],form select").prop('checked',false);
	$("form .zelect").remove();
	$("form .intro select").zelect();
}




function refreshSelect(classe,data){
	var opt = '<option value="">Todas</option>';
	for(var i in data){
		opt+= '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
	}

	$(classe).html(opt);
	$(classe).parent().find('.zelect').remove();
	$(classe).zelect();
}

