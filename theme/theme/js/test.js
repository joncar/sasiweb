     var areas = ( <?php echo json_encode($areas);?> );
  var coords = ( <?php echo json_encode($coords);?> );

  var drawingManager;
  var selectedShape;
  var colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
  var selectedColor;
  var colorButtons = {};

  // Saving vars
  var selectedShape;
  var contentString;
  var polygons = [];
  var newPolys = [];

  function clearSelection() {
    if (selectedShape) {
      selectedShape.setEditable(false);
      selectedShape = null;
    }
  }

  function setSelection(shape) {
    clearSelection();
    selectedShape = shape;
    shape.setEditable(true);
    selectColor(shape.get('fillColor') || shape.get('strokeColor'));
  }

  function deleteSelectedShape(e) {
    if (selectedShape) {
      selectedShape.setMap(null);
      for (i=0; i < polygons.length; i++) {   // Clear out the polygons entry
        if (selectedShape.getPath == polygons[i].getPath) {
            polygons.splice(i, 1);
        }
      }
    }
  }

  function selectColor(color) {
    selectedColor = color;
    for (var i = 0; i < colors.length; ++i) {
      var currColor = colors[i];
      colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
    }

    // Retrieves the current options from the drawing manager and replaces the
    // stroke or fill color as appropriate.
    var polylineOptions = drawingManager.get('polylineOptions');
    polylineOptions.strokeColor = color;
    drawingManager.set('polylineOptions', polylineOptions);

    var rectangleOptions = drawingManager.get('rectangleOptions');
    rectangleOptions.fillColor = color;
    drawingManager.set('rectangleOptions', rectangleOptions);

    var circleOptions = drawingManager.get('circleOptions');
    circleOptions.fillColor = color;
    drawingManager.set('circleOptions', circleOptions);

    var polygonOptions = drawingManager.get('polygonOptions');
    polygonOptions.fillColor = color;
    drawingManager.set('polygonOptions', polygonOptions);
  }

  function setSelectedShapeColor(color) {
    if (selectedShape) {
      if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
        selectedShape.set('strokeColor', color);
      } else {
        selectedShape.set('fillColor', color);
      }
    }
  }

  function makeColorButton(color) {
    var button = document.createElement('span');
    button.className = 'color-button';
    button.style.backgroundColor = color;
    google.maps.event.addDomListener(button, 'click', function() {
      selectColor(color);
      setSelectedShapeColor(color);
    });

    return button;
  }

   function buildColorPalette() {
     var colorPalette = document.getElementById('color-palette');
     for (var i = 0; i < colors.length; ++i) {
       var currColor = colors[i];
       var colorButton = makeColorButton(currColor);
       colorPalette.appendChild(colorButton);
       colorButtons[currColor] = colorButton;
     }
     selectColor(colors[0]);
   }

  function initialize() {
      var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: new google.maps.LatLng(40.2444, -111.6608),           // Utah default coords
      mapTypeId: google.maps.MapTypeId.Terrain,
      disableDefaultUI: true,
      zoomControl: true
    });

    var polyOptions = {
      strokeWeight: 0,
      fillOpacity: 0.45,
      editable: true
    };


    // Creates a drawing manager attached to the map that allows the user to draw
    // markers, lines, and shapes.
    drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControlOptions: {
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]},
      markerOptions: {
        draggable: true
      },
      polylineOptions: {
        editable: true
      },
      rectangleOptions: polyOptions,
      circleOptions: polyOptions,
      polygonOptions: polyOptions,
      map: map
    });

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
        if (e.type != google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        drawingManager.setDrawingMode(null);
        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        var newShape = e.overlay;
        newShape.type = e.type;
        polygons.push(newShape);

        setSelection(newShape);


        google.maps.event.addListener(newShape, 'click', function() {
          setSelection(newShape);
        });

        google.maps.event.addListener(newShape, 'mouseup', function() {
          for (i=0; i < polygons.length; i++) {   // Clear out the old polygons entry
            if (newShape.getPath == polygons[i].getPath) {
                polygons.splice(i, 1);
            }
          }
          polygons.push(newShape);
        });
      }
    });


    // Clear the current selection when the drawing mode is changed, or when the
    // map is clicked.
    google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
    google.maps.event.addListener(map, 'click', clearSelection);
    google.maps.event.addDomListener(document.getElementById('delete-button'), 'click', deleteSelectedShape);

    buildColorPalette();


   /* Load Shapes that were previously saved */
    for (var inc = 0, ii = areas.length; inc < ii; inc++) {
      var newCoords = [];
      for (var c = 0, cc = coords.length; c < cc; c++) {
          if (coords[c].polygon == areas[inc].polygon) {
            var point = coords[c];
            newCoords.push(new google.maps.LatLng(point.lat, point.lng));
          }
      }

      newPolys[inc] = new google.maps.Polygon({
        path: newCoords,
        strokeWeight: 0,
        fillColor: areas[inc].fillColor,
        fillOpacity: areas[inc].fillOpacity
      });
      newPolys[inc].setMap(map);
      polygons.push(newPolys[inc]);
      addNewPolys(newPolys[inc]);
    }


    function addNewPolys(newPoly) {
        google.maps.event.addListener(newPoly, 'click', function() {
            setSelection(newPoly);
        });
    }

  }

  google.maps.event.addDomListener(window, 'load', initialize);